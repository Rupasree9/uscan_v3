$(document).ready(function(){

$("#portal-table").validate({

					rules: {
						customer_id: {
                selectcheck: true
            },
						plant_id: {
                selectcheck: true
            },
						portal_name: "required",
						portal_url:{
							 required: true,
							// url:true
						 },
						portal_user_id: "required",
						// password_expiry_date: "required",

						portal_password:{
							 required: true,
						 },
						portal_repeat_password:{
							 required: true,
							 equalTo: "#portal_password",
						 },
					},
					messages: {

						plant_id:"&nbsp;&nbsp;*",
						customer_id:"&nbsp;&nbsp;*",
						portal_name: "&nbsp;&nbsp;*",
						portal_url:"&nbsp;&nbsp;*",
						portal_user_id:"&nbsp;&nbsp;*",
						portal_password:"&nbsp;&nbsp;*",
						portal_repeat_password:"&nbsp;&nbsp;*",
						// password_expiry_date:"&nbsp;&nbsp;*",


				}



			});
});
