$(document).ready(function(){



$("#child-table-form-create").validate({

					rules: {
						parent_company_name_id: {
                selectcheck: true
            },
						customer_type: {
                selectcheck: true
            },

						customer_name:
                {
                    required: true,
                    "remote":
                    {
                      url: 'validateCustomerCreate',
                      type: "post",
											async: false,
                      data:
                      {
                          customer_name: function()
                          {
                              return $('#child-table-form-create :input[name="customer_name"]').val();
                          },
													_token: function()
                          {
                              return $('#customer_name_token_id').val();
                          },


                      }
                    }
                },

					  // customer_name:{
 					// 		 required: true,
            //  },
						 p_contact_no: {
							 phoneUS: true,
					  },
						s_contact_no: {
								 phoneUS: true,
						 },
						 p_contact_email: {
                email: true
            },
						s_contact_email: {
								 email: true
					 },


					},
					messages: {

						parent_company_name_id:"&nbsp;&nbsp;*",
						customer_type:"&nbsp;&nbsp;*",
          	customer_name:
                 {
                    required: "&nbsp;&nbsp;*",
                    remote: jQuery.validator.format("&nbsp; Already exists")
                 },
				}



			});

			$("#child-table-form-change").validate({

								rules: {
									parent_company_name_id: {
			                selectcheck: true
			            },
									customer_type: {
			                selectcheck: true
			            },

									customer_name:
			                {
			                    required: true,
			                    "remote":
			                    {
			                      url: 'validateCustomerChange',
			                      type: "post",
														async: false,
			                      data:
			                      {
			                          customer_name: function()
			                          {
			                              return $('#child-table-form-change :input[name="customer_name"]').val();
			                          },
																_token: function()
			                          {
			                              return $('#customer_name_token_id').val();
			                          },
																customer_name_valid : function()
			                          {
			                              // return $('#customer_name').data("id");
																		return $('#child-table-form-change :input[name="customer_name_valid"]').val();
			                          }




			                      }
			                    }
			                },

								  // customer_name:{
			 					// 		 required: true,
			            //  },
									 p_contact_no: {
										 phoneUS: true,
								  },
									s_contact_no: {
											 phoneUS: true,
									 },
									 p_contact_email: {
			                email: true
			            },
									s_contact_email: {
											 email: true
								 },


								},
								messages: {

									parent_company_name_id:"&nbsp;&nbsp;*",
									customer_type:"&nbsp;&nbsp;*",
			          	customer_name:
			                 {
			                    required: "&nbsp;&nbsp;*",
			                    remote: jQuery.validator.format("&nbsp; Already exists")
			                 },
							}



						});



			$("#parent-table-form-create").validate({

								rules: {
			            parent_name:{
	                    required: true,
	                    "remote":
	                    {
	                      url: 'validateCustomerParentCreate',
	                      type: "post",
												async: false,
	                      data:
	                      {
	                          parent_name: function()
	                          {
	                              return $('#parent-table-form-create	 :input[name="parent_name"]').val();
	                          },
														_token: function()
	                          {
	                              return $('#customer_parent_name_token_id').val();
	                          },


	                      }
	                    }
	                },
									 parent_code:{
 										 required: true,
 									 },
								},
								messages: {

									parent_name:
									{
                     required: "&nbsp;&nbsp;*",
                     remote: jQuery.validator.format("&nbsp; Already exists")
                  },
									parent_code:"&nbsp;&nbsp;*",

							}



						});

						$("#parent-table-form-change").validate({

											rules: {
						            parent_name:{
				                    required: true,
														async: false,
				                    "remote":
				                    {
				                      url: 'validateCustomerParentChange',
				                      type: "post",
															async: false,
				                      data:
				                      {
				                          parent_name: function()
				                          {
				                              return $('#parent-table-form-change	 :input[name="parent_name"]').val();
				                          },
																	_token: function()
				                          {
				                              return $('#customer_parent_name_token_id').val();
				                          },
																	parent_name_valid : function()
				                          {
				                            	return $('#parent-table-form-change	:input[name="customer_parent_name_valid"]').val();
				                          }




				                      }
				                    }
				                },
												 parent_code:{
			 										 required: true,
			 									 },
											},
											messages: {

												parent_name:
												{
			                     required: "&nbsp;&nbsp;*",
			                     remote: jQuery.validator.format(" &nbsp; Already exists")
			                  },
												parent_code:"&nbsp;&nbsp;*",

										}



									});



						// $("#child-table-form").validate({
						//    onkeyup: false //turn off auto validate whilst typing
						// });
});
