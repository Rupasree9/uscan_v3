$(document).ready(function(){

$("#connection-table-form").validate({

					rules: {
						connection_name: {
                required: true,
            },
						protocol: {
                required: true,
            },

					  server:{
 							 required: true,
             },
						 port: {
							 required: true,
					  },

					},
					messages: {

						connection_name:"&nbsp;&nbsp;*",
						protocol:"&nbsp;&nbsp;*",
						server: "&nbsp;&nbsp;*",
				}
			});

			$("#ftp-connection-table-form").validate({

				rules: {
					connection_name: {
							required: true,
					},
					server_type: {
							selectcheck: true,
					},

					host:{
						 required: true,
					 },
					 port: {
						 required: true,
					},

				},
				messages: {

					connection_name:"&nbsp;&nbsp;*",
					server_type:"&nbsp;&nbsp;*",
					host: "&nbsp;&nbsp;*",
			}

						});

						$("#ftp-address-table-form").validate({

							rules: {
								address_name: {
										required: true,
								},
								user_name: {
										required: true,
								},

								pwd:{
									 required: true,
								 },

								ftp_connection_name:{
									selectcheck: true,
								},

							},
							messages: {

								address_name:"&nbsp;&nbsp;*",
								user_name:"&nbsp;&nbsp;*",
								pwd: "&nbsp;&nbsp;*",
								ftp_connection_name:"&nbsp;&nbsp;*",
						}

									});

						$("#system-user-table-form").validate({

							rules: {
								system_user_name: {
										required: true,
								},
								smtp_user_name: {
										required: true,
								},

								smtp_password:{
									 required: true,
								 },
								 mail_connection: {
									 selectcheck: true,
								},
								email_id:{
									email: true
								},

							},
							messages: {

								system_user_name:"&nbsp;&nbsp;*",
								smtp_user_name: "&nbsp;&nbsp;*",
								smtp_password:"&nbsp;&nbsp;*",
								mail_connection: "&nbsp;&nbsp;*",
						}

				});

				$("#usergroup-table-form").validate({

					rules: {
						user_group_name: {
								required: true,
						},
						user_group_id: {
								required: true,
						},

					},
					messages: {

						user_group_name:"&nbsp;&nbsp;*",
						user_group_id: "&nbsp;&nbsp;*",

				}

				});

				$("#user-table-form").validate({

					rules: {
						ent_user_name: {
								required: true,
						},
						ent_email_id: {
								email: true,
						},

					},
					messages: {

						ent_user_name:"&nbsp;&nbsp;*",
						ent_email_id: "&nbsp;&nbsp;*",

				}

				});

});
