$(document).ready(function(){
  $("#scheduler_create_form").validate({
          rules: {
            task_type: {
                selectcheck: true
            },
            job_priority: {
                selectcheck: true
            },
            task_name:{
							selectcheck: true
						 },
             job_name:{
 							 required: true,
 						 },
          },
        messages: {
                task_type:"&nbsp;&nbsp;*",
                job_priority:"&nbsp;&nbsp;*",
                job_name:"&nbsp;&nbsp;*",
                task_name:"&nbsp;&nbsp;*",
            }
  });

  $("#start_conditions_form").validate({
          rules: {
            start_conditions: {
                selectcheck: true
            },
          },
        messages: {
              start_conditions:"&nbsp;&nbsp;*",
            }
  });

  $("#immediate_form").validate({
          rules: {
            frequency: {
                selectcheck: true
            },
          },
        messages: {
              frequency:"&nbsp;&nbsp;*",
            }
  });

  $("#date_time_form").validate({
          rules: {
              start_date: {
  									 required: true,
  						 },
               start_time: {
                  required: true,
              },

          },
        messages: {
              start_date:"&nbsp;&nbsp;*",
              start_time:"&nbsp;&nbsp;*",
            }
  });

  $("#date_time_form_frequency").validate({
          rules: {
              frequency_date_time: {
                  selectcheck: true
              },
          },
        messages: {
              frequency_date_time:"&nbsp;&nbsp;*",
            }
  });

  $("#after_job_from").validate({
          rules: {
              predecessor_job_name: {
                  selectcheck: true
              },
          },
        messages: {
              predecessor_job_name:"&nbsp;&nbsp;*",
            }
  });

});
