$(document).ready(function(){
  $("#user_table").validate({
          rules: {
            group_name: {
                selectcheck: true
            },
               user_id:{
                        required: true,
                        email: true,
                       "remote":
                       {
                         url: 'validate_user_name',
                         type: "post",
                         data:
                         {
                             user_id: function()
                             {
                                 return $('#user_table :input[name="user_id"]').val();
                             },
   													_token: function()
                             {
                                 return $('#user_name_token_id').val();
                             },
                         }
                       }
                   },


            first_name:{
							 required: true,
						 },
             last_name:{
 							 required: true,
 						 },
             display_roles: {
                 selectcheck: true
             },
             password:{
 							 required: true,
 						 },
 						repeat_password:{
 							 required: true,
 							 equalTo: "#password",
 						 },
          },
        messages: {
            group_name:"&nbsp;&nbsp;*",
            first_name:"&nbsp;&nbsp;*",
            last_name:"&nbsp;&nbsp;*",
            display_roles:"&nbsp;&nbsp;*",
            password:"&nbsp;&nbsp;*",
            repeat_password:"&nbsp;&nbsp;*",
            user_id:
                 {
                    required: "&nbsp;&nbsp;*",
                    remote: jQuery.validator.format("{0} already exists")
                 },
            }
  });
  // $("#role_table").validate({
  //         rules: {
  //           display_roles: {
  //               selectcheck: true
  //           },
  //         },
  //       messages: {
  //         display_roles:"&nbsp;&nbsp;*",
  //           }
  // });
  // $("#right_table").validate({
  //         rules: {
  //           group_name: {
  //               selectcheck: true
  //           },
  //           user_id:{
  //              required: true,
  //            },
  //           first_name:{
  //              required: true,
  //            },
  //         },
  //       messages: {
  //         user_id:"&nbsp;&nbsp;*",
  //         group_name:"&nbsp;&nbsp;*",
  //         first_name:"&nbsp;&nbsp;*",
  //           }
  // });
  $("#user_table_form").validate({

            rules: {

              user_id:
                  {
                      required: true,
                      email: true,
                      "remote":
                      {
                        url: 'validate_user_name_change',
                        type: "post",
                        async: false,
                        data:
                        {
                            user_id: function()
                            {
                                return $('#user_table_form :input[name="user_id"]').val();
                            },
                            _token: function()
                            {
                                return $('#user_name_token_id').val();
                            },
                            user_name_valid : function()
                            {
                                return $('#user_id').data("id");
                            }


                        }
                      }
                  },
            },
            messages: {
              user_id:
                   {
                      required: "&nbsp;&nbsp;*",
                      remote: jQuery.validator.format("{0} already exists")
                   },
          }



        });




});
