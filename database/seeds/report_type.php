<?php

use Illuminate\Database\Seeder;

class report_type extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $report_type  = array(array('report_type_id' => '1','report_type_name' => 'Master Dashboard','customer_id' => '0030000001','enterprise_id' => '0010000002')

        );
        DB::table('report_types')->insert($report_type);

      }
}
