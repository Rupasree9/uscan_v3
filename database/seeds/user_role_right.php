<?php

use Illuminate\Database\Seeder;

class user_role_right extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $role_right  = array(array('role_right_id' => '1','role_id' => '1','right_id' => '1','enterprise_id' => '0010000002')
      ,array('role_right_id' => '2','role_id' => '1','right_id' => '2','enterprise_id' => '0010000002')
      ,array('role_right_id' => '3','role_id' => '1','right_id' => '3','enterprise_id' => '0010000002')
      ,array('role_right_id' => '4','role_id' => '1','right_id' => '4','enterprise_id' => '0010000002')
      ,array('role_right_id' => '5','role_id' => '2','right_id' => '1','enterprise_id' => '0010000002')
      ,array('role_right_id' => '6','role_id' => '2','right_id' => '2','enterprise_id' => '0010000002')
      ,array('role_right_id' => '7','role_id' => '2','right_id' => '3','enterprise_id' => '0010000002')
      ,array('role_right_id' => '8','role_id' => '2','right_id' => '4','enterprise_id' => '0010000002')
      ,array('role_right_id' => '9','role_id' => '3','right_id' => '2','enterprise_id' => '0010000002')
      ,array('role_right_id' => '10','role_id' => '3','right_id' => '4','enterprise_id' => '0010000002')
      ,array('role_right_id' => '11','role_id' => '4','right_id' => '2','enterprise_id' => '0010000002')
      ,array('role_right_id' => '12','role_id' => '4','right_id' => '4','enterprise_id' => '0010000002')
    );
      DB::table('role_right')->insert($role_right);
    }
}
