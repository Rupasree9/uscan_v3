<?php

use Illuminate\Database\Seeder;

class start_type_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
                $start_type  = array(array('start_type_id' => '1','start_type_name' => 'Immediately','start_type_desc' => 'Immediately','enterprise_id' => '0010000002')
                ,array('start_type_id' => '2','start_type_name' => 'Date Time','start_type_desc' => 'Date_time','enterprise_id' => '0010000002')
                ,array('start_type_id' => '3','start_type_name' => 'After Job','start_type_desc' => 'After_job','enterprise_id' => '0010000002')
              );
              DB::table('start_type')->insert($start_type);
    }
}
