<?php

use Illuminate\Database\Seeder;

class task_type_details extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
         $task_type  = array( array('task_type_id' => 1, 'task_type_name' => 'BI','details' => 'BI Extractor','enterprise_id' => '0010000002'), array('task_type_id' => 2,'task_type_name' => 'Notification','details' => 'BI Notification','enterprise_id' => '0010000002'), array('task_type_id' => 3,'task_type_name' => 'Interface','details' => 'Interface','enterprise_id' => '0010000002'));
         DB::table('task_type')->insert($task_type);
     }
}
