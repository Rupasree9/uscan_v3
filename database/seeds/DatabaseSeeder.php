<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(enterprise::class);
        $this->call(number_range::class);
        $this->call(user_right::class);
        $this->call(user_role::class);
        $this->call(user_role_right::class);
        $this->call(user_group::class);
        $this->call(customer_master_type::class);
        $this->call(customer_type::class);
        $this->call(customer::class);
        $this->call(task_type_details::class);
        $this->call(start_type_table::class);
        $this->call(frequency::class);
        $this->call(task_status::class);
        $this->call(task_communication_options::class);
        $this->call(mail_enterprise_user_groups::class);
        $this->call(mail_enterprise_users::class);
        //$this->call(sample_program_test_data::class);
        //$this->call(report_type::class);
        //$this->call(report_name::class);
        $this->call(plant_status::class);
        $this->call(user::class);
        
    }
}
