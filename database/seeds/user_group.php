<?php

use Illuminate\Database\Seeder;

class user_group extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $user_group  = array(array('group_id' => '1','group_name' => 'Super Admin','role_id' => '1','enterprise_id' => '0010000002')
        ,array('group_id' => '2','group_name' => 'Enterprise Admin','role_id' => '2','enterprise_id' => '0010000002')
        ,array('group_id' => '3','group_name' => 'Enterprise Corporate','role_id' => '3','enterprise_id' => '0010000002')
        ,array('group_id' => '4','group_name' => 'Enterprise Plant','role_id' => '4','enterprise_id' => '0010000002')
    );

      DB::table('user_group')->insert($user_group);
    }
}
