<?php

use Illuminate\Database\Seeder;

class user_right extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $right  = array(array('right_id' => '1','right_name' => 'Add','right_desc' => 'Add','enterprise_id' => '0010000002')
      ,array('right_id' => '2','right_name' => 'Change','right_desc' => 'Change','enterprise_id' => '0010000002')
      ,array('right_id' => '3','right_name' => 'Delete','right_desc' => 'Delete','enterprise_id' => '0010000002')
      ,array('right_id' => '4','right_name' => 'Display','right_desc' => 'Display','enterprise_id' => '0010000002')
    );
      DB::table('rights')->insert($right);

    }
}
