<?php

use Illuminate\Database\Seeder;

class customer_type extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //


        $customer_types  = array( array('customer_type_id' => 1,'customer_type' => 'ALL','enterprise_id' => '0010000002'), array('customer_type_id' => 2,'customer_type' => 'Original-Equipment','enterprise_id' => '0010000002'), array('customer_type_id' => 3,'customer_type' => 'Aftermarket','enterprise_id' => '0010000002'), array('customer_type_id' => 4,'customer_type' => 'Inter-Company','enterprise_id' => '0010000002'));
        DB::table('customer_type')->insert($customer_types);
    }
}
