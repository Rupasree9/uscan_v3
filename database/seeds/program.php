<?php

use Illuminate\Database\Seeder;

class program extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $report_type  = array(array('program_id' => '01003000','program_type' => 'Report','program_name' => 'GM Bidlist reports','report_type_id' => '1',,'enterprise_id' => '0010000002')
        ,array('report_name_id' => '2','report_name' => 'GM Scorecard','report_type_id' => '1','program_id' => '01003001','enterprise_id' => '0010000002')
        ,array('report_name_id' => '3','report_name' => 'FORD Q1 Scorecard','report_type_id' => '2','program_id' => '01003002','enterprise_id' => '0010000002')
        ,array('report_name_id' => '4','report_name' => 'FORD Q1 Results','report_type_id' => '2','program_id' => '01003003','enterprise_id' => '0010000002')
        ,array('report_name_id' => '5','report_name' => 'FCA Scorecard','report_type_id' => '3','program_id' => '01003004','enterprise_id' => '0010000002')
        ,array('report_name_id' => '6','report_name' => 'FCA reports','report_type_id' => '3','program_id' => '01003005','enterprise_id' => '0010000002')
      );
        DB::table('report_types')->insert($report_type);
    }
}
