<?php

use Illuminate\Database\Seeder;

class customer_master_type extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //


        $customer_types  = array(array('customer_master_type_id' => 1,'customer_master_type' => 'Parent Company','enterprise_id' => '0010000002'), array('customer_master_type_id' => 2,'customer_master_type' => 'Customer','enterprise_id' => '0010000002'));
        DB::table('customer_master_type')->insert($customer_types);

    }
}
