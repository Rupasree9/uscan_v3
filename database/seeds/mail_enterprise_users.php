<?php

use Illuminate\Database\Seeder;

class mail_enterprise_users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $mail_enterprise_users = array(array('mail_comm_id' => '1','ent_user_id' => 'Jhondoe@doe.com','ent_user_name' => 'Jhondoe','user_group_id' => '1','created_by' => 'Jhondoe','updated_by' => 'Jhondoe','enterprise_id' => '0010000002'),
                                       array('mail_comm_id' => '2','ent_user_id' => 'Dan@dd.com','ent_user_name' => 'Dan','user_group_id' => '1','created_by' => 'Dan','updated_by' => 'Dan','enterprise_id' => '0010000002')
                                      );
        DB::table('mail_enterprise_users')->insert($mail_enterprise_users);
    }
}
