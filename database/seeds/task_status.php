<?php

use Illuminate\Database\Seeder;

class task_status extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $task_status  = array(array('status_id' => '1','status_name' => 'Success','enterprise_id' => '0010000002'),
                         array('status_id' => '2','status_name' => 'Failed','enterprise_id' => '0010000002')
                       );
        DB::table('task_status')->insert($task_status);
    }
}
