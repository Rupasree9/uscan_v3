<?php

use Illuminate\Database\Seeder;

class sample_program_test_data extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $program_details  = array(
                            array('program_id' => '0090000001','program_type' => 'crawler','program_name' => 'P_BI_GM_ALL_COVISINT_GM_BIDLIST','path' => 'programs_gm','active' => '1','details' => 'GM program','enterprise_id' => '0010000002','customer_id' => '0030000002','plant_id' => '0040000001','portal_id' => '0050000001','created_by' => 'Super Admin','updated_by' => 'Super Admin','param1' => 'none','param2' => 'none','param3' => 'none','param4' => 'none','param5' => 'none','param6' => 'none','param7' => 'none','param8' => 'none','param9' => 'none','param10' => 'none'),
                            array('program_id' => '0090000002','program_type' => 'crawler','program_name' => 'P_BI_FCA_PRODUCTION_ALL_ESUPPLIERCONNECT_SUPPLIER_SCORECARD','path' => 'programs','active' => '1','details' => 'FCA Production program','enterprise_id' => '0010000002','customer_id' => '0030000003','plant_id' => '0040000001','portal_id' => '0050000002','created_by' => 'Super Admin','updated_by' => 'Super Admin','param1' => 'none','param2' => 'none','param3' => 'none','param4' => 'none','param5' => 'none','param6' => 'none','param7' => 'none','param8' => 'none','param9' => 'none','param10' => 'none'),
                            array('program_id' => '0090000003','program_type' => 'crawler','program_name' => 'P_BI_FCA_MOPAR_ALL_ESUPPLIERCONNECT_SUPPLIER_SCORECARD','path' => 'programs','active' => '1','details' => 'FCA Mopar program','enterprise_id' => '0010000002','customer_id' => '0030000004','plant_id' => '0040000001','portal_id' => '0050000003','created_by' => 'Super Admin','updated_by' => 'Super Admin','param1' => 'none','param2' => 'none','param3' => 'none','param4' => 'none','param5' => 'none','param6' => 'none','param7' => 'none','param8' => 'none','param9' => 'none','param10' => 'none'),
                            array('program_id' => '0090000004','program_type' => 'crawler','program_name' => 'P_BI_FORD_ALL_COVISINT_Q1_SCORECARD','path' => 'programs','active' => '1','details' => 'FORD program','enterprise_id' => '0010000002','customer_id' => '0030000005','plant_id' => '0040000001','portal_id' => '0050000004','created_by' => 'Super Admin','updated_by' => 'Super Admin','param1' => 'none','param2' => 'none','param3' => 'none','param4' => 'none','param5' => 'none','param6' => 'none','param7' => 'none','param8' => 'none','param9' => 'none','param10' => 'none'),
                            array('program_id' => '0090000005','program_type' => 'crawler','program_name' => 'P_BI_PSA_ALL_B2BPORTAL_PSA_BIDLIST','path' => 'programs','active' => '1','details' => 'PSA program','enterprise_id' => '0010000002','customer_id' => '0030000006','plant_id' => '0040000001','portal_id' => '0050000005','created_by' => 'Super Admin','updated_by' => 'Super Admin','param1' => 'none','param2' => 'none','param3' => 'none','param4' => 'none','param5' => 'none','param6' => 'none','param7' => 'none','param8' => 'none','param9' => 'none','param10' => 'none'),
                            array('program_id' => '0090000006','program_type' => 'report','program_name' => 'P_RT_ALL_CUSTOMERS_ALL_PLANTS_MASTERDASHBOARD','path' => 'programs','active' => '1','details' => 'master dashboard','enterprise_id' => '0010000002','customer_id' => '0030000001','plant_id' => '0040000001','portal_id' => 'none','created_by' => 'Super Admin','updated_by' => 'Super Admin','param1' => 'none','param2' => 'none','param3' => 'none','param4' => 'none','param5' => 'none','param6' => 'none','param7' => 'none','param8' => 'none','param9' => 'none','param10' => 'none'));

        DB::table('program')->insert($program_details);
    }
}
