<?php

use Illuminate\Database\Seeder;

class user_role extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $role  = array(array('role_id' => '1','role_name' => 'All Screens','role_desc' => 'AllScreens','enterprise_id' => '0010000002')
      ,array('role_id' => '2','role_name' => 'Enterprise Screens','role_desc' => 'EnterpriseScreen','enterprise_id' => '0010000002')
      ,array('role_id' => '3','role_name' => 'Report Generator','role_desc' => 'Report Generator','enterprise_id' => '0010000002')
      ,array('role_id' => '4','role_name' => 'Report Generator','role_desc' => 'Report Generator','enterprise_id' => '0010000002'));
      DB::table('role')->insert($role);
    }
}
