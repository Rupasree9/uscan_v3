<?php

use Illuminate\Database\Seeder;

class frequency extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $frequency  = array(array('frequency_id' => '1','frequency_name' => 'Hourly','desc' => 'Hourly','enterprise_id' => '0010000002')
      ,array('frequency_id' => '2','frequency_name' => 'Daily','desc' => 'Daily','enterprise_id' => '0010000002')
      ,array('frequency_id' => '3','frequency_name' => 'Weekly','desc' => 'Weekly','enterprise_id' => '0010000002')
      ,array('frequency_id' => '4','frequency_name' => 'Monthly','desc' => 'Monthly','enterprise_id' => '0010000002')
    );
      DB::table('frequency')->insert($frequency);
    }
}
