<?php

use Illuminate\Database\Seeder;

class customer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $customer_parent  = array(
                          array('parent_id' => '0030001','parent_name' => 'ALL Customers','parent_code' => 'All','created_by' => 'Super Admin','updated_by' => 'Super Admin','created_at' => '2017-01-31 09:00:00','updated_at' => '2017-01-31 09:00:00','enterprise_id' => '0010000002'),
                          array('parent_id' => '0030002','parent_name' => 'General Motor Corporation','parent_code' => 'GM','created_by' => 'Super Admin','updated_by' => 'Super Admin','created_at' => '2017-01-31 09:00:00','updated_at' => '2017-01-31 09:00:00','enterprise_id' => '0010000002'),
                          array('parent_id' => '0030003','parent_name' => 'Fiat Chrysler Automobiles','parent_code' => 'FCA','created_by' => 'Super Admin','updated_by' => 'Super Admin','created_at' => '2017-01-31 09:00:00','updated_at' => '2017-01-31 09:00:00','enterprise_id' => '0010000002'),
                          array('parent_id' => '0030004','parent_name' => 'Ford Motor Company','parent_code' => 'FORD','created_by' => 'Super Admin','updated_by' => 'Super Admin','created_at' => '2017-01-31 09:00:00','updated_at' => '2017-01-31 09:00:00','enterprise_id' => '0010000002'),
                          array('parent_id' => '0030005','parent_name' => 'PSA Group','parent_code' => 'PSA','created_by' => 'Super Admin','updated_by' => 'Super Admin','created_at' => '2017-01-31 09:00:00','updated_at' => '2017-01-31 09:00:00','enterprise_id' => '0010000002'),
            );
      DB::table('customer_parent')->insert($customer_parent);

      $customer_child  = array(
                          array('customer_id' => '0030000001','customer_name' => 'ALL Customers','customer_code' => 'All','duns_no' => '','customer_type_id' => '1','created_by' => 'Super Admin','updated_by' => 'Super Admin','active' => '1','created_at' => '2017-01-31 09:00:00','updated_at' => '2017-01-31 09:00:00','parent_id' => '0030001','enterprise_id' => '0010000002'),
                          array('customer_id' => '0030000002','customer_name' => 'General Motor Corporation','customer_code' => 'GM','duns_no' => '','customer_type_id' => '2','created_by' => 'Super Admin','updated_by' => 'Super Admin','active' => '1','created_at' => '2017-01-31 09:00:00','updated_at' => '2017-01-31 09:00:00','parent_id' => '0030002','enterprise_id' => '0010000002'),
                          array('customer_id' => '0030000003','customer_name' => 'FCA Production','customer_code' => 'FCA Mopar','duns_no' => '','customer_type_id' => '2','created_by' => 'Super Admin','updated_by' => 'Super Admin','active' => '1','created_at' => '2017-01-31 09:00:00','updated_at' => '2017-01-31 09:00:00','parent_id' => '0030003','enterprise_id' => '0010000002'),
                          array('customer_id' => '0030000004','customer_name' => 'FCA Mopar','customer_code' => 'FCA Production','duns_no' => '','customer_type_id' => '2','created_by' => 'Super Admin','updated_by' => 'Super Admin','active' => '1','created_at' => '2017-01-31 09:00:00','updated_at' => '2017-01-31 09:00:00','parent_id' => '0030003','enterprise_id' => '0010000002'),
                          array('customer_id' => '0030000005','customer_name' => 'Ford Motor Company','customer_code' => 'FORD','duns_no' => '','customer_type_id' => '2','created_by' => 'Super Admin','updated_by' => 'Super Admin','active' => '1','created_at' => '2017-01-31 09:00:00','updated_at' => '2017-01-31 09:00:00','parent_id' => '0030004','enterprise_id' => '0010000002'),
                          array('customer_id' => '0030000006','customer_name' => 'PSA','customer_code' => 'PSA','duns_no' => '','customer_type_id' => '2','created_by' => 'Super Admin','updated_by' => 'Super Admin','active' => '1','created_at' => '2017-01-31 09:00:00','updated_at' => '2017-01-31 09:00:00','parent_id' => '0030005','enterprise_id' => '0010000002')

      );
      DB::table('customer_child')->insert($customer_child);


      $customer_contact  = array(
                         array('contact_id' => '1','primary_contact_name' => '','primary_contact_email_id' => '','primary_contact_number' => '','secondary_contact_name' => '','secondary_contact_email_id' => '','secondary_contact_number' => '','enterprise_id' => '0010000002','customer_id' => '0030000001'),
                         array('contact_id' => '2','primary_contact_name' => '','primary_contact_email_id' => '','primary_contact_number' => '','secondary_contact_name' => '','secondary_contact_email_id' => '','secondary_contact_number' => '','enterprise_id' => '0010000002','customer_id' => '0030000002'),
                         array('contact_id' => '3','primary_contact_name' => '','primary_contact_email_id' => '','primary_contact_number' => '','secondary_contact_name' => '','secondary_contact_email_id' => '','secondary_contact_number' => '','enterprise_id' => '0010000002','customer_id' => '0030000003'),
                         array('contact_id' => '4','primary_contact_name' => '','primary_contact_email_id' => '','primary_contact_number' => '','secondary_contact_name' => '','secondary_contact_email_id' => '','secondary_contact_number' => '','enterprise_id' => '0010000002','customer_id' => '0030000004'),
                         array('contact_id' => '5','primary_contact_name' => '','primary_contact_email_id' => '','primary_contact_number' => '','secondary_contact_name' => '','secondary_contact_email_id' => '','secondary_contact_number' => '','enterprise_id' => '0010000002','customer_id' => '0030000005'),
                         array('contact_id' => '6','primary_contact_name' => '','primary_contact_email_id' => '','primary_contact_number' => '','secondary_contact_name' => '','secondary_contact_email_id' => '','secondary_contact_number' => '','enterprise_id' => '0010000002','customer_id' => '0030000006'),
                                );
      DB::table('customer_contact')->insert($customer_contact);
    }
}
