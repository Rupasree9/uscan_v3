<?php

use Illuminate\Database\Seeder;

class plant_status extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $plant_status  = array(
        array('plant_status_id' => '1','plant_status' => 'Red','enterprise_id' => '0010000002')
        ,array('plant_status_id' => '2','plant_status' => 'Yellow','enterprise_id' => '0010000002')
        ,array('plant_status_id' => '3','plant_status' => 'Green','enterprise_id' => '0010000002')

      );
      DB::table('plant_status')->insert($plant_status);
    }
}
