<?php

use Illuminate\Database\Seeder;

class user extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user  = array(
                         array('id' => '0020000001','email' => 'johndoe@doe.com','first_name' => 'John','last_name' => 'Doe','group_id' => '1','role_id' => '1','password' => '$2y$10$aB6s2um2.XRdzA74A28dXOic36hLYKAfDvGrrKWksvbTOwLB0A6RO','password_expires' => '2017-01-31 09:00:00','active' => '1','valid_till' => '2017-01-31 09:00:00','created_by' => 'Super Admin','updated_by' => 'Super Admin','enterprise_id' => '0010000002','created_at' => '2017-01-31 09:00:00','updated_at' => '2017-01-31 09:00:00')
                        ,array('id' => '0020000002','email' => 'jimdoe@doe.com','first_name' => 'John','last_name' => 'Doe','group_id' => '3','role_id' => '4','password' => '$2y$10$aB6s2um2.XRdzA74A28dXOic36hLYKAfDvGrrKWksvbTOwLB0A6RO','password_expires' => '2017-01-31 09:00:00','active' => '1','valid_till' => '2017-01-31 09:00:00','created_by' => 'Super Admin','updated_by' => 'Super Admin','enterprise_id' => '0010000002','created_at' => '2017-01-31 09:00:00','updated_at' => '2017-01-31 09:00:00')
                      );
      DB::table('users')->insert($user);
    }
}
