<?php

use Illuminate\Database\Seeder;

class mail_enterprise_user_groups extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $mail_enterprise_user_groups  = array(array('user_group_id' => '1','ent_user_group_name' => 'Production Support','ent_user_group_id' => 'ProductionSupport@uscan.com','created_by' => 'Jhondoe','updated_by' => 'Jhondoe','enterprise_id' => '0010000002')
                       );
        DB::table('mail_enterprise_user_groups')->insert($mail_enterprise_user_groups);
    }
}
