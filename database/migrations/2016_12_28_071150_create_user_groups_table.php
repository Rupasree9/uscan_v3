<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_group', function (Blueprint $table) {
            $table->increments('group_id')->unsigned();
            $table->string('group_name',35);

            $table->Integer('role_id')->unsigned();
            $table->foreign('role_id')->references('role_id')->on('role');

            $table->timestamps();

            $table->softDeletes();

            $table->string('enterprise_id',15);
            $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_groups');
    }
}
