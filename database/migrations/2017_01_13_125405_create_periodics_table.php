<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeriodicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('periodic', function (Blueprint $table) {
            $table->increments('periodic_id');

            $table->string('frequency_id',15);
            $table->foreign('frequency_id')->references('frequency_id')->on('frequency');

            $table->string('job_id',15);
            $table->foreign('job_id')->references('job_id')->on('job');

            $table->string('enterprise_id',15);
            $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('periodic');
    }
}
