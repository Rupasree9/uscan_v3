<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortalModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portal', function (Blueprint $table) {
            $table->string('portal_id',15);
            $table->string('portal_name', 30);
             $table->tinyInteger('active');
             $table->tinyInteger('locked');

            $table->string('created_by', 100);
            $table->string('updated_by', 100);


            $table->string('enterprise_id',15);
            $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');

            $table->string('customer_id',15);
            $table->foreign('customer_id')->references('customer_id')->on('customer_child');

            $table->string('plant_id',15);
            $table->foreign('plant_id')->references('plant_id')->on('plant');

            $table->softDeletes();
            $table->timestamps();
            $table->primary(['portal_id','enterprise_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portal');
    }
}
