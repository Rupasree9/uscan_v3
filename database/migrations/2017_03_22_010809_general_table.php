<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GeneralTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('general_parameter', function (Blueprint $table) {
                $table->integer('general_parameter_id')->unsigned();
                $table->string('parameter_name',50);
                $table->string('table_name',50);
                $table->string('field_one_name',50);
                $table->string('field_two_name',50);
                $table->string('enterprise_id',50);
              // $table->string('step_no',5);
              // $table->longText('description');
              // $table->dateTime('date_time');
              // $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
              // $table->foreign('log_id')->references('log_id')->on('process_monitor');
              // $table->primary(['log_id','step_no','enterprise_id'],'multiple_primary_key');
              $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
              $table->primary(['general_parameter_id','parameter_name','enterprise_id'],'multiple_primary_key');
              // $table->primary(array('parameter_name','enterprise_id'),'general_primary_key');
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_parameter');
    }
}
