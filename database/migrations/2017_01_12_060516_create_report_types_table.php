<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('report_types', function (Blueprint $table) {

          $table->integer('report_type_id');
          $table->string('report_type_name',60);


          $table->string('customer_id',15);
          $table->foreign('customer_id')->references('customer_id')->on('customer_child');

          $table->string('enterprise_id',15);
          $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
          $table->primary(['report_type_id','enterprise_id']);
          $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('report_types');
    }
}
