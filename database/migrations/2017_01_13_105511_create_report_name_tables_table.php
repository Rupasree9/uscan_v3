<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportNameTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_name', function (Blueprint $table) {

          $table->integer('report_name_id');
          
          $table->string('report_name',60);
          $table->integer('report_type_id');
          $table->foreign('report_type_id')->references('report_type_id')->on('report_types');
          $table->string('program_id',15);
          $table->foreign('program_id')->references('program_id')->on('program');
          $table->string('enterprise_id',15);
          $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
          $table->softDeletes();
          $table->primary(['report_name_id','enterprise_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_name');
    }
}
