<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TaskType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('task_type', function (Blueprint $table) {

            $table->integer('task_type_id');
            $table->string('task_type_name', 30);
            $table->string('details', 30);
            // $table->string('created_by', 100);
            // $table->string('updated_by', 100);
            $table->string('enterprise_id',15);
            $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
            $table->softDeletes();
            $table->primary(['task_type_id','enterprise_id']);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
