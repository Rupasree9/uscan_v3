<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlantModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plant', function (Blueprint $table) {
             $table->string('plant_id',15);
             $table->string('plant_name', 50);
             $table->string('plant_code',10);
             $table->string('duns_no', 50);
             $table->string('qmis_id');
             $table->string('business_unit', 100);
             $table->string('product_group', 50);
             $table->string('function', 50);
             $table->string('erp_system', 50);
             $table->string('quality_manager', 50);
             $table->string('plant_manager', 50);
             $table->string('logistics_manager', 50);
             $table->string('finance_manager', 50);
             $table->string('hr_manager', 50);
             $table->string('plant_warranty_champion', 50);
             $table->string('energy_champion', 50);
             $table->string('health_and_safety_coordinator', 50);
             $table->string('environmental_coordinator', 50);
             $table->string('created_by', 100);
             $table->string('updated_by', 100);
             $table->tinyInteger('active');

             $table->softDeletes();


            //  $table->string('customer_id',15);
            //  $table->foreign('customer_id')->references('customer_id')->on('customer_child');

            $table->string('enterprise_id',15);
            $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
            $table->primary(['plant_id','enterprise_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plant');
    }
}
