<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailEnterpriseUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mail_enterprise_users', function (Blueprint $table) {
            $table->increments('mail_comm_id')->unsigned();
            $table->string('ent_user_id',35);
            $table->string('ent_user_name',35);
            $table->integer('user_group_id')->unsigned();
            // $table->foreign('group_id')->references('group_id')->on('mail_communicaion_user_group');

            $table->string('enterprise_id',15);
            $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');

            $table->string('created_by', 100);
            $table->string('updated_by', 100);

            $table->timestamps();
            $table->softDeletes();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mail_enterprise_users');
    }
}
