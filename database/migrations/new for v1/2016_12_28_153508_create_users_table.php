<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
          $table->string('id',15);
          $table->string('email')->unique();
          $table->string('first_name',128);
          $table->string('last_name',128);
          $table->string('group_id');
          $table->string('password');
          $table->date('password_expires');
          $table->integer('role_id')->unsigned();
          $table->foreign('role_id')->references('role_id')->on('role');
          $table->tinyInteger('active')->unsigned();
          $table->date('valid_till');
          $table->string('created_by');
          $table->string('updated_by');
          $table->string('enterprise_id',15);
          $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
          $table->rememberToken();
          $table->timestamps();
          $table->softDeletes();
          $table->primary(['id','email','enterprise_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
