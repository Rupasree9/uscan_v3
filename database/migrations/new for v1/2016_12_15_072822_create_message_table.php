<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('message_table', function (Blueprint $table) {
         $table->integer('message_id');
         $table->string('message',50);
         $table->string('description', 50);
         $table->string('created_by', 100);
         $table->string('updated_by', 100);

         $table->string('enterprise_id',15);
         $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
         $table->primary(['enterprise_id']);
         $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_table');
    }
}
