<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortalDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('portal_details', function (Blueprint $table) {

         $table->increments('portal_details_id');
         $table->string('portal_id',15);
         $table->foreign('portal_id')->references('portal_id')->on('portal');

         $table->string('portal_url', 50);
         $table->string('portal_user_id', 30);
         $table->string('portal_password', 30);
         $table->string('portal_repeat_password', 30);
         $table->Integer('max_attempt');
         $table->date('password_expiry_date');
         $table->tinyInteger('locked');


          $table->string('enterprise_id',15);
          $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');


          $table->softDeletes();
          $table->timestamps();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
