<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportNamePlantStatusReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_name_plant_status', function (Blueprint $table) {
          $table->string('report_id',15);
          $table->foreign('report_id')->references('report_id')->on('report_generators');

          $table->string('plant_status_id',10);
          
          $table->string('enterprise_id',15);
          $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
          $table->primary(array('report_id', 'plant_status_id','enterprise_id'),'multiple_primary_key');
          $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_name_plant_status');
    }
}
