<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFrequencyModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('frequency', function (Blueprint $table) {
          $table->string('frequency_id',15);
          $table->string('frequency_name', 30);
          $table->string('desc', 45);          

         $table->string('enterprise_id',15);
         $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
         $table->timestamps();
         $table->primary(['frequency_id','enterprise_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('frequency');
    }
}
