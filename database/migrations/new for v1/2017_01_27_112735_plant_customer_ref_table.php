<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlantCustomerRefTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('plant_customer_ref', function (Blueprint $table) {
          $table->Increments('plant_cust_id')->unsigned();
          $table->string('supplier_code',30);

          $table->string('plant_id',15);
          $table->foreign('plant_id')->references('plant_id')->on('plant');

          $table->string('customer_id',15);
          $table->foreign('customer_id')->references('customer_id')->on('customer_child');

          $table->string('enterprise_id',15);
          $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');

          $table->string('created_by', 100);
          $table->string('updated_by', 100);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plant_customer_ref');
    }
}
