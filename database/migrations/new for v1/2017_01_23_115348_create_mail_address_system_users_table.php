<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailAddressSystemUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mail_address_system_users', function (Blueprint $table) {
            $table->Increments('system_user_id')->unsigned();
            // $table->string('conn_system_user_id',35);
            $table->string('conn_system_user_name',35);
            $table->string('conn_system_user_email',35);
            $table->string('conn_smtp_user_name',35);
            $table->string('conn_smtp_pwd',35);

            $table->Integer('mail_conn_id')->unsigned();
            $table->foreign('mail_conn_id')->references('mail_conn_id')->on('mail_connections');

            $table->string('enterprise_id',15);
            $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
            $table->string('created_by', 100);
            $table->string('updated_by', 100);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mail_address_system_users');
    }
}
