<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnterpriseContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enterprise_contact', function (Blueprint $table) {
          $table->increments('contact_id')->unsigned();
          $table->string('primary_contact_name',30);
          $table->string('primary_contact_email_id',50);
          $table->string('primary_contact_number',15);
          $table->string('secondary_contact_name',30);
          $table->string('secondary_contact_email_id',50);
          $table->string('secondary_contact_number',15);

          $table->softDeletes();

          $table->string('enterprise_id',15);
          $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enterprise_contact');
    }
}
