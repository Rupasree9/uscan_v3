<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportNameProductGroupReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_name_product_group', function (Blueprint $table) {

          $table->string('report_id',15);
          $table->foreign('report_id')->references('report_id')->on('report_generators');
          $table->string('product_group', 50);


          $table->string('enterprise_id',15);
          $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
          $table->primary(array('report_id', 'product_group','enterprise_id'),'multiple_primary_key');
          $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_name_product_group');
    }
}
