<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlantStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plant_status', function (Blueprint $table) {
          $table->string('plant_status_id',10);
          $table->string('plant_status', 30);

          $table->string('enterprise_id',15);
          $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');

          $table->softDeletes();
          $table->primary(['plant_status_id','enterprise_id']);

       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plant_status');
    }
}
