<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNumberRangeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('number_range', function (Blueprint $table) {
         $table->integer('number_range_id');
         $table->string('table_name', 50);
         $table->string('current_no',15);
         $table->string('starting_no',15);
         $table->string('ending_no',15);
         $table->string('description', 50);
         $table->string('created_by', 100);
         $table->string('updated_by', 100);

         $table->string('enterprise_id',15);
         $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
         $table->primary(['number_range_id','enterprise_id']);
         $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('number_range');
    }
}
