<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailConnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mail_connections', function (Blueprint $table) {
            $table->Increments('mail_conn_id')->unsigned();
            $table->string('connection_name',35);
            $table->string('protocol',35);
            $table->string('server',35);
            $table->string('port',35);
            $table->string('proxy_mode',35);
            $table->tinyInteger('request_notification')->unsigned();
            $table->tinyInteger('dump_messages')->unsigned();

            $table->string('created_by', 100);
            $table->string('updated_by', 100);
            
            $table->string('enterprise_id',15);
            $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mail_connections');
    }
}
