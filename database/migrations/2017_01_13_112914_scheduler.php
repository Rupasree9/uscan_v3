<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Scheduler extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('job', function (Blueprint $table) {
            $table->string('job_id',15);
            $table->string('job_name',35);
            $table->string('job_type',10);
            $table->string('task_id',60);
            $table->Integer('job_priority');
            $table->string('job_status',50);
            $table->tinyInteger('active');
            $table->tinyInteger('periodic');
            $table->string('after_job',15);
            $table->Integer('start_type_id')->unsigned();
            $table->foreign('start_type_id')->references('start_type_id')->on('start_type');
            
            $table->date('job_start_date');
            $table->time('job_start_time');
            $table->date('job_end_date');
            $table->time('job_end_time');
            $table->date('job_last_run_date');
            $table->time('job_last_run_time');
            $table->string('created_by', 100);
            $table->string('updated_by', 100);

            $table->timestamps();

            $table->string('enterprise_id',15);
            $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
            $table->primary(['job_id','enterprise_id']);
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('job');
    }
}
