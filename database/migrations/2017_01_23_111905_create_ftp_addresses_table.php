<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFtpAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ftp_addresses', function (Blueprint $table) {
            $table->Increments('ftp_addr_id')->unsigned();
            $table->string('address_name',35);
            $table->string('user_name',35);
            $table->string('password',35);
            $table->string('file_mask',35);
            $table->string('source_path',35);
            $table->string('destination_path',35);
            $table->string('encoding',35);
            $table->string('file_name',60);

            $table->Integer('ftp_conn_id')->unsigned();
            $table->foreign('ftp_conn_id')->references('ftp_conn_id')->on('ftp_connections');

            $table->string('enterprise_id',15);
            $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
            $table->string('created_by', 100);
            $table->string('updated_by', 100);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ftp_addresses');
    }
}
