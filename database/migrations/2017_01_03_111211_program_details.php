<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProgramDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('program', function (Blueprint $table) {

            $table->string('program_id',15);
            $table->string('program_type', 50);
            $table->string('program_name', 150);
            $table->string('path', 225);
            $table->string('details', 100);
            $table->tinyInteger('active');
            $table->string('created_by', 100);
            $table->string('updated_by', 100);
            $table->string('param1', 50);
            $table->string('param2', 50);
            $table->string('param3', 50);
            $table->string('param4', 50);
            $table->string('param5', 50);
            $table->string('param6', 50);
            $table->string('param7', 50);
            $table->string('param8', 50);
            $table->string('param9', 50);
            $table->string('param10',50);


            $table->string('enterprise_id',15);
            $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
            $table->string('customer_id',15);
            $table->foreign('customer_id')->references('customer_id')->on('customer_child');
            $table->string('plant_id',15);
            $table->foreign('plant_id')->references('plant_id')->on('plant');
            $table->string('portal_id',15);
            // $table->foreign('portal_id')->references('portal_id')->on('portal');
            $table->softDeletes();
            $table->timestamps();
            $table->primary(['program_id','enterprise_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
