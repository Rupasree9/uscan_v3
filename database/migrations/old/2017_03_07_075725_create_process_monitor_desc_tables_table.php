<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessMonitorDescTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('process_monitor_descs', function (Blueprint $table) {
          $table->string('log_id',15);
          $table->string('enterprise_id',15);
          $table->string('step_no',5);
          $table->longText('description');
          $table->dateTime('date_time');
          $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
          $table->foreign('log_id')->references('log_id')->on('process_monitor');
          $table->primary(['log_id','step_no','enterprise_id'],'multiple_primary_key');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('process_monitor_descs');
    }
}
