<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerChildModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_child', function (Blueprint $table) {
             $table->string('customer_id',15);
             $table->string('customer_name', 30);
             $table->string('customer_code',20)->nullable();;
             $table->string('duns_no', 30)->nullable();;
             $table->integer('customer_type_id');
             $table->string('created_by', 100);
             $table->string('updated_by', 100);
             $table->tinyInteger('active')->unsigned();
             $table->timestamps();
             $table->softDeletes();



             $table->string('parent_id',15);
             $table->foreign('parent_id')->references('parent_id')->on('customer_parent');

             $table->foreign('customer_type_id')->references('customer_type_id')->on('customer_type');

             $table->string('enterprise_id',15);
             $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');

             $table->primary(['customer_id','enterprise_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_child');
    }
}
