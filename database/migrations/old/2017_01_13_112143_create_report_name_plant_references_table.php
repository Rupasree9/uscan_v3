<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportNamePlantReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_name_plant', function (Blueprint $table) {

          $table->string('report_id',15);
          $table->foreign('report_id')->references('report_id')->on('report_generators');

          $table->string('plant_id',15);
          $table->foreign('plant_id')->references('plant_id')->on('plant');

          $table->string('enterprise_id',15);
          $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
          $table->primary(array('report_id', 'plant_id','enterprise_id'));
          $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_name_plant');
    }
}
