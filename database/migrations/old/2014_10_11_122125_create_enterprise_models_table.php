<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnterpriseModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enterprise', function (Blueprint $table) {
            $table->string('enterprise_id',15);
            $table->string('enterprise_name', 30);
            $table->string('tin_number', 30);
            $table->string('duns_number', 30);
            $table->string('license_number', 30);
            $table->tinyInteger('active');
            $table->string('created_by', 100);
            $table->string('updated_by', 100);

            $table->softDeletes();
            $table->primary('enterprise_id');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enterprise');
    }
}
