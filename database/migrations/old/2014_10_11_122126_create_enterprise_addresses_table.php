<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnterpriseAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enterprise_address', function (Blueprint $table) {
          $table->increments('address_id')->unsigned();

          $table->string('soldto_street',50);
          $table->string('soldto_city', 30);
          $table->string('soldto_state', 30);
          $table->string('soldto_country', 30);
          $table->string('soldto_region', 30);
          $table->string('soldto_zip_code',10);

          $table->string('billto_street',50);
          $table->string('billto_city', 30);
          $table->string('billto_state', 30);
          $table->string('billto_country', 30);
          $table->string('billto_region', 30);
          $table->string('billto_zip_code',10);


          $table->string('enterprise_id',15);
          $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');

          $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enterprise_address');
    }
}
