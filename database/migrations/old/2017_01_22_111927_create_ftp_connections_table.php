<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFtpConnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ftp_connections', function (Blueprint $table) {
            $table->Increments('ftp_conn_id')->unsigned();
            $table->string('connection_name',35);
            $table->string('server_type',35);
            $table->string('host',35);
            $table->string('port',35);
            $table->string('connection_mode',35);
            $table->tinyInteger('keep_alive')->unsigned();
            $table->string('transfer_type',35);
            $table->string('append_mode',35);
            $table->tinyInteger('lock_file')->unsigned();
            $table->tinyInteger('delete_file')->unsigned();

            $table->string('enterprise_id',15);
            $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
            $table->string('created_by', 100);
            $table->string('updated_by', 100);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ftp_connections');
    }
}
