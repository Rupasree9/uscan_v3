<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlantAddressModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plant_address', function (Blueprint $table) {
            $table->increments('address_id')->unsigned();
            $table->string('street',30);
            $table->string('city', 30);
            $table->string('state', 30);
            $table->string('country', 30);
            $table->string('region', 30);
            $table->string('zip_code',10);

            $table->softDeletes();
            $table->string('plant_id',15);
            $table->foreign('plant_id')->references('plant_id')->on('plant');

            $table->string('enterprise_id',15);
            $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plant_address_models');
    }
}
