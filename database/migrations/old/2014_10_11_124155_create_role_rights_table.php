<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleRightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_right', function (Blueprint $table) {
            $table->Increments('role_right_id')->unsigned();

            $table->Integer('role_id')->unsigned();
            $table->foreign('role_id')->references('role_id')->on('role');

            $table->Integer('right_id')->unsigned();
            $table->foreign('right_id')->references('right_id')->on('rights');

            $table->timestamps();
            $table->softDeletes();

            $table->string('enterprise_id',15);
            $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_right');
    }
}
