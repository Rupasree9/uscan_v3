<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerContactModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('customer_contact', function (Blueprint $table) {
          $table->increments('contact_id')->unsigned();
          $table->string('primary_contact_name',30)->nullable();;
          $table->string('primary_contact_email_id',50)->nullable();;
          $table->string('primary_contact_number',15)->nullable();;
          $table->string('secondary_contact_name',30)->nullable();;
          $table->string('secondary_contact_email_id',50)->nullable();;
          $table->string('secondary_contact_number',15)->nullable();;


          $table->string('enterprise_id',15);
          $table->string('customer_id',15);

          $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
          $table->foreign('customer_id')->references('customer_id')->on('customer_child');

          $table->softDeletes();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_contact_models');
    }
}
