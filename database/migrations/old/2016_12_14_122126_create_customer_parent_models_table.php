<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerParentModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_parent', function (Blueprint $table) {
            $table->string('parent_id',15);
            $table->string('parent_name', 30);
      			$table->string('parent_code', 30);
            $table->string('created_by', 100);
            $table->string('updated_by', 100);
            $table->timestamps();


            $table->string('enterprise_id',15);
            $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
            //$table->primary('');
            $table->primary(['parent_id','enterprise_id']);
            $table->softDeletes();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_parent');
    }
}
