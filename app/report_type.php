<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class report_type extends Model
{
      protected $table = 'report_types';
      protected $dates = ['deleted_at'];
      public $timestamps = false;
}
