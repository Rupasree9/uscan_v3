<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class enterprise_contact extends Model
{
  use SoftDeletes;
  protected $table = 'enterprise_contact';
  public $timestamps = false;
  protected $dates = ['deleted_at'];
}
