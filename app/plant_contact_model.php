<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class plant_contact_model extends Model
{
    use SoftDeletes;
    protected $table = 'plant_contact';
    public $timestamps = false;
    protected $dates = ['deleted_at'];
}
