<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class plant_status extends Model
{
  protected $table = 'plant_status';
  protected $dates = ['deleted_at'];
  
}
