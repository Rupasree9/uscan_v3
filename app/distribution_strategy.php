<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class distribution_strategy extends Model
{
    //
    use SoftDeletes;
    protected $table = 'distribution_strategy';
    protected $dates = ['deleted_at'];
}
