<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class task_type_reference_model extends Model
{
    //
    protected $table = 'task_type_reference';
    protected $dates = ['deleted_at'];
    public $timestamps = false;
}
