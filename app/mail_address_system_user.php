<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mail_address_system_user extends Model
{
    //
    protected $table = 'mail_address_system_users';
    protected $dates = ['deleted_at'];
}
