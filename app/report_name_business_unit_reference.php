<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class report_name_business_unit_reference extends Model
{
    //
    protected $table = 'report_name_business_unit';
    protected $dates = ['deleted_at'];
    public $timestamps = false;
}
