<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class customer_master_type extends Model
{
    //
    use SoftDeletes;
    protected $table = 'customer_master_type';
    protected $dates = ['deleted_at'];
}
