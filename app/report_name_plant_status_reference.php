<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class report_name_plant_status_reference extends Model
{
    //
    protected $table = 'report_name_plant_status';
    protected $dates = ['deleted_at'];
    public $timestamps = false;
}
