<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class program_model extends Model
{
    //
    use SoftDeletes;
    protected $table = 'program';
    protected $dates = ['deleted_at'];
}
