<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class customer_parent_model extends Model
{
     use SoftDeletes;
     protected $table = 'customer_parent';
     protected $dates = ['deleted_at'];
}
