<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class report_generator extends Model
{
    protected $table = 'report_generators';
    protected $dates = ['deleted_at'];

}
