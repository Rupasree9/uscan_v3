<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class scheduler_model extends Model
{
  use SoftDeletes;
  protected $table = 'job';
  protected $dates = ['deleted_at'];
}
