<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Auth;

class plant_controller extends Controller
{



  public function store(Request $request)
  {
    // $max_address_id = \DB::table('address')->max('address_id');
    $query = 'select current_no from number_range where table_name = "plant" and enterprise_id = '.Auth::user()->enterprise_id;
    $result = \DB::select($query);
    $plant_id = $result[0]->current_no + 1;

    $update_current_no =   \DB::table('number_range')
    ->where('table_name',  'plant')
    ->limit(1)
    ->update(array('current_no' => $plant_id));

    $plant = new App\plant_model;
    $plant->plant_id = $plant_id;
    $plant->plant_name = $request->plant_name;
    $plant->plant_code = $request->plant_code;
    $plant->duns_no = $request->duns_no;
    $plant->qmis_id = $request->qmis_id;
    $plant->business_unit = $request->business_unit;
    $plant->product_group = $request->product_group;
    $plant->function = $request->function_plant;
    $plant->erp_system = $request->erp_system;
    $plant->quality_manager = $request->quality_manager;
    $plant->plant_manager = $request->plant_manager;
    $plant->logistics_manager = $request->logistics_manager;
    $plant->finance_manager = $request->finance_manager;
    $plant->hr_manager = $request->hr_manager;
    $plant->plant_warranty_champion= $request->plant_warranty_champion;
    $plant->energy_champion = $request->energy_champion;
    $plant->health_and_safety_coordinator = $request->health_and_safety_coordinator;
    $plant->environmental_coordinator = $request->environmental_coordinator;
    $plant->active= $request->active;
    $plant->enterprise_id = Auth::user()->enterprise_id;
    $plant->created_by = Auth::user()->email;
    $plant->updated_by = Auth::user()->email;
    $plant->save();


    $contact = new App\plant_contact_model;
    $contact->contact_name = $request->contact_name;
    $contact->contact_email_id = $request->contact_email;
    $contact->contact_number = $request->contact_no;
    $contact->plant_id = $plant_id;
    $contact->enterprise_id = Auth::user()->enterprise_id;
    $contact->save();

    $address = new App\plant_address_model;
    $address->street = $request->street;
    $address->city = $request->city;
    $address->state = $request->state;
    $address->country = $request->country;
    $address->region = $request->region;
    $address->zip_code =$request->zip_code;
    $address->enterprise_id = Auth::user()->enterprise_id;
    $address->plant_id = $plant_id;
    $address->save();
    }

    public function show(Request $request)
    {
        $plant_id = $request->plant_id;
        $plant_name =$request->plant_name;
        $plant_code =$request->plant_code;
        $duns_no =$request->duns_no;
        $query_data = "";
        if($plant_id != "")
        {
          $query_data = " plant.plant_id = ". $plant_id. " and ";
        }
        if($plant_name!="")
        {
          $query_data =$query_data . " plant.plant_name = '" . $plant_name."' and ";
        }
        if($plant_code!="")
        {
          $query_data =$query_data . "   plant.plant_code = '" . $plant_code."' and ";
        }
        if($duns_no!="")
        {
          $query_data =$query_data . "  plant.duns_no = '" . $duns_no."' and ";
        }

        $query_data = substr($query_data, 0, -4);
        $data = \DB::select("select   plant.plant_id,
          plant_name,
          plant_code,
          plant.duns_no as plant_duns_no,
          qmis_id,
          business_unit,
          product_group,
          function,
          erp_system,
          quality_manager,
          plant_manager,
          logistics_manager,
          finance_manager,
          hr_manager,
          plant_warranty_champion,
          energy_champion,
          health_and_safety_coordinator,
          environmental_coordinator,
          plant.created_by,
          plant.updated_by,
          plant.active,
          plant.created_at,
          plant.updated_at,
          address_id,
          street,
          city,
          state,
          country,
          region,
          zip_code,
          contact_id,
          contact_name,
          contact_email_id,
          contact_number
          from plant,plant_address, plant_contact where  plant.plant_id=plant_address.plant_id and plant.plant_id=plant_contact.plant_id and plant.plant_name != 'ALL Plants' and " .$query_data );
        if (empty($data)) {
            return "0";
        }
        else {
          return $data;
        }
    }

    public function showall(Request $request)
    {
          $data = \DB::table('plant')
          ->select(
            'plant.plant_id',
            'plant_name',
            'plant_code',
            'plant.duns_no as plant_duns_no',
            'qmis_id',
            'business_unit',
            'product_group',
            'function',
            'erp_system',
            'quality_manager',
            'plant_manager',
            'logistics_manager',
            'finance_manager',
            'hr_manager',
            'plant_warranty_champion',
            'energy_champion',
            'health_and_safety_coordinator',
            'environmental_coordinator',
            'plant.created_by',
            'plant.updated_by',
            'plant.active',
            'plant.created_at',
            'plant.updated_at',
            'address_id',
            'street',
            'city',
            'state',
            'country',
            'region',
            'zip_code',
            'contact_id',
            'contact_name',
            'contact_email_id',
            'contact_number')
          ->leftJoin('plant_address', 'plant.plant_id', '=', 'plant_address.plant_id')
          ->leftJoin('plant_contact', 'plant.plant_id', '=', 'plant_contact.plant_id')
          ->where('plant.enterprise_id',Auth::user()->enterprise_id)
          ->where('plant.plant_name','<>',"ALL Plants")
          ->orderBy('plant.plant_name', 'asc')
          ->get();
           return $data;
          // $data = \DB::select("call plant()");
          // return $data;
    }

    public function destroy(Request $request)
    {
       $plant_id = $request->plant_id;
       $data = \DB::select("delete from plant_contact where plant_id='$plant_id' and enterprise_id = ".Auth::user()->enterprise_id);
       $data = \DB::select("delete from plant_address where plant_id='$plant_id' and enterprise_id = ".Auth::user()->enterprise_id);
       $data = \DB::select("delete from plant where plant_id='$plant_id' and enterprise_id = ".Auth::user()->enterprise_id);
       return $data;
    }

    public function edit(Request $request)
    {
       $plant_id = $request->plant_id;
       $data = \DB::table('plant')
        ->select('*','plant.duns_no as plant_duns_no')
       ->leftJoin('address', 'plant.address_id', '=', 'address.address_id')
       ->leftJoin('contact_uscan', 'plant.contact_id', '=', 'contact_uscan.contact_id')
        ->where('plant.plant_id', '=', $plant_id)
        ->get();
       return $data;
    }

    public function update(Request $request)
    {
      $updated_at = date('Y-m-d H:i:s', time());

      $plant = new App\plant_model;
      $plant->plant_id = $request->plant_id;

       $data =   \DB::table('plant')
      ->where('plant_id',  $plant->plant_id)
      ->where('plant.enterprise_id',Auth::user()->enterprise_id)
      ->limit(1)
      ->update(array('plant_name' => $plant->plant_name = $request->plant_name,
      'plant_code' =>   $plant->plant_code = $request->plant_code,
      'duns_no' => $plant->duns_no = $request->duns_no,
      'qmis_id' => $plant->qmis_id = $request->qmis_id,
      'business_unit' =>   $plant->business_unit = $request->business_unit,
      'product_group' => $plant->product_group = $request->product_group,
      'function' => $plant->function = $request->function_plant,
      'erp_system' => $plant->erp_system = $request->erp_system,
      'quality_manager' => $plant->quality_manager = $request->quality_manager,
      'plant_manager' => $plant->plant_manager = $request->plant_manager,
      'logistics_manager' => $plant->logistics_manager = $request->logistics_manager,
      'finance_manager' => $plant->finance_manager = $request->finance_manager,
      'plant_warranty_champion' => $plant->plant_warranty_champion= $request->plant_warranty_champion,
      'hr_manager' => $plant->hr_manager = $request->hr_manager,
      'energy_champion' => $plant->energy_champion = $request->energy_champion,
      'health_and_safety_coordinator' => $plant->health_and_safety_coordinator = $request->health_and_safety_coordinator,
      'environmental_coordinator' => $plant->environmental_coordinator = $request->environmental_coordinator,
      'active' => $plant->active = $request->active,
      'updated_at' =>  $updated_at,
      'updated_by' => Auth::user()->email));


     $data =   \DB::table('plant_contact')
     ->where('plant_id', $plant->plant_id)
     ->where('enterprise_id', Auth::user()->enterprise_id)
     ->limit(1)
     ->update(array('contact_name' => $plant->contact_name = $request->contact_name,
     'contact_email_id' => $plant->contact_email= $request->contact_email,
     'contact_number' => $plant->contact_no= $request->contact_no));

        $data =   \DB::table('plant_address')
        ->where('plant_id',  $plant->plant_id)
        ->where('enterprise_id', Auth::user()->enterprise_id)
        ->limit(1)
        ->update(array('street' =>$plant->street= $request->street,
          'city' =>$plant->city= $request->city,
          'state' => $plant->state= $request->state,
          'country' => $plant->country= $request->country,
          'region' => $plant->region= $request->region,
          'zip_code' => $plant->zip_code= $request->zip_code));
          return $data;
    }

    public function fetch_plant_name(Request $request)
    {
      $plant_name = $request->plant_name;
      $results = array();
      $results;
      $query = "select plant_name from plant where plant_name LIKE '%$plant_name%' and plant.plant_name != 'ALL Plants' and  enterprise_id = ".Auth::user()->enterprise_id;
      $res   = \DB::select($query);
      foreach ($res as $query)
        {
            array_push($results,$query->plant_name);
        }
      return $results;
    }

    public function fetch_plant_id(Request $request)
    {
      $plant_id = $request->plant_id;
      $results = array();
      $results;
      $query = "select plant_id from plant where plant_id LIKE '%$plant_id%' and enterprise_id = ".Auth::user()->enterprise_id;
      $res   = \DB::select($query);
      foreach ($res as $query)
        {
            array_push($results,$query->plant_id);
        }
      return $results;
    }

    public function fetch_plant_code(Request $request)
    {
      $dynamic="";
      $plant_name = $request->plant_name_for_fetch;
      if($plant_name != "")
      {
        $dynamic = " plant_name = '". $plant_name. "' and ";
      }
      $plant_code = $request->plant_code;
      $results = array();
      $results;
      $query = "select plant_code from plant where  plant_code LIKE '%$plant_code%' and ".$dynamic;
      $query = substr($query, 0, -4);
      $res   = \DB::select($query);
      foreach ($res as $query)
        {
            array_push($results,$query->plant_code);
        }
      return $results;
    }

    public function fetch_plant_duns_no(Request $request)
    {
      $duns_no = $request->duns_no;
      $results = array();
      $results;
      $query = "select duns_no from plant where duns_no LIKE '%$duns_no%' and enterprise_id = ".Auth::user()->enterprise_id;
      $res   = \DB::select($query);
      foreach ($res as $query)
        {
            array_push($results,$query->duns_no);
        }
      return $results;
    }




}
