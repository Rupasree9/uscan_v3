<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Auth;

class communication_controller extends Controller
{
  public function save_communication(Request $request){

    $connection1 = new \App\mail_connection;
    $connection1->connection_name = $request->connection_name;
    $connection1->protocol = $request->protocol;
    $connection1->server = $request->server_name;
    $connection1->port = $request->port;
    $connection1->proxy_mode = $request->proxy_mode;
    $connection1->request_notification = $request->request_notification;
    $connection1->dump_messages = $request->dump_message;
    $connection1->enterprise_id= Auth::user()->enterprise_id;
    $connection1->created_by = Auth::user()->email;
    $connection1->updated_by = Auth::user()->email;

    $connection1-> save();
    return "New Mail Connection added successfully";
  }

  public function connection_search(Request $request)
  {
       $query_data = "";
       $search_connection_name         =  $request->search_connection_name;
       $search_protocol                =  $request->search_protocol;
       $search_server_name             =  $request->search_server_name;


         if($search_connection_name!="")
         {
           $query_data =$query_data . "   connection_name = '" . $search_connection_name."' and ";
         }
         if($search_protocol!="")
         {
           $query_data =$query_data . "   protocol = '" . $search_protocol."' and ";
         }
         if($search_server_name!="")
         {
           $query_data =$query_data . "   server = '" . $search_server_name."' and ";
         }

         $query_data = substr($query_data, 0, -4);
         $data = \DB::select("select * from mail_connections where mail_connections.enterprise_id = '".Auth::user()->enterprise_id. "' and $query_data");

         if(empty($data))
         {
           return "0";
         }
         else {
           return $data;
         }
  }

  public function autocomplete_connection_name(Request $request)
  {

           $term = $request->fetch_search_connection_name;
           $results = array();
           $results;
           $i = 0;
           $query = "select distinct connection_name from mail_connections where connection_name LIKE '%$term%' and enterprise_id = '". Auth::user()->enterprise_id ."'";
           $res   = \DB::select($query);
           foreach ($res as $query)
            {
                 array_push($results,$query->connection_name);
            }
           return $results;
  }

  public function autocomplete_protocol(Request $request)
  {

           $term = $request->fetch_search_protocol_display;
           $results = array();
           $results;
           $i = 0;
           $query = "select distinct protocol from mail_connections where protocol LIKE '%$term%' and enterprise_id = '". Auth::user()->enterprise_id ."'";
           $res   = \DB::select($query);
           foreach ($res as $query)
            {
                 array_push($results,$query->protocol);
            }
           return $results;
  }
  public function autocomplete_server(Request $request)
  {

           $term = $request->fetch_search_server_display;
           $results = array();
           $results;
           $i = 0;
           $query = "select distinct server from mail_connections where server LIKE '%$term%' and enterprise_id = '". Auth::user()->enterprise_id ."'";
           $res   = \DB::select($query);
           foreach ($res as $query)
            {
                 array_push($results,$query->server);
            }
           return $results;
  }

  public function get_connection_details(Request $request)
  {
    $results = \DB::table('mail_connections')->where('enterprise_id',Auth::user()->enterprise_id)->get();
    return $results;
  }
  public function update_connection(Request $request)
  {
     $data =   \DB::table('mail_connections')
     ->where('mail_conn_id',  $request->mail_conn_id)
     ->where('enterprise_id',Auth::user()->enterprise_id)
     ->limit(1)
     ->update(array('connection_name' => $request->connection_name,
     'protocol' => $request->protocol,
     'server' => $request->server_name,
     'port' => $request->port,
     'proxy_mode' => $request->proxy_mode,
     'request_notification' => $request->request_notification,
     'dump_messages' => $request->dump_message,
     'updated_by' =>   Auth::user()->email));
      return "Mail Connection Updated Sucessfully";
  }

  public function delete_connection(Request $request)
  {
    $mail_conn_id = $request->mail_conn_id;

    $data = \DB::select("delete from mail_connections where mail_conn_id='$mail_conn_id' and enterprise_id = '". Auth::user()->enterprise_id ."'");

    return "Mail Connection Deleted Successfully";
  }

  public function get_connection(Request $request){
    $data = \DB::select("select mail_conn_id,connection_name from mail_connections");
    return $data;
  }

  public function save_system_user(Request $request){

    $connection1 = new \App\mail_address_system_user;
    $connection1->conn_system_user_name = $request->system_user_name;
    $connection1->conn_system_user_email = $request->email_id;
    $connection1->conn_smtp_user_name = $request->smtp_user_name;
    $connection1->conn_smtp_pwd = $request->smtp_password;
    $connection1->mail_conn_id = $request->mail_connection;
    $connection1->enterprise_id= Auth::user()->enterprise_id;
    $connection1->created_by = Auth::user()->email;
    $connection1->updated_by = Auth::user()->email;

    $connection1-> save();
    return "New System User added successfully";
  }

  public function get_system_user_details(Request $request){
    $results = \DB::select("select addr.*,mailc.connection_name from mail_address_system_users as addr join mail_connections as mailc on addr.mail_conn_id = mailc.mail_conn_id where addr.enterprise_id = '". Auth::user()->enterprise_id ."'");
    return $results;

  }
  public function search_system_user(Request $request)
  {
       $query_data = "";
       $search_system_user_name         =  $request->search_system_user_name;
       $search_email_id                =  $request->search_email_id;
       $search_smtp_user_name             =  $request->search_smtp_user_name;

         if($search_system_user_name!="")
         {
           $query_data =$query_data . "   conn_system_user_name = '" . $search_system_user_name."' and ";
         }
         if($search_email_id!="")
         {
           $query_data =$query_data . "   conn_system_user_email = '" . $search_email_id."' and ";
         }
         if($search_smtp_user_name!="")
         {
           $query_data =$query_data . "   conn_smtp_user_name = '" . $search_smtp_user_name."' and ";
         }

         $query_data = substr($query_data, 0, -4);
          $data = \DB::select("select addr.*,mailc.connection_name from mail_address_system_users as addr join mail_connections as mailc on addr.mail_conn_id = mailc.mail_conn_id where addr.enterprise_id = '". Auth::user()->enterprise_id ."' and $query_data");
         if(empty($data))
         {
           return "0";
         }
         else {
           return $data;
         }
  }
  public function autocomplete_system_user_name(Request $request)
  {
           $term = $request->fetch_search_system_user_name;
           $results = array();
           $results;
           $i = 0;
           $query = "select distinct conn_system_user_name from mail_address_system_users where conn_system_user_name LIKE '%$term%' and enterprise_id = '". Auth::user()->enterprise_id ."'";
           $res   = \DB::select($query);
           foreach ($res as $query)
            {
                 array_push($results,$query->conn_system_user_name);
            }
           return $results;
  }

  public function autocomplete_email_id(Request $request)
  {

           $term = $request->fetch_search_email_id_display;
           $results = array();
           $results;
           $i = 0;
           $query = "select distinct conn_system_user_email from mail_address_system_users where conn_system_user_email LIKE '%$term%' and enterprise_id = '". Auth::user()->enterprise_id ."'";
           $res   = \DB::select($query);
           foreach ($res as $query)
            {
                 array_push($results,$query->conn_system_user_email);
            }
           return $results;
  }
  public function autocomplete_smtp_user_name(Request $request)
  {
           $term = $request->fetch_search_smtp_user_name_display;
           $results = array();
           $results;
           $i = 0;
           $query = "select distinct conn_smtp_user_name from mail_address_system_users where conn_smtp_user_name LIKE '%$term%' and enterprise_id = '". Auth::user()->enterprise_id ."'";
           $res   = \DB::select($query);
           foreach ($res as $query)
            {
                 array_push($results,$query->conn_smtp_user_name);
            }
           return $results;
  }
  public function update_system_user(Request $request)
  {
     $data =   \DB::table('mail_address_system_users')
     ->where('system_user_id',  $request->system_user_id)
     ->where('enterprise_id',Auth::user()->enterprise_id)
     ->limit(1)
     ->update(array('conn_system_user_name' => $request->system_user_name,
     'conn_system_user_email' => $request->email_id,
     'conn_smtp_user_name' => $request->smtp_user_name,
     'conn_smtp_pwd' => $request->smtp_password,
     'mail_conn_id' => $request->mail_connection,
     'updated_by' =>   Auth::user()->email));

      return "System User Updated Sucessfully";

  }
  public function delete_system_user(Request $request)
  {
    $system_user_id = $request->system_user_id;

    $data = \DB::select("delete from mail_address_system_users where system_user_id='$system_user_id' and enterprise_id = '". Auth::user()->enterprise_id ."'");

    return "System User Deleted Successfully";
  }

  public function save_ftp_connection(Request $request){
    $connection1 = new \App\ftp_connection;
    $connection1->connection_name = $request->connection_name;
    $connection1->server_type = $request->server_type;
    $connection1->host = $request->host;
    $connection1->port = $request->port;
    $connection1->connection_mode = $request->connection_mode;
    $connection1->transfer_type = $request->transfer_file;
    $connection1->append_mode = $request->mode_of_file;
    $connection1->keep_alive = $request->keep_alive;
    $connection1->lock_file = $request->lock_file;
    $connection1->delete_file = $request->delete_file;
    $connection1->enterprise_id= Auth::user()->enterprise_id;
    $connection1->created_by = Auth::user()->email;
    $connection1->updated_by = Auth::user()->email;

    $connection1-> save();
    return "New FTP Connection added successfully";

  }
  public function get_ftp_connection_details(Request $request){
    $data = \DB::table('ftp_connections')->where('enterprise_id',Auth::user()->enterprise_id)->get();
    return $data;
  }

  public function ftp_connection_search(Request $request){

    $query_data = "";
    $search_connection_name         =  $request->search_connection_name;
    $search_host                     =  $request->search_host;
    $search_server_type              =  $request->search_server_type;

      if($search_connection_name!="")
      {
        $query_data =$query_data . "   connection_name = '" . $search_connection_name."' and ";
      }
      if($search_host!="")
      {
        $query_data =$query_data . "   host = '" . $search_host."' and ";
      }
      if($search_server_type!=""){
        $query_data= $query_data ." server_type ='".$search_server_type."' and ";
      }

      $query_data = substr($query_data, 0, -4);
      $data = \DB::select("select * from ftp_connections where ftp_connections.enterprise_id = '".Auth::user()->enterprise_id. "' and $query_data");
      if(empty($data))
      {
        return "0";
      }
      else {
        return $data;
      }

  }
  public function autocomplete_ftp_connection_name(Request $request)
  {
    $term = $request->fetch_search_connection_name;
    $results = array();
    $results;
    $i = 0;
    $query = "select distinct connection_name from ftp_connections where connection_name LIKE '%$term%' and enterprise_id = '". Auth::user()->enterprise_id ."'";
    $res   = \DB::select($query);
    foreach ($res as $query)
     {
          array_push($results,$query->connection_name);
     }
    return $results;
  }

  public function autocomplete_ftp_host(Request $request)
  {
    $term = $request->fetch_search_host;
    $results = array();
    $results;
    $i = 0;
    $query = "select distinct host from ftp_connections where host LIKE '%$term%' and enterprise_id = '". Auth::user()->enterprise_id ."'";
    $res   = \DB::select($query);
    foreach ($res as $query)
     {
          array_push($results,$query->host);
     }
    return $results;
  }

  public function autocomplete_ftp_server_type(Request $request)
  {
    $term = $request->fetch_search_server_type;
    $results = array();
    $results;
    $i = 0;
    $query = "select distinct server_type from ftp_connections where server_type LIKE '%$term%' and enterprise_id = '". Auth::user()->enterprise_id ."'";
    $res   = \DB::select($query);
    foreach ($res as $query)
     {
          array_push($results,$query->server_type);
     }
    return $results;
  }
  public function update_ftp_connection(Request $request)
  {
     $data =   \DB::table('ftp_connections')
     ->where('ftp_conn_id',  $request->ftp_conn_id)
     ->where('enterprise_id',Auth::user()->enterprise_id)
     ->limit(1)
     ->update(array('connection_name' => $request->connection_name,
     'server_type' => $request->server_type,
     'host' => $request->host,
     'port' => $request->port,
     'connection_mode' => $request->connection_mode,
     'keep_alive' => $request->keep_alive,
     'transfer_type' => $request->transfer_file,
     'append_mode' => $request->mode_of_file,
     'lock_file' => $request->lock_file,
     'delete_file' => $request->delete_file,
     'updated_by' =>   Auth::user()->email));

      return "FTP Connection Updated Sucessfully";

  }
  public function ftp_connection_delete(Request $request){
    $ftp_conn_id = $request->ftp_conn_id;

    $data = \DB::select("delete from ftp_connections where ftp_conn_id='$ftp_conn_id' and enterprise_id = '". Auth::user()->enterprise_id ."'");

    return "FTP Connection Deleted Successfully";
  }

public function get_ftp_connection(Request $request){
  $data = \DB::select("select connection_name,ftp_conn_id from ftp_connections");
  return $data;
}
  public function save_ftp_address(Request $request){
    $addr = new \App\ftp_addresses;
    $addr->address_name = $request->address_name;
    $addr->user_name = $request->user_name;
    $addr->password = $request->pwd;
    $addr->file_mask = $request->file_mask;
    $addr->source_path = $request->source_path;
    $addr->destination_path = $request->dest_path;
    $addr->encoding = $request->encoding;
    $addr->file_name = $request->file_name;
    $addr->ftp_conn_id = $request->ftp_connection_name;
    $addr->enterprise_id= Auth::user()->enterprise_id;
    $addr->created_by = Auth::user()->email;
    $addr->updated_by = Auth::user()->email;

    $addr-> save();
    return "New FTP Address added successfully";
  }

  public function get_ftp_address_details(Request $request){
    $data = \DB::select("select ftp_addresses.*,ftpcn.connection_name, ftpcn.ftp_conn_id from ftp_addresses as ftp_addresses join ftp_connections as ftpcn on ftp_addresses.ftp_conn_id = ftpcn.ftp_conn_id where ftp_addresses.enterprise_id = '" . Auth::user()->enterprise_id ."' ");
    return $data;
  }

  public function update_ftp_addresses(Request $request){
    $data = \DB::table('ftp_addresses')
    ->where('ftp_addr_id', $request->ftp_addr_id)
    ->where('enterprise_id',Auth::user()->enterprise_id)
    ->limit(1)
    ->update(array('address_name' => $request->address_name,
    'user_name' =>$request->user_name,
    'password'=> $request->pwd,
    'file_mask' => $request->file_mask,
    'source_path' => $request->source_path,
    'destination_path' => $request->dest_path,
    'encoding' => $request->encoding,
     'file_name' => $request->file_name,
    'ftp_conn_id' => $request->ftp_connection_name,
    'updated_by' =>   Auth::user()->email));

    return "FTP Address Updated Successfully";
  }

  public function ftp_address_delete(Request $request){
    $ftp_addr_id = $request->ftp_addr_id;
    $data = \DB::select("delete from ftp_addresses where ftp_addr_id = '$ftp_addr_id' and enterprise_id = '". Auth::user()->enterprise_id ."' ");
    return "FTP Address Deleted Successfully";
  }

  public function get_connection_for_address(Request $request){
    $data = \DB::select("select connection_name,ftp_conn_id from ftp_connections where enterprise_id = '". Auth::user()->enterprise_id ."'");
    return $data;
  }

  public function ftp_address_search(Request $request){

    $query_data = "";
    $search_address_name            =  $request->search_address_name;
    $search_user_name               =  $request->search_user_name;

      if($search_address_name!="")
      {
        $query_data =$query_data . "   address_name = '" . $search_address_name."' and ";
      }
      if($search_user_name!="")
      {
        $query_data =$query_data . "   user_name = '" . $search_user_name."' and ";
      }

      $query_data = substr($query_data, 0, -4);
      $data = \DB::select("select ftp_addresses.*,ftpcn.connection_name, ftpcn.ftp_conn_id from ftp_addresses as ftp_addresses join ftp_connections as ftpcn on ftp_addresses.ftp_conn_id = ftpcn.ftp_conn_id where ftp_addresses.enterprise_id = '" . Auth::user()->enterprise_id ."' and $query_data");
      if(empty($data))
      {
        return "0";
      }
      else {
        return $data;
      }

  }
  public function autocomplete_address_name(Request $request)
  {
    $term = $request->fetch_search_address_name;
    $results = array();
    $results;
    $i = 0;
    $query = "select distinct address_name from ftp_addresses where address_name LIKE '%$term%' and enterprise_id = '". Auth::user()->enterprise_id ."'";
    $res   = \DB::select($query);
    foreach ($res as $query)
     {
          array_push($results,$query->address_name);
     }
    return $results;
  }

  public function autocomplete_search_user_name(Request $request)
  {
    $term = $request->fetch_search_user_name;
    $results = array();
    $results;
    $i = 0;
    $query = "select distinct user_name from ftp_addresses where user_name LIKE '%$term%' and enterprise_id = '". Auth::user()->enterprise_id ."'";
    $res   = \DB::select($query);
    foreach ($res as $query)
     {
          array_push($results,$query->user_name);
     }
    return $results;
  }

  public function get_enterprise_user_group(Request $request){
    $data = \DB::select("select * from mail_enterprise_user_groups where enterprise_id = '". Auth::user()->enterprise_id ."'");
    return $data;
  }

  public function user_group_insert(Request $request){
    $userGroup = new \App\mail_enterprise_user_group;
    $userGroup->ent_user_group_name = $request->user_group_name;
    $userGroup->ent_user_group_id = $request->user_group_id;

    $userGroup->enterprise_id= Auth::user()->enterprise_id;
    $userGroup->created_by = Auth::user()->email;
    $userGroup->updated_by = Auth::user()->email;

    $userGroup-> save();
    return $userGroup;
  }

public function user_insert(Request $request){
  $user = new \App\mail_enterprise_user;
  $user->ent_user_id = $request->ent_email_id;
  $user->ent_user_name = $request->ent_user_name;
  $user->user_group_id = $request->ent_user_group_id;

  $user->enterprise_id= Auth::user()->enterprise_id;
  $user->created_by = Auth::user()->email;
  $user->updated_by = Auth::user()->email;

  $user-> save();
  return "New Enterprise User added successfully";
}

public function user_results(Request $request){
  $data = \DB::select("select mail_enterprise_users.*,eug.ent_user_group_name,eug.ent_user_group_id from mail_enterprise_users as mail_enterprise_users left outer join mail_enterprise_user_groups as eug on eug.user_group_id = mail_enterprise_users.user_group_id where mail_enterprise_users.enterprise_id = '". Auth::user()->enterprise_id ."'");
  return $data;
}

public function update_enterprise_user(Request $request){

  $data = \DB::table('mail_enterprise_users')
  ->where('mail_comm_id', $request->mail_comm_id)
  ->where('enterprise_id',Auth::user()->enterprise_id)
  ->limit(1)
  ->update(array('ent_user_name' => $request->ent_user_name,
  'ent_user_id' =>$request->ent_email_id,
  'user_group_id'=> $request->ent_user_group_id,
  'updated_by' =>   Auth::user()->email));

  return "Enterprise User Updated Successfully";
}

public function usergroup_results(Request $request){
  $variant = \DB::select("select user_group_id,ent_user_group_name from mail_enterprise_user_groups");
  return $variant;
}

public function delete_enterprise_user(Request $request){
  $mail_comm_id = $request->mail_comm_id;

  $data = "select mail_address from distribution_strategy where mail_address = '$mail_comm_id' and enterprise_id = '". Auth::user()->enterprise_id ."' ";
  $res   = \DB::select($data);
  if(!($res))
  {
    $data = \DB::select("delete from mail_enterprise_users where mail_comm_id = '$mail_comm_id' and enterprise_id = '". Auth::user()->enterprise_id ."' ");
    return "Enterprise User Deleted Successfully";
  }
  else {
    return "0";
  }

}

public function enterprise_user_search(Request $request){

  $query_data = "";
  $search_ent_user_name            =  $request->search_ent_user_name;
  $search_ent_email_id             =  $request->search_ent_email_id;
  $search_ent_user_group_id        =  $request->search_ent_user_group_id;

    if($search_ent_user_name!="")
    {
      $query_data =$query_data . "  ent_user_name  = '" . $search_ent_user_name."' and ";
    }
    if($search_ent_email_id!="")
    {
      $query_data =$query_data . "   ent_user_id = '" . $search_ent_email_id."' and ";
    }
    if($search_ent_user_group_id!=""){
      $search_ent_user_group = \DB::select("select `user_group_id` FROM mail_enterprise_user_groups where ent_user_group_name = '".$search_ent_user_group_id."' ");
      if (empty($search_ent_user_group))
       {
          return "0";
       }
       else
       {
           $query_data = " eug.`user_group_id` = '". $search_ent_user_group[0]->user_group_id . "' and ";
       }

    }

    $query_data = substr($query_data, 0, -4);
    $data = \DB::select("select mail_enterprise_users.*,eug.ent_user_group_name, eug.user_group_id from mail_enterprise_users as mail_enterprise_users left outer join mail_enterprise_user_groups as eug on mail_enterprise_users.user_group_id = eug.user_group_id where  mail_enterprise_users.enterprise_id = '" . Auth::user()->enterprise_id ."' and $query_data");
    if(empty($data))
    {
      return "0";
    }
    else {
      return $data;
    }

}
public function autocomplete_search_ent_user_name(Request $request)
{
  $term = $request->fetch_search_ent_user_name;
  $results = array();
  $results;
  $i = 0;
  $query = "select distinct ent_user_name from mail_enterprise_users where ent_user_name LIKE '%$term%' and enterprise_id = '". Auth::user()->enterprise_id ."'";
  $res   = \DB::select($query);
  foreach ($res as $query)
   {
        array_push($results,$query->ent_user_name);
   }
  return $results;
}

public function autocomplete_search_ent_email_id(Request $request)
{
  $term = $request->fetch_search_ent_email_id;
  $results = array();
  $results;
  $i = 0;
  $query = "select distinct ent_user_id from mail_enterprise_users where ent_user_id LIKE '%$term%' and enterprise_id = '". Auth::user()->enterprise_id ."'";
  $res   = \DB::select($query);
  foreach ($res as $query)
   {
        array_push($results,$query->ent_user_id);
   }
  return $results;
}

public function autocomplete_search_ent_user_group_id(Request $request)
{
  $term = $request->fetch_search_ent_user_group_id;
  $results = array();
  $results;
  $i = 0;
  $query = "select distinct ent_user_group_name from mail_enterprise_user_groups where ent_user_group_name LIKE '%$term%' and enterprise_id = '". Auth::user()->enterprise_id ."'";
  $res   = \DB::select($query);
  foreach ($res as $query)
   {
        array_push($results,$query->ent_user_group_name);
   }
  return $results;
}

}
