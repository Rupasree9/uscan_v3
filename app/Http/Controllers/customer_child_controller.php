<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\customer_child;
use App\contact;
use Auth;

class customer_child_controller extends Controller
{

      public function __construct()
      {
          // $this->middleware(ProductAdmin::class);
      }
      /**
      * Display a listing of the resource.
      *
      * @return Response
      */
       public function showall()
       {

        //  $data = \DB::select("select c.`customer_id`,c.`customer_name`,c.`customer_code`,c.`parent_id`,c.`customer_type`,c.`duns_no`,c.`active`,c.`customer_plant`,c.`created_at`,c.`created_by`,c.`updated_by`,c.`updated_at`,co.`contact_name`,co.`contact_mail_id`,co.`contact_number`,cp.`parent_company_name` FROM customer_child c,customer_contact_detail co ,customer_parent cp where c.`customer_id` = co.`customer_id` and cp.`parent_id` = c.`parent_id`;");
        //  return  $data;
         $data = \DB::table('customer_child')
         ->select(
           'active',
           'contact_id',
           'customer_child.created_at',
           'customer_child.created_by',
           'customer_code',
           'customer_child.customer_id',
           'customer_name',
           'customer_type',
           'customer_child.customer_type_id',
           'duns_no',
           'customer_child.enterprise_id',
           'parent_code',
           'customer_child.parent_id',
           'parent_name',
           'primary_contact_email_id',
           'primary_contact_name',
           'primary_contact_number',
           'secondary_contact_email_id',
           'secondary_contact_name',
           'secondary_contact_number',
           'customer_child.updated_at',
           'customer_child.updated_by')
        ->leftJoin('customer_contact', 'customer_child.customer_id', '=', 'customer_contact.customer_id')
        ->leftJoin('customer_parent', 'customer_child.parent_id', '=', 'customer_parent.parent_id')
        ->leftJoin('customer_type', 'customer_child.customer_type_id', '=', 'customer_type.customer_type_id')
        ->where('customer_child.enterprise_id',Auth::user()->enterprise_id)
        ->where('customer_child.customer_name','<>',"ALL Customers")
        ->where('customer_child.deleted_at',NULL)
        ->orderBy('customer_child.customer_name', 'asc')
         ->get();
        return $data;


       }

       public function get_customer(Request $request)
       {
         $cust_name=  \DB::table('customer_child')->where('active','=', '1')
         ->where('customer_child.enterprise_id',Auth::user()->enterprise_id)
         ->where('customer_child.customer_name','<>',"ALL Customers")
         ->where('customer_child.deleted_at',NULL)
         ->get();
          return $cust_name;
       }

       public function validateCustomerCreate(Request $request)
       {
         $cust_name=  \DB::table('customer_child')->where('active','=', '1')
         ->where('customer_child.enterprise_id',Auth::user()->enterprise_id)
         ->where('customer_child.customer_name',$request->customer_name)
         ->where('customer_child.deleted_at',NULL)
         ->get();
          // return $cust_name;
          if (count($cust_name) > 0)
            {
              return "false";
            }
            else {
              return "true";
            }


       }

       public function validateCustomerChange(Request $request)
       {

         if(strtoupper($request->customer_name) == strtoupper($request->customer_name_valid))
         {
           return "true";
         }
         else {
           # code...

         $cust_name=  \DB::table('customer_child')->where('active','=', '1')
         ->where('customer_child.enterprise_id',Auth::user()->enterprise_id)
         ->where('customer_child.customer_name',$request->customer_name)
         ->where('customer_child.deleted_at',NULL)
         ->get();
          // return $cust_name;
          if (count($cust_name) > 0)
            {
              return "false";
            }
            else {
              return "true";
            }

          }


       }


       /**
        * Show the form for creating a new resource.
        *
        * @return Response
        */
       public function create()
       {

       }

       /**
        * Store a newly created resource in storage.
        *
        * @return Response
        */
        public function store(Request $request)
        {

           

          $this->validate($request, [
             'parent_company_name_id' => 'bail|required',
             'customer_name' => 'required',
             'customer_type' => 'required',
         ]);


            $query = "select current_no from number_range where table_name = 'customer_child'and enterprise_id = '". Auth::user()->enterprise_id ."'";
            $result = \DB::select($query);
            $customer_id = $result[0]->current_no + 1;

            $update_current_no =   \DB::table('number_range')
            ->where('table_name',  'customer_child')
            ->where('enterprise_id',Auth::user()->enterprise_id)
            ->limit(1)
            ->update(array('current_no' => $customer_id));

            $customer_child = new \App\customer_child_model;
            $customer_child->customer_id         = $customer_id;
            $customer_child->parent_id           = $request->parent_company_name_id;
            $customer_child->customer_name       = $request->customer_name;
            $customer_child->customer_code       = $request->customer_code;
            $customer_child->duns_no             = $request->duns_no;
            $customer_child->customer_type_id    = $request->customer_type;
            $customer_child->active              = $request->customer_active;
            $customer_child->created_by          = Auth::user()->email;
            $customer_child->updated_by          = Auth::user()->email;
            $customer_child->enterprise_id       = Auth::user()->enterprise_id;
            $customer_child->save();

            // $insertedId = $customer_child->id;

            $contact = new  \App\customer_contact_model;
            $contact->customer_id = $customer_id;
            $contact->primary_contact_name = $request->p_contact_name;
            $contact->primary_contact_email_id = $request->p_contact_email;
            $contact->primary_contact_number = $request->p_contact_number;
            $contact->secondary_contact_name = $request->s_contact_name;
            $contact->secondary_contact_email_id = $request->s_contact_email;
            $contact->secondary_contact_number = $request->s_contact_number;
            $contact->enterprise_id = Auth::user()->enterprise_id;
            $contact->save();

            return "Customer created sucessfully";

        }


       /**
        * Display the specified resource.
        *
        * @param  int  $id
        * @return Response
        */
       public function show(Request $request)
       {
            $query_data = "";
            $search_parent_company_name_id =  $request->search_parent_company_name_id;
            $search_customer_name_id       =  $request->search_customer_name_id;
            $search_customer_code_id       =  $request->search_customer_code_id;
            $search_duns_number_id         =  $request->search_duns_number_id;

            if ($search_parent_company_name_id  != "")
            {
              $parent_id_search = \DB::select("select `parent_id` FROM customer_parent where parent_name = '".$search_parent_company_name_id."' ");
              if (empty($parent_id_search))
              {
                 return "0";
              }
              else {
                # code...
                  $query_data = " customer_parent.`parent_id` = '". $parent_id_search[0]->parent_id . "' and ";
                 }

            }

              if($search_customer_name_id!="")
              {
                $query_data =$query_data . " customer_name = '" . $search_customer_name_id."' and ";
              }


              //$query_data = "customer_parent.`parent_name` <> 'ALL Customers' and ";




              if($search_customer_code_id!="")
              {
                $query_data =$query_data . "   customer_code = '" . $search_customer_code_id."' and ";
              }
              if($search_duns_number_id!="")
              {
                $query_data =$query_data . "  duns_no = '" . $search_duns_number_id."' and ";
              }

              $query_data = substr($query_data, 0, -4);


              // $data = \DB::select("select c.`customer_id`,c.`customer_name`,c.`customer_code`,c.`parent_id`,ct.`customer_type`,ct.`customer_type_id`,c.`duns_no`,c.`active`,c.`created_at`,c.`created_by`,c.`updated_by`,c.`updated_at`,co.`primary_contact_name`,co.`primary_contact_email_id`,co.`primary_contact_number`,co.`secondary_contact_name`,co.`secondary_contact_email_id`,co.`secondary_contact_number`,cp.`parent_name` FROM customer_child ,customer_contact  ,customer_parent ,customer_type  where c.`customer_id` = co.`customer_id` and ct.customer_type_id = c.customer_type_id and c.`parent_id` = cp.`parent_id` and $query_data");

              $data = \DB::select("select
              active,
           contact_id,
           customer_child.created_at,
           customer_child.created_by,
           customer_code,
           customer_child.customer_id,
           customer_name,
           customer_type,
           customer_child.customer_type_id,
           duns_no,
           customer_child.enterprise_id,
           parent_code,
           customer_child.parent_id,
           parent_name,
           primary_contact_email_id,
           primary_contact_name,
           primary_contact_number,
           secondary_contact_email_id,
           secondary_contact_name,
           secondary_contact_number,
           customer_child.updated_at,
           customer_child.updated_by from customer_child ,customer_contact  ,customer_parent ,customer_type where  customer_child.`customer_id` = customer_contact.`customer_id` and customer_type.customer_type_id = customer_child.customer_type_id and customer_child.`parent_id` = customer_parent.`parent_id` and customer_child.deleted_at IS NULL and customer_contact.deleted_at IS NULL and customer_child.enterprise_id = '".Auth::user()->enterprise_id. "' and customer_name != 'ALL Customers' and $query_data");

              if(empty($data))
              {
                return "0";
              }
              else {
                return $data;
              }


       }

       /**
        * Show the form for editing the specified resource.
        *
        * @param  int  $id
        * @return Response
        */
       public function edit($id)
       {

       }

       public function autocomplete_customer_name(Request $request)
       {

                $term = $request->search_customer_name_id;
              	$results = array();
                $results;
                $i = 0;
                $query = "select  distinct customer_name from customer_child where customer_name LIKE '%$term%' and customer_name != 'ALL Customers' and deleted_at IS NULL and enterprise_id = '". Auth::user()->enterprise_id ."'";
                $res   = \DB::select($query);
                foreach ($res as $query)
                	{
                      array_push($results,$query->customer_name);
                	}
                return $results;
       }

       public function autocomplete_customer_code(Request $request)
       {

                $term = $request->search_customer_code_id;
                $results = array();
                $results;
                $i = 0;
                $query = "select distinct customer_code from customer_child where customer_code LIKE '%$term%' and customer_code != 'all' and deleted_at IS NULL and enterprise_id = '". Auth::user()->enterprise_id ."'";
                $res   = \DB::select($query);
                foreach ($res as $query)
                 {
                      array_push($results,$query->customer_code);
                 }
                return $results;
       }

       public function autocomplete_duns_no(Request $request)
       {

                $term = $request->search_duns_number_id;
                $results = array();
                $results;
                $i = 0;
                $query = "select distinct duns_no from customer_child where duns_no LIKE '%$term%' and deleted_at IS NULL and enterprise_id = '". Auth::user()->enterprise_id ."'";
                $res   = \DB::select($query);
                foreach ($res as $query)
                 {
                      array_push($results,$query->duns_no);
                 }
                return $results;
       }

       public function autocomplete_parent_name(Request $request)
       {

                $term = $request->search_parent_company_name_id;
                $results = array();
                $results;
                $i = 0;
                $query = "select distinct parent_name from customer_parent where parent_name LIKE '%$term%' and deleted_at IS NULL and parent_name != 'ALL Customers' and enterprise_id = '". Auth::user()->enterprise_id ."'";
                $res   = \DB::select($query);
                foreach ($res as $query)
                 {
                      array_push($results,$query->parent_name);
                 }
                return $results;
       }

       public function autocomplete_parent_code(Request $request)
       {

                $term = $request->search_parent_company_code_id;
                $results = array();
                $results;
                $i = 0;
                $query = "select distinct parent_code from customer_parent where parent_code LIKE '%$term%' and parent_code != 'All' and deleted_at IS NULL and  enterprise_id = '". Auth::user()->enterprise_id ."'";
                $res   = \DB::select($query);
                foreach ($res as $query)
                 {
                      array_push($results,$query->parent_code);
                 }
                return $results;
       }

       /**
        * Update the specified resource in storage.
        *
        * @param  int  $id
        * @return Response
        */
       public function update(Request $request)
       {
           $this->validate($request, [
              'parent_company_name_id' => 'bail|required',
              'customer_name' => 'required',
              'customer_type' => 'required',

          ]);

          $updated_at = date('Y-m-d H:i:s', time());
          $data =   \DB::table('customer_child')
          ->where('customer_id',  $request->customer_id)
          ->where('customer_child.enterprise_id',Auth::user()->enterprise_id)
          ->where('customer_child.deleted_at',NULL)

          ->limit(1)
          ->update(array('parent_id' => $request->parent_company_name_id,
          'updated_at' => $updated_at,
          'customer_name' => $request->customer_name,
          'customer_code' => $request->customer_code,
          'duns_no' => $request->duns_no,
          'customer_type_id' => $request->customer_type,
          // 'customer_plant' => $request->customer_plants,
          'active' => $request->customer_active,
          'updated_by' =>   Auth::user()->email));

          $data =   \DB::table('customer_contact')
          ->where('customer_id',  $request->customer_id)
          ->where('enterprise_id',Auth::user()->enterprise_id)
          ->where('customer_contact.deleted_at',NULL)
          ->limit(1)
          ->update(array('primary_contact_name' => $request->p_contact_name,
          'primary_contact_email_id' => $request->p_contact_email,
          'primary_contact_number' => $request->p_contact_number,
          'secondary_contact_name' => $request->s_contact_name,
          'secondary_contact_email_id' => $request->s_contact_email,
          'secondary_contact_number' => $request->s_contact_number));

         return "Customer updated sucessfully";



       }

       /**
        * Remove the specified resource from storage.
        *
        * @param  int  $id
        * @return Response
        */
       public function destroy(Request $request)
       {
         $customer_id = $request->customer_id;

         $deletedRows_customer_child = \App\customer_child_model::where('customer_id', $customer_id)->where('enterprise_id', Auth::user()->enterprise_id)->delete();
         $deletedRows_contact = \App\customer_contact_model::where('customer_id', $customer_id)->where('enterprise_id', Auth::user()->enterprise_id)->delete();

        //  $data = \DB::select("delete from customer_contact where customer_id='$customer_id' and enterprise_id = '". Auth::user()->enterprise_id ."'");
        //  $data = \DB::select("delete from customer_child where customer_id='$customer_id' and enterprise_id = '". Auth::user()->enterprise_id ."'");
         return "Customer deleted sucessfully";

       }

}
