<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class monitor_controller extends Controller
{

  public function get_process_monitor_details(Request $request){
    $data = \DB::select("select tk.task_name, dt.* FROM task AS tk RIGHT JOIN (
select pm.*,rg.variant AS variant FROM process_monitor AS pm LEFT JOIN report_generators AS rg ON pm.reference_id = rg.report_id ) AS dt ON tk.task_id = reference_id order by log_id desc");
    return $data;
  }

  public function get_process_monitor_description(Request $request){

    $log_id = $request->log_id;
    $data =\DB::select("select * from process_monitor_descs where log_id = '$log_id' ORDER BY date_time ASC ");
    return $data;
  }

}
