<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Auth;

class gm_report_controller extends Controller
{
    //
    public function index()
    {

    }
    public function show()
    {
      $data = DB::select("Select business_unit,product_group from plant");
      if (empty($data)) {
          return "0";
      }
      else {
        return $data;
      }
    }

    public function get_gmreport_master()
    {
      $db_connection = DB::connection('mysql_transaction');
      $data = $db_connection->select("Select quality,supplychain,cca,name,location,mfg_duns from gm_historical_bidlist");
      //insert_logger("1001","1001","1001","active","running","Executing db query","2017-01-24 12:00:00","2017-01-24 12:01:34");
      if (empty($data)) {
          return "0";
      }
      else {
        return $data;
      }
    }

    public function gmreport_master(Request $request)
    {
      $plant_id = $request->plant_id;
      $plant_ids = str_replace(",","','",$plant_id);
      $business_unit = $request->business_unit;
      $business_units = str_replace(",","','",$business_unit);
      $product_group = $request->product_group;
      $product_groups = str_replace(",","','",$product_group);
      $cust_id = $request->cust_id;
      $cust_name = $request->cust_name;
      $cust_ids = str_replace(",","','",$cust_id);
      $plant_status = $request->plant_status;
      // $fp1 = fopen(dirname(__FILE__).'/log.txt', 'w+');
      // fwrite($fp1,$plant_ids);
      // fwrite($fp1,"\r\n");
      // fwrite($fp1,$business_units);
      // fwrite($fp1,"\r\n");
      // fwrite($fp1,$product_groups);
      // fwrite($fp1,"\r\n");
      // fwrite($fp1,$cust_name);
      // fwrite($fp1,"\r\n");
      // fwrite($fp1,$cust_ids);
      // fwrite($fp1,"\r\n");
      // fwrite($fp1,$plant_status);
      // fwrite($fp1,"\r\n");
      $query_data = "";
      if($business_units != ""){
        if($query_data != "")
        {
            $query_data = $query_data. " and p.business_unit IN('".$business_units."')";
        }
        else {
            $query_data = $query_data. " p.business_unit IN('".$business_units."')";
        }
      }
      if($product_groups != ""){
        if($query_data != "")
        {
            $query_data = $query_data. " and p.product_group IN('".$product_groups."')";
        }
        else {
            $query_data = $query_data. " p.product_group IN('".$product_groups."')";
        }
      }
      if($plant_ids != ""){
        if($query_data != "")
        {
          $query_data = $query_data. " and p.plant_id IN('".$plant_ids."')";
        }
        else {
          $query_data = $query_data. " p.plant_id IN('".$plant_ids."')";
        }
      }
      if($cust_ids != ""){
        if($query_data != "")
        {
          $query_data = $query_data. " and cc.customer_id IN ('".$cust_ids."')";
        }
        else {
          $query_data = $query_data. " cc.customer_id IN ('".$cust_ids."')";
        }

      }
      //$query = "SELECT p.duns_no,p.plant_name,pc.supplier_code FROM plant p JOIN plant_customer_ref pc ON p.plant_id =  pc.plant_id JOIN customer_child cc ON pc.customer_id = cc.customer_id WHERE p.`plant_name` IN('".$plant_names."') OR p.business_unit IN('".$business_units."') OR p.product_group IN('".$product_groups."') OR cc.customer_name IN ('".$cust_ids."')";
      $query = "SELECT p.duns_no,p.plant_name,pc.supplier_code,pc.customer_id
      FROM plant p JOIN plant_customer_ref pc ON p.plant_id =  pc.plant_id
      JOIN customer_child cc ON pc.customer_id = cc.customer_id WHERE ".$query_data;
      $result = \DB::select($query);
      // fwrite($fp1,$query);
      $mfg_duns = "";
      $supplier_code = "";
      $customer_id = "";
      for($i=0;$i<count($result);$i++)
      {
        if($result[$i]->supplier_code != $result[$i]->duns_no)
        {
          if($supplier_code == "")
          {
            $supplier_code = "'".$result[$i]->supplier_code."'";
          }
          else
          {
            $supplier_code = $supplier_code.",'".$result[$i]->supplier_code."'";
          }
        }
        else {
          if($mfg_duns == "")
          {
             $mfg_duns = "'".$result[$i]->duns_no."'";
          }
          else {
             $mfg_duns = $mfg_duns.",'". $result[$i]->duns_no."'";
          }
        }
        if($customer_id == "")
        {
           $customer_id = "'".$result[$i]->customer_id."'";
        }
        else {
           $customer_id = $customer_id.",'". $result[$i]->customer_id."'";
        }
      }
      // fwrite($fp1,"\r\n");
      // fwrite($fp1,"supplier_code".$supplier_code);
      // fwrite($fp1,"\r\n");
      // fwrite($fp1,$mfg_duns);
      // fwrite($fp1,"\r\n");
      // fwrite($fp1,$customer_id);
      // fwrite($fp1,"\r\n");
      // fwrite($fp1,$query);
      // fwrite($fp1,"\r\n");
      $db_connection = DB::connection('mysql_transaction');
      if($cust_name == "General Motor Corporation")
      {
        //$gm_report_query = "SELECT p.duns_no,p.plant_name,pc.supplier_code,cc.customer_name,gm.quality,gm.supplychain,gm.cca FROM uscan_master.plant p JOIN uscan_master.plant_customer_ref pc ON p.plant_id =  pc.plant_id JOIN uscan_master.customer_child cc ON pc.customer_id = cc.customer_id JOIN uscan_transactions.gm_historical_bidlist gm ON p.duns_no = gm.mfg_duns WHERE p.duns_no in(".$mfg_duns.") and p.duns_no = pc.supplier_code";
        $gm_plant_status_qry = "";
        if($plant_status == "Red"){
        //fwrite($fp1,"Inside Red Block"."\r\n");
          if($gm_plant_status_qry == "")
          {
            $gm_plant_status_qry = " AND (gm.quality != 0 AND gm.supplychain != 'NR') AND
            (gm.cca = 'R' OR gm.quality < 80 OR gm.supplychain < 75)";
          }
          else {
            $gm_plant_status_qry = $gm_plant_status_qry. " AND (gm.quality != 0 AND gm.supplychain != 'NR') AND
            (gm.cca = 'R' OR gm.quality < 80 OR gm.supplychain < 75)";
          }
        }
        elseif($plant_status == "Green") {
          // fwrite($fp1,"Inside Green Block"."\r\n");
          if($gm_plant_status_qry == "")
          {
            $gm_plant_status_qry = " AND (gm.quality != 0 AND gm.supplychain != 'NR') AND
            (gm.cca = 'G' AND gm.quality >= 80 AND gm.supplychain >= 85)";
          }
          else {
            $gm_plant_status_qry = $gm_plant_status_qry. " AND (gm.quality != 0 AND gm.supplychain != 'NR') AND
            (gm.cca = 'G' AND gm.quality >= 80 AND gm.supplychain >= 85)";
          }
        }
        elseif($plant_status == "Yellow") {
          // fwrite($fp1,"Inside Yellow Block"."\r\n");
          if($gm_plant_status_qry == "")
          {
            $gm_plant_status_qry = " AND (gm.quality != 0 AND gm.supplychain != 'NR') AND
            (gm.cca = 'G' AND gm.quality >= 80 AND gm.supplychain >= 85)";
          }
          else {
            $gm_plant_status_qry = $gm_plant_status_qry. " AND (gm.quality != 0 AND gm.supplychain != 'NR') AND
            (gm.cca = 'G' AND gm.quality >= 80 AND gm.supplychain >= 85)";
          }
        }
        elseif($plant_status == "Green,Red" || $plant_status == "Red,Green") {
          //fwrite($fp1,"Inside Green,Red Block"."\r\n");
          if($gm_plant_status_qry == "")
          {
            $gm_plant_status_qry = " AND (gm.quality != 0 AND gm.supplychain != 'NR') AND
            ((gm.cca = 'R' OR gm.quality < 80 OR gm.supplychain < 75) OR (gm.cca = 'G' AND gm.quality >= 80 AND gm.supplychain >= 85))";
          }
          else {
            $gm_plant_status_qry = $gm_plant_status_qry. " AND (gm.quality != 0 AND gm.supplychain != 'NR') AND
            ((gm.cca = 'R' OR gm.quality < 80 OR gm.supplychain < 75) OR (gm.cca = 'G' AND gm.quality >= 80 AND gm.supplychain >= 85))";
          }
        }
        elseif ($plant_status == "Red,Yellow" || $plant_status == "Yellow,Red") {
          //fwrite($fp1,"Inside Yellow,Red Block"."\r\n");
          if($gm_plant_status_qry == "")
          {
            $gm_plant_status_qry = " AND (gm.quality != 0 AND gm.supplychain != 'NR') AND
            ((gm.cca = 'Y' AND gm.quality >= 80 AND (gm.supplychain >= 75 AND gm.supplychain < 85)) OR (gm.cca = 'R' OR gm.quality < 80 OR gm.supplychain < 75))";
          }
          else {
            $gm_plant_status_qry = $gm_plant_status_qry. " AND (gm.quality != 0 AND gm.supplychain != 'NR') AND
            ((gm.cca = 'Y' AND gm.quality >= 80 AND (gm.supplychain >= 75 AND gm.supplychain < 85)) OR (gm.cca = 'R' OR gm.quality < 80 OR gm.supplychain < 75))";
          }
        }
        elseif ($plant_status == "Green,Yellow" || $plant_status == "Yellow,Green") {
          //fwrite($fp1,"Inside Green,Yellow Block"."\r\n");
          if($gm_plant_status_qry == "")
          {
            $gm_plant_status_qry = " AND (gm.quality != 0 AND gm.supplychain != 'NR') AND
            ((gm.cca = 'Y' AND gm.quality >= 80 AND (gm.supplychain >= 75 AND gm.supplychain < 85)) OR (gm.cca = 'G' AND gm.quality >= 80 AND gm.supplychain >= 85))";
          }
          else {
            $gm_plant_status_qry = $gm_plant_status_qry. " AND (gm.quality != 0 AND gm.supplychain != 'NR') AND
            ((gm.cca = 'Y' AND gm.quality >= 80 AND (gm.supplychain >= 75 AND gm.supplychain < 85)) OR (gm.cca = 'G' AND gm.quality >= 80 AND gm.supplychain >= 85))";
          }
        }
        if($plant_status == "" || $plant_status == "Red,Green,Yellow")
        {
          //fwrite($fp1,"Inside plant_status empty Block"."\r\n");
          $gm_report_query ="SELECT p.duns_no,p.plant_name,pc.supplier_code,cc.customer_name,gm.quality,gm.supplychain,gm.cca,
          gm.transaction_date
          FROM uscan_master.plant p
          JOIN uscan_master.plant_customer_ref pc ON p.plant_id = pc.plant_id
          JOIN uscan_master.customer_child cc ON pc.customer_id = cc.customer_id
          JOIN uscan_transactions.gm_historical_bidlist gm ON p.duns_no = gm.mfg_duns COLLATE utf8_unicode_ci
          WHERE p.duns_no IN(".$mfg_duns.") AND
          pc.customer_id IN (".$customer_id.") AND
          p.duns_no = pc.supplier_code AND
          gm.transaction_date = (SELECT MAX(transaction_date) AS transaction_date FROM uscan_transactions.gm_historical_bidlist)
          ORDER BY p.plant_name ASC";
        }
        else {
          //fwrite($fp1,"Inside plant_status non empty Block"."\r\n");
          $gm_report_query ="SELECT p.duns_no,p.plant_name,pc.supplier_code,cc.customer_name,gm.quality,gm.supplychain,gm.cca,
          gm.transaction_date
          FROM uscan_master.plant p
          JOIN uscan_master.plant_customer_ref pc ON p.plant_id = pc.plant_id
          JOIN uscan_master.customer_child cc ON pc.customer_id = cc.customer_id
          JOIN uscan_transactions.gm_historical_bidlist gm ON p.duns_no = gm.mfg_duns COLLATE utf8_unicode_ci
          WHERE p.duns_no IN(".$mfg_duns.") AND
          pc.customer_id IN (".$customer_id.") AND
          p.duns_no = pc.supplier_code AND
          gm.transaction_date = (SELECT MAX(transaction_date) AS transaction_date FROM uscan_transactions.gm_historical_bidlist)";

          $gm_report_query = $gm_report_query. $gm_plant_status_qry. " ORDER BY p.plant_name ASC";
        }

        // fwrite($fp1,$gm_report_query);
        $gm_report_data = $db_connection->select($gm_report_query);
        $gm_performance_criteria_query = "Select * from gm_performance_criteria_bidlist";
        $gm_performance_criteria = $db_connection->select($gm_performance_criteria_query);
        if (empty($gm_report_data)) {
            return "0";
        }
        else {
          return array($gm_report_data,$gm_performance_criteria,$cust_name,$gm_report_query);
      }
      }
      elseif ($cust_name == "FCA Mopar" || $cust_name == "FCA Production") {

        $fca_data = "";
        if($cust_name == "FCA Production")
        {
          //$fca_prdn_query = "SELECT p.plant_name,pc.supplier_code,fca.overall FROM uscan_master.plant p JOIN uscan_master.plant_customer_ref pc ON p.plant_id = pc.plant_id JOIN uscan_transactions.fca_supplier_scorecard_mopar fca ON pc.supplier_code = fca.code COLLATE utf8_unicode_ci WHERE pc.supplier_code IN(".$supplier_code.")";

          $fca_prdn_query = "SELECT p.plant_name,pc.supplier_code,cc.customer_name,fca.overall,fca.transaction_date
          FROM uscan_master.plant p
          JOIN uscan_master.plant_customer_ref pc ON p.plant_id = pc.plant_id
          JOIN uscan_master.customer_child cc ON pc.customer_id = cc.customer_id
          JOIN uscan_transactions.fca_supplier_scorecard_production fca ON pc.supplier_code = fca.code COLLATE utf8_unicode_ci
          WHERE pc.supplier_code IN (".$supplier_code.")
          AND pc.customer_id IN (".$customer_id.") AND
          fca.transaction_date = (SELECT MAX(transaction_date) AS transaction_date FROM uscan_transactions.fca_supplier_scorecard_production)
          ORDER BY p.plant_name ASC";

          $fca_data = $db_connection->select($fca_prdn_query);
          // fwrite($fp1,"\n");
          // fwrite($fp1,$fca_prdn_query);
        }
        else {
          //$fca_mopar_query = "SELECT p.plant_name,pc.supplier_code,cc.customer_name,fca.overall FROM uscan_master.plant p JOIN uscan_master.plant_customer_ref pc ON p.plant_id = pc.plant_id JOIN uscan_master.customer_child cc ON pc.customer_id = cc.customer_id JOIN uscan_transactions.fca_supplier_scorecard_production fca ON pc.supplier_code = fca.code COLLATE utf8_unicode_ci WHERE pc.supplier_code IN(".$supplier_code.")";

          $fca_mopar_query = "SELECT p.plant_name,pc.supplier_code,cc.customer_name,fca.overall,fca.transaction_date
          FROM uscan_master.plant p
          JOIN uscan_master.plant_customer_ref pc ON p.plant_id = pc.plant_id
          JOIN uscan_master.customer_child cc ON pc.customer_id = cc.customer_id
          JOIN uscan_transactions.fca_supplier_scorecard_mopar fca ON pc.supplier_code = fca.code COLLATE utf8_unicode_ci
          WHERE pc.supplier_code IN (".$supplier_code.") AND
          pc.customer_id IN (".$customer_id.") AND
          fca.transaction_date = (SELECT MAX(transaction_date) AS transaction_date FROM uscan_transactions.fca_supplier_scorecard_mopar)
          ORDER BY p.plant_name ASC";

          $fca_data = $db_connection->select($fca_mopar_query);
          // fwrite($fp1,"\n");
          // fwrite($fp1,$fca_mopar_query);
        }
        if (empty($fca_data)) {
            return "0";
        }
        else {
          return array($fca_data,$cust_name,$cust_name);
      }
      }
      elseif ($cust_name == "Ford Motor Company") {
        //$ford_query = "SELECT p.plant_name,pc.supplier_code,fd.present_q1_status,fd.q1_score FROM uscan_master.plant p JOIN uscan_master.plant_customer_ref pc ON p.plant_id = pc.plant_id JOIN uscan_transactions.ford_q1_scores fd ON pc.supplier_code = fd.site_code COLLATE utf8_unicode_ci WHERE pc.supplier_code IN(".$supplier_code.") ORDER BY p.plant_name ASC";

        $ford_query = "SELECT p.plant_name,pc.supplier_code,fd.present_q1_status,fd.q1_score,fd.transaction_date
        FROM uscan_master.plant p
        JOIN uscan_master.plant_customer_ref pc ON p.plant_id = pc.plant_id
        JOIN uscan_transactions.ford_q1_scores fd ON pc.supplier_code = fd.site_code COLLATE utf8_unicode_ci
        WHERE pc.supplier_code IN (".$supplier_code.") AND
        pc.customer_id IN (".$customer_id.") AND
        fd.transaction_date = (SELECT MAX(transaction_date) AS transaction_date FROM uscan_transactions.ford_q1_scores)
        ORDER BY p.plant_name ASC";

        $ford_data = $db_connection->select($ford_query);
        // fwrite($fp1,"\n");
        // fwrite($fp1,$ford_query);
        if (empty($ford_query)) {
            return "0";
        }
        else {
          return array($ford_data,$cust_id,$cust_name);
      }
      }
      elseif ($cust_name == "PSA") {
        $psa_plant_status_qry = "";
        if($plant_status == "Red"){
          //fwrite($fp1,"Inside Red Block"."\r\n");
          if($psa_plant_status_qry == "")
          {
            $psa_plant_status_qry = " AND (psa.quality != 'U' || psa.supply_chain != 'U' || psa.after_sales != 'U') AND
            (psa.after_sales < 75 OR psa.quality < 80 OR psa.supply_chain < 75)";
          }
          else {
            $psa_plant_status_qry = $psa_plant_status_qry. " AND (psa.quality != 'U' || psa.supply_chain != 'U' || psa.after_sales != 'U') AND
            (psa.after_sales < 75 OR psa.quality < 80 OR psa.supply_chain < 75)";
          }
        }
        if($psa_plant_status_qry == "" || $plant_status == "Red,Green,Yellow")
        {
          $psa_query = "SELECT p.plant_name,pc.supplier_code,cc.customer_name,psa.quality AS quality_psa,psa.supply_chain AS supplychain_psa,
          psa.after_sales AS aftersales_psa,psa.transaction_date
          FROM uscan_master.plant p
          JOIN uscan_master.plant_customer_ref pc ON p.plant_id = pc.plant_id
          JOIN uscan_master.customer_child cc ON pc.customer_id = cc.customer_id
          JOIN uscan_transactions.psa_bidlist psa ON pc.supplier_code = psa.manufacturing_duns COLLATE utf8_unicode_ci
          WHERE pc.supplier_code IN(".$mfg_duns.") AND
          pc.customer_id IN (".$customer_id.") AND
          psa.transaction_date = (SELECT MAX(transaction_date) AS transaction_date FROM uscan_transactions.psa_bidlist)
          ORDER BY p.plant_name ASC";
        }
        else{
          $psa_query = "SELECT p.plant_name,pc.supplier_code,cc.customer_name,psa.quality AS quality_psa,psa.supply_chain AS supplychain_psa,
          psa.after_sales AS aftersales_psa,psa.transaction_date
          FROM uscan_master.plant p
          JOIN uscan_master.plant_customer_ref pc ON p.plant_id = pc.plant_id
          JOIN uscan_master.customer_child cc ON pc.customer_id = cc.customer_id
          JOIN uscan_transactions.psa_bidlist psa ON pc.supplier_code = psa.manufacturing_duns COLLATE utf8_unicode_ci
          WHERE pc.supplier_code IN(".$mfg_duns.") AND
          pc.customer_id IN (".$customer_id.") AND
          psa.transaction_date = (SELECT MAX(transaction_date) AS transaction_date FROM uscan_transactions.psa_bidlist) "
          .$psa_plant_status_qry." ORDER BY p.plant_name ASC";
        }
        $psa_data = $db_connection->select($psa_query);
        // fwrite($fp1,"\n");
        // fwrite($fp1,$psa_query);
        if (empty($psa_data)) {
            return "0";
        }
        else {
          return array($psa_data,$cust_id,$cust_name);
      }
      }
      else {
        if($mfg_duns != "" && $supplier_code != "")
        {
          $master_dashboard = "Select * from uscan_transactions.master_dashboard
          where supplier_code in (".$mfg_duns.",".$supplier_code.") AND customer_id IN (".$customer_id.")
          ORDER BY plant_name ASC";

          $master_plant_list = "SELECT DISTINCT(plant_id),plant_name FROM uscan_transactions.master_dashboard
          WHERE supplier_code IN (".$mfg_duns.",".$supplier_code.") AND customer_id IN (".$customer_id.") ORDER BY plant_name ASC";

          $master_customer_list = "SELECT DISTINCT(customer_id),customer_name FROM uscan_transactions.master_dashboard
          WHERE supplier_code IN (".$mfg_duns.",".$supplier_code.") AND customer_id IN (".$customer_id.") ORDER BY customer_name ASC";
        }
        elseif ($mfg_duns != "" && $supplier_code == "") {
          $master_dashboard = "Select * from uscan_transactions.master_dashboard
          where supplier_code in (".$mfg_duns.") AND customer_id IN (".$customer_id.") ORDER BY plant_name ASC";

          $master_plant_list = "SELECT DISTINCT(plant_id),plant_name FROM uscan_transactions.master_dashboard
          WHERE supplier_code IN (".$mfg_duns.") AND customer_id IN (".$customer_id.") ORDER BY plant_name ASC";

          $master_customer_list = "SELECT DISTINCT(customer_id),customer_name FROM uscan_transactions.master_dashboard
          WHERE supplier_code IN (".$mfg_duns.") AND customer_id IN (".$customer_id.") ORDER BY customer_name ASC";
        }
        elseif ($mfg_duns == "" && $supplier_code != "") {
          $master_dashboard = "SELECT * from uscan_transactions.master_dashboard
          WHERE supplier_code in (".$supplier_code.") AND customer_id IN (".$customer_id.") ORDER BY plant_name ASC";

          $master_plant_list = "SELECT DISTINCT(plant_id),plant_name FROM uscan_transactions.master_dashboard
          WHERE supplier_code IN (".$supplier_code.") AND customer_id IN (".$customer_id.") ORDER BY plant_name ASC";

          $master_customer_list = "SELECT DISTINCT(customer_id),customer_name FROM uscan_transactions.master_dashboard
          WHERE supplier_code IN (".$supplier_code.") AND customer_id IN (".$customer_id.") ORDER BY customer_name ASC";
        }

        $master_dashboard_data = $db_connection->select($master_dashboard);
        $master_dashboard_plant = $db_connection->select($master_plant_list);
        $master_dashboard_customer = $db_connection->Select($master_customer_list);

        // fwrite($fp1,"\n");
        // fwrite($fp1,"Inside Master Data");
        // fwrite($fp1,"\n");
        // fwrite($fp1,$master_dashboard);
        // fwrite($fp1,"\n");
        // fwrite($fp1,$master_plant_list);
        // fwrite($fp1,"\n");
        // fwrite($fp1,$master_customer_list);
        if (empty($master_dashboard_data)) {
            return "0";
        }
        else {
          return array($master_dashboard_data,"master","master",$master_dashboard_plant,$master_dashboard_customer);
      }

      }

      //$fca_data = $db_connection->select("Select * from fca_supplier_scorecard");
      //insert_logger("1001","1001","1001","active","running","Executing db query","2017-01-24 12:00:00","2017-01-24 12:01:34");

}
  public function generate_pdf(Request $request)
  {
    $file="E:/xampp/htdocs/uscan/demo.xls";
    //$test="<table  ><tr><td>Cell 1</td><td>Cell 2</td></tr></table>";
    $test = $request->source;
    file_put_contents($file,$test);

  }
    public function get_plantdata(Request $request)
    {
      $mfg_duns = $request->mfg_duns;
      $report_type = $request->report_type;
      $select_data="";
      $db_connection = DB::connection('mysql_transaction');
      if($report_type == "mfgduns"){
        // $select_data = "Select quality,supplychain,cca,country_risk,financial_risk,mmog_le,conflict_materials,diversity,ipb,
        // severity_score,severity_ipb,transaction_date from gm_historical_bidlist where mfg_duns = '".$mfg_duns."' AND transaction_date =
        // (SELECT MAX(transaction_date) AS transaction_date FROM uscan_transactions.gm_historical_bidlist)";

        $select_data = "SELECT gm.quality,gm.supplychain,gm.cca,gm.country_risk,gm.financial_risk,gm.mmog_le,gm.conflict_materials,
        gm.diversity,gm.ipb,gm.severity_score,gm.severity_ipb,gm.transaction_date,p.plant_name,pc.plant_id,pc.customer_id,
        cc.customer_name
        FROM uscan_transactions.gm_historical_bidlist gm
        JOIN uscan_master.plant p ON gm.mfg_duns = p.duns_no COLLATE utf8_unicode_ci
        JOIN uscan_master.plant_customer_ref pc ON p.plant_id = pc.plant_id AND p.duns_no = pc.supplier_code COLLATE utf8_unicode_ci
        JOIN uscan_master.customer_child cc ON pc.customer_id = cc.customer_id
        WHERE gm.mfg_duns = '".$mfg_duns."' AND
        gm.transaction_date = (SELECT MAX(transaction_date) AS transaction_date FROM uscan_transactions.gm_historical_bidlist)";
      }
      elseif ($report_type == "quality") {
        // $select_data = "Select ipb,severity_score,severity_ipb,prog_mgt_prrs,unauth_change,customer_sat_open,launch_prrs,field_action,
        // plant_disruption,controlled_shipping_level1,controlled_shipping_level2,nbh,iso_14001,ts16949_cert,qsb_quality_systems_basis,
        // supplier_quality_excellence_award,transaction_date from gm_historical_bidlist where mfg_duns = '".$mfg_duns."' AND transaction_date =
        // (SELECT MAX(transaction_date) AS transaction_date FROM uscan_transactions.gm_historical_bidlist)";

        $select_data = "SELECT gm.ipb,gm.severity_score,gm.severity_ipb,gm.prog_mgt_prrs,gm.unauth_change,gm.customer_sat_open,
        gm.launch_prrs,gm.field_action,gm.plant_disruption,gm.controlled_shipping_level1,gm.controlled_shipping_level2,gm.nbh,
        gm.iso_14001,gm.ts16949_cert,gm.qsb_quality_systems_basis,gm.supplier_quality_excellence_award,gm.transaction_date,
        p.plant_name,pc.plant_id,pc.customer_id,cc.customer_name
        FROM uscan_transactions.gm_historical_bidlist gm
        JOIN uscan_master.plant p ON gm.mfg_duns = p.duns_no COLLATE utf8_unicode_ci
        JOIN uscan_master.plant_customer_ref pc ON p.plant_id = pc.plant_id AND p.duns_no = pc.supplier_code COLLATE utf8_unicode_ci
        JOIN uscan_master.customer_child cc ON pc.customer_id = cc.customer_id
        WHERE gm.mfg_duns = '".$mfg_duns."' AND
        gm.transaction_date = (SELECT MAX(transaction_date) AS transaction_date FROM uscan_transactions.gm_historical_bidlist)";
      }
      elseif ($report_type == "supplychain"){
        // $select_data = "Select sc_rating,transaction_date from gm_historical_bidlist where mfg_duns = '".$mfg_duns."' AND transaction_date =
        // (SELECT MAX(transaction_date) AS transaction_date FROM uscan_transactions.gm_historical_bidlist)";

        $select_data = "SELECT gm.sc_rating,gm.transaction_date,p.plant_name,pc.plant_id,pc.customer_id,cc.customer_name
        FROM uscan_transactions.gm_historical_bidlist gm
        JOIN uscan_master.plant p ON gm.mfg_duns = p.duns_no COLLATE utf8_unicode_ci
        JOIN uscan_master.plant_customer_ref pc ON p.plant_id = pc.plant_id AND p.duns_no = pc.supplier_code COLLATE utf8_unicode_ci
        JOIN uscan_master.customer_child cc ON pc.customer_id = cc.customer_id
        WHERE gm.mfg_duns = '".$mfg_duns."' AND
        gm.transaction_date = (SELECT MAX(transaction_date) AS transaction_date FROM uscan_transactions.gm_historical_bidlist)";
      }
      elseif ($report_type == "cca"){
        // $select_data = "Select quality_score,service_score,transaction_date from gm_historical_bidlist where mfg_duns = '".$mfg_duns."' AND transaction_date =
        // (SELECT MAX(transaction_date) AS transaction_date FROM uscan_transactions.gm_historical_bidlist)";

        $select_data = "SELECT gm.quality_score,gm.service_score,gm.transaction_date,p.plant_name,pc.plant_id,pc.customer_id,
        cc.customer_name FROM uscan_transactions.gm_historical_bidlist gm
        JOIN uscan_master.plant p ON gm.mfg_duns = p.duns_no COLLATE utf8_unicode_ci
        JOIN uscan_master.plant_customer_ref pc ON p.plant_id = pc.plant_id AND p.duns_no = pc.supplier_code COLLATE utf8_unicode_ci
        JOIN uscan_master.customer_child cc ON pc.customer_id = cc.customer_id
        WHERE gm.mfg_duns = '".$mfg_duns."' AND
        gm.transaction_date = (SELECT MAX(transaction_date) AS transaction_date FROM uscan_transactions.gm_historical_bidlist)";
      }
      $data = $db_connection->select($select_data);
      if (empty($data)) {
          return "0";
      }
      else {
        return $data;
      }
    }

    public function get_fca_plantdata(Request $request)
    {
      $supplier_code = $request->code;
      $report_type = $request->report_type;
      $select_data="";
      $db_connection = DB::connection('mysql_transaction');
      if($report_type == "prdn"){
        $select_data = "SELECT incoming_material_quality,delivery,warranty,cost,partnership,overall,transaction_date
        FROM fca_supplier_scorecard_production WHERE code = '".$supplier_code."' AND
        transaction_date = (SELECT MAX(transaction_date) AS transaction_date FROM uscan_transactions.fca_supplier_scorecard_production)";
      }
      elseif ($report_type == "mopar") {
        $select_data = "SELECT incoming_material_quality,delivery,warranty,cost,partnership,overall,transaction_date
        FROM fca_supplier_scorecard_mopar WHERE code = '".$supplier_code."' AND
        transaction_date = (SELECT MAX(transaction_date) AS transaction_date FROM uscan_transactions.fca_supplier_scorecard_mopar)";
      }
      $data = $db_connection->select($select_data);
      if (empty($data)) {
          return "0";
      }
      else {
        return $data;
      }
    }

    public function get_ford_plantdata(Request $request)
    {
      $supplier_code = $request->code;
      $report_type = $request->report_type;
      $select_data="";
      $db_connection = DB::connection('mysql_transaction');
      $select_data = "SELECT present_q1_status,q1_score,q1msa_q1_score,capable_system_q1_score,iso14001_q1_score,iso_ts16949_q1_score,mmog_le_q1_score,
      ongoing_performance_q1_score,site_ppm_prod_applied_q1_score,site_ppm_svc_applied_q1_score,comm_ppm_prod_applied_q1_score,
      comm_ppm_svc_applied_q1_score,dlv_prod_applied_q1_score,dlv_svc_applied_q1_score,field_service_actions_q1_score,
      stop_shipments_q1_score,violation_of_trust_production_q1_score,violation_of_trust_service_q1_score,transaction_date FROM uscan_transactions.ford_q1_scores
      WHERE site_code = '".$supplier_code."' AND
      transaction_date = (SELECT MAX(transaction_date) AS transaction_date FROM uscan_transactions.ford_q1_scores)";

      $data = $db_connection->select($select_data);
      if (empty($data)) {
          return "0";
      }
      else {
        return $data;
      }
    }

    public function get_psa_plantdata(Request $request)
    {
      $supplier_code = $request->code;
      $report_type = $request->report_type;
      $select_data="";
      $db_connection = DB::connection('mysql_transaction');
      $select_data = "SELECT quality,supply_chain,after_sales,manufacturing_duns,ipb_threshold,ipb_num_of_issues,car_impact_yard_hold,mass_production_minor_major,mass_production_unauth_change,
        escalation_level_quality,program_management,car_impact_yard_hold_customer,qsb_certification_status,isots_16049_certification_status,
        neologistics_service_rate,ship_pack_delivery_status,stock_out,plant_prod_sequence_changed,incomplete_car_part,production_down_time,
        escalation_level_supplychain,mmog_le_self_assessment,speed_service_rate_starting_n2,quality_after_sales,supplychain_after_sales,
        speed_codes_736_743_764,speed_codes_711,speed_codes_716_722_731_5_742_744_762_3,speed_codes_753,speed_codes_712_715_721_752_754_756,
        speed_codes_751_757,speed_codes_755,transaction_date FROM uscan_transactions.psa_bidlist WHERE manufacturing_duns = '".$supplier_code."' AND
        transaction_date = (SELECT MAX(transaction_date) AS transaction_date FROM uscan_transactions.psa_bidlist)";

      $data = $db_connection->select($select_data);
      if (empty($data)) {
          return "0";
      }
      else {
        return $data;
      }
    }
}
