<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Http\Controllers\portal_model;
use Auth;


class portal_controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function get_customer(Request $request)
     {
         $cust_name = App\customer_child_model::all()->toArray();
         return $cust_name;
     }


    public function get_plant(Request $request)
   {
       $plant_name = App\plant_model::all()->toArray();
       return $plant_name;
   }



    public function portal_create_index()
    {
        return view("portal/portal_create");
    }


    public function portal_display_index(Request $request)
    {
      $data = \DB::table('portal');

      // ->join('customer_child', 'portal.customer_id', '=', 'customer_child.customer_id')
      // ->join('plant', 'portal.plant_id', '=', 'plant.plant_id')
      // ->get();
         $data = \DB::select("select p.`portal_id`,p.`plant_id`,p.`customer_id`,p.`portal_name`,p.`portal_url`,p.`portal_password`,p.`portal_repeat_password`,p.`max_attempt`,p.`password_expiry_date`,p.`portal_user_id`,p.`locked`,p.`active`,p.`created_at`,p.`created_by`,p.`updated_by`,p.`updated_at`,c.`customer_name`,pl.`plant_name` FROM customer_child c,portal p ,plant pl where c.`customer_id` = p.`customer_id` and p.`plant_id` = pl.`plant_id`");
        if(empty($data))
        {
          return "0";
        }
        else {
          return $data;
        }
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)


    {
      $query = 'select current_no from number_range where table_name = "portal"';
      $result   = \DB::select($query);
      $portal_id  = $result[0]->current_no +1;

      $data =   \DB::table('number_range')
      ->where('table_name','portal')
      ->limit(1)
      ->update(array('current_no' => $portal_id
        ));

      $portal = new App\portal_model;
      $portal->portal_id = $portal_id;
      $portal->customer_id = $request->customer_id;
      $portal->plant_id = $request->plant_id;
      $portal->portal_name = $request->portal_name;
      $portal->portal_url = $request->portal_url;
      $portal->portal_user_id = $request->portal_user_id;
      $portal->portal_password = $request->portal_password;
      $portal->portal_repeat_password = $request->portal_repeat_password;
      if ($request->max_attempt != "")
      {
        $portal->max_attempt = $request->max_attempt;
      }
      else {
        $portal->max_attempt = 0;
      }

      if ($request->password_expiry_date != "")
      {
        $portal->password_expiry_date = $request->password_expiry_date;
      }
      else {
        $portal->password_expiry_date = "9999-12-31";
      }

      // $portal->password_expiry_date = $request->password_expiry_date;


      $portal->locked= 0;
      $portal->active= $request->active;
      $portal->enterprise_id= Auth::user()->enterprise_id;
      $portal->created_by = Auth::user()->email;
      $portal->updated_by = Auth::user()->email;
      $portal->save();

      }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show(Request $request)
    {
        // $portal_id = $request->search_portal_id;
        $search_portal_name_id = $request->search_portal_name_id;
        $search_portal_purl = $request->search_portal_purl;
        $search_customer_name_id = $request->search_customer_name_id;
        $search_plant_name_id = $request->search_plant_name_id;
        $query_data = "";


        if($search_portal_name_id!="")
         {
           $query_data = " portal_name = '" . $search_portal_name_id."' and ";
         }

         if($search_portal_purl!="")
          {
            $query_data =$query_data . " portal_url = '" . $search_portal_purl."' and ";
          }

          if($search_customer_name_id!="")
          {
            $search_customer_name = \DB::select("select `customer_id` FROM customer_child where customer_name = '".$search_customer_name_id."' ");

            if (empty($search_customer_name))
             {
                return "0";
             }
             else
             {
                 $query_data = " c.`customer_id` = '". $search_customer_name[0]->customer_id . "' and ";
             }
          }

          if($search_plant_name_id!="")
          {
            $search_plant_name = \DB::select("select `plant_id` FROM plant where plant_name = '".$search_plant_name_id."' ");

            if (empty($search_plant_name))
             {
                return "0";
             }
             else
             {
                 $query_data = " pl.`plant_id` = '". $search_plant_name[0]->plant_id . "' and ";
             }
          }


        $query_data = substr($query_data, 0, -4);
       $data = \DB::select("select p.portal_id,p.plant_id,p.customer_id,p.portal_name,p.portal_url,p.portal_password,p.portal_repeat_password,p.max_attempt,p.password_expiry_date,p.portal_user_id,p.locked,p.active,p.created_at,p.created_by,p.updated_by,p.updated_at,c.customer_name,pl.plant_name FROM customer_child c,portal p ,plant pl where c.customer_id = p.customer_id and p.plant_id = pl.plant_id and" .$query_data);
       if(empty($data))
       {
         return "0";
       }
       else {
         return $data;
       }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request)
    {
                $updated_at = date('Y-m-d H:i:s', time());
                $data =   \DB::table('portal')
                ->where('portal_id',  $request->portal_id)
                ->limit(1)
                ->update(array('customer_id' => $request->customer_id,
                'updated_at' => $updated_at,
                'plant_id' => $request->plant_id,
                'portal_name' => $request->portal_name,
                'portal_url' => $request->portal_url,
                'portal_user_id' => $request->portal_user_id,
                'portal_password' => $request->portal_password,
                'portal_repeat_password' => $request->portal_repeat_password,
                'max_attempt' => $request->max_attempt,
                'password_expiry_date' => $request->password_expiry_date,
                'locked' => $request->locked,
                'active' => $request->active,
                'updated_by' =>   Auth::user()->email,
                ));
                 return $data;
    }

    public function autocomplete_portal_name(Request $request)
    {
             $term = $request->search_portal_name_id;
             $results = array();
             $results;
             $i = 0;
             $query = "select portal_name from portal where portal_name LIKE '%$term%'";
             $res   = \DB::select($query);
             foreach ($res as $query)
               {
                   array_push($results,$query->portal_name);
               }
             return $results;
    }

    public function autocomplete_portal_url(Request $request)
    {
             $term = $request->search_portal_url;
             $results = array();
             $results;
             $i = 0;
             $query = "select portal_url from portal where portal_url LIKE '%$term%'";
             $res   = \DB::select($query);
             foreach ($res as $query)
               {
                   array_push($results,$query->portal_url);
               }
             return $results;
    }

    public function autocomplete_customer_name(Request $request)
    {
             $term = $request->search_customer_name_id;
             $results = array();
             $results;
             $i = 0;
             $query = "select customer_name from customer_child where customer_name LIKE '%$term%'";
             $res   = \DB::select($query);
             foreach ($res as $query)
               {
                   array_push($results,$query->customer_name);
               }
             return $results;
    }

    public function autocomplete_plant_name(Request $request)
    {
             $term = $request->search_plant_name_id;
             $results = array();
             $results;
             $i = 0;
             $query = "select plant_name from plant where plant_name LIKE '%$term%'";
             $res   = \DB::select($query);
             foreach ($res as $query)
               {
                   array_push($results,$query->plant_name);
               }
             return $results;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function destroy(Request $request)
    {
        $portal_id = $request->portal_id;
       $data = \DB::select("delete from portal where portal_id='$portal_id'");

    // echo sizeof($data);
  return $data;

  // $portal = App\portal_model::find($portal_id);
  // $portal->delete();
  //
  //   $trashedAndNotTrashed = portal_model::withTrashed()->get();
  //   return $trashedAndNotTrashed;

    }
}
