<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Auth;


class task_controller extends Controller
{
    //

     public function return_task_type()
     {
       $task_type = App\task_type_model::where('enterprise_id',Auth::user()->enterprise_id)->select('task_type_id','task_type_name')->get();
       return $task_type;
     }


     public function task_get_program_for_customer_and_type(Request $request)
     {

            if ($request->task_type == 1)
            {
              $program_type = "crawler";
            }

            switch ($request->task_type) {
                case 1:
                    $program_type = "crawler";
                    break;
                case 2:
                    $program_type = "notification";
                    break;
                case 3:
                    $program_type = "interface";
                    break;
                default:
                    //code to be executed
            }


//            $task_type = App\program_model::where('enterprise_id',Auth::user()->enterprise_id)->where('customer_id','=',$request->customer_id)->where('active','=', '1')->where('program_type','=', $program_type)->select('program_id','program_name')->get();
            $task_type = App\program_model::where('enterprise_id',Auth::user()->enterprise_id)->where('active','=', '1')->where('program_type','=', $program_type)->select('program_id','program_name')->get();


            if ($task_type->count()) {

              return $task_type;

            }
            else {

              return  0;

            }

     }


     public function task_get_plant_for_customer(Request $request)
     {

        // $task_plant = App\plant_model::where('enterprise_id',Auth::user()->enterprise_id)->where('customer_id','=',$request->customer_id)->where('active','=', '1')->select('plant_id','plant_name')->get();

        $task_plant = \DB::table('plant_customer_ref')
        ->select(
          'plant_customer_ref.plant_id',
          'plant_name')
       ->leftJoin('plant', 'plant.plant_id', '=', 'plant_customer_ref.plant_id')
       ->where('plant_customer_ref.enterprise_id',Auth::user()->enterprise_id)
       ->where('customer_id','=',$request->customer_id)
       ->where('active','=', '1')
       ->get();


        if ($task_plant->count()) {

          return $task_plant;

        }
        else {

          return  0;

        }

     }
     public function task_get_portal_for_plant(Request $request)
     {

        $task_portal = App\portal_model::where('enterprise_id',Auth::user()->enterprise_id)->where('plant_id','=',$request->plant_id)->where('customer_id','=',$request->customer_id)->where('active','=', '1')->select('portal_id','portal_name')->get();

            if ($task_portal->count()) {

              return $task_portal;

            }
            else {

              return  0;

            }

     }

     public function task_insert(Request $request)
     {

      //return  $request->distribution_strategy;

       $query = "select current_no from number_range where table_name = 'task' and enterprise_id = '". Auth::user()->enterprise_id ."'";
       $result = \DB::select($query);
       $task_id = $result[0]->current_no + 1;

       $update_current_no =   \DB::table('number_range')
       ->where('table_name',  'task')
       ->limit(1)
       ->update(array('current_no' => $task_id));

       $task_model = new \App\task_model;
       $task_model->task_id                         = $task_id;
       $task_model->task_name                       = $request->task_name;
       $task_model->active                          = $request->active;
       $task_model->task_type_id             = $request->task_type;
       $task_model->customer_id              = $request->customer_id;
       $task_model->plant_id                 = $request->plant_id;
       $task_model->portal_id                = $request->portal_id;
       $task_model->program_id               = $request->program_id;
       $task_model->created_by               = Auth::user()->email;
       $task_model->updated_by               = Auth::user()->email;
       $task_model->enterprise_id            = Auth::user()->enterprise_id;
       $task_model->save();


       $length =  sizeof($request->distribution_strategy);

       for ($i = 0; $i < $length; $i++) {

         $task_model = new \App\distribution_strategy;
         $task_model->task_status              = $request->distribution_strategy[$i]['task_status'];
         $task_model->communication_options    = $request->distribution_strategy[$i]['communication_options'];
         $task_model->mail_type                = $request->distribution_strategy[$i]['mail_type'];
         $task_model->mail_address             = $request->distribution_strategy[$i]['mail_address'];
         $task_model->task_id                  = $task_id;
         $task_model->enterprise_id            = Auth::user()->enterprise_id;
         $task_model->save();
      }

//$task_id = "";
      return "Task Created Successfully";
}
     public function show_all_task()
     {
       $data = \DB::table('task')
       ->select(
         'task_id',
         'task_name',
         'task.active',
         //'distribution_strategy_id',
         'task.created_by',
         'task.updated_by',
         'task.created_at',
         'task.updated_at',
         'task.task_type_id',
         'task_type.task_type_name',
         'customer_child.customer_name',
         'task.customer_id',
         'plant.plant_name',
         'task.plant_id',
         'task.program_id',
         'program_name',
         'task.portal_id',
         'portal.portal_name'
         )
      ->leftJoin('plant', 'plant.plant_id', '=', 'task.plant_id')
      ->leftJoin('portal', 'portal.portal_id', '=', 'task.portal_id')
      ->leftJoin('program', 'program.program_id', '=', 'task.program_id')
      ->leftJoin('task_type', 'task_type.task_type_id', '=', 'task.task_type_id')
      ->leftJoin('customer_child', 'customer_child.customer_id', '=', 'task.customer_id')
      ->where( 'task.enterprise_id',Auth::user()->enterprise_id)
      ->orderBy('task_name', 'asc')
       ->get();

      return $data;
     }




     public function task_search(Request $request)
     {
          $query_data = "";
          $search_task_type_id =  $request->search_task_type_id;
          $search_task_name_id       =  $request->search_task_name_id;
          $search_plant_name_id       =  $request->search_plant_name_id;
          $search_customer_name_id         =  $request->search_customer_name_id;


            if($search_task_type_id!="")
            {
              $query_data =$query_data . " task_type.task_type_name = '" . $search_task_type_id ."' and ";
            }
            if($search_task_name_id!="")
            {
              $query_data =$query_data . "   task_name = '" . $search_task_name_id ."' and ";
            }
            if($search_plant_name_id!="")
            {
              $query_data =$query_data . "  plant_name = '" . $search_plant_name_id."' and ";
            }
            if($search_customer_name_id!="")
            {
              $query_data =$query_data . "  customer_name = '" . $search_customer_name_id."' and ";
            }

            $query_data = substr($query_data, 0, -4);


            $data = \DB::select("select
             task_id,
             task_name,
             task.active,
             task.created_by,
             task.updated_by,
             task.created_at,
             task.updated_at,
             task.task_type_id,
             task_type.task_type_name,
             customer_child.customer_name,
             task.customer_id,
             plant.plant_name,
             task.plant_id,
             task.program_id,
             program_name,
             task.portal_id,
             portal.portal_name from task , plant ,portal,program ,task_type,customer_child where  customer_child.`customer_id` = task.`customer_id` and plant.plant_id = task.plant_id and portal.`portal_id` = task.`portal_id` and program.`program_id` = task.`program_id` and task_type.`task_type_id` = task.`task_type_id` and task.enterprise_id = '".Auth::user()->enterprise_id. "' and $query_data");

            if(empty($data))
            {
              return "0";
            }
            else {
              return $data;
            }


     }

     public function destroy(Request $request)
     {
       $task_id = $request->task_id;

       $data = \DB::select("delete from task where task_id='$task_id' and enterprise_id = '". Auth::user()->enterprise_id ."'");
       return "Data Deleted Sucessfully";

     }


     public function update(Request $request)
     {
        $updated_at = date('Y-m-d H:i:s', time());
        $task_id = $request->task_id;
        $data =   \DB::table('task')
        ->where('task_id',  $request->task_id)
        ->where('task.enterprise_id',Auth::user()->enterprise_id)
        ->limit(1)
        ->update(array('task_type_id' => $request->task_type_id,
        'task_name' => $request->task_name,
        'updated_at' => $updated_at,
        'customer_id' => $request->customer_id,
        'plant_id' => $request->plant_id,
        'portal_id' => $request->portal_id,
        'program_id' => $request->program_id,
        'active' => $request->active,
        'updated_by' =>  Auth::user()->email));

        $ds_count =  sizeof($request->distribution_strategy);
        $distribution_strategy_check = $request->distribution_strategy_check;

        if($distribution_strategy_check == 1)
        {
                for($i=0;$i<$ds_count;$i++)
                {
                    $distribution_strategy_id = $request->distribution_strategy[$i]['distribution_strategy_id'];
                    $query = "select * from distribution_strategy where distribution_strategy_id = '$distribution_strategy_id'";
                    $result = \DB::select($query);

                             if(count($result))
                                {
                                $data =   \DB::table('distribution_strategy')
                                ->where('distribution_strategy_id',  $request->distribution_strategy[$i]['distribution_strategy_id'])
                                ->where('distribution_strategy.enterprise_id',Auth::user()->enterprise_id)
                                ->limit(1)
                                ->update(array('task_status' => $request->distribution_strategy[$i]['task_status'],
                                'communication_options' => $request->distribution_strategy[$i]['communication_options'],
                                'mail_type' => $request->distribution_strategy[$i]['mail_type'],
                                'mail_address' => $request->distribution_strategy[$i]['mail_id'],
                                  ));
                               }
                               else if(!count($result))
                               {
                                 $length =  sizeof($request->distribution_strategy);
                                 for ($i = 0; $i < $length; $i++)
                                 {
                                   $task_model = new \App\distribution_strategy;
                                   $task_model->task_status              = $request->distribution_strategy[$i]['task_status'];
                                   $task_model->communication_options    = $request->distribution_strategy[$i]['communication_options'];
                                   $task_model->mail_type                = $request->distribution_strategy[$i]['mail_type'];
                                   $task_model->mail_address             = $request->distribution_strategy[$i]['mail_id'];
                                   $task_model->task_id                  = $task_id;
                                   $task_model->enterprise_id            = Auth::user()->enterprise_id;
                                   $task_model->save();
                                    }
                               }
                    }
           }

         else if($distribution_strategy_check == 0)
         {
               $data = \DB::select("delete from distribution_strategy where task_id='$task_id' and enterprise_id = '". Auth::user()->enterprise_id ."'");

         }
    return "Task and DS Updated Sucessfully";
     }

     public function autocomplete_task_type(Request $request)
     {

              $term = $request->search_task_type;
              $results = array();
              $results;
              $i = 0;
              $query = "select distinct task_type_name from task_type where task_type_name LIKE '%$term%' and enterprise_id = '". Auth::user()->enterprise_id ."'";
              $res   = \DB::select($query);
              foreach ($res as $query)
               {
                    array_push($results,$query->task_type_name);
               }
              return $results;
     }
     public function autocomplete_task_name(Request $request)
     {

              $term = $request->search_task_name;
              $results = array();
              $results;
              $i = 0;
              $query = "select distinct task_name from task where task_name LIKE '%$term%' and enterprise_id = '". Auth::user()->enterprise_id ."'";
              $res   = \DB::select($query);
              foreach ($res as $query)
               {
                    array_push($results,$query->task_name);
               }
              return $results;
     }

     public function get_task_status(Request $request)
     {
       $query = "select distinct status_id,status_name from task_status where enterprise_id = '". Auth::user()->enterprise_id ."'";
         $res   = \DB::select($query);
         return $res ;
     }

     public function get_communication_options(Request $request)
     {
       $query = "select distinct communication_option_id,communication_option_name from task_communication_options where enterprise_id = '". Auth::user()->enterprise_id ."'";
         $res   = \DB::select($query);
         return $res ;
     }

     public function get_mail_address(Request $request)
     {
       $query = "select distinct mail_comm_id,ent_user_id from mail_enterprise_users where enterprise_id = '". Auth::user()->enterprise_id ."'";
         $res  = \DB::select($query);
         return $res ;
     }

    public function get_group_mail_address(Request $request)
    {
      $query = "select distinct user_group_id,ent_user_group_id from mail_enterprise_user_groups where enterprise_id = '". Auth::user()->enterprise_id ."'";
        $res   = \DB::select($query);
          return $res;
    }



    public function task_ds_ref(Request $request)
    {
         $task_id =  $request->task_id;

           $data = \DB::table('distribution_strategy')
           ->select(
           'distribution_strategy.task_id'
           )
            ->where( 'distribution_strategy.task_id', $task_id)
            ->get();
            //  return $data;
                       if(!count($data))
                       {
                         return 0;
                       }
                       else
                       {
                         return $data;
                       }

                  }


    public function show_all_ds(Request $request)
    {
      $query_data = "";
      $task_id_ref_ds =  $request->task_id_ref_ds;


      $data = \DB::table('distribution_strategy')
      ->select(
      'distribution_strategy.distribution_strategy_id',
     'distribution_strategy.task_id',
     'distribution_strategy.enterprise_id',
     'distribution_strategy.mail_type',
     'distribution_strategy.mail_address',
     'distribution_strategy.communication_options',
     'distribution_strategy.task_status',
     'task_status.status_name',
     'task_status.status_id',
     'task_communication_options.communication_option_name'
      )
       ->where( 'distribution_strategy.enterprise_id',Auth::user()->enterprise_id)
       ->where( 'distribution_strategy.task_id', $task_id_ref_ds)
       ->Join('task_status', 'task_status.status_id', '=', 'distribution_strategy.task_status')
       ->Join('task_communication_options', 'task_communication_options.communication_option_id', '=', 'distribution_strategy.communication_options')
        ->get();


    $count = count($data);
     //return $data;
    $i = 0;
    foreach ($data as $key)

     {
      # code...
     $i = $i +1;

      $distribution_strategy_id = $key->distribution_strategy_id;
      $status_id = $key->status_id;
      $task_status = $key->task_status;
      $mail_address = $key->mail_address;
      $mail_type = $key->mail_type;
      $status_name = $key->status_name;
      $communication_option_name = $key->communication_option_name;
      $communication_options = $key->communication_options;


       if($mail_type == "personnel")
        {
         $data1 = \DB::select("select ent_user_id,mail_comm_id from mail_enterprise_users
         where `mail_comm_id` = '$mail_address'");
         //return $data1;
        $mail_id = $data1[0]->mail_comm_id;
        $mail_id_address = $data1[0]->ent_user_id;
         }

       if($mail_type == "group")
         {
           $data1 = \DB::select("select user_group_id,ent_user_group_id from mail_enterprise_user_groups
           where `user_group_id` = '$mail_address'");
          $mail_id = $data1[0]->user_group_id;
          $mail_id_address = $data1[0]->ent_user_group_id;
           }

           $channel[] = array(
                        'distribution_strategy_id' => $distribution_strategy_id,
                     'status_name' => $status_name,
                     'task_status' =>$task_status,
                      'status_id'  =>$status_id,
                     'communication_option_name' => $communication_option_name,
                     'communication_options' =>$communication_options,
                     'mail_address' => $mail_address,
                     'mail_type' => $mail_type,
                     'mail_id' => $mail_id,
                     'mail_id_address' => $mail_id_address
                                  );
          }
          if(empty($channel))
          {
            return "0";
          }
          else {
            return $channel;
          }
    }
    public function ds_delete(Request $request)
    {
      $distribution_strategy_id = $request->distribution_strategy_id;

      $data = \DB::select("delete from distribution_strategy where distribution_strategy_id='$distribution_strategy_id' and enterprise_id = '". Auth::user()->enterprise_id ."'");
      return "Data Deleted Sucessfully";

    }

    public function delete_ds_ref_task(Request $request)
    {
      $task_id = $request->task_id;
      $data = \DB::select("delete from distribution_strategy where task_id='$task_id' and enterprise_id = '". Auth::user()->enterprise_id ."'");
      return "Data Deleted Sucessfully";
    }


    public function get_mail_id(Request $request)
    {

      $mail_type =  $request->mail_type;
      $mail_id =  $request->mail_id;

      if($mail_id!="" && $mail_type=="personnel")
      {
        $query = "select mail_comm_id,ent_user_id from mail_enterprise_users where ent_user_id='$mail_id' and enterprise_id = '". Auth::user()->enterprise_id ."'";
           $result = \DB::select($query);
      }
      else if($mail_id!="" && $mail_type=="group")
      {
        $data = \DB::select("select user_group_id,ent_user_group_id from mail_enterprise_user_groups where ent_user_group_id='$mail_id' and enterprise_id = '". Auth::user()->enterprise_id ."'");
         $result = \DB::select($query);
      }

      if(empty($result))
      {
        return "0";
      }
      else {
        return $result;
      }

    }
      function get_current_ds_val(Request $request)
      {
        $query = "select max(distribution_strategy_id) as distribution_strategy_id from distribution_strategy where enterprise_id = '". Auth::user()->enterprise_id ."'";
           $result = \DB::select($query);
           return $result;
      }

}
