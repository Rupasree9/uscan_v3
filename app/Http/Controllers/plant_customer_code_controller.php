<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Auth;

class plant_customer_code_controller extends Controller
{
    //
    public function plant_customer_code_view_change()
    {
      return view('cross_reference_tables/plant_customer_change');

    }
    public function plants_customer_code_view_display()
    {
      return view('cross_reference_tables/plant_customer_display');
    }

    public function get_all_plant_customer_code()
    {

      $all_plants  =  \DB::table('plant')
                      ->select('plant_id','plant_name','duns_no')
                      ->where('plant.enterprise_id',Auth::user()->enterprise_id)
                      ->where('plant.plant_name','<>',"ALL Plants")
                      ->orderBy('plant.plant_name', 'asc')
                      ->get();

      $plants = array();
      $plants_name = array();
      for ($i=0; $i < count($all_plants) ; $i++) {
        $plants[$i] =  $all_plants[$i]->plant_id;
        $plants_name[$i] = $all_plants[$i]->plant_name . "," . $all_plants[$i]->duns_no;
      }


      $all_customers  =  \DB::table('customer_child')
                  ->select('customer_id','customer_name')
                  ->where('customer_child.enterprise_id',Auth::user()->enterprise_id)
                  ->where('customer_child.customer_name','<>',"ALL Customers")
                  ->get();

      $customers = array();
      $customers_name = array();
      for ($j=0; $j < count($all_customers) ; $j++) {
        $customers[$j] =  $all_customers[$j]->customer_id;
        $customers_name[$j] =  $all_customers[$j]->customer_name;
      }

      // return count($customers);
      $final_data = array();

      for ($p=0; $p < count($plants) ; $p++) {

        for ($c=0; $c < count($customers) ; $c++) {

          $data = \DB::table('plant_customer_ref')
          ->select(
            'supplier_code')
            ->leftJoin('plant', 'plant.plant_id', '=', 'plant_customer_ref.plant_id')
            ->leftJoin('customer_child', 'customer_child.customer_id', '=', 'plant_customer_ref.customer_id')
            ->where('plant_customer_ref.enterprise_id',Auth::user()->enterprise_id)
            ->where('plant_customer_ref.customer_id',$customers[$c])
            ->where('plant_customer_ref.plant_id',$plants[$p])
            ->first();

        if (count($data))
        {
          $final_data[$plants[$p]][$customers[$c]] =  $data->supplier_code;
        }
        else {
          $final_data[$plants[$p]][$customers[$c]] =  "";
        }

        // return $final_data;
        }

      }
      $info = array();
      $info[1] = $plants;
      $info[2] = $plants_name;
      $info[3] = $customers;
      $info[4] = $customers_name;
      $info[5] = $final_data;


      return $info;

    }

    public function update_plant_customer_code(Request $request)
    {

      for ($u=0; $u < count($request->data) ; $u++) {

          $ref_data =  $request->data[$u]['value'];
          $update_data = explode("_", $ref_data);
          $plant_ref_id = $update_data[0];
          $customer_ref_id = $update_data[1];
          $supplier_code   = $update_data[2];

          $check_supplier_code = \DB::table('plant_customer_ref')
           ->select(
             'supplier_code')
             ->where('plant_customer_ref.enterprise_id',Auth::user()->enterprise_id)
             ->where('plant_customer_ref.customer_id',$customer_ref_id)
             ->where('plant_customer_ref.plant_id',$plant_ref_id)
             ->first();

         if (count($check_supplier_code) > 0)
         {

           if($supplier_code)
           {


           $updated_at = date('Y-m-d H:i:s', time());
           $data =   \DB::table('plant_customer_ref')
           ->where('plant_customer_ref.enterprise_id',Auth::user()->enterprise_id)
           ->where('plant_customer_ref.customer_id',$customer_ref_id)
           ->where('plant_customer_ref.plant_id',$plant_ref_id)
           ->limit(1)
           ->update(array('supplier_code' => $supplier_code,'updated_at' => $updated_at,'updated_by' =>  Auth::user()->email));

           }
           else {
             # code...
             $data = \DB::select("delete from plant_customer_ref where plant_id='$plant_ref_id' and customer_id= '$customer_ref_id' and enterprise_id = ".Auth::user()->enterprise_id);

           }

         }
         else {
                      if ($supplier_code)
                      {
                         $plant_customer_ref = new \App\plants_customers_model;
                         $plant_customer_ref->plant_id      = $plant_ref_id;
                         $plant_customer_ref->customer_id   = $customer_ref_id;
                         $plant_customer_ref->supplier_code = $supplier_code;
                         $plant_customer_ref->created_by              = Auth::user()->email;
                         $plant_customer_ref->updated_by              = Auth::user()->email;

                         $plant_customer_ref->enterprise_id           = Auth::user()->enterprise_id;
                         $plant_customer_ref->save();
                      }
            }







      }

      return "Updated Sucessfully";




    }

}
