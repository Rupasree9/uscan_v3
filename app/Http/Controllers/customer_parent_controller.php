<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Auth;


class customer_parent_controller extends Controller
{
    //
    /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {

  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {

  }

  public function validateCustomerParentCreate(Request $request)
  {

    $parent_results = \DB::table('customer_parent')
                           ->where('enterprise_id',Auth::user()->enterprise_id)
                           ->where('parent_name' ,$request->parent_name)
                           ->where('customer_parent.deleted_at',NULL)->get();
     // return $cust_name;
     if (count($parent_results) > 0)
       {
         return "false";
       }
       else {
         return "true";
       }


  }

  public function validateCustomerParentChange(Request $request)
  {
    if(strtoupper($request->parent_name) == strtoupper($request->parent_name_valid))
    {
      return "true";
    }
    else
     {

    $parent_results = \DB::table('customer_parent')
                           ->where('enterprise_id',Auth::user()->enterprise_id)
                           ->where('parent_name' ,$request->parent_name)
                           ->where('customer_parent.deleted_at',NULL)->get();
     // return $cust_name;
     if (count($parent_results) > 0)
       {
         return "false";
       }
       else {
         return "true";
       }
     }


  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */

  public function store(Request $request)
  {
      // return   $request->parent_name;
      $this->validate($request, [
         'parent_name' => 'bail|required',
         'parent_code' => 'required',
     ]);

      $query = "select current_no from number_range where table_name = 'customer_parent'  and enterprise_id = '". Auth::user()->enterprise_id ."'";
      $result = \DB::select($query);
      $parent_id = $result[0]->current_no + 1;

      $update_current_no =   \DB::table('number_range')
      ->where('table_name',  'customer_parent')
      ->where('enterprise_id',Auth::user()->enterprise_id)
      ->limit(1)
      ->update(array('current_no' => $parent_id));

      $newparent = new \App\customer_parent_model;
      $newparent->parent_id   = $parent_id;
      $newparent->parent_name = $request->parent_name;
      $newparent->parent_code = $request->parent_code;
      $newparent->created_by              = Auth::user()->email;
      $newparent->updated_by              = Auth::user()->email;
      $newparent->enterprise_id           = Auth::user()->enterprise_id;
      $newparent->save();

      return "Parent Company created sucessfully";

  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show(Request $request)
  {

    $parent_results = \DB::table('customer_parent')
                           ->where('enterprise_id',Auth::user()->enterprise_id)
                           ->where('parent_name' ,"<>","ALL Customers")
                           ->where('customer_parent.deleted_at',NULL)->get();
    return $parent_results;

  }

  public function search(Request $request)
  {
       $query_data = "";
       $search_parent_name_id         =  $request->search_parent_name_id;
       $search_parent_code_id         =  $request->search_parent_code_id;

         if($search_parent_name_id!="")
         {
           $query_data =$query_data . "   parent_name = '" . $search_parent_name_id."' and ";
         }
         if($search_parent_code_id!="")
         {
           $query_data =$query_data . "   parent_code = '" . $search_parent_code_id."' and ";
         }

         $query_data = substr($query_data, 0, -4);
         $data = \DB::select("select * from customer_parent where customer_parent.enterprise_id = '".Auth::user()->enterprise_id. "' and parent_name != 'ALL Customers' and deleted_at IS NULL and $query_data");

         if(empty($data))
         {
           return "0";
         }
         else {
           return $data;
         }


  }


  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
   public function update(Request $request)
   {

       $this->validate($request, [
          'search_parent_name_id_display' => 'bail|required',
          'search_parent_code_id_display' => 'required',
      ]);

      $updated_at = date('Y-m-d H:i:s', time());
      $data =   \DB::table('customer_parent')
      ->where('parent_id',  $request->parent_id)
      ->where('enterprise_id',Auth::user()->enterprise_id)
      ->where('customer_parent.deleted_at',NULL)
      ->limit(1)
      ->update(array('parent_name' => $request->search_parent_name_id_display,
      'updated_at' => $updated_at,
      'parent_code' => $request->search_parent_code_id_display,
      'updated_by' =>  Auth::user()->email));
      return "Parent Company updated sucessfully";

   }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy(Request $request)
  {
    $parent_id = $request->parent_id;

    $child_result_set = \App\customer_child_model::where('parent_id', $parent_id)->first();

    if($child_result_set)
    {
       return "Unable to delete the Parent Company</br>Please delete the dependent customers";
    }
    else {

          $deletedRows_parent = \App\customer_parent_model::where('parent_id', $parent_id)->where('enterprise_id', Auth::user()->enterprise_id)->delete();
          return "Parent Company deleted successfully";
    }
  }

}
