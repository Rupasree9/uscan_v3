<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Auth;
use DateTime;

class user_controller extends Controller
{
  public function save_role(Request $request)
{
  $newrole = new App\role;
  $newrole->role_name = $request->role_name;
  $newrole->role_desc = $request->role_desc;
  $newrole->enterprise_id= Auth::user()->enterprise_id;
  $newrole-> save();
  $newRoleId = $newrole->id;

  $assignedright = $request->assigned_right;
                foreach ($assignedright as $value) {
                  $role_right = new App\role_right;
                  $role_right->right_id = $value;
                  $role_right->role_id = $newRoleId;
                  $role_right->enterprise_id= Auth::user()->enterprise_id;
                  $role_right->save();
          }

   return "New Role Added Sucessfully";
}

  //  public function get_role(){
  //  $user_role = App\role::all()->toArray();
  //  return $user_role;
  // }

  public function get_role(){
        $user_role = \DB::table('role')->where('enterprise_id',Auth::user()->enterprise_id)->get();
        return $user_role;
 }

  public function get_right(){
    // $user_right = App\right::all()->toArray();
    $user_role = \DB::table('rights')->where('enterprise_id',Auth::user()->enterprise_id)->get();
    return $user_right;
  }

  public function get_right_for_role(Request $request){
    $role_id = $request->role_id;
    $data = \DB::select("select rlRt.right_id, rlRt.role_id, rt.right_name FROM role_right AS rlRt JOIN rights AS rt ON rlRt.right_id = rt.right_id WHERE rlRt.role_id= '$role_id'");
    return $data;
  }

  public function save_usergroup(Request $request){
    $newUserGrp = new App\user_group;
    $newUserGrp->user_group_name = $request->user_group_name;
    $newUserGrp->role_id = $request->role_id;
    $newUserGrp->enterprise_id= Auth::user()->enterprise_id;
    $newUserGrp-> save();
    return "New user group added successfully";
  }

  public function get_user_group(){
    // $user_group = App\user_group::all()->toArray();
    $user_group = \DB::table('user_group')->where('enterprise_id',Auth::user()->enterprise_id)->get();
    return $user_group;
    //Pavan
  }

  public function get_role_by_groupId(Request $request){
    $group_id = $request->group_id;

    // $group_name = \DB::select("select ug.user_group_name FROM user_groups AS ug ON rl.role_id = ug.role_id WHERE ug.group_id= '$group_id'");
    $data = \DB::select("select rl.role_name,rl.role_id FROM role AS rl JOIN user_group AS ug ON rl.role_id = ug.role_id WHERE ug.group_id= '$group_id'");
    return $data;
  }


  // public function get_role_by_groupId(Request $request){
  //   $group_name = $request->group_name;
  //
  //   $data = \DB::select("select rl.role_name,rl.role_id FROM role AS rl JOIN user_groups AS ug ON rl.role_id = ug.role_id WHERE ug.user_group_name= '$group_name'");
  //   return $data;
  // }

  public function save_user(Request $request){
    $query = 'select current_no from number_range where table_name = "user" and enterprise_id = '.Auth::user()->enterprise_id;
    $result = \DB::select($query);
    $user_id = $result[0]->current_no + 1;

    $update_current_no =   \DB::table('number_range')
    ->where('table_name',  'user')
    ->limit(1)
    ->update(array('current_no' => $user_id));

    $newUser = new App\user;
    $newUser->user_id = $user_id;
    $newUser->user = $request->user_id;
    $newUser->first_name = $request->first_name;
    $newUser->last_name = $request->last_name;
    $newUser->password = bcrypt($request->password);
    if ($request->password_expires != "")
    {
        $newUser->password_expires = $request->password_expires;
    }
    else {
        $newUser->password_expires = "9999-12-31";
    }
    if ($request->valid_till != "")
    {
        $newUser->valid_till = $request->valid_till;
    }
    else {
          $newUser->valid_till = "9999-12-31";
    }
    $newUser->active = $request->active;
    $newUser->role_id = $request->role_id;
    $newUser->group_id = $request->group_id;
    $newUser->enterprise_id= Auth::user()->enterprise_id;
    $newUser->created_by = Auth::user()->email;
    $newUser->updated_by = Auth::user()->email;

    $newUser-> save();
    return "New user added successfully";
  }

  public function get_user(Request $request){
    $data = \DB::select("select user.*,rl.role_name,grpname.group_name,grpname.group_id FROM user JOIN role AS rl ON user.role_id = rl.role_id JOIN user_group AS grpname ON user.group_id = grpname.group_id where user.deleted_at IS NULL;");
    return $data;
  }

  // public function get_user_group(Request $request){
  //   $data = \DB::select("select user_groups.*,rl.role_name FROM user_groups JOIN role AS rl ON user_groups.role_id=rl.role_id");
  //   return $data;
  // }

  public function delete_role(Request $request)
  {
    $role_id = $request->role_id;

    $data = \DB::select("delete from role where role_id=' $role_id ' and enterprise_id = ".Auth::user()->enterprise_id);
    return "Data Deleted Sucessfully";

  }

  public function fetch_user_id(Request $request)
  {
    $user_id = $request->user_id;
    $results = array();
    $results;
    $query = "select user from user where user LIKE '%$user_id%' and enterprise_id = ".Auth::user()->enterprise_id;
    $res   = \DB::select($query);
    foreach ($res as $query)
      {
          array_push($results,$query->user);
      }
    return $results;
  }

  public function fetch_first_name(Request $request)
  {
    $first_name = $request->first_name;
    $results = array();
    $results;
    $query = "select first_name from user where first_name LIKE '%$first_name%' and enterprise_id = ".Auth::user()->enterprise_id;
    $res   = \DB::select($query);
    foreach ($res as $query)
      {
          array_push($results,$query->first_name);
      }
    return $results;
  }

  public function fetch_last_name(Request $request)
  {
    $last_name = $request->last_name;
    $results = array();
    $results;
    $query = "select last_name from user where last_name LIKE '%$last_name%' and enterprise_id = ".Auth::user()->enterprise_id;
    $res   = \DB::select($query);
    foreach ($res as $query)
      {
          array_push($results,$query->last_name);
      }
    return $results;
  }

  public function show(Request $request)
  {
      $user_id_search = $request->user_id_search;
      $first_name_search =$request->first_name_search;
      $last_name_search =$request->last_name_search;
      $query_data = "";
      if($user_id_search != "")
      {
        $query_data = " user.user = '". $user_id_search. "' and ";
      }
      if($first_name_search!="")
      {
        $query_data =$query_data . " user.first_name = '" . $first_name_search."' and ";
      }
      if($last_name_search!="")
      {
        $query_data =$query_data . "   user.last_name = '" . $last_name_search."' and ";
      }
      $query_data = substr($query_data, 0, -4);
      $data = \DB::select("select user.*,rl.role_name,grpname.group_name,grpname.group_id FROM user JOIN role AS rl ON user.role_id = rl.role_id JOIN user_group AS grpname ON user.group_id = grpname.group_id where user.deleted_at IS NULL and " .$query_data );
      if (empty($data)) {
          return "0";
      }
      else {
        return $data;
      }
  }

  public function update(Request $request)
  {
         $updated_at = date('Y-m-d H:i:s', time());

         $data =   \DB::table('user')
        ->where('user_id',  $request->user_id)
        ->where('user.enterprise_id',Auth::user()->enterprise_id)
        ->limit(1)
        ->update(array('user' => $request->user,
        'first_name'  => $request->first_name,
        'last_name'  => $request->last_name,
        'password'  => bcrypt($request->password),
        'password_expires'  => $request->password_expires,
        'valid_till'  => $request->valid_till,
        'role_id'  => $request->role_id,
        'group_id'  => $request->group_id,
        'active' => $request->active,
        'updated_at' => $updated_at,
        'updated_by' => Auth::user()->email,
  ));

     return $data;
  }

  public function destroy(Request $request)
  {
     $user_id = $request->user_id;
     $deleted_at = $request->deleted_at;
     $data = \DB::select("update user set deleted_at='$deleted_at',active = 0  where user_id='$user_id' and enterprise_id = ". Auth::user()->enterprise_id);
    //  $data = \DB::select("delete from plant_address where plant_id='$plant_id' and enterprise_id = 1");
    //  $data = \DB::select("delete from plant where plant_id='$plant_id'");
     return $data;
  }

  public function validate_user_name(Request $request)
  {
    $user_name=  \DB::table('user')->where('active','=', '1')
    ->where('user.enterprise_id',Auth::user()->enterprise_id)
    ->where('user.user',$request->user_name)
    ->where('user.deleted_at',NULL)
    ->get();
     // return $cust_name;
     if (count($user_name) > 0)
       {
         return "false";
       }
       else {
         return "true";
       }


  }

  public function validate_user_name_change(Request $request)
  {

    if($request->user_id == $request->user_name_valid)
    {
      return "true";
    }
    else {
      # code...

    $user_name=  \DB::table('user')->where('active','=', '1')
    ->where('user.enterprise_id',Auth::user()->enterprise_id)
    ->where('user.user',$request->user_id)
    ->where('user.deleted_at',NULL)
    ->get();
     // return $cust_name;
     if (count($user_name) > 0)
       {
         return "false";
       }
       else {
         return "true";
       }

     }


  }


}
