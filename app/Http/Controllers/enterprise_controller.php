<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\enterprise_model;
use App;
use Auth;

class enterprise_controller  extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function enterprise_create_index()
    {
        return view("enterprise_create");
    }

    public function enterprise_show_all(Request $request)
    {
      $data = \DB::table('enterprise')
      ->select(
        'enterprise.enterprise_id',
        'enterprise.created_at',
        'enterprise.updated_at',
        'enterprise_name',
        'tin_number',
        'duns_number',
        'license_number',
        'active',
        'created_by',
        'updated_by',
        'soldto_street',
        'soldto_city',
        'soldto_state',
        'soldto_country',
        'soldto_region',
        'soldto_zip_code',
        'billto_street',
        'billto_city',
        'billto_state',
        'billto_country',
        'billto_region',
        'billto_zip_code',
        'primary_contact_name',
        'primary_contact_email_id',
        'primary_contact_number',
        'secondary_contact_name',
        'secondary_contact_email_id',
        'contact_id',
        'secondary_contact_number')
     ->leftJoin('enterprise_address', 'enterprise_address.enterprise_id', '=', 'enterprise.enterprise_id')
     ->leftJoin('enterprise_contact', 'enterprise_contact.enterprise_id', '=', 'enterprise.enterprise_id')
     ->orderBy('enterprise.enterprise_name', 'asc')
      ->get();
     return $data;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }



        public function fetch_enterprise_name(Request $request)
        {
          $enterprise_name = $request->enterprise_name;
          $results = array();
          $results;
          $query = "select distinct enterprise_name from enterprise where enterprise_name LIKE '%$enterprise_name%'";
          $res   = \DB::select($query);
          foreach ($res as $query)
            {
                array_push($results,$query->enterprise_name);
            }
          return $results;
        }


        public function fetch_duns_number_name(Request $request)
        {
          $duns_number_name = $request->duns_number_name;
          $results = array();
          $results;
          $query = "select distinct duns_number from enterprise where duns_number LIKE '%$duns_number_name%'";
          $res   = \DB::select($query);
          foreach ($res as $query)
            {
                array_push($results,$query->duns_number);
            }
          return $results;
        }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


      $query = 'select current_no from number_range where table_name = "enterprise"';
      $result   = \DB::select($query);
      $enterprise_id  = $result[0]->current_no +1;

      $data =   \DB::table('number_range')
      ->where('table_name','enterprise')
      ->limit(1)
      ->update(array('current_no' => $enterprise_id
        ));


            $enterprise = new App\enterprise_model;
            $enterprise->enterprise_name = $request->enterprise_name;
            $enterprise->enterprise_id = $enterprise_id;
            $enterprise->tin_number = $request->tin_number;
            $enterprise->duns_number = $request->duns_number;
            $enterprise->license_number = $request->license_number;
            $enterprise->active = $request->active;
            $enterprise->created_by = 'superuser';
            $enterprise->updated_by = 'superuser';
            $enterprise->save();

            $address = new App\enterprise_address;
            $address->soldto_street = $request->soldto_street;
            $address->soldto_city = $request->soldto_city;
            $address->soldto_state = $request->soldto_state;
            $address->soldto_country = $request->soldto_country;
            $address->soldto_region =$request->soldto_region;
            $address->soldto_zip_code =$request->soldto_zip_code;
            $address->enterprise_id = $enterprise_id;

            $address->billto_street = $request->billto_street;
            $address->billto_city = $request->billto_city;
            $address->billto_state = $request->billto_state;
            $address->billto_country = $request->billto_country;
            $address->billto_region =$request->billto_region;
            $address->billto_zip_code =$request->billto_zip_code;
            $address->enterprise_id = $enterprise_id;
            $address->save();

            $contact = new App\enterprise_contact;
            $contact->primary_contact_name = $request->p_contact_name;
            $contact->primary_contact_email_id = $request->p_contact_email;
            $contact->primary_contact_number = $request->p_contact_no;
            $contact->secondary_contact_name = $request->s_contact_name;
            $contact->secondary_contact_email_id = $request->s_contact_email;
            $contact->secondary_contact_number = $request->s_contact_no;
            $contact->enterprise_id =$enterprise_id;
            $contact->save();

          //  return "New Enterprise Added Sucessfully";

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function enterprise_search(Request $request)
    {

      $enterprise_name = $request->enterprise_name;
      $duns_number = $request->duns_number;

      $query_data = "";

      if($enterprise_name!="")
       {
         $query_data =$query_data . " enterprise_name = '" . $enterprise_name."' and ";
       }
       if($duns_number!="")
        {
          $query_data =$query_data . " duns_number = '" . $duns_number."' and ";
        }

          $query_data = substr($query_data, 0, -4);
          //  $data = \DB::select("select * from enterprise where " .$query_data);

          $data = \DB::select("select
                    enterprise.enterprise_id,
                    enterprise.created_at,
                    enterprise.updated_at,
                    enterprise_name,
                    tin_number,
                    duns_number,
                    license_number,
                    active,
                    created_by,
                    updated_by,
                    soldto_street,
                    soldto_city,
                    soldto_state,
                    soldto_country,
                    soldto_region,
                    soldto_zip_code,
                    billto_street,
                    billto_city,
                    billto_state,
                    billto_country,
                    billto_region,
                    primary_contact_name,
                    primary_contact_email_id,
                    primary_contact_number,
                    secondary_contact_name,
                    secondary_contact_email_id,
                    secondary_contact_number,
                    contact_id,
                    billto_zip_code from enterprise,enterprise_address  ,enterprise_contact  where  enterprise.`enterprise_id` = enterprise_address.`enterprise_id` and enterprise.`enterprise_id` = enterprise_contact.`enterprise_id` and $query_data");
         if(empty($data))
         {
           return "0";
         }
         else {
           return $data;
         }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function update(Request $request)
     {

        $data =   \DB::table('enterprise')
        ->where('enterprise_id',  $request->enterprise_id)
        ->update(array('enterprise_name' => $request->enterprise_name,
        'tin_number' => $request->tin_number,
        'duns_number' => $request->duns_number,
        'license_number' => $request->license_number,
        'active' => $request->active,
        'updated_by' =>   "superuser",
        'created_by' =>    "superuser"));

        $data =   \DB::table('enterprise_address')
        ->where('enterprise_id',  $request->enterprise_id)
        ->update(array('soldto_street' => $request->soldto_street,
        'billto_street' => $request->billto_street,
        'soldto_city' => $request->soldto_city,
        'billto_city' => $request->billto_city,
        'soldto_state' => $request->soldto_state,
        'billto_state' => $request->billto_state,
        'soldto_country' => $request->soldto_country,
        'billto_country' => $request->billto_country,
        'soldto_zip_code' => $request->soldto_zip_code,
        'billto_zip_code' => $request->billto_zip_code,
        'billto_region' => $request->billto_region,
        'soldto_region' => $request->soldto_region));

        $data =   \DB::table('enterprise_contact')
        ->where('enterprise_id',  $request->enterprise_id)
        ->update(array('primary_contact_name' => $request->p_contact_name,
        'primary_contact_email_id' => $request->p_contact_email,
        'primary_contact_number' => $request->p_contact_no,
        'secondary_contact_name' => $request->s_contact_name,
        'secondary_contact_email_id' => $request->s_contact_email,
        'secondary_contact_number' => $request->s_contact_no));

         return "Enterprise Updated Sucessfully";
        //  return $data;


     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     //
    // }

    public function destroy(Request $request)
    {
      // $enterprise_id = $request->enterprise_id;

      // $data = \DB::select("delete from enterprise where enterprise_id ='$enterprise_id'");
      // return "Data Deleted Sucessfully";

      // $data = \DB::select("delete from enterprise_address where enterprise_id='$enterprise_id'");
      // $data = \DB::select("delete from enterprise_contact where enterprise_id='$enterprise_id'");
      // $data = \DB::select("delete from enterprise where customer_id='$customer_id' and enterprise_id = '1' ");
      // return "Data Deleted Sucessfully";

    }
}
