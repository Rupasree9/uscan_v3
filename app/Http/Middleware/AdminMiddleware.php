<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

      if (Auth::check())
        {
           // The user is logged in...
          //  return redirect('home');

           if((Auth::user()->group_id == '1') or (Auth::user()->group_id == '2') )
           {
            //  return redirect('home');
             return $next($request);
           }
           else {
             # code...
             return redirect('home');
           }

            // return $next($request);



        }
        else {
          # code...
          return view('auth/login');
          // return view('welcome');
        }



    }
}
