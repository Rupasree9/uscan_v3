<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class enterprise_model extends Model
{
    use SoftDeletes;
    protected $table = 'enterprise';
    protected $dates = ['deleted_at'];
}
