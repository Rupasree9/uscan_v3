<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class user extends Model
{
    use SoftDeletes;
    protected $table = 'user';
    protected $dates = ['deleted_at'];
}
