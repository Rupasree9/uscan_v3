<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mail_connection extends Model
{
    //
    protected $table = 'mail_connections';
    protected $dates = ['deleted_at'];
}
