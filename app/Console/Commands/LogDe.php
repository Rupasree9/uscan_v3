<?php

namespace App\Console\Commands;
use App\classes;
use App;
use Illuminate\Console\Command;

class LogDe extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string`
     */
    protected $signature = 'log:de {job_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pavan';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // \Log::info('Pavan daily');
        $job_id = $this->argument('job_id');
        \Log::info("pavan job_id: ".$job_id);

        $query = "select current_no from number_range where table_name = 'process_monitor' and enterprise_id = '0010000002'";
        $result = \DB::select($query);
        $log_id = $result[0]->current_no + 1;

        $update_current_no =   \DB::table('number_range')
        ->where('table_name',  'process_monitor')
        ->limit(1)
        ->update(array('current_no' => $log_id));

        $date_time_for_mysql_log = date('Y-m-d H:i:s');

        $query_task_report = 'SELECT A.job_id,A.task_id,A.enterprise_id,A.job_type
                              FROM job A
                    		      WHERE A.active   = 1
                              AND A.job_id     = '.$job_id.'';
          $schedule_job_task_report = \DB::select($query_task_report);
          $job_type[0] =  $schedule_job_task_report[0]->job_type;


          //Get report or task
          if($job_type[0] == "T"){
                          $query_job = 'SELECT A.job_id,A.task_id,A.enterprise_id,B.customer_id,B.program_id,C.portal_id,C.portal_user_id,C.portal_password,C.portal_url,D.path,D.details
                                        FROM job A,task B,portal C, program D
										                    WHERE A.active   = 1
                                        AND A.task_id = B.task_id
                                        AND B.portal_id=C.portal_id
                                        AND B.program_id= D.program_id
                                        AND A.job_id = '.$job_id.'';
                                        $schedule_job_details = \DB::select($query_job);
                                        $customer_id[0] =  $schedule_job_details[0]->customer_id;
                                        $task_id[0] =  $schedule_job_details[0]->task_id;
                                        $enterprise_id[0] =  $schedule_job_details[0]->enterprise_id;
                                        $portal_user_id[0] =  $schedule_job_details[0]->portal_user_id;
                                        $portal_password[0] =  $schedule_job_details[0]->portal_password;
                                        $path[0] =  $schedule_job_details[0]->path;
                                        $details[0] =  $schedule_job_details[0]->details;

                            $log = new App\process_monitor_table;
                            $log->log_id = $log_id;
                            $log->reference_id = $task_id[0];
                            $log->referece_type = 'Task';
                            $log->state = 'Running';
                            $log->start_date = $date_time_for_mysql_log;
                            $log->end_date = $date_time_for_mysql_log;
                            $log->enterprise_id = $enterprise_id[0];
                            $log->save();

                            // $program = new $path[0]();
                            // $program_function = $program->$details[0]($job_id,$log_id,$portal_user_id[0],$portal_password[0],$task_id[0],$enterprise_id[0]);
                            // \Log::info($gm_bidlist_new);

                            if($customer_id[0] == '0030000002')
                            {
                                $gm_bidlist = new gm_bidlist();
                                $gm_bidlist_new = $gm_bidlist->get_gm_bidlist($job_id,$log_id,$portal_user_id[0],$portal_password[0],$task_id[0],$enterprise_id[0]);
                                \Log::info($gm_bidlist_new);
                            }
                            elseif ($customer_id[0] == '0030000003') {
                                $fca_production_program = new fca_production_program();
                                $fca_production_program_new = $fca_production_program->get_monthly_supplier_scorecard_production($job_id,$log_id,$portal_user_id[0],$portal_password[0],$task_id[0],$enterprise_id[0]);
                                \Log::info($fca_production_program_new);
                            }
                            elseif ($customer_id[0] == '0030000004') {
                              $fca_mopar_program = new fca_mopar_program();
                              $fca_mopar_program_new = $fca_mopar_program->get_monthly_supplier_scorecard_mopar($job_id,$log_id,$portal_user_id[0],$portal_password[0],$task_id[0],$enterprise_id[0]);
                              \Log::info($fca_mopar_program_new);
                            }
                            elseif ($customer_id[0] == '0030000005') {
                                $ford_program = new ford_program();
                                $ford_program_new = $ford_program->get_ford_q1_scores($job_id,$log_id,$portal_user_id[0],$portal_password[0],$task_id[0],$enterprise_id[0]);
                                \Log::info($ford_program_new);
                            }
                            elseif ($customer_id[0] == '0030000006') {
                                $psa = new psa();
                                $psa_bidlist_new = $psa->get_psa_bidlist($job_id,$log_id,$portal_user_id[0],$portal_password[0],$task_id[0],$enterprise_id[0]);
                                \Log::info($psa_bidlist_new);
                            }
        }
        elseif ($job_type[0] == "R") {



              $query_job = 'SELECT A.job_id,A.task_id,A.enterprise_id
                            FROM job A
                            WHERE A.active   = 1
                            AND A.job_id = '.$job_id.'';
                            $schedule_job_details = \DB::select($query_job);
                            $task_id[0] =  $schedule_job_details[0]->task_id;
                            $enterprise_id[0] =  $schedule_job_details[0]->enterprise_id;

                            $log = new App\process_monitor_table;
                            $log->log_id = $log_id;
                            $log->reference_id = $task_id[0];
                            $log->referece_type = 'Report';
                            $log->state = 'Running';
                            $log->start_date = $date_time_for_mysql_log;
                            $log->end_date = $date_time_for_mysql_log;
                            $log->enterprise_id = '0010000002';
                            $log->save();

                  $report_scheduler = new report_scheduler();
                  $report_scheduler_new = $report_scheduler->report_generator($job_id,$log_id,$task_id[0],$enterprise_id[0]);
                  \Log::info($report_scheduler_new);
                  // \Log::info("Report Type");
        }


    }
}
