<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\send_mail;

class distribution_strategy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dist:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        //echo "hello";
    }
    public function distribution_strategy_check(string $task_id,string $enterprise_id,string $subject,string $path,string $FileName)
    {

      // $task_id = "60000017";
      // $enterprise_id = "0010000002";
      // $subject = 'Scheduled Task';

      $distribution_strategy_details = DB::table('distribution_strategy')->where('enterprise_id', '=',$enterprise_id)->where('task_id', '=',$task_id)->get();

      if(count($distribution_strategy_details))
      {

           foreach ($distribution_strategy_details as $row) {

                 if($row->mail_type == 'group')
                 {

                   $mail_users_group = array();
                   $mail_enterprise_users_group = DB::table('mail_enterprise_users')->select('ent_user_id')->where('enterprise_id', '=',$enterprise_id)->where('user_group_id', '=',$row->mail_address)->get();

                   foreach ($mail_enterprise_users_group as $mail_user_group)
                   {

                     array_push($mail_users_group, $mail_user_group->ent_user_id);

                   }

                  // $path    = "C:\\xampp\htdocs\uscan\public\crawler\documents\\ford\\ford_q1_scores\\Test Data.xls";
                  Mail::to($mail_users_group)->send(new send_mail($path,$subject,$FileName));
                  return "Mail Sent Successfully";

                 }
                 elseif ($row->mail_type == 'personnel') {
                   # code...
                  //  echo "inside personnel";
                   $mail_users_personnel = array();

                   $mail_enterprise_users_personnel = DB::table('mail_enterprise_users')->select('ent_user_id')->where('enterprise_id', '=',$enterprise_id)->where('mail_comm_id', '=',$row->mail_address)->get();

                   foreach ($mail_enterprise_users_personnel as $mail_user_personnel)
                   {

                     array_push($mail_users_personnel, $mail_user_personnel->ent_user_id);

                   }

                  //  $path    = "C:\\xampp\htdocs\uscan\public\crawler\documents\\ford\\ford_q1_scores\\Test Data.xls";
                   Mail::to($mail_users_personnel)->send(new send_mail($path,$subject,$FileName));
                   return "Mail Sent Successfully";
                 }
           }
      }

      else {

        return "";

      }

    }
}
