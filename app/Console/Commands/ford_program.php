<?php

namespace App\Console\Commands;
// namespace App\Console\Commands\PHPExcel;
// namespace App\Console\Commands\PHPExcel\IOFactory;
// use App\Console\Commands\PHPExcel;
// use App\Console\Commands\PHPExcel\IOFactory;
use Illuminate\Console\Command;
use Excel;

class ford_program extends Command
{

      /**
       * The name and signature of the console command.
       *
       * @var string
       */
      protected $signature = 'command:name';

      /**
       * The console command description.
       *
       * @var string
       */
      protected $description = 'Command description';

      /**
       * Create a new command instance.
       *
       * @return void
       */
      public function __construct()
      {
          parent::__construct();
      }

      /**
       * Execute the console command.
       *
       * @return mixed
       */
      public function handle()
      {
          //
      }

      public function clean($string)
    	{
    	$string = str_replace("'", ' ', $string); // Replaces all spaces with hyphens.
    	return str_replace('"', ' ', $string);
    	}

      public function get_ford_q1_scores(string $job_id,string $log_id,string $user_name_parameter,string $password_parameter,string $task_id,string $enterprise_id) {

        // include('../../general/db_connect.php');
        // include('../../general/log_functions.php');
        // include('../../general/simple_html_dom.php');
        // include('../../general/PHPExcel/IOFactory.php');

        $process_monitor_log_update = new process_monitor_log_update();

        include('/var/www/uscan/uscan/public/crawler/general/db_connect.php');
        include('/var/www/uscan/uscan/public/crawler/general/log_functions.php');
        include('/var/www/uscan/uscan/public/crawler/general/simple_html_dom.php');
        require_once('/var/www/uscan/uscan/public/crawler/general/PHPExcel/IOFactory.php');


        $cookie              = "cookie.txt";
        $date_time_file  = date('Y-m-d His');
        $step_no             = 0;
        $log_id              = $log_id;
        $enterprise_id       = "0010000002";
        $login_user = $user_name_parameter;
        $login_password = $password_parameter;


        $step_no     = $step_no + 1;
        $description = "Task Started";
        $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

        set_time_limit(0);

        $currTime = time();
        $data_login = array(
        	'auth_mode' => 'basic',
        	'langSelect' => '1',
        	'submitTime' => $currTime,
        	'user' => $login_user,
        	'password' => $login_password,
        	'login' => 'Login'
        );
        $nav_url = 'https://us.sso.covisint.com/sso?cmd=LOGIN';
        $fi_login = http_build_query($data_login);

              $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL, $nav_url);
              curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
              curl_setopt($ch, CURLOPT_POST, true);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $fi_login);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_COOKIESESSION, true);
              curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name'); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
              curl_setopt($ch, CURLOPT_TIMEOUT, 180);
              $answer = curl_exec($ch);

              $html_login_check = str_get_html($answer);
              $check = $html_login_check->find('.whatsNewTitleSm',0);

              if($check)
              {
              			$pos = strpos($check->innertext, "invalid password ");

              			if ($pos) {

              				$step_no = $step_no + 1;
              				$description = "Incorrect USER ID or PASSWORD Login Failed";

                      $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              				$step_no = $step_no + 1;
              				$description = "Task Completed";

                      $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

                      update_log_state($log_id,'Failed');

                      $GLOBALS['conn_master']->close();
                      $GLOBALS['conn_transaction']->close();
              				curl_close($ch);
              				exit;

              			}

              }
              else
              {
              // echo $answer;

              $status = curl_getinfo($ch);

              if ($status['http_code'] == '200' or $status['http_code'] == '302')
              	{
              	  $status_info = "Successful";

              		$step_no = $step_no + 1;
              		$description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url;
              		$process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              		$step_no = $step_no + 1;
              		$description = "Login Successful";
              		$process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);


              	}
                else
              	{
              	$status_info = "Failed";

              	$step_no = $step_no + 1;
              	$description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url;

                $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              	$step_no = $step_no + 1;
              	$description = "Login Failed";
                $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);
                update_log_state($log_id,'Failed');
                curl_close($ch);
              	exit;

              	}

              // echo "</br>Status :- $status_info &nbsp &nbsp Response code :- $status[http_code] &nbsp &nbsp";
              // echo "URL :- $nav_url </br>";



                if (curl_error($ch))
              	{
              	echo curl_error($ch);
              	}

              // another request preserving the session

              $nav_url_1 = 'https://portal.covisint.com/web/portal/home';
              curl_setopt($ch, CURLOPT_URL, $nav_url_1);
              curl_setopt($ch, CURLOPT_POST, false);
              curl_setopt($ch, CURLOPT_POSTFIELDS, "");
              curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
              curl_setopt($ch, CURLOPT_TIMEOUT, 180);
              $answer1 = curl_exec($ch);

              // echo $answer1;

              if (curl_error($ch))
              	{
              	echo curl_error($ch);
              	}

              $status = curl_getinfo($ch);

              if ($status['http_code'] == '200' or $status['http_code'] == '302')
              	{
              	$status_info = "Successful";
              	}
                else
              	{
              	$status_info = "Failed";
              	}

              // echo "</br>Status :- $status_info &nbsp &nbsp Response code :- $status[http_code] &nbsp &nbsp";
              // echo "URL :- $nav_url_1 </br>";

              $step_no = $step_no + 1;
              $description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url_1;
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              $nav_url_2 = 'https://fim.covisint.com/ap/ford?TARGET=https://www.sim.ford.com';
              curl_setopt($ch, CURLOPT_URL, $nav_url_2);
              curl_setopt($ch, CURLOPT_HEADER, true);
              curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
              curl_setopt($ch, CURLOPT_COOKIESESSION, true);
              curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name'); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
              curl_setopt($ch, CURLOPT_TIMEOUT, 180);
              $answer2 = curl_exec($ch);

              // echo $answer2;

              if (curl_error($ch))
              	{
              	echo curl_error($ch);
              	}

              $status = curl_getinfo($ch);

              if ($status['http_code'] == '200' or $status['http_code'] == '302')
              	{
              	$status_info = "Successful";
              	}
                else
              	{
              	$status_info = "Failed";
              	}

              // echo "</br>Status :- $status_info &nbsp &nbsp Response code :- $status[http_code] &nbsp &nbsp";
              // echo "URL :- $nav_url_2 </br>";

              $step_no = $step_no + 1;
              $description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url_2;
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);
              $html_str = str_get_html($answer2);
              $saml_str = $html_str->find('input', 0);
              $target_str = $html_str->find('input', 1);
              $saml = $saml_str->value;
              $target = $target_str->value;
              $data = array(
              	'SAMLResponse' => $saml,
              	'TARGET' => $target
              );
              $fi = http_build_query($data);
              $nav_url_3 = 'https://www.wsl-federation.ford.com/Federation/Redirect/Covisint';
              curl_setopt($ch, CURLOPT_URL, $nav_url_3);
              curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
              curl_setopt($ch, CURLOPT_POST, true);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $fi);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_COOKIESESSION, true);
              curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name'); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
              curl_setopt($ch, CURLOPT_TIMEOUT, 180);
              $answer3 = curl_exec($ch);

              // echo $answer3;

              if (curl_error($ch))
              	{
              	echo curl_error($ch);
              	}

              $status = curl_getinfo($ch);

              if ($status['http_code'] == '200' or $status['http_code'] == '302')
              	{
              	$status_info = "Successful";
              	}
                else
              	{
              	$status_info = "Failed";
              	}

              // echo "</br>Status :- $status_info &nbsp &nbsp Response code :- $status[http_code] &nbsp &nbsp";
              // echo "URL :- $nav_url_3 </br>";

              $step_no = $step_no + 1;
              $description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url_3;
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);
              $html_str_1 = str_get_html($answer3);

              if (($html_str_1->find("#FSNloginUserIdLabel", 0)))
              	{
              	$action_span = $html_str_1->find("#FSNloginHeaderText4", 0);
              	$a_tag = $action_span->find("a", 0);
              	$nav_url_4 = $a_tag->href;
              	curl_setopt($ch, CURLOPT_URL, $a_tag->href);
              	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
              	curl_setopt($ch, CURLOPT_HEADER, false);
              	curl_setopt($ch, CURLOPT_HTTPGET, true);
              	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              	curl_setopt($ch, CURLOPT_COOKIESESSION, true);
              	curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name'); //could be empty, but cause problems on some hosts
              	curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie); //could be empty, but cause problems on some hosts
              	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
              	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
              	curl_setopt($ch, CURLOPT_TIMEOUT, 180);
              	$answer_temp = curl_exec($ch);

              	// echo $answer_temp;

              	if (curl_error($ch))
              		{
              		echo curl_error($ch);
              		}

              	$status = curl_getinfo($ch);
              	if ($status['http_code'] == '200' or $status['http_code'] == '302')
              		{
              		$status_info = "Successful";
              		}
              	  else
              		{
              		$status_info = "Failed";
              		}

              	// echo "</br>Status :- $status_info &nbsp &nbsp Response code :- $status[http_code] &nbsp &nbsp";
              	// echo "URL :- $nav_url_4 </br>";

              	$step_no = $step_no + 1;
              	$description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url_4;
                $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);
              	$html_str_ref = str_get_html($answer_temp);
              	$saml_str_ref = $html_str_ref->find('input', 0);
              	$target_str_ref = $html_str_ref->find('input', 1);
              	$redirect_ref = $html_str_ref->find('input', 2);
              	$saml_str_ref = $saml_str_ref->value;
              	$target_str_ref = $target_str_ref->value;
              	$redirect_ref = $redirect_ref->value;
              	$data_ref = array(
              		'SAMLResponse' => $saml_str_ref,
              		'TARGET' => $target_str_ref,
              		'redirect' => $redirect_ref
              	);
              	$fi_ref = http_build_query($data_ref);
              	$nav_url_5 = 'https://www.wsl-federation.ford.com/Federation/Redirect/Covisint';
              	curl_setopt($ch, CURLOPT_URL, $nav_url_5);
              	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
              	curl_setopt($ch, CURLOPT_POST, true);
              	curl_setopt($ch, CURLOPT_POSTFIELDS, $fi_ref);
              	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              	curl_setopt($ch, CURLOPT_COOKIESESSION, true);
              	curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name'); //could be empty, but cause problems on some hosts
              	curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie); //could be empty, but cause problems on some hosts
              	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
              	curl_setopt($ch, CURLOPT_TIMEOUT, 180);
              	$answer_ref = curl_exec($ch);
              	$status = curl_getinfo($ch);
              	if ($status['http_code'] == '200' or $status['http_code'] == '302')
              		{
              		$status_info = "Successful";
              		}
              	  else
              		{
              		$status_info = "Failed";
              		}

              	// echo "</br>Status :- $status_info &nbsp &nbsp Response code :- $status[http_code] &nbsp &nbsp";
              	// echo "URL :- $nav_url_5 </br>";

              	$step_no = $step_no + 1;
              	$description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url_5;

                $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              	// echo $answer_ref;

              	if (curl_error($ch))
              		{
              		echo curl_error($ch);
              		}
              	}
                else
              	{
              	}

              $nav_url_6 = 'https://www.sim.ford.com/SIMNG-Web/homePagePre.do';
              curl_setopt($ch, CURLOPT_URL, $nav_url_6);
              curl_setopt($ch, CURLOPT_POST, false);
              curl_setopt($ch, CURLOPT_POSTFIELDS, "");
              curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
              curl_setopt($ch, CURLOPT_TIMEOUT, 180);
              $answer4 = curl_exec($ch);

              // echo $answer4;

              $status = curl_getinfo($ch);

              if ($status['http_code'] == '200' or $status['http_code'] == '302')
              	{
              	$status_info = "Successful";

              	$step_no = $step_no + 1;
              	$description = "www.sim.ford.com Navigation Successful";

                $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);


              	}
                else
              	{
              	$status_info = "Failed";
              	}

              // echo "</br>Status :- $status_info &nbsp &nbsp Response code :- $status[http_code] &nbsp &nbsp";
              // echo "URL :- $nav_url_6 </br>";

              $step_no = $step_no + 1;
              $description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url_6;
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              if (curl_error($ch))
              	{
              	echo curl_error($ch);
              	}

              $data_1 = array(
              	'webEntity' => 'homePageWithAccess.jsp',
              	'userSelectedViewPoint' => 'Supplier+Parent',
              	'supParntVP1' => 'F048',
              	'supParntVP2' => '',
              	'method' => 'Search',
              	'method' => 'Search',
              	'method' => 'Search',
              	'method' => 'Search',
              	'method' => 'Search',
              	'method' => 'Search'
              );
              $fi_2 = http_build_query($data_1);
              $nav_url_7 = "https://www.sim.ford.com/SIMNG-Web/homePagePost.do";
              curl_setopt($ch, CURLOPT_URL, $nav_url_7);
              curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
              curl_setopt($ch, CURLOPT_POST, true);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $fi_2);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_COOKIESESSION, true);
              curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name'); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
              curl_setopt($ch, CURLOPT_MAXREDIRS, 20);
              curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
              curl_setopt($ch, CURLOPT_TIMEOUT, 180);
              $answer5 = curl_exec($ch);

              // echo $answer5;

              $status = curl_getinfo($ch);

              if ($status['http_code'] == '200' or $status['http_code'] == '302')
              	{
              	$status_info = "Successful";
              	}
                else
              	{
              	$status_info = "Failed";
              	}

              // echo "</br>Status :- $status_info &nbsp &nbsp Response code :- $status[http_code] &nbsp &nbsp";
              // echo "URL :- $nav_url_7 </br>";

              $step_no = $step_no + 1;
              $description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url_7;
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              if (curl_error($ch))
              	{
              	echo curl_error($ch);
              	}

              $nav_url_8 = 'https://www.sim.ford.com/SIMNG-Web/viewPointViewPreAction.do?userSelectedKeyCriteria=F048&supParentFullName=FEDERAL+MOGUL+CORP&fromSearch=true';
              curl_setopt($ch, CURLOPT_URL, $nav_url_8);
              curl_setopt($ch, CURLOPT_POST, false);
              curl_setopt($ch, CURLOPT_POSTFIELDS, "");
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_COOKIESESSION, true);
              curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name'); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
              curl_setopt($ch, CURLOPT_MAXREDIRS, 20);
              curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
              curl_setopt($ch, CURLOPT_TIMEOUT, 180);
              $answer6 = curl_exec($ch);

              // echo $answe6;

              if (curl_error($ch))
              	{
              	echo curl_error($ch);
              	}

              $status = curl_getinfo($ch);

              if ($status['http_code'] == '200' or $status['http_code'] == '302')
              	{
              	$status_info = "Successful";
              	}
                else
              	{
              	$status_info = "Failed";
              	}

              // echo "</br>Status :- $status_info &nbsp &nbsp Response code :- $status[http_code] &nbsp &nbsp";
              // echo "URL :- $nav_url_8 </br>";

              $step_no = $step_no + 1;
              $description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url_8;
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);
              $nav_url_9 = 'https://www.sim.ford.com/SIMNG-Web/q1MangementBeginAction.do?method=httpGet&viewPoint=Supplier%20Parent&code=F048';
              curl_setopt($ch, CURLOPT_URL, $nav_url_9);
              curl_setopt($ch, CURLOPT_POST, false);
              curl_setopt($ch, CURLOPT_POSTFIELDS, "");
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_COOKIESESSION, true);
              curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name'); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
              curl_setopt($ch, CURLOPT_MAXREDIRS, 20);
              curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
              curl_setopt($ch, CURLOPT_TIMEOUT, 180);
              $answer7 = curl_exec($ch);

              // echo $answer7;

              $status = curl_getinfo($ch);

              if ($status['http_code'] == '200' or $status['http_code'] == '302')
              	{
              	$status_info = "Successful";
              	}
                else
              	{
              	$status_info = "Failed";
              	}

              // echo "</br>Status :- $status_info &nbsp &nbsp Response code :- $status[http_code] &nbsp &nbsp";
              // echo "URL :- $nav_url_9 </br>";

              $step_no = $step_no + 1;
              $description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url_9;
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              if (curl_error($ch))
              	{
              	echo curl_error($ch);
              	}

              $data11 = array(
              	'loadExtendedExcelData' => 'Yes',
              	'method' => 'httpGet'
              );
              $fi11 = http_build_query($data11);
              $nav_url_10 = 'https://www.sim.ford.com/SIMNG-Web/q1MangementPreAction.do?method=httpGet&loadExtendedExcelData=Yes';
              curl_setopt($ch, CURLOPT_URL, $nav_url_10);
              curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
              curl_setopt($ch, CURLOPT_POST, true);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $fi11);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_COOKIESESSION, true);
              curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name'); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
              curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
              curl_setopt($ch, CURLOPT_TIMEOUT, 180);;
              $answer8 = curl_exec($ch);

              // echo $answer8;

              $status = curl_getinfo($ch);

              if ($status['http_code'] == '200' or $status['http_code'] == '302')
              	{
              	$status_info = "Successful";
              	}
                else
              	{
              	$status_info = "Failed";
              	}

              // echo "</br>Status :- $status_info &nbsp &nbsp Response code :- $status[http_code] &nbsp &nbsp";
              // echo "URL :- $nav_url_10 </br>";

              $step_no = $step_no + 1;
              $description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url_10;
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              if (curl_error($ch))
              	{
              	echo curl_error($ch);
              	}

              $nav_url_11 = 'https://www.sim.ford.com/SIMNG-Web/MultiTableExportServlet?multitablesId=extendedQ1MgtDetl&name=Q1ManagementExtExcelReport.xls&type=excel';
              curl_setopt($ch, CURLOPT_URL, $nav_url_11);
              curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
              curl_setopt($ch, CURLOPT_HEADER, false);
              curl_setopt($ch, CURLOPT_HTTPGET, true);
              curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
              curl_setopt($ch, CURLOPT_TIMEOUT, 180);
              $answer9 = curl_exec($ch);

              // echo $answer9;

              if (curl_error($ch))
              	{
              	echo curl_error($ch);
              	}

              $status = curl_getinfo($ch);

              if ($status['http_code'] == '200' or $status['http_code'] == '302')
              	{
              	$status_info = "Successful";
              	}
                else
              	{
              	$status_info = "Failed";
              	}

              // echo "</br>Status :- $status_info &nbsp &nbsp Response code :- $status[http_code] &nbsp &nbsp";
              // echo "URL :- $nav_url_11 </br>";

              $step_no = $step_no + 1;
              $description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url_11;
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              $inputFileName = "/var/www/uscan/uscan/public/crawler/documents/ford/ford_q1_scores/ford_" . $log_id . "_" . $date_time_file . ".xls";

              if (!(file_put_contents($inputFileName, $answer9)))
              	{
              	echo "file_put error";
              	$step_no = $step_no + 1;
              	$description = "Excel Sheet Download Failed";
              	$process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);
              	}
              	else {
              		# code...
              		$step_no = $step_no + 1;
              		$description = "Excel Sheet Downloaded Successfully";
              		$process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);
              	}
              curl_close($ch);

              //  Read your Excel workbook

                  Excel::load($inputFileName, function($reader) use (&$excel) {
                  $date_time_for_mysql_1 = date('Y-m-d H:i:s');
                  $objExcel = $reader->getExcel();
                  $sheet = $objExcel->getSheet(1);
                  $highestRow = $sheet->getHighestRow();
                  $highestColumn = $sheet->getHighestColumn();
                  echo $highestRow;
                  echo $highestColumn;
                  $record_status = "";

                  $transaction_date = date('Y-m-d H:i:s');



              // echo $highestRow;
              // echo "</br>$highestColumn</br>";
              //  Loop through each row of the worksheet in turn

              for ($row = 2; $row <= $highestRow; $row++)
              	{

              	// //  Read a row of data into an array
                // $range = 'A'.$row.':'.$highestColumn.$row;
                // $objPHPExcel->getActiveSheet()
                // ->getStyle($range)
                // ->getNumberFormat()
                // ->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );


              	$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);


              	//  Insert row data array into your database of choice here
              	// print_r($rowData[0]);

              	$rowData_0 = $this->clean($rowData[0][0]);
              	$rowData_1 = $this->clean($rowData[0][1]);
              	$rowData_2 = $this->clean($rowData[0][2]);
              	$rowData_3 = $this->clean($rowData[0][3]);
              	$rowData_4 = $this->clean($rowData[0][4]);
              	$rowData_5 = $this->clean($rowData[0][5]);
              	$rowData_6 = $this->clean($rowData[0][6]);
              	$rowData_7 = $this->clean($rowData[0][7]);
              	$rowData_8 = $this->clean($rowData[0][8]);
              	$rowData_9 = $this->clean($rowData[0][9]);
              	$rowData_10 = $this->clean($rowData[0][10]);
              	$rowData_11 = $this->clean($rowData[0][11]);
              	$rowData_12 = $this->clean($rowData[0][12]);
              	$rowData_13 = $this->clean($rowData[0][13]);
              	$rowData_14 = $this->clean($rowData[0][14]);
              	$rowData_15 = $this->clean($rowData[0][15]);
              	$rowData_16 = $this->clean($rowData[0][16]);
              	$rowData_17 = $this->clean($rowData[0][17]);
              	$rowData_18 = $this->clean($rowData[0][18]);
              	$rowData_19 = $this->clean($rowData[0][19]);
              	$rowData_20 = $this->clean($rowData[0][20]);
              	$rowData_21 = $this->clean($rowData[0][21]);
              	$rowData_22 = $this->clean($rowData[0][22]);
              	$rowData_23 = $this->clean($rowData[0][23]);
              	$rowData_24 = $this->clean($rowData[0][24]);
              	$rowData_25 = $this->clean($rowData[0][25]);
              	$rowData_26 = $this->clean($rowData[0][26]);
              	$rowData_27 = $this->clean($rowData[0][27]);
                $rowData_27 = ($rowData_27 - 25569) * 86400;
                $rowData_27 =  gmdate("d-M-Y H:i:s", $rowData_27);


              	$rowData_28 = $this->clean($rowData[0][28]);
              	$rowData_29 = $this->clean($rowData[0][29]);
              	$rowData_30 = $this->clean($rowData[0][30]);
              	$rowData_31 = $this->clean($rowData[0][31]);
              	$rowData_32 = $this->clean($rowData[0][32]);
              	$rowData_33 = $this->clean($rowData[0][33]);
                $rowData_33 = ($rowData_33 - 25569) * 86400;
                $rowData_33 =  gmdate("d-M-Y H:i:s", $rowData_33);
              	$rowData_34 = $this->clean($rowData[0][34]);
              	$rowData_35 = $this->clean($rowData[0][35]);
              	$rowData_36 = $this->clean($rowData[0][36]);
                $rowData_36 = ($rowData_36 - 25569) * 86400;
                $rowData_36 =  gmdate("d-M-Y H:i:s", $rowData_36);
              	$rowData_37 = $this->clean($rowData[0][37]);
              	$rowData_38 = $this->clean($rowData[0][38]);
              	$rowData_39 = $this->clean($rowData[0][39]);
              	$rowData_40 = $this->clean($rowData[0][40]);
              	$rowData_41 = $this->clean($rowData[0][41]);
              	$rowData_42 = $this->clean($rowData[0][42]);
              	$rowData_43 = $this->clean($rowData[0][43]);
              	$rowData_44 = $this->clean($rowData[0][44]);
              	$rowData_45 = $this->clean($rowData[0][45]);
              	$rowData_46 = $this->clean($rowData[0][46]);
                $rowData_46 = ($rowData_46 - 25569) * 86400;
                $rowData_46 =  gmdate("d-M-Y H:i:s", $rowData_46);
              	$rowData_47 = $this->clean($rowData[0][47]);
              	$rowData_48 = $this->clean($rowData[0][48]);
              	$rowData_49 = $this->clean($rowData[0][49]);
              	$rowData_50 = $this->clean($rowData[0][50]);
              	$rowData_51 = $this->clean($rowData[0][51]);
                $rowData_51 = ($rowData_51 - 25569) * 86400;
                $rowData_51 =  gmdate("d-M-Y H:i:s", $rowData_51);
              	$rowData_52 = $this->clean($rowData[0][52]);
              	$rowData_53 = $this->clean($rowData[0][53]);
              	$rowData_54 = $this->clean($rowData[0][54]);
              	$rowData_55 = $this->clean($rowData[0][55]);
              	$rowData_56 = $this->clean($rowData[0][56]);
                $rowData_56 = ($rowData_56 - 25569) * 86400;
                $rowData_56 =  gmdate("d-M-Y H:i:s", $rowData_56);
              	$rowData_57 = $this->clean($rowData[0][57]);
              	$rowData_58 = $this->clean($rowData[0][58]);
              	$rowData_59 = $this->clean($rowData[0][59]);
              	$rowData_60 = $this->clean($rowData[0][60]);
              	$rowData_61 = $this->clean($rowData[0][61]);
                $rowData_61 = ($rowData_61 - 25569) * 86400;
                $rowData_61 =  gmdate("d-M-Y H:i:s", $rowData_61);
              	$rowData_62 = $this->clean($rowData[0][62]);
              	$rowData_63 = $this->clean($rowData[0][63]);
              	$rowData_64 = $this->clean($rowData[0][64]);
              	$rowData_65 = $this->clean($rowData[0][65]);
                $rowData_65 = ($rowData_65 - 25569) * 86400;
                $rowData_65 =  gmdate("d-M-Y H:i:s", $rowData_65);
              	$rowData_66 = $this->clean($rowData[0][66]);
              	$rowData_67 = $this->clean($rowData[0][67]);
              	$rowData_68 = $this->clean($rowData[0][68]);
              	$rowData_69 = $this->clean($rowData[0][69]);
                $rowData_69 = ($rowData_69 - 25569) * 86400;
                $rowData_69 =  gmdate("d-M-Y H:i:s", $rowData_69);
              	$rowData_70 = $this->clean($rowData[0][70]);
              	$rowData_71 = $this->clean($rowData[0][71]);
              	$rowData_72 = $this->clean($rowData[0][72]);
              	$rowData_73 = $this->clean($rowData[0][73]);
              	$rowData_74 = $this->clean($rowData[0][74]);
              	$rowData_75 = $this->clean($rowData[0][75]);
              	$rowData_76 = $this->clean($rowData[0][76]);
              	$rowData_77 = $this->clean($rowData[0][77]);
              	$rowData_78 = $this->clean($rowData[0][78]);
              	$rowData_79 = $this->clean($rowData[0][79]);
              	$rowData_80 = $this->clean($rowData[0][80]);
              	$rowData_81 = $this->clean($rowData[0][81]);
              	$rowData_82 = $this->clean($rowData[0][82]);
              	$rowData_83 = $this->clean($rowData[0][83]);
              	$rowData_84 = $this->clean($rowData[0][84]);
              	$rowData_85 = $this->clean($rowData[0][85]);
              	$rowData_86 = $this->clean($rowData[0][86]);
              	$rowData_87 = $this->clean($rowData[0][87]);
              	$rowData_88 = $this->clean($rowData[0][88]);
              	$rowData_89 = $this->clean($rowData[0][89]);
              	$rowData_90 = $this->clean($rowData[0][90]);
              	$rowData_91 = $this->clean($rowData[0][91]);
              	$rowData_92 = $this->clean($rowData[0][92]);
              	$rowData_93 = $this->clean($rowData[0][93]);
              	$rowData_94 = $this->clean($rowData[0][94]);
              	$rowData_95 = $this->clean($rowData[0][95]);
              	$rowData_96 = $this->clean($rowData[0][96]);
              	$rowData_97 = $this->clean($rowData[0][97]);
              	$rowData_98 = $this->clean($rowData[0][98]);
              	$rowData_99 = $this->clean($rowData[0][99]);
              	$rowData_100 = $this->clean($rowData[0][100]);
              	$rowData_101 = $this->clean($rowData[0][101]);
              	$rowData_102 = $this->clean($rowData[0][102]);
              	$rowData_103 = $this->clean($rowData[0][103]);
              	$rowData_104 = $this->clean($rowData[0][104]);
              	$rowData_105 = $this->clean($rowData[0][105]);
              	$rowData_106 = $this->clean($rowData[0][106]);
              	$rowData_107 = $this->clean($rowData[0][107]);
              	$rowData_108 = $this->clean($rowData[0][108]);
              	$rowData_109 = $this->clean($rowData[0][109]);
              	$rowData_110 = $this->clean($rowData[0][110]);
              	$rowData_111 = $this->clean($rowData[0][111]);
              	$rowData_112 = $this->clean($rowData[0][112]);
              	$rowData_113 = $this->clean($rowData[0][113]);
              	$rowData_114 = $this->clean($rowData[0][114]);
              	$rowData_115 = $this->clean($rowData[0][115]);
              	$rowData_116 = $this->clean($rowData[0][116]);
              	$rowData_117 = $this->clean($rowData[0][117]);
              	$rowData_118 = $this->clean($rowData[0][118]);
              	$rowData_119 = $this->clean($rowData[0][119]);
              	$rowData_120 = $this->clean($rowData[0][120]);
              	$rowData_121 = $this->clean($rowData[0][121]);
              	$rowData_122 = $this->clean($rowData[0][122]);
              	$rowData_123 = $this->clean($rowData[0][123]);
              	$rowData_124 = $this->clean($rowData[0][124]);
              	$rowData_125 = $this->clean($rowData[0][125]);
              	$rowData_126 = $this->clean($rowData[0][126]);
              	$rowData_127 = $this->clean($rowData[0][127]);
              	$rowData_128 = $this->clean($rowData[0][128]);
              	$rowData_129 = $this->clean($rowData[0][129]);
              	$rowData_130 = $this->clean($rowData[0][130]);
              	$rowData_131 = $this->clean($rowData[0][131]);
                $rowData_131 = ($rowData_131 - 25569) * 86400;
                $rowData_131 =  gmdate("d-M-Y H:i:s", $rowData_131);
              	$rowData_132 = $this->clean($rowData[0][132]);
                $rowData_132 = ($rowData_132 - 25569) * 86400;
                $rowData_132 =  gmdate("d-M-Y H:i:s", $rowData_132);
              	$rowData_133 = $this->clean($rowData[0][133]);
                $rowData_133 = ($rowData_133 - 25569) * 86400;
                $rowData_133 =  gmdate("d-M-Y H:i:s", $rowData_133);
              	$rowData_134 = $this->clean($rowData[0][134]);
              	$rowData_135 = $this->clean($rowData[0][135]);
                $rowData_135 = ($rowData_135 - 25569) * 86400;
                $rowData_135 =  gmdate("d-M-Y H:i:s", $rowData_135);
              	$rowData_136 = $this->clean($rowData[0][136]);
              	$rowData_137 = $this->clean($rowData[0][137]);
              	$rowData_138 = $this->clean($rowData[0][138]);
              	$rowData_139 = $this->clean($rowData[0][139]);
              	$rowData_140 = $this->clean($rowData[0][140]);
              	$rowData_141 = $this->clean($rowData[0][141]);
              	$rowData_142 = $this->clean($rowData[0][142]);
              	$rowData_143 = $this->clean($rowData[0][143]);
              	$rowData_144 = $this->clean($rowData[0][144]);
              	$rowData_145 = $this->clean($rowData[0][145]);
              	$rowData_146 = $this->clean($rowData[0][146]);
              	$rowData_147 = $this->clean($rowData[0][147]);
              	$rowData_148 = $this->clean($rowData[0][148]);
              	$rowData_149 = $this->clean($rowData[0][149]);
              	$rowData_150 = $this->clean($rowData[0][150]);
              	$rowData_151 = $this->clean($rowData[0][151]);
              	$rowData_152 = $this->clean($rowData[0][152]);
              	$rowData_153 = $this->clean($rowData[0][153]);
              	$rowData_154 = $this->clean($rowData[0][154]);
              	$sql = "insert into uscan_transactions.ford_q1_scores(`selected_viewpoint`,`selected_code`,`selection_description`,`site_code`,`site_name`,`site_address`,`site_city`,`site_state`,`site_country`,`parent_code`,`parent_name`,`prod_sta_engineer`,`prod_sta_org`,`prod_sta_manager_cdsid`,`service_sta_engineer`,`service_sta_org`,`service_manager_cdsid`,`sta_active_y_n`,`site_q1_status`,`lead_sta_org`,`lead_sta_engineer_name`,`lead_sta_manager_name`,`lead_sta_engineer_cdsid`,`lead_sta_engineer_phone`,`prod_ppap_level`,`svc_ppap_level`,`present_q1_status`,`present_q1_status_date`,`recommended_q1_status`,`recommended_q1_status_aging`,`q1_at_risk_flag`,`override_reason`,`override_placed_by`,`q1_targ_date`,`target_date_change_count`,`target_q1_status`,`pe_date`,`raw_material_flag`,`mp_l_site_type`,`mp_l_visitation_date`,`excluded_from_q1_flag`,`q1_score`,`q1_score_incomplete_flag`,`prevents_q1attainment_flag`,`q1msa_q1_score`,`q1msa_status`,`q1msa_assessment_status_date`,`q1msa_by`,`q1msa_cap_due_date`,`q1msa_cap_status`,`q1msa_cap_review_date`,`q1msa_next_assessment_due_date`,`capable_system_q1_score`,`iso14001_q1_score`,`iso14001_status`,`iso14001_registrar`,`iso14001_valid_to_date`,`iso14001_cert_number`,`iso_ts16949_q1_score`,`iso_ts16949_status`,`iso_ts16949_registrar`,`iso_ts16949_valid_to_date`,`iso_ts16949_cert_number`,`mmog_le_q1_score`,`mmog_le_status`,`mmog_le_valid_to_date`,`mmog_le_level`,`cqi_9_compliant`,`cqi_9_status`,`cqi_9_assessment_date`,`ongoing_performance_q1_score`,`site_6m_ppm_prod_total`,`site_6m_ppm_prod_q1`,`site_6m_rej_rew_prod_q1`,`site_6m_receipts_prod_q1`,`site_ppm_prod_initial_score`,`site_ppm_prod_applied_q1_score`,`site_ppm_notes_production`,`site_6m_ppm_svc_total`,`site_6m_ppm_svc_q1`,`site_6m_rej_rew_serv_q1`,`site_6m_receipts_serv_q1`,`site_ppm_svc_initial_score`,`site_ppm_svc_applied_q1_score`,`site_ppm_notes_service`,`comm_ppm_prod_init_score`,`comm_ppm_prod_applied_q1_score`,`comm_ppm_prod_incomplete_flag`,`comm_ppm_prod_notes`,`comm_ppm_prod_comm_used`,`comm_ppm_prod_6m_ppm`,`comm_ppm_prod_q1_ppm_level`,`comm_ppm_prod_perf_vs_q1_level`,`comm_ppm_prod_months_over_limit`,`comm_ppm_prod_warnings`,`comm_ppm_prod_prevents_q1_flag`,`comm_ppm_svc_init_score`,`comm_ppm_svc_applied_q1_score`,`comm_ppm_svc_incomplete_flag`,`comm_ppm_svc_notes`,`comm_ppm_svc_comm_used`,`comm_ppm_svc_6m_ppm`,`comm_ppm_svc_q1_ppm_level`,`comm_ppm_svc_perf_vs_q1_level`,`comm_ppm_svc_months_over_limit`,`comm_ppm_svc_warnings`,`comm_ppm_svc_prevents_q1`,`dlv_prod_intial_score`,`dlv_prod_applied_q1_score`,`dlv_prod_incomplete_flag`,`dlv_prod_notes`,`dlv_prod_low_score_ship_pt`,`dlv_prod_org`,`dlv_prod_region`,`dlv_prod_warning_y_n`,`dlv_prod_consec_month_81`,`dlv_prod_risk_indicator_y_n`,`dlv_svc_intial_score`,`dlv_svc_applied_q1_score`,`dlv_svc_incomplete_flag`,`dlv_svc_notes`,`dlv_svc_low_score_ship_pt`,`dlv_svc_org`,`dlv_svc_region`,`dlv_svc_warning_y_n`,`dlv_svc_consec_month_81`,`dlv_svc_risk_indicator_y_n`,`field_service_actions_q1_score`,`stop_shipments_q1_score`,`violation_of_trust_production_q1_score`,`violation_of_trust_service_q1_score`,`sta_endorsement_date`,`plant_endorsement_date`,`fcsd_endorsement_date`,`fcsd_endorsement_waived`,`mp_l_endorsement_date`,`mp_l_endorsement_waived`,`gsdb_site_status`,`gsdb_alt_site`,`gsdb_prod_qual_role`,`gsdb_prod_mfg_role`,`gsdb_svc_qual_role`,`gsdb_svc_mfg_role`,`metrics_redirected_to_this_site`,`metrics_redirected_from_this_site`,`site_plant_mgr_name`,`site_plant_mgr_email`,`site_plant_mgr_phone`,`site_qc_mgr_name`,`site_qc_mgr_email`,`site_qc_mgr_phone`,`site_qc_mgr_fax`,`site_deliv_contact_name`,`site_deliv_contact_email`,`site_deliv_contact_phone`,`transaction_date`,`log_id`) values ('$rowData_0','$rowData_1','$rowData_2','$rowData_3','$rowData_4','$rowData_5','$rowData_6','$rowData_7','$rowData_8','$rowData_9','$rowData_10','$rowData_11','$rowData_12','$rowData_13','$rowData_14','$rowData_15','$rowData_16','$rowData_17','$rowData_18','$rowData_19','$rowData_20','$rowData_21','$rowData_22','$rowData_23','$rowData_24','$rowData_25','$rowData_26','$rowData_27','$rowData_28','$rowData_29','$rowData_30','$rowData_31','$rowData_32','$rowData_33','$rowData_34','$rowData_35','$rowData_36','$rowData_37','$rowData_38','$rowData_39','$rowData_40','$rowData_41','$rowData_42','$rowData_43','$rowData_44','$rowData_45','$rowData_46','$rowData_47','$rowData_48','$rowData_49','$rowData_50','$rowData_51','$rowData_52','$rowData_53','$rowData_54','$rowData_55','$rowData_56','$rowData_57','$rowData_58','$rowData_59','$rowData_60','$rowData_61','$rowData_62','$rowData_63','$rowData_64','$rowData_65','$rowData_66','$rowData_67','$rowData_68','$rowData_69','$rowData_70','$rowData_71','$rowData_72','$rowData_73','$rowData_74','$rowData_75','$rowData_76','$rowData_77','$rowData_78','$rowData_79','$rowData_80','$rowData_81','$rowData_82','$rowData_83','$rowData_84','$rowData_85','$rowData_86','$rowData_87','$rowData_88','$rowData_89','$rowData_90','$rowData_91','$rowData_92','$rowData_93','$rowData_94','$rowData_95','$rowData_96','$rowData_97','$rowData_98','$rowData_99','$rowData_100','$rowData_101','$rowData_102','$rowData_103','$rowData_104','$rowData_105','$rowData_106','$rowData_107','$rowData_108','$rowData_109','$rowData_110','$rowData_111','$rowData_112','$rowData_113','$rowData_114','$rowData_115','$rowData_116','$rowData_117','$rowData_118','$rowData_119','$rowData_120','$rowData_121','$rowData_122','$rowData_123','$rowData_124','$rowData_125','$rowData_126','$rowData_127','$rowData_128','$rowData_129','$rowData_130','$rowData_131','$rowData_132','$rowData_133','$rowData_134','$rowData_135','$rowData_136','$rowData_137','$rowData_138','$rowData_139','$rowData_140','$rowData_141','$rowData_142','$rowData_143','$rowData_144','$rowData_145','$rowData_146','$rowData_147','$rowData_148','$rowData_149','$rowData_150','$rowData_151','$rowData_152','$rowData_153','$rowData_154','$transaction_date','')";

                if ($GLOBALS['conn_transaction']->query($sql) === TRUE) {
                    echo " Record created successfully ";
                } else {
                    echo "Error: " . $sql . "<br />" . $GLOBALS['conn_transaction']->error;
                }


              	}
                });


              $step_no     = $step_no + 1;
              $description = "Data Inserted Successfully";
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              $subject = 'Scheduled Task';
              $FileName  = "FORD Q1 Scores_" . $log_id . "_" . $date_time_file . ".xls";
              // $path = "C:\\xampp\htdocs\uscan\public\crawler\documents\\ford\\ford_q1_scores\\Test Data.xls";

              $distribution_strategy = new distribution_strategy();
              $distribution_strategy_data = $distribution_strategy->distribution_strategy_check($task_id,$enterprise_id,$subject,$inputFileName,$FileName);

              if ($distribution_strategy_data)
              {
                $step_no = $step_no + 1;
                $description = $distribution_strategy_data;
                $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);
              }


              $step_no = $step_no + 1;
              $description = "Task Completed";
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              }

              $GLOBALS['conn_master']->close();
              $GLOBALS['conn_transaction']->close();

              // update_log_state($log_id,'Finished');
              $process_monitor_log_update_new = $process_monitor_log_update->update_log_state($log_id,'Finished');
              

              return "job executed successfully Ford Q1 Scores job id: $job_id";

      }

}
