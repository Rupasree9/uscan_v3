<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class fca_mopar_program extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    }

    public function get_monthly_supplier_scorecard_mopar(string $job_id,string $log_id,string $user_name_parameter,string $password_parameter,string $task_id,string $enterprise_id) {

                $process_monitor_log_update = new process_monitor_log_update();


                include('/var/www/uscan/uscan/public/crawler/general/db_connect.php');
                include('/var/www/uscan/uscan/public/crawler/general/log_functions.php');
                include('/var/www/uscan/uscan/public/crawler/general/simple_html_dom.php');

                  $date_time_for_file = date('Y-m-d His');
                  echo $date_time_for_file;
                  $step_no            = 0;
                  $log_id             = $log_id;
                  // $enterprise_id      = '0010000002';

              set_time_limit(0);

              $step_no     = $step_no + 1;
              $description = "Task Started";
              // $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              $date_time_file = date('Y-m-d His');
              $Data_Mopar_date_time = date('Y-m-d H:i:s');
              set_time_limit(0);
              $cookie = "cookie.txt";

              $now = new \DateTime('now');
              $month = $now->format('m');
              $year = $now->format('Y');
              $date = $now->format('d');

              //echo $date ;
              //echo $month;
              //echo $year;

              if ($month < 03)
              {
                if ($date < 20)
                {
                  $year = $year - 1;
                }

              }

              $username = $user_name_parameter;
              $password = $password_parameter;

              $ch = curl_init();
              $nav_url = 'https://sts.fiatgroup.com/adfs/ls/IdpInitiatedSignOn.aspx?RelayState=RPID%3Dfederation.chrysler.com%253Asaml2.0%253AentityId%26RelayState%3Dhttps%3A%2F%2Fgsp.extra.chrysler.com%2Fgeneral%2Ftarget%2FeSC_ebsc.html&fhr=default';
              curl_setopt($ch, CURLOPT_URL, $nav_url);
              curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
              curl_setopt($ch, CURLOPT_HEADER, true);
              curl_setopt($ch, CURLOPT_HTTPGET, true);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_COOKIESESSION, true);
              curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name'); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
              $answer1 = curl_exec($ch);

              if (curl_error($ch))
              	{
              		echo curl_error($ch);
              	}

              $status = curl_getinfo($ch);

              if ($status['http_code'] == '200' or $status['http_code'] == '302')
              	{
              		$status_info = "Successful";
              	}
              else
              	{
              		$status_info = "Failed";
              	}

              //echo "</br>Status :- $status_info &nbsp &nbsp Response code :- $status[http_code] &nbsp &nbsp";
              //echo "URL :- $status[url] </br>";

              $step_no = $step_no + 1;
              $description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url;
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);


              // print_r($status);

              $header = curl_getinfo($ch);
              $action_url = $header['url'];
              $html_str = str_get_html($answer1);
              $VIEWSTATE = $html_str->find('input', 0)->value;
              $VIEWSTATEGENERATOR = $html_str->find('input', 1)->value;
              $EVENTVALIDATION = $html_str->find('input', 2)->value;
              $db = $html_str->find('input', 3)->value;
              $data = array(
              		'__VIEWSTATE' => "$VIEWSTATE",
              		'__VIEWSTATEGENERATOR' => "$VIEWSTATEGENERATOR",
              		'__EVENTVALIDATION' => "$EVENTVALIDATION",
              		'__db' => "$db",
              		'ctl00$ContentPlaceHolder1$UsernameTextBox' => "$username",
              		'ctl00$ContentPlaceHolder1$PasswordTextBox' => "$password"
              );
              $fi = http_build_query($data);
              curl_setopt($ch, CURLOPT_URL, $action_url);
              curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
              curl_setopt($ch, CURLOPT_HEADER, true);
              curl_setopt($ch, CURLOPT_POST, true);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $fi);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_COOKIESESSION, true);
              curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name'); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
              curl_setopt($ch, CURLOPT_MAXREDIRS, 20);
              curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
              curl_setopt($ch, CURLOPT_TIMEOUT, 180);
              $answer2 = curl_exec($ch);


              $html_login_check = str_get_html($answer2);
              $check = $html_login_check->find('.errorMessage',0);
              if($check)
              {
              			$pos = strpos($check->innertext, "Incorrect USER ID or PASSWORD");
              			if ($pos) {

              				$step_no = $step_no + 1;
              				$description = "Incorrect USER ID or PASSWORD Login Failed";

                      $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);


              				$step_no = $step_no + 1;
              				$description = "Task Completed";
              			  $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

                      update_log_state($log_id,'Failed');
                      $GLOBALS['conn_master']->close();
                      $GLOBALS['conn_transaction']->close();
              				curl_close($ch);
              				exit;

              			}

              }
              else
              {

              if (curl_error($ch))
              	{
              		echo curl_error($ch);
              	}

              $status = curl_getinfo($ch);

              if ($status['http_code'] == '200' or $status['http_code'] == '302')
              	{
              		$status_info = "Successful";
              	}
              else
              	{
              		$status_info = "Failed";
              	}

              //echo "</br>Status :- $status_info &nbsp &nbsp Response code :- $status[http_code] &nbsp &nbsp";
              //echo "URL :- $status[url] </br>";

              $step_no = $step_no + 1;
              $description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $action_url;
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);




              $html_str_1 = str_get_html($answer2);

              foreach($html_str_1->find('form') as $element) $action_url = $element->action;

              // echo $action_url . "</br>";

              $SAMLResponse = $html_str_1->find('input', 0)->value;
              $RelayState = $html_str_1->find('input', 1)->value;

              // echo "SAMLResponse = $SAMLResponse </br> ";
              // echo "RelayState = $RelayState </br> ";

              $data_1 = array(
              		'SAMLResponse' => "$SAMLResponse",
              		'RelayState' => "$RelayState"
              );
              $fi_1 = http_build_query($data_1);
              curl_setopt($ch, CURLOPT_URL, $action_url);
              curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
              curl_setopt($ch, CURLOPT_HEADER, true);
              curl_setopt($ch, CURLOPT_POST, true);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $fi_1);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_COOKIESESSION, true);
              curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name'); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
              curl_setopt($ch, CURLOPT_MAXREDIRS, 20);
              curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
              curl_setopt($ch, CURLOPT_TIMEOUT, 180);
              $answer3 = curl_exec($ch);

              if (curl_error($ch))
              	{
              		echo curl_error($ch);
              	}

              $status = curl_getinfo($ch);

              if ($status['http_code'] == '200' or $status['http_code'] == '302')
              	{
              		$status_info = "Successful";
              	}
              else
              	{
              		$status_info = "Failed";
              	}

              //echo "</br>Status :- $status_info &nbsp &nbsp Response code :- $status[http_code] &nbsp &nbsp";
              //echo "URL :- $status[url] </br>";

              $step_no = $step_no + 1;
              $description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $action_url;
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);



              $nav_url_1 = 'https://gpsis.extra.chrysler.com/prod/restricted/ps/gpsis/home/index.jsp';
              curl_setopt($ch, CURLOPT_URL, $nav_url_1);
              curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
              curl_setopt($ch, CURLOPT_HEADER, true);
              curl_setopt($ch, CURLOPT_HTTPGET, true);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_COOKIESESSION, true);
              curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name'); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
              $answer4 = curl_exec($ch);

              if (curl_error($ch))
              	{
              		echo curl_error($ch);
              	}

              $status = curl_getinfo($ch);

              if ($status['http_code'] == '200' or $status['http_code'] == '302')
              	{
              		$status_info = "Successful";

              		$step_no = $step_no + 1;
              		$description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url_1;
              	  $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);


              		$step_no = $step_no + 1;
              	  $description = "Login Successful";
                  $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              	}
              else
              	{
              		$status_info = "Failed";

              		$step_no = $step_no + 1;
              		$description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url_1;
                  $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);



              		$step_no = $step_no + 1;
                  $description = "Login Failed";
                  $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

                  update_log_state($log_id,'Failed');
              		curl_close($ch);
              		exit;
              	}

              //echo "</br>Status :- $status_info &nbsp &nbsp Response code :- $status[http_code] &nbsp &nbsp";
              //echo "URL :- $status[url] </br>";

              $nav_url_3 = "https://gpsis.extra.chrysler.com/prod/ebsc/ext/LocalSuppliers?yr=$year&cnt=3&bu=10&st=38793&repType=excel";
              curl_setopt($ch, CURLOPT_URL, $nav_url_3);
              curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
              curl_setopt($ch, CURLOPT_HEADER, false);
              curl_setopt($ch, CURLOPT_HTTPGET, true);
              curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
              curl_setopt($ch, CURLOPT_TIMEOUT, 180);
              $answer6 = curl_exec($ch);

              if (curl_error($ch))
              	{
              		echo curl_error($ch);
              	}

              $status = curl_getinfo($ch);

              if ($status['http_code'] == '200' or $status['http_code'] == '302')
              	{
              		$status_info = "Successful";
              	}
              else
              	{
              		$status_info = "Failed";
              	}

              //echo "</br>Status :- $status_info &nbsp &nbsp Response code :- $status[http_code] &nbsp &nbsp";
              //echo "URL :- $status[url] </br>";
              $step_no = $step_no + 1;
              $description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url_3;
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);


              $table1 = str_get_html($answer6);
              $i1 = 0;
              $j1 = 0;
              $Data_Mopar = array();

              foreach($table1->find('tr') as $event)
              	{
              		$i1 = $i1 + 1;

              		// echo $i1;

              		if ($i1 > 5)
              			{
              				$Data_Mopar[$j1][0] = trim($event->find('td', 0)->plaintext);
              				$Data_Mopar[$j1][1] = trim($event->find('td', 1)->plaintext);
              				$Data_Mopar[$j1][2] = trim($event->find('td', 2)->plaintext);
              				$Data_Mopar[$j1][3] = trim($event->find('td', 3)->plaintext);
              				$Data_Mopar[$j1][4] = trim($event->find('td', 4)->plaintext);
              				$Data_Mopar[$j1][5] = trim($event->find('td', 5)->plaintext);
              				$Data_Mopar[$j1][6] = trim($event->find('td', 6)->plaintext);
              				$Data_Mopar[$j1][7] = trim($event->find('td', 7)->plaintext);
              				$j1 = $j1 + 1;
              			}
              	}

              for ($x1 = 0; $x1 <= count($Data_Mopar) - 2; $x1++)
              	{
              		if ($Data_Mopar[$x1][0] == "&nbsp;")
              			{
              				$code = "";
              			}
              		else
              			{
              				$code = $Data_Mopar[$x1][0];
              			}

              		if ($Data_Mopar[$x1][1] == "&nbsp;")
              			{
              				$description = "";
              			}
              		else
              			{
              				$description = $Data_Mopar[$x1][1];
              			}

              		if ($Data_Mopar[$x1][2] == "&nbsp;")
              			{
              				$i1ncoming_material_quality = "";
              			}
              		else
              			{
              				$i1ncoming_material_quality = $Data_Mopar[$x1][2];
              			}

              		if ($Data_Mopar[$x1][3] == "&nbsp;")
              			{
              				$delivery = "";
              			}
              		else
              			{
              				$delivery = $Data_Mopar[$x1][3];
              			}

              		if ($Data_Mopar[$x1][4] == "&nbsp;")
              			{
              				$warranty = "";
              			}
              		else
              			{
              				$warranty = $Data_Mopar[$x1][4];
              			}

              		if ($Data_Mopar[$x1][5] == "&nbsp;")
              			{
              				$cost = "";
              			}
              		else
              			{
              				$cost = $Data_Mopar[$x1][5];
              			}

              		if ($Data_Mopar[$x1][6] == "&nbsp;")
              			{
              				$partnership = "";
              			}
              		else
              			{
              				$partnership = $Data_Mopar[$x1][6];
              			}

              		if ($Data_Mopar[$x1][7] == "&nbsp;")
              			{
              				$overall = "";
              			}
              		else
              			{
              				$overall = $Data_Mopar[$x1][7];
              			}

              		$sql = "insert into `uscan_transactions`.`fca_supplier_scorecard_mopar`(`code`,`description`,`incoming_material_quality`,`delivery`,`warranty`,`cost`,`partnership`,`overall`,`transaction_date`,`log_id`)
                          values ('$code','$description','$i1ncoming_material_quality','$delivery','$warranty','$cost','$partnership','$overall','$Data_Mopar_date_time','$log_id')";
              						if ($GLOBALS['conn_transaction']->query($sql) === TRUE)
              						{
              						$record_status = "success";
              						}
              					  else
              						{
              						$record_status = "error";
              						}
              	}

              	$step_no = $step_no + 1;

              	if ($record_status == "success")
              		{
              		$description = "Mopar Data Inserted Successfully";
              		}
              	  else
              		{
              		$description = "Error: <br />" . $GLOBALS['conn_transaction']->error;
              		}

                  $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

                  $inputFileName_mopar = "/var/www/uscan/uscan/public/crawler/documents/fca/fca_mopar/supplier_scorecard/fca" . "_" . $log_id . "_" . $date_time_file . ".xls";

              	if (!(file_put_contents($inputFileName_mopar, $answer6)))
              		{
              		echo "file_put error";
              		$step_no = $step_no + 1;
              		$description = "Excel Sheet Download Failed(Mopar Data)";
                  $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              		}
              		else {
              			# code...
              			$step_no = $step_no + 1;
              			$description = "Excel Sheet Downloaded Successfully(Mopar Data)";
                    $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              		}


              curl_close($ch);

              $subject = 'Scheduled Task';
              $FileName  = "FCA Mopar Monthly Supplier Scorecard" . "_" . $log_id . "_" . $date_time_file . ".xls";
              // $path = "C:\\xampp\htdocs\uscan\public\crawler\documents\\ford\\ford_q1_scores\\Test Data.xls";

              $distribution_strategy = new distribution_strategy();
              $distribution_strategy_data = $distribution_strategy->distribution_strategy_check($task_id,$enterprise_id,$subject,$inputFileName_mopar,$FileName);

              if ($distribution_strategy_data)
              {
                $step_no = $step_no + 1;
                $description = $distribution_strategy_data;
                $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);
              }


              $step_no = $step_no + 1;
              $description = "Task Completed";
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);


              // update_log_state($log_id,'Finished');
             $process_monitor_log_update_new = $process_monitor_log_update->update_log_state($log_id,'Finished');


              }

              $GLOBALS['conn_master']->close();
              $GLOBALS['conn_transaction']->close();


      return "job executed successfully fca mopar  job id: $job_id";

    }
}
