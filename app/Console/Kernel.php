<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Excel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
         \App\Console\Commands\Inspire::class,
         \App\Console\Commands\LogDe::class,
         \App\Console\Commands\report_scheduler::class,
         \App\Console\Commands\distribution_strategy::class,
         \App\Console\Commands\fca_production_program::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
      protected function schedule(Schedule $schedule)
      {
           /*
                      $currTimeStamp = date('c');
                      $date = date('Y-m-d', time());
                      $h = date('H', time());
                      $i = date('i', time());
                      $s = date('s', time());
                      $curr_time_exact = $h .':'.$i.':'.$s;
                      \Log::info("Current time : ".$curr_time_exact);

                      if($i == "4" OR $i == "3" OR $i == "2" OR $i == "1" OR $i == "0")
                      {
                          if($h == "00")
                          {
                              $hr = "23";
                          }
                          else {
                              $hr = $h - 1;
                          }
                          $minuntes_for_start = "59";
                          $time_start = $hr .':'.$minuntes_for_start.':00';
                          \Log::info("Start time   : ".$time_start);
                      }
                      else {
                          $minuntes_for_start = $i - 5;  //Reducing 5 min to check the results
                          $time_start = $h .':'.$minuntes_for_start.':00';
                          \Log::info("Start time   : ".$time_start);
                      }



                      if($i == "59" OR $i == "58" OR $i == "57" OR $i == "56" OR $i == "55")
                      {
                              if($h == 23)
                              {
                                  $h = "00";
                              }
                              else {
                                  $h = $h + 1;
                              }
                              $minutes_for_end = "01";
                      }
                      else {
                          $minutes_for_end = $i + 5;  //Increasing 5 min to check the results
                      }
                      $time_end = $h .':'.$minutes_for_end.':00';
                      \Log::info("End time     : ".$time_end);

                      $query_job_is_present = "SELECT A.job_id,job_name,job_start_time,job_type,task_id,job_priority,job_status
                                      active,periodic, start_type_id
                                      FROM job A
                                      WHERE active   = 1
                                      AND A.job_start_date <= '$date'
                                      AND A.job_end_date >= '$date'
                                      AND A.job_start_time >= '$time_start'
                                      AND A.job_start_time <= '$time_end'";

                      $schedule_job_is_present = \DB::select($query_job_is_present);
                      $arrlength_job_is_present = count($schedule_job_is_present);
                      \Log::info("No. Of Jobs  : ".$arrlength_job_is_present);

                      if($arrlength_job_is_present > 0)
                      {
                            \Log::info("Got results. Run respective Task");
                            //Immediately query
                            $query_hourly = "SELECT A.job_id,job_name,job_start_time,job_type,task_id,job_priority,job_status
                                            active,periodic, start_type_id
                                            FROM job A
                                            WHERE active   = 1
                                            AND start_type_id = 1
                                            AND A.job_start_date <= '$date'
                                            AND A.job_end_date >= '$date'";

                                      $schedule_jobs_immedialtely = \DB::select($query_hourly);
                                      $arrlength_immediately = count($schedule_jobs_immedialtely);
                                      $job_id_immediately = array();
                                      $job_start_time_immediately = array();
                                        for ($k=0; $k < $arrlength_immediately; $k++) {
                                            \Log::info("Immediate Run");
                                            $job_id_immediately[$k] =  $schedule_jobs_immedialtely[$k]->job_id;
                                            $job_start_time_immediately[$k] =  $schedule_jobs_immedialtely[$k]->job_start_time;

                                            $time_at_immediately = substr($job_start_time_immediately[$k], 0, -3);
                                            // $schedule->command("log:de $job_id_immediately[$k]")->dailyAt($time_at_immediately)->timezone('Asia/Kolkata');
                                            $schedule->command("log:de $job_id_immediately[$k]")->dailyAt($time_at_immediately);
                                        }

                                //Hourly query
                                $query_hourly = "SELECT A.job_id,job_name,job_start_time,job_type,task_id,job_priority,job_status
                                                active,periodic, start_type_id,B.frequency_id AS frequency
                                                FROM job A,periodic B
                                                WHERE active   = 1
                                                AND start_type_id = 2
                                                AND A.periodic = 1
                                                AND B.frequency_id = 1
                                                AND A.job_id = B.job_id
                                                AND A.job_start_date <= '$date'
                                                AND A.job_end_date >= '$date'";

                                          $schedule_jobs_hourly = \DB::select($query_hourly);
                                          $arrlength_hourly = count($schedule_jobs_hourly);
                                          $job_id_hourly = array();
                                            for ($j=0; $j < $arrlength_hourly; $j++) {
                                                \Log::info("Hourly Run");
                                                $job_id_hourly[$j] =  $schedule_jobs_hourly[$j]->job_id;
                                                $job_start_time_hourly[$j] =  $schedule_jobs_hourly[$j]->job_start_time;
                                                $time_at_hourly = substr($job_start_time_hourly[$j], 0, -3);
                                                $time_at_hourly = substr($time_at_hourly, -2);
                                                \Log::info('time_at_hourly = '.$time_at_hourly);
                                                $schedule->command("log:de $job_id_hourly[$j]")->hourlyAt($time_at_hourly);

                                                // \Log::info('job_id_hourly = '.$job_id_hourly[$j]);
                                            }


                                  //Daily query
                                  $query_daily = "SELECT A.job_id,job_name,job_start_time,job_type,task_id,job_priority,job_status
                                                  active,periodic, start_type_id,B.frequency_id AS frequency
                                                  FROM job A,periodic B
                                                  WHERE active   = 1
                                                  AND start_type_id = 2
                                                  AND A.periodic = 1
                                                  AND B.frequency_id = 2
                                                  AND A.job_id = B.job_id
                                                  AND job_start_date <= '$date'
                                                  AND job_end_date >= '$date'";

                                            $schedule_jobs_daily = \DB::select($query_daily);
                                            $arrlength_daily = count($schedule_jobs_daily);

                                            $job_id_daily = array();
                                            $job_start_time_daily = array();
                                            for ($i=0; $i < ($arrlength_daily) ; $i++) {
                                                  \Log::info("Daily Run");
                                                  $job_id_daily[$i] =  $schedule_jobs_daily[$i]->job_id;
                                                  $job_start_time_daily[$i] =  $schedule_jobs_daily[$i]->job_start_time;
                                                  $time_at = substr($job_start_time_daily[$i], 0, -3);
                                                  $schedule->command("log:de $job_id_daily[$i]")->dailyAt($time_at);
                                                }


                                            //Weekly query
                                            $query_weekly = "SELECT A.job_id,job_name,job_start_time,job_type,task_id,job_priority,job_status
                                                            active,periodic, start_type_id,B.frequency_id AS frequency
                                                            FROM job A,periodic B
                                                            WHERE active   = 1
                                                            AND start_type_id = 2
                                                            AND A.periodic = 1
                                                            AND B.frequency_id = 3
                                                            AND A.job_id = B.job_id
                                                            AND job_start_date <= '$date'
                                                            AND job_end_date >= '$date'";

                                                      $schedule_jobs_weekly = \DB::select($query_weekly);
                                                      $arrlength_weekly = count($schedule_jobs_weekly);

                                                      $job_id_weekly = array();
                                                      for ($i=0; $i < ($arrlength_weekly) ; $i++) {
                                                            \Log::info("Weekly Run");
                                                            $job_id_weekly[$i] =  $schedule_jobs_weekly[$i]->job_id;
                                                            $schedule->command("log:de $job_id_weekly[$i]")->weekly();
                                                          }


                                              //Monthly query
                                              $query_monthly = "SELECT A.job_id,job_name,job_start_time,job_type,task_id,job_priority,job_status
                                                              active,periodic, start_type_id,B.frequency_id AS frequency
                                                              FROM job A,periodic B
                                                              WHERE active   = 1
                                                              AND start_type_id = 2
                                                              AND A.periodic = 1
                                                              AND B.frequency_id = 4
                                                              AND A.job_id = B.job_id
                                                              AND job_start_date <= '$date'
                                                              AND job_end_date >= '$date'";

                                                        $schedule_jobs_monthly = \DB::select($query_monthly);
                                                        $arrlength_monthly = count($schedule_jobs_monthly);

                                                        $job_id_monthly = array();
                                                        for ($i=0; $i < ($arrlength_monthly) ; $i++) {
                                                              \Log::info("Monthly Run");
                                                              $job_id_monthly[$i] =  $schedule_jobs_monthly[$i]->job_id;
                                                              $schedule->command("log:de $job_id_monthly[$i]")->monthly();
                                                            }


                                                            //No periodic query
                                                            $query_no_periodic = "SELECT A.job_id,job_name,job_start_time,job_type,task_id,job_priority,job_status
                                                                            active,periodic, start_type_id
                                                                            FROM job A
                                                                            WHERE active   = 1
                                                                            AND start_type_id = 2
                                                                            AND A.periodic = 0
                                                                            AND job_start_date <= '$date'
                                                                            AND job_end_date >= '$date'";

                                                                      $schedule_jobs_no_periodic = \DB::select($query_no_periodic);
                                                                      $arrlength_no_periodic = count($schedule_jobs_no_periodic);

                                                                      $job_id_no_periodic = array();
                                                                      $job_start_time = array();
                                                                      for ($l=0; $l < ($arrlength_no_periodic) ; $l++) {
                                                                            \Log::info("No periodic Run");
                                                                            $job_id_no_periodic[$l] =  $schedule_jobs_no_periodic[$l]->job_id;
                                                                            $job_start_time[$l] =  $schedule_jobs_no_periodic[$l]->job_start_time;

                                                                            $time_at_no_periodic = substr($job_start_time[$l], 0, -3);
                                                                            $schedule->command("log:de $job_id_no_periodic[$l]")->dailyAt($time_at_no_periodic);
                                                                          }
                      }
                */      
      }

    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
