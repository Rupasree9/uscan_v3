<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class process_monitor_table extends Model
{
    //
    protected $table = 'process_monitor';
    public $timestamps = false;
}
