<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class plant_model extends Model
{
  use SoftDeletes;
  protected $table = 'plant';
  protected $dates = ['deleted_at'];
}
