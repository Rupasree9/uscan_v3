<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class customer_contact_model extends Model
{
    //
    use SoftDeletes;

    protected $table = 'customer_contact';
    protected $dates = ['deleted_at'];
    public $timestamps = false;
}
