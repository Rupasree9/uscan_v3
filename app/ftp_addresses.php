<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ftp_addresses extends Model
{
    //
    protected $table = 'ftp_addresses';
    protected $dates = ['deleted_at'];
}
