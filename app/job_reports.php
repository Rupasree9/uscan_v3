<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class job_reports extends Model
{
    //
    protected $table = 'job_reports';
    protected $dates = ['deleted_at'];
    
}
