<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class periodic_reports extends Model
{
    //
    protected $table = 'periodic_reports';
    public $timestamps = false;
}
