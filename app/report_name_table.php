<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class report_name_table extends Model
{
    //
    protected $table = 'report_name';
    protected $dates = ['deleted_at'];
    public $timestamps = false;
}
