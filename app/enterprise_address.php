<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class enterprise_address extends Model
{
  use SoftDeletes;
  protected $table = 'enterprise_address';
  public $timestamps = false;
  protected $dates = ['deleted_at'];
}
