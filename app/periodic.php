<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class periodic extends Model
{
    protected $table = 'periodic';
    public $timestamps = false;
}
