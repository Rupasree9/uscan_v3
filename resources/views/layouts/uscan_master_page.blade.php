<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    @include('includes.head')
    @yield('header')

    <!-- <link rel="shortcut icon" href="images/U-26.png"> -->
    <!-- <link rel="shortcut icon" href="images/uscan.jpg"> -->
</head>
<body>
<div class="container-fluid">

    <header class="row" style="background: -webkit-linear-gradient(top, #295070, white);">
        @include('includes.header')
        @yield('display_screen_name')
    </header>

    <div id="main" class="row">
        <!-- sidebar content -->
        <!-- <div class="upper_band col-xs-12">

        </div> -->
        <div id="sidebar" class="col-md-3 col-xs-3" style="background:white; padding-left: 0px;padding-right: 10px;" >
          <div class="upper_band col-xs-12" style="border-bottom: 0px solid white;">
            <div class="col-xs-8 display-title">

            </div>
          </div>
          <div class="col-xs-12">
               @include('includes.sidebar')
          </div>
          <div class="col-xs-12 lower_band" style="border-top: 0px solid white;">

          </div>
        </div>
        <!-- <div class="col-xs-12 lower_band">

        </div> -->

        <!-- main content -->
        <div id="content_header" class="col-md-9 col-xs-9" style="font-size:11px;">
            <!-- <div id="content_bg_image"></div> -->
            @yield('upper_band')
            @yield('content')
            @yield('lower_band')


        </div>
    </div>

    <footer class="row" style="height:40px;background: -webkit-linear-gradient(top,white, #295070);">
        @include('includes.footer')
    </footer>

</div>
</body>
</html>
