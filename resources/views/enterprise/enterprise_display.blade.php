@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" type="text/css" href="css/plant.css">
<link rel="stylesheet" type="text/css" href="css/plant_change.css">
<!-- <script src="js/plant_validation.js"></script> -->
@stop
@section('display_screen_name')
<div class="col-md-9" style="position:relative;">
</div>
@stop
@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="display-title">
      Display Enterprise Master
   </div>
   <div id="show_all_link" class="col-xs-2" style="float: right;text-align:center;line-height:30px;">
      <span  id="show_all" style="">Show All Enterprise</span>
   </div>
</div>
@stop
@section('content')
<div class="col-xs-12 content no_scroll" id="content no_scroll">
   <form id="search-para" class="no_scroll" autocomplete="off">
      <table class="hide_show" style="width:40%;">
         <tr>
            <td class="details_color">Search Parameters</td>
         </tr>
         <tr>
         <td>Enterprise Name</td>
         <td><input type="text" id ="enterprise_name_search" name = "enterprise" autocomplete="off"></td>
       </tr>

       <tr>
         <td>DUN'S Number</td>
         <td><input type="text" id ="duns_number_search" name = "duns_number_name" autocomplete="off"></td>
       </tr>
      </table>
   </form>
   <div id="list-of-ent"  class="div1 overflow table_height">
      <table style="width:100%;margin-bottom: 371px" class="tab">
      </table>
   </div>
   <div class="col-xs-12 ">
          <ul class="nav nav-tabs  " id="myTabs">
           <li id="1" class="active"><span id="span_one" class="tab_one "  data-toggle="tab" >Basic Details</span></li>
           <li id="2"><span id="span_two" class="tab_two "  data-toggle="tab" >Address</span></li>
           <li id="3"><span id="span_three" class="tab_three "  data-toggle="tab" >Contact</span></li>
           <li id="4"><span id="span_four" class="tab_four" data-toggle="tab" >Additional Details</span></li>
        </ul>
          <div class="tab-content">
                <div id="tab_one" class="">
                   <form id="enterprise-table" autocomplete="off">
                      <table style="width:40%;">
                        <tr>
                           <td></td>
                        </tr>
                        <tr>
                           <td>Enterprise Name</td>
                           <td><input type="text" id ="enterprise_name" name="enterprise_name" disabled>
                           </td>
                        </tr>
                        <tr>
                           <td>Tin Number</td>
                           <td><input type="text" id ="tin_number" name="tin_number" disabled></td>
                        </tr>
                        <tr>
                           <td>DUN's No</td>
                           <td><input type="text" id ="duns_number" name="duns_number" disabled></td>
                        </tr>
                        <tr>
                           <td>License Number</td>
                           <td><input type="text" id ="license_number" name="license_number" disabled></td>
                        </tr>
                        <tr>
                           <td>Active</td>
                           <td><input type="checkbox" id="enterprise_active" name="enterprise_active" disabled></td>
                        </tr>
                      </table>
                   </form>
                </div>
                <div id="tab_two" class="load">
                   <form id="second_form" class="" autocomplete="off">
                      <table style="width:80%; margin-bottom: 20px">
                        <tr>
                           <td></td>
                        </tr>
                        <tr>
                           <td style="background-color:#d4d4d4;"><strong>Sold-to Address</strong></td>
                           <td></td>
                           <td style="background-color:#d4d4d4;"><strong>Bill-to Address</strong></td>
                           <td></td>
                        </tr>
                        <tr>
                           <td>Street </td>
                           <td><input type="text" id ="soldto_street" name="soldto_street" disabled></td>
                           <td>Street </td>
                           <td><input type="text" id ="billto_street" name="billto_street" disabled></td>
                        </tr>
                        <tr>
                           <td>City</td>
                           <td><input type="text" id ="soldto_city" name="soldto_city" disabled></td>
                           <td>City </td>
                           <td><input type="text" id ="billto_city" name="billto_city" disabled></td>

                        </tr>
                        <tr>
                           <td>State</td>
                           <td><input type="text" id ="soldto_state" name="soldto_state" disabled></td>
                           <td>State </td>
                           <td><input type="text" id ="billto_state" name="billto_state" disabled></td>
                        </tr>
                        <tr>
                          <td>Country</td>
                          <td><input type="text" id ="soldto_country" name="soldto_country" disabled></td>
                          <td>Country </td>
                          <td><input type="text" id ="billto_country" name="billto_country" disabled></td>
                        </tr>
                        <tr>
                          <td>Zip Code</td>
                          <td><input type="text" id ="soldto_zip_code" name="soldto_zip_code" disabled></td>
                          <td>Zip Code </td>
                          <td><input type="text" id ="billto_zip_code" name="billto_zip_code" disabled></td>
                        </tr>
                        <tr>
                          <td>Region</td>
                          <td><input type="text" id ="soldto_region" name="soldto_region" disabled></td>
                          <td>Region</td>
                          <td><input type="text" id ="billto_region" name="billto_region" disabled></td>
                        </tr>
                      </table>
                   </form>
                </div>
                <div id="tab_three" class="load">
                   <form id="third_form" class="" autocomplete="off">
                      <table class="table-responsive" style="width:50%; margin-bottom: 20px">
                        <tr>
                           <td></td>
                        </tr>
                        <tr>
                           <td>Primary Contact Name</td>
                           <td><input type="text" id="p_contact_name" name="p_contact_name" disabled></td>
                        </tr>
                        <tr>
                           <td>Primary Contact Mail ID</td>
                           <td><input type="email" id="p_contact_email" name="p_contact_email" disabled></td>
                        </tr>
                        <tr>
                           <td>Primary Contact Number</td>
                           <td><input type="text" id="p_contact_no" name="p_contact_no" disabled></td>
                        </tr>
                        <tr>
                           <td>Secondary Contact Name </td>
                           <td><input type="text" id="s_contact_name" name="s_contact_name" disabled></td>
                        </tr>
                        <tr>
                           <td>Secondary Contact Mail ID</td>
                           <td><input type="email" id="s_contact_email" name="s_contact_email" disabled></td>
                        </tr>
                        <tr>
                           <td>Secondary Contact Number</td>
                           <td><input type="text" id="s_contact_no" name="s_contact_no" disabled></td>
                        </tr>
                      </table>
                   </form>
                </div>
                <div id="tab_four" class="load">
                    <form id="additional_details" autocomplete="off">
                      <table style="width:40%; margin-bottom: 20px">
                        <tr>
                           <td></td>
                        </tr>
                       <tr>
                         <td>Created By</td>
                         <td><input type="text" name="created_by" id="created_by" disabled></td>
                       </tr>
                       <tr>
                         <td>Created Date</td>
                         <td><input type="text" name="created_date" id="created_date" disabled></td>
                       </tr>
                       <tr>
                         <td>Modified By</td>
                         <td><input type="text" name="modified_by" id="modified_by" disabled></td>
                       </tr>
                       <tr>
                         <td>Modified Date</td>
                         <td><input type="text" name="modified_date" id="modified_date" disabled></td>
                       </tr>
                      </table>
                     </form>
                    </div>
          </div>
   </div>

   <script type="text/javascript">
     jQuery.validator.addMethod('selectcheck', function (value) {
                    return (value != '0');
            }, "");
      $(window).load(function() {
        $("#master_data").trigger('click');
        $("#enterprise_master").trigger('click');
          $('.tab-content').hide();

          $("#first_set_buttons").hide();
          $("#second_set_buttons").hide();
          $("#third_set_buttons").hide();
          $("#fourth_set_buttons").hide();

          $('#tab_one').show();
          $('#tab_two').hide();
          $('#tab_three').hide();
          $('#tab_four').hide();

          $('#span_one').addClass('custom_click_tab_active');
          $('#span_two,#span_three,#span_four').addClass('custom_click_tab');
      });

      $(document).ready(function() {
          var filter_clicked=false;
          var show_all_link_clicked=false;
          var results = "";
          var contact_id = "";
          var address_id = "";
          var ent_id = "";
          var active_check;
          var plant_name,plant_code,supplier_code,duns_no;



          $('#next').click(function() {
              // $('#additional').show();
              $('#enterprise-table').hide();
              // $('#back_edit').show();
              $('#next').hide();
          });

          function filter_click()
          {
                      $('#list-of-ent').show();
                      show_all_link_clicked=false;
                      var enterprise_name = $('#enterprise_name_search').val();
                      var duns_number = $('#duns_number_search').val();

                      if ((enterprise_name == "") && (duns_number == "")) {

                          $.msgBox({
                              title: "Alert",
                              content: "Search Criteria Not Available",
                              type: "alert",
                          });
                      } else            {
                        $('#search_page_buttons').show();
                        $('#edit').show();
                        $('#delete').show();
                        $('#export').show();
                        $('#search-para').hide();
                        display_data();
                        $('.tab').show();
                        $('#filter').hide();
                        $('#back_search').show();
                        $('#show_all').hide();
                      }
          }

          $('#filter').click(function() {
              filter_clicked=true;
                filter_click();

          });

          $(document).on('click', '#back_search', function() {
                back_search();
          });

          function back_search()
          {
            $('#search_page_buttons').hide();
            $('.tab').hide();
            $('#search-para').show();
            $('.tab tr').remove();
            $('.hide_show').show();
            $('#filter').show();
            $('#show_all').show();
            $('#back_search').hide();
            // $("#show_all,#search-para,#filter").addClass("show");
            // $("#back_search,#list-of-ent").addClass("hide");
          }

          $(document).on('click', '#show_all', function() {

            show_all_link_clicked=true;
            filter_clicked=false;
              hide_buttons_show_all();
              show_table();

          });

          function hide_div_after_update() {
                    $('#fourth_set_buttons').hide();
                    $('#cancelButton').hide();
                    // $('#span_one').addClass('custom_click_tab_active');
                    $('#span_two,#span_three,#span_four').removeClass('custom_click_tab_active');
                    // $('#span_two,#span_three,#span_four').addClass('custom_click_tab');
                    if(filter_clicked==true)
                    {
                      filter_clicked = false;
                      filter_click();
                    }
                    else {
                      $("#show_all_link").trigger("click");
                      $("#show_all").trigger("click");
                      $('#list-of-ent').show();
                    }
          }

          function hide_buttons_show_all() {
              $('.hide_show').hide();
              $('#search_page_buttons').show();
              $('#edit').show();
              $('#delete').show();
              $('#export').show();
              $('#search-para').hide();
              $('.tab').show();
              $('#Create').css("display", "none");
              $('#filter').hide();
              $('#Create').hide();
              $('#enterprise-table').hide();
              $('#Create').hide();
              $('#enterprise-table').hide();
              $('#list-of-ent').show();
              $('#next').hide();
              $('#cancelButton').hide();
              $('#back_search').show();
              $('#show_all').hide();
          }

          $(document).on('click', '#cancelButton', function() {
              $('#back_search').show();
              $('#Create').hide();
              $('#enterprise-table').hide();
              $('#list-of-ent').show();
              $('#search_page_buttons').show();
              $('#next').hide();
              $('#cancelButton').hide();
              $('#first_set_buttons').hide();
              $('#second_set_buttons').hide();
              $('#third_set_buttons').hide();
              $('#fourth_set_buttons').hide();
              $('#span_one,#span_two,#span_three,#span_four').removeClass('custom_click_tab_active');
          });

          $(document).on('click', '.edit', function() {
              {
                  show_change_screen();
                  $('#Create').show();
                  $('#enterprise-table').show();
                  $('#search_page_buttons').hide();
                  $('#cancelButton').show();
                  $('#list-of-ent').hide();
                  $('#next').show();
                  $('.tab-content').show();
                  $('#back_search').hide();
                  $('#show_all').hide();


                  var id = $(this).attr('id');
                  ent_id = results[id].enterprise_id;
                  // alert(ent_id);
                  $('#enterprise_name').val(results[id].enterprise_name);
                  $('#tin_number').val(results[id].tin_number);
                  $('#duns_number').val(results[id].duns_number);
                  $('#license_number').val(results[id].license_number);

                  if (results[id].active == 0) {
                      $('#enterprise_active').prop('checked', false);
                  } else {
                      $('#enterprise_active').prop('checked', true);
                  }

                   $('#soldto_street').val(results[id].soldto_street);
                   $('#billto_street').val(results[id].billto_street);
                   $('#soldto_city').val(results[id].soldto_city);
                   $('#billto_city').val(results[id].billto_city);
                   $('#soldto_state').val(results[id].soldto_state);
                   $('#billto_state').val(results[id].billto_state);
                   $('#soldto_country').val(results[id].soldto_country);
                   $('#billto_country').val(results[id].billto_country);
                   $('#soldto_zip_code').val(results[id].soldto_zip_code);
                   $('#billto_zip_code').val(results[id].billto_zip_code);
                   $('#soldto_region').val(results[id].soldto_region);
                   $('#billto_region').val(results[id].billto_region);
                   $('#p_contact_email').val(results[id].primary_contact_email_id);
                   $('#p_contact_no').val(results[id].primary_contact_number);
                   $('#p_contact_name').val(results[id].primary_contact_name);
                   $('#s_contact_email').val(results[id].secondary_contact_email_id);
                   $('#s_contact_name').val(results[id].secondary_contact_name);
                   $('#s_contact_no').val(results[id].secondary_contact_number);
                   $('#created_by').val(results[id].created_by);
                   $('#created_date').val(results[id].created_at);
                   $('#modified_by').val(results[id].updated_by);
                   $('#modified_date').val(results[id].updated_at);


              }
          });

          function show_table() {
                $.ajax({
                  type: "GET",
                  url: "enterprise_show_all",
                  dataType: "json",
                  success: function(data) {
                      results = data;
                      console.log(JSON.stringify(results));
                      $('.tab tr').remove();
                      $('.tab').append('<tr style="background-color: #D3D3D3;"><th>Enterprise  Name</th><th>TIN number</th><th>Duns Number</th><th>license Number</th><th>Active</th><th>Created By</th><th>Updated By</th></tr>');

                      for (var i = 0; i < results.length; i++) {
                        if (results[i].active == 0) {
                            active_check = '<input type="checkbox" disabled readonly>';
                        } else {
                            active_check = '<input type="checkbox" checked disabled readonly>';
                        }
                          $('.tab').append('<tr data-id="' + i + '" class="double_click"><td class"ent_id" style="display:none;">' + results[i].enterprise_id + '</td><td>' + results[i].enterprise_name + '</td><td>' + results[i].tin_number + '</td><td>' + results[i].duns_number + '</td><td>' + results[i].license_number + '</td><td>' + active_check + '</td><td>' + results[i].created_by + '</td><td>' + results[i].updated_by + '</td></tr>')
                      }
                  },
                  beforeSend: function() {

                  },
                  error: function() {
                  }
              });
          }

          function show_change_screen() {
                $("#first_set_buttons").show();
                $("#second_set_buttons").hide();
                $("#third_set_buttons").hide();
                $("#fourth_set_buttons").hide();

                $('#tab_one').show();
                $('#tab_two').hide();
                $('#tab_three').hide();
                $('#tab_four').hide();
                $('#span_one').addClass('custom_click_tab_active');
                $('#span_two,#span_three,#span_four').addClass('custom_click_tab');
          }

          $(document).on('dblclick', '.double_click', function() {
              show_change_screen();
              $('.tab-content').show();
              $('#back_search').hide();
              $('#show_all').hide();
              $('#Create').show();
              $('#enterprise-table').show();
              $('#search_page_buttons').hide();
              $('#cancelButton').show();
              $('#list-of-ent').hide();
              $('#next').show();

              var id = $(this).data("id");
              ent_id = results[id].enterprise_id;
              // alert(ent_id);
              $('#enterprise_name').val(results[id].enterprise_name);
              $('#tin_number').val(results[id].tin_number);
              $('#duns_number').val(results[id].duns_number);
              $('#license_number').val(results[id].license_number);

              if (results[id].active == 0) {
                  $('#enterprise_active').prop('checked', false);
              } else {
                  $('#enterprise_active').prop('checked', true);
              }

               $('#soldto_street').val(results[id].soldto_street);
               $('#billto_street').val(results[id].billto_street);
               $('#soldto_city').val(results[id].soldto_city);
               $('#billto_city').val(results[id].billto_city);
               $('#soldto_state').val(results[id].soldto_state);
               $('#billto_state').val(results[id].billto_state);
               $('#soldto_country').val(results[id].soldto_country);
               $('#billto_country').val(results[id].billto_country);
               $('#soldto_zip_code').val(results[id].soldto_zip_code);
               $('#billto_zip_code').val(results[id].billto_zip_code);
               $('#soldto_region').val(results[id].soldto_region);
               $('#billto_region').val(results[id].billto_region);
               $('#p_contact_email').val(results[id].primary_contact_email_id);
               $('#p_contact_no').val(results[id].primary_contact_number);
               $('#p_contact_name').val(results[id].primary_contact_name);
               $('#s_contact_email').val(results[id].secondary_contact_email_id);
               $('#s_contact_name').val(results[id].secondary_contact_name);
               $('#s_contact_no').val(results[id].secondary_contact_number);
               $('#created_by').val(results[id].created_by);
               $('#created_date').val(results[id].created_at);
               $('#modified_by').val(results[id].updated_by);
               $('#modified_date').val(results[id].updated_at);



          });

          function display_data() {

            var enterprise_name = $('#enterprise_name_search').val();
            var duns_number = $('#duns_number_search').val();

              $.ajax({
                  type: "POST",
                  url: "enterprise_search",
                  data: {
                      "_token": "{{ csrf_token() }}",
                      "enterprise_name": enterprise_name,
                      "duns_number": duns_number,
                        },
                  success: function(data) {
                      if (data != "0") {
                          results = data;
                          $('.tab tr').remove();
                          $('.tab').append('<tr style="background-color: #D3D3D3;"><th>Enterprise  Name</th><th>TIN number</th><th>Duns Number</th><th>license Number</th><th>Active</th><th>Created By</th><th>Updated By</th></tr>');

                          for (var i = 0; i < results.length; i++) {
                            if (results[i].active == 0) {
                                active_check = '<input type="checkbox" disabled readonly>';
                            } else {
                                active_check = '<input type="checkbox" checked disabled readonly>';
                            }
                              $('.tab').append('<tr data-id="' + i + '" class="double_click"><td  class="id" style="display:none;">' + results[i].enterprise_id + '</td><td>' + results[i].enterprise_name + '</td><td>' + results[i].tin_number + '</td>><td>' + results[i].duns_number + '</td><td>' + results[i].license_number  + '</td><td>' + active_check + '</td><td>' + results[i].created_by + '</td><td>' + results[i].updated_by + '</td></tr>')
                          }
                      } else {
                          if(filter_clicked == false){
                              back_search();
                          }
                          else {
                            back_search();
                            $.msgBox({
                              title:"Alert",
                              content:"Data Not Found",
                              type :"alert",
                            });
                          }

                      }
                  },
                  beforeSend: function() {

                  },
                  error: function() {
                  }
              });
          }



          function reset()      {
              // $('#enterprise-table')[0].reset();
              // $('#additional')[0].reset();
          }

          var fetch_enterprise_name;
          $('input[name="enterprise"]').autoComplete({
              minChars: 1,
              source: function(term, response) {
                  try {
                      fetch_enterprise_name.abort();
                  } catch (e) {}
                  fetch_enterprise_name = $.getJSON('fetch_enterprise_name', {
                      enterprise_name: term
                  }, function(data) {
                      response(data);
                  });
              }
          });

          var fetch_duns_number_name;
          $('input[name="duns_number_name"]').autoComplete({
              minChars: 0,
              source: function(term, response) {
                  try {
                      fetch_duns_number_name.abort();
                  } catch (e) {}
                  fetch_plant_id = $.getJSON('fetch_duns_number_name', {
                      duns_number_name: term
                  }, function(data) {
                      response(data);
                  });
              }
          });

          $('#next_button_first').click(function() {
            if ($('#enterprise-table').valid()) {
                $('#first_set_buttons').hide();
                $('#tab_one').hide();
                $('#tab_two').show();
                $('#tab_three').hide();
                $('#tab_four').hide();
                $('#second_set_buttons').show();

                $('#span_two').addClass('custom_click_tab_active');
                $('#span_one').removeClass('custom_click_tab_active');
                $('#span_one,#span_three,#span_four').addClass('custom_click_tab');
              }
          });

          $('#back_button_second').click(function() {
                $('#first_set_buttons').show();
                $('#tab_one').show();
                $('#tab_two').hide();
                $('#tab_three').hide();
                $('#tab_four').hide();
                $('#second_set_buttons').hide();

                $('#span_one').addClass('custom_click_tab_active');
                $('#span_two').removeClass('custom_click_tab_active');
                $('#span_two,#span_three,#span_four').addClass('custom_click_tab');
          });

          $('#next_button_second').click(function() {
                $('#third_set_buttons').show();
                $('#tab_one').hide();
                $('#tab_two').hide();
                $('#tab_three').show();
                $('#tab_four').hide();
                $('#second_set_buttons').hide();

                $('#span_three').addClass('custom_click_tab_active');
                $('#span_two').removeClass('custom_click_tab_active');
                $('#span_two,#span_one,#span_four').addClass('custom_click_tab');
          });

          $('#back_button_third').click(function() {
                $('#third_set_buttons').hide();
                $('#tab_one').hide();
                $('#tab_two').show();
                $('#tab_three').hide();
                $('#tab_four').hide();
                $('#second_set_buttons').show();

                $('#span_two').addClass('custom_click_tab_active');
                $('#span_three').removeClass('custom_click_tab_active');
                $('#span_one,#span_three,#span_four').addClass('custom_click_tab');
          });

          $('#next_button_third').click(function() {
                $('#fourth_set_buttons').show();
                $('#tab_one').hide();
                $('#tab_two').hide();
                $('#tab_three').hide();
                $('#tab_four').show();
                $('#third_set_buttons').hide();

                $('#span_four').addClass('custom_click_tab_active');
                $('#span_three').removeClass('custom_click_tab_active');
                $('#span_two,#span_one,#span_three').addClass('custom_click_tab');
          });

          $('#back_button_fourth').click(function() {
                $('#fourth_set_buttons').hide();
                $('#tab_one').hide();
                $('#tab_two').hide();
                $('#tab_three').show();
                $('#tab_four').hide();
                $('#third_set_buttons').show();

                $('#span_three').addClass('custom_click_tab_active');
                $('#span_four').removeClass('custom_click_tab_active');
                $('#span_one,#span_two,#span_four').addClass('custom_click_tab');
          });

      });
   </script>
</div>
@stop
@section('lower_band')
<div class="col-xs-12 lower_band">
   <button class="headerbuttons" id="back_search" type="button">Back</button>
   <button class="headerbuttons" id="filter" type="button">Search</button>
   <button class="headerbuttons" type="button" id="cancelButton" style="float: right; margin-right: 20px; display:none;">Cancel</button>


   <div class="first_set_buttons load" id="first_set_buttons">
      <button id="next_button_first" class="headerbuttons" type="button"  style="margin-right: 20px;">Next</button>
   </div>
   <div class="second_set_buttons load" id="second_set_buttons">

     <button id="back_button_second" class="headerbuttons" type="button"  style="margin-right: 20px;">Back</button>
      <button id="next_button_second" class="headerbuttons" type="button"  style="margin-right: 20px;">Next</button>
   </div>
   <div class="third_set_buttons load" id="third_set_buttons">
      <button id="back_button_third" class="headerbuttons" type="button"  style="margin-right: 20px;">Back</button>
      <button id="next_button_third" class="headerbuttons" type="button"  style="margin-right: 20px;">Next</button>

   </div>
   <div class="fourth_set_buttons load" id="fourth_set_buttons">
      <button id="back_button_fourth" class="headerbuttons" type="button"  style="margin-right: 20px;">Back</button>

   </div>
</div>
@stop
