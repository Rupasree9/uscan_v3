@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" type="text/css" href="css/enterprise.css">
<script src="js/enterprise_validation.js"></script>
@stop
@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="display-title">
      <p>Create Enetrprise Master</p>
   </div>
</div>
@stop
@section('content')
<div class="col-xs-12 content">
   <ul class="nav nav-tabs  " id="myTabs">
      <li id="1" class="active"><span id="span_one" class="tab_one "  data-toggle="tab" >Enetrprise Details</span></li>
      <li id="2"><span id="span_two" class="tab_two "  data-toggle="tab" >Address</span></li>
      <li id="3"><span id="span_three" class="tab_three "  data-toggle="tab" >Contact</span></li>
   </ul>
   <div class="tab-content">
      <div id="tab_one" class="">
         <form id="enterprise-table" autocomplete="off">
            <table style="width:40%;">
               <tr>
                  <td></td>
               </tr>
               <tr>
                  <td>Enterprise Name</td>
                  <td><input type="text" id ="enterprise_name" name="enterprise_name">
                  </td>
               </tr>
               <tr>
                  <td>Tin Number</td>
                  <td><input type="text" id ="tin_number" name="tin_number"></td>
               </tr>
               <tr>
                  <td>DUN's No</td>
                  <td><input type="text" id ="duns_number" name="duns_number"></td>
               </tr>
               <tr>
                  <td>License Number</td>
                  <td><input type="text" id ="license_number" name="license_number"></td>
               </tr>
               <tr>
                  <td>Active</td>
                  <td><input type="checkbox" id="enterprise_active" name="enterprise_active"></td>
               </tr>
            </table>
         </form>
      </div>
      <div id="tab_two" class="">
         <form id="second_form" class="" autocomplete="off">
            <table style="width:80%; margin-bottom: 20px">
               <tr>
                  <td></td>
               </tr>
               <tr>
                  <td style="background-color: #d4d4d4;"><strong>Sold-to Address</strong></td>
                  <td></td>
                  <td style="background-color: #d4d4d4;"><strong>Bill-to Address</strong></td>
                  <td></td>
               </tr>
               <tr>
                  <td>Street </td>
                  <td><input type="text" id ="soldto_street" name="soldto_street"></td>
                  <td>Street </td>
                  <td><input type="text" id ="billto_street" name="billto_street"></td>
               </tr>
               <tr>
                  <td>City</td>
                  <td><input type="text" id ="soldto_city" name="soldto_city"></td>
                  <td>City </td>
                  <td><input type="text" id ="billto_city" name="billto_city"></td>

               </tr>
               <tr>
                  <td>State</td>
                  <td><input type="text" id ="soldto_state" name="soldto_state"></td>
                  <td>State </td>
                  <td><input type="text" id ="billto_state" name="billto_state"></td>
               </tr>
               <tr>
                 <td>Country</td>
                 <td><input type="text" id ="soldto_country" name="soldto_country"></td>
                 <td>Country </td>
                 <td><input type="text" id ="billto_country" name="billto_country"></td>
               </tr>
               <tr>
                 <td>Zip Code</td>
                 <td><input type="text" id ="soldto_zip_code" name="soldto_zip_code"></td>
                 <td>Zip Code </td>
                 <td><input type="text" id ="billto_zip_code" name="billto_zip_code"></td>
               </tr>
               <tr>
                 <td>Region</td>
                 <td><input type="text" id ="soldto_region" name="soldto_region"></td>
                 <td>Region</td>
                 <td><input type="text" id ="billto_region" name="billto_region"></td>
               </tr>
            </table>
         </form>
      </div>
      <div id="tab_three" class="">
         <form id="third_form" class="" autocomplete="off">
            <table class="table-responsive" style="width:50%; margin-bottom: 20px">
               <tr>
                  <td></td>
               </tr>
               <tr>
                  <td>Primary Contact Name</td>
                  <td><input type="text" id="p_contact_name" name="p_contact_name"></td>
               </tr>
               <tr>
                  <td>Primary Contact Mail ID</td>
                  <td><input type="email" id="p_contact_email" name="p_contact_email"></td>
               </tr>
               <tr>
                  <td>Primary Contact Number</td>
                  <td><input type="text" id="p_contact_no" name="p_contact_no"></td>
               </tr>
               <tr>
                  <td>Secondary Contact Name </td>
                  <td><input type="text" id="s_contact_name" name="s_contact_name"></td>
               </tr>
               <tr>
                  <td>Secondary Contact Mail ID</td>
                  <td><input type="email" id="s_contact_email" name="s_contact_email"></td>
               </tr>
               <tr>
                  <td>Secondary Contact Number</td>
                  <td><input type="text" id="s_contact_no" name="s_contact_no"></td>
               </tr>
            </table>
         </form>
      </div>

   </div>
   <script type="text/javascript">
      jQuery.validator.addMethod('selectcheck', function (value) {
                     return (value != '0');
             }, "");

         $(window).load(function() {
             $("#master_data").trigger('click');
             $("#enterprise_master").trigger('click');
             $("#first_set_buttons").show();
             $("#second_set_buttons").hide();
             $("#third_set_buttons").hide();
             $("#fourth_set_buttons").hide();

             $('#tab_one').show();
             $('#tab_two').hide();
             $('#tab_three').hide();
             $('#span_one').addClass('custom_click_tab_active');
             $('#span_two,#span_three,#span_four').addClass('custom_click_tab');
         });

         $(document).ready(function() {

           var first_tab_basic_details = false;
             function reset() {
                 $('#enterprise-table').trigger("reset");
                 $('#second_form').trigger("reset");
                 $('#third_form').trigger("reset");
                 $('#back_button_second').trigger("click");
                 $("#first_set_buttons").show();
                 $("#second_set_buttons").hide();
                 $("#third_set_buttons").hide();


                 $('#span_one').addClass('custom_click_tab_active');
                 $('#span_two,#span_three,#span_four').removeClass('custom_click_tab_active');
                 $('#span_two,#span_three,#span_four').addClass('custom_click_tab');
             }

             $('#cancel_button').click(function() {
                 reset();
             });

             var is_valid_array = [];
             var active = 0;

             $(document).on('click', '#save_button', function() {

                                   var enterprise_name = $('#enterprise_name').val();
                                   var tin_number = $('#tin_number').val();
                                   var duns_number = $('#duns_number').val();
                                   var license_number = $('#license_number').val();
                                   var enterprise_active = $('#enterprise_active').val();

                                   if ($('#enterprise_active').is(":checked")) {
                                       active = 1;
                                   } else {
                                       active = 0;
                                   }
                                   var enterprise_active = active;
                                   var soldto_street = $('#soldto_street').val();
                                   var soldto_city = $('#soldto_city').val();
                                   var soldto_state = $('#soldto_state').val();
                                   var soldto_country = $('#soldto_country').val();
                                   var soldto_region = $('#soldto_region').val();
                                   var soldto_zip_code = $('#soldto_zip_code').val();

                                   var billto_street = $('#billto_street').val();
                                   var billto_city = $('#billto_city').val();
                                   var billto_state = $('#billto_state').val();
                                   var billto_country = $('#billto_country').val();
                                   var billto_region = $('#billto_region').val();
                                   var billto_zip_code = $('#billto_zip_code').val();

                                   var p_contact_name = $('#p_contact_name').val();
                                   var p_contact_email = $('#p_contact_email').val();
                                   var p_contact_no = $('#p_contact_no').val();
                                   var s_contact_name = $('#s_contact_name').val();
                                   var s_contact_email = $('#s_contact_email').val();
                                   var s_contact_no = $('#s_contact_no').val();

                                   $.ajax({
                                       type: "POST",
                                       url: "enterprise_save",
                                       data: {
                                           "_token": "{{ csrf_token() }}",

                                           "enterprise_name": enterprise_name,
                                           "tin_number": tin_number,
                                           "duns_number": duns_number,
                                           "duns_number": duns_number,
                                           "license_number": license_number,
                                           "active": active,

                                           "soldto_street": soldto_street,
                                           "soldto_city": soldto_city,
                                           "soldto_state": soldto_state,
                                           "soldto_country": soldto_country,
                                           "soldto_region": soldto_region,
                                           "soldto_zip_code": soldto_zip_code,

                                           "billto_street": billto_street,
                                           "billto_city": billto_city,
                                           "billto_state": billto_state,
                                           "billto_country": billto_country,
                                           "billto_region": billto_region,
                                           "billto_zip_code": billto_zip_code,

                                           "p_contact_name": p_contact_name,
                                           "p_contact_email": p_contact_email,
                                           "p_contact_no": p_contact_no,
                                           "s_contact_name": s_contact_name,
                                           "s_contact_email": s_contact_email,
                                           "s_contact_no": s_contact_no,


                                       },
                                       success: function(data) {
                                         $.msgBox({
                                                      title:"Message",
                                                      type:"info",
                                                      content:"Enterprise created successfully",
                                                 });

                                       },
                                       beforeSend: function() {
                                           $('#msg').show();
                                       },
                                       error: function() {
                                         $.msgBox({
                                                      title:"Error",
                                                      type:"error",
                                                      content:"Something went wrong",
                                                 });
                                            }
                                     });

                                   reset();
                      					});

               $('#next_button_first').click(function() {
                if ($('#enterprise-table').valid()) {
                     $('#first_set_buttons').hide();
                     $('#tab_one').hide();
                     $('#tab_two').show();
                     $('#tab_three').hide();
                     $('#second_set_buttons').show();

                     $('#span_two').addClass('custom_click_tab_active');
                     $('#span_one').removeClass('custom_click_tab_active');
                     $('#span_one,#span_three').addClass('custom_click_tab');
                }
               });

               $('#back_button_second').click(function() {
                     $('#first_set_buttons').show();
                     $('#tab_one').show();
                     $('#tab_two').hide();
                     $('#tab_three').hide();
                     $('#second_set_buttons').hide();
                     $('#span_one').addClass('custom_click_tab_active');
                     $('#span_two').removeClass('custom_click_tab_active');
                     $('#span_two,#span_three,#span_four').addClass('custom_click_tab');
               });

               $('#next_button_second').click(function() {
                     $('#third_set_buttons').show();
                     $('#tab_one').hide();
                     $('#tab_two').hide();
                     $('#tab_three').show();
                     $('#second_set_buttons').hide();

                     $('#span_three').addClass('custom_click_tab_active');
                     $('#span_two').removeClass('custom_click_tab_active');
                     $('#span_two,#span_one').addClass('custom_click_tab');
               });

               $('#back_button_third').click(function() {
                     $('#third_set_buttons').hide();
                     $('#tab_one').hide();
                     $('#tab_two').show();
                     $('#tab_three').hide();
                     $('#second_set_buttons').show();

                     $('#span_two').addClass('custom_click_tab_active');
                     $('#span_three').removeClass('custom_click_tab_active');
                     $('#span_one,#span_three').addClass('custom_click_tab');
               });

         });
   </script>
</div>
@stop
@section('lower_band')
<div class="col-xs-12 lower_band">
   <button id="cancel_button" class="headerbuttons" type="button" style="float: right; margin-right: 20px;">Cancel</button>
   <div class="first_set_buttons" id="first_set_buttons">
      <button id="next_button_first" class="headerbuttons" type="button"  style="float: right; margin-right: 20px;">Next</button>
   </div>
   <div class="second_set_buttons" id="second_set_buttons">
      <button id="next_button_second" class="headerbuttons" type="button"  style="float: right; margin-right: 20px;">Next</button>
      <button id="back_button_second" class="headerbuttons" type="button"  style="float: right; margin-right: 20px;">Back</button>
   </div>
   <!-- <div class="third_set_buttons" id="third_set_buttons">
      <button id="next_button_third" class="headerbuttons" type="button"  style="float: right; margin-right: 20px;">Next</button>
      <button id="back_button_third" class="headerbuttons" type="button"  style="float: right; margin-right: 20px;">Back</button>
   </div> -->
   <div class="third_set_buttons" id="third_set_buttons">
      <button id="save_button" class="headerbuttons" type="button"  style="float: right; margin-right: 20px;">Save</button>
        <button id="back_button_third" class="headerbuttons" type="button"  style="float: right; margin-right: 20px;">Back</button>   </div>
</div>
@stop
