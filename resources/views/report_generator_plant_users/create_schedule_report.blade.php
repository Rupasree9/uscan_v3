@extends('layouts.uscan_master_page_reports')
@section('header')
<link rel="stylesheet" type="text/css" href="css/schedule_reports.css">
<script src="js/schedule_reports_validation.js"></script>
@stop

@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="display-title">
      <p>Create Shedule Report Job</p>
   </div>
</div>
@stop

@section('content')
<div class="col-xs-12 content">
   <div id="tab_one">
      <form id="scheduler_reports_form" autocomplete="off">
         <table style="width:30%;">
            <tr>
               <td style="background-color: #eee;"><strong>Basic Details</strong></td>
               <td></td>
            </tr>
            <tr>
               <td></td>
            </tr>
            <tr>
               <td>Job Name</td>
               <td><input type="text" id="job_name" name="job_name"></td>
            </tr>

            <tr>
               <td>Variant</td>
               <td><select id = "variant_select" name="variant_select" >
                  </select>
               </td>
            </tr>
            <tr>
               <td>Job Priority</td>
               <td>
                  <select id = "job_priority" name="job_priority" >
                     <option id="0" value="0">Select </option>
                     <option id="1" value="1">High</option>
                     <option id="2" value="2">Medium</option>
                     <option id="3" value="3">Low</option>
                  </select>
               </td>
            </tr>
            <tr>
               <td>Active</td>
               <td><input type="checkbox" name="active" id="active"></td>
            </tr>
            <tr>
               <td></td>
            </tr>
         </table>
      </form>
       <div id="start_conditions_div" class="">
         <form id="start_conditions_form" class="">
            <table style="width:30%;">
               <tr>
                  <td style="background-color: #eee;"><strong>Start Conditions</strong></td>
                  <td></td>
               </tr>
               <tr>
                  <td>Start Conditions</td>
                  <td><select id = "start_conditions" name="start_conditions">
                     </select>
                  </td>
               </tr>

            </table>
         </form>
      </div>
      <div class="load" id="immediate">
         <form id="immediate_form" class="">
            <table style="width:30%;">
               <tr>
                  <td>Periodic</td>
                  <td><input type="checkbox" id="period_immediately" name="period_immediately" value=""></td>
               </tr>
               <tr>
                  <td>Frequency</td>
                  <td><select id="frequency" class="" name="frequency">
                     </select>
                  </td>
               </tr>
               <tr>
                  <td></td>
               </tr>
            </table>
         </form>
      </div>
      <div id="date_time_div" class="load">
         <form id="date_time_form" class="date_time_form">
            <table style="width:60%;">
              <tr>
                 <td>
                    Start Date
                 </td>
                 <td><input type="date" name="start_date" id="start_date"></td>
                 <td>
                    End Date
                 </td>
                 <td><input type="date" name="end_date" id="end_date"></td>
              </tr>
              <tr>
                 <td>Start Time</td>
                 <td><input type="time" name="start_time" id="start_time"></td>
                 <td>End Time</td>
                 <td><input type="time" name="end_time" id="end_time"></td>
              </tr>
              <tr>
                 <td>Periodic</td>
                 <td><input type="checkbox" name="periodic_date_time" id="periodic_date_time"></td>
              </tr>
            </table>
         </form>
         <form id="date_time_form_frequency">
            <table style="width:30%;">
               <tr>
                  <td id="frequency_label">Frequency</td>
                  <td><select id ="frequency_date_time" name="frequency_date_time">
                     </select>
                  </td>
               </tr>
            </table>
         </form>
      </div>
      <div id="after_job_div" class="load">
         <form id="after_job_from" class="after_job_from">
            <table style="width:30%;">
               <tr>
                  <td>Job Name</td>
                  <td>
                     <select id = "predecessor_job_name" name="predecessor_job_name">
                     </select>
                  </td>
               </tr>

            </table>
         </form>
      </div>
      <!-- <div class="" id="communication_options">
        <form id="communication_options_form" class="communication_options_form">
           <table style="width:40%;">
             <tr>
                <td></td>
             </tr>
             <tr>
                <td style="background-color: #eee;"><strong>Distribution Strategy</strong></td>
                <td></td>
             </tr>
             <tr>
                <td></td>
             </tr>
              <tr>
                 <td>Communication Options</td>
                 <td>
                    <select id = "select_comm_name" name="select_comm_name">
                      <option value="">Select</option>
                      <option value="">Send Email</option>
                      <option value="">FTP</option>
                    </select>
                 </td>
              </tr>
              <tr>
                 <td>Email ID</td>
                 <td>
                    <select id = "email_id" name="email_id">
                      <option value="">Select</option>
                      <option value="">contact1@fm.com</option>
                    </select>
                 </td>
              </tr>
           </table>
        </form>
      </div> -->
   </div>


   <script type="text/javascript">
    jQuery.validator.addMethod('selectcheck', function(value) {
        return (value != '0');
    }, "");

    function populateSelectOptions(result, selectId, valueAttr, displayAttr,isDefault) {
      $('#' + selectId).empty();
      if(!isDefault)
        $('#'+ selectId).append("<option id =\"null\" value=\"0\">Select</option>");

      if(result == null || result.length == 0) {
        alert('Data not found...!!!');
      } else {
        for (var i = 0; i < result.length; i++)
        {
          var value = result[i][valueAttr];
          var display = result[i][displayAttr];
          if(!isDefault)
          $('#'+ selectId).append("<option value='"+ value +"'>"+display+"</option>");
          else
          $('#'+ selectId).append("<option selected value='"+ value +"'>"+display+"</option>");
        }
      }
    }

    function getDataFromDB(methodType, url, dataType, selectId, valueAttr, displayAttr, data,inputType, isDefault) {

      $.ajax({
        type:methodType,
        url:url,
        dataType:dataType,
        data:data?data:{},
        success:function(result){
            populateSelectOptions(result, selectId, valueAttr, displayAttr,isDefault);
        },
        error: function() {
          alert('Error while fetching the record');
        }
      });
    }

    $(document).ready(function() {
      $('#date_time_div,#after_job_div,#immediate').hide();

      getDataFromDB('GET','get_variant_name','json', 'variant_select', 'report_id', 'variant');
      getDataFromDB('GET','get_start_conditions_for_scheduler_reports','json', 'start_conditions', 'start_type_id', 'start_type_name');


      $('#period_immediately').on('change', function() {
          if (jQuery(this).is(":checked")) {
              $("#frequency").prop('disabled', false);
          } else {
              $('#frequency').val(0);
              $("#frequency").prop('disabled', true);
          }
      });

      $('#periodic_date_time').on('change', function() {
          if (jQuery(this).is(":checked")) {
              $("#frequency_date_time").prop('disabled', false);
          } else {
              $('#frequency_date_time').val(0);
              $("#frequency_date_time").prop('disabled', true);
          }
      });

      $('#start_conditions').on('change', function() {
          if (this.value == 1) {
              $('#immediate').hide();
              $('#date_time_div,#after_job_div').hide();
              $("#frequency").prop('disabled', true);
              getDataFromDB('GET','frequency_for_scheduler_report','json', 'frequency', 'frequency_id', 'frequency_name');
          } else if (this.value == 2) {
              $('#immediate,#after_job_div').hide();
              $('#date_time_div').show();
              $("#frequency_date_time").prop('disabled', true);
              getDataFromDB('GET','frequency_for_scheduler_report','json', 'frequency_date_time', 'frequency_id', 'frequency_name');
          } else if (this.value == 3) {
              $('#immediate,#date_time_div').hide();
              $('#after_job_div').show();
              getDataFromDB('GET','get_jobs_for_scheduler_reports','json', 'predecessor_job_name', 'job_id ', 'job_name');
          }
      });


        //on first time load start
        var active = 0;
        var period_immediately = 0;
        var start_conditions = 0;
        var periodic_date_time = 0;
        var frequency_date_time = 0;
        $("#report_generator_screen").trigger('click');
        $("#schedule_reports").trigger('click');

        $(document).on('click', '#save_button', function() {

            //start_conditions = $("#start_conditions option:selected").attr('id');
            start_conditions = $("#start_conditions").val();
            console.log("start_conditions",start_conditions);
            if ($('#scheduler_reports_form').valid()) {
                if ($('#start_conditions_form').valid()) {
                    if (start_conditions == "1") {
                        if ($('#period_immediately').is(":checked")) {
                            if ($('#immediate_form').valid()) {
                                save_msg(start_conditions);
                            }
                        } else {
                            save_msg(start_conditions);
                        }
                    } else if (start_conditions == "2") {
                        if ($('#periodic_date_time').is(":checked")) {
                            if ($('#date_time_form_frequency').valid()) {
                                save_msg(start_conditions);
                            }
                        } else {
                            if ($('#date_time_form').valid()) {
                                save_msg(start_conditions);
                            }
                        }

                    } else if (start_conditions == "3") {
                        if ($('#after_job_from').valid()) {
                            save_msg(start_conditions);
                        }
                    }
                }
            }

        });

        function save_msg(start_conditions) {
            $.msgBox({
                title: "Confirm",
                content: "Do you want to save the information?",
                type: "confirm",
                buttons: [{
                    value: "Yes"
                }, {
                    value: "No"
                }],
                success: function(result) {
                    if (result == "Yes") {
                        save(start_conditions);
                    } else {

                    }
                }
            });
        }

        function save(start_conditions) {

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd
            }
            if (mm < 10) {
                mm = '0' + mm
            }
            today = yyyy + '-' + mm + '-' + dd;

            var dt = new Date();
            var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

            var job_name = $('#job_name').val();
            //var task_type = $("#task_type option:selected").attr('id');
            var variant = $("#variant_select option:selected").val();
            var job_priority = $("#job_priority option:selected").attr('id');

            var frequency = 0;

            if ($('#active').is(":checked")) {
                active = 1;
            } else {
                active = 0;
            }

            if (start_conditions == "1") {
                var job_start_date = today;
                var job_end_date = today;
                var job_last_run_date = today;
                var job_start_time = time;
                var job_end_time = time;
                var job_last_run_time = time;
                if ($('#period_immediately').is(":checked")) {
                    period_immediately = 1;
                    var frequency = $("#frequency").val();
                } else {
                    period_immediately = 0;
                    var frequency = 0;
                }
            } else if (start_conditions == "2") {
                var job_start_date = $('#start_date').val();
                var job_end_date = $('#end_date').val();
                var job_last_run_date = $('#start_date').val();
                var job_start_time = $('#start_time').val();
                var job_end_time = time;
                var job_last_run_time = $('#start_time').val();

                if ($('#periodic_date_time').is(":checked")) {
                    var periodic_date_time = 1;
                    var frequency_date_time = $("#frequency_date_time option:selected").val();;
                } else {
                    var periodic_date_time = 0;
                    var frequency_date_time = 0;
                }
            } else if (start_conditions == "3") {
                var job_start_date = today;
                var job_end_date = today;
                var job_last_run_date = today;
                var job_start_time = time;
                var job_end_time = time;
                var job_last_run_time = time;

                var after_job = $("#predecessor_job_name option:selected").attr('id');

                // var after_job = 1;
                // var predecessor_job_name = $("#predecessor_job_name option:selected").val();
                  // var predecessor_job_name = $("#predecessor_job_name").val();
                console.log('predecessor_job_name',predecessor_job_name);
            }

            $.ajax({
                type: "POST",
                url: "scheduler_save_report",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "job_name": job_name,
                    //"task_type": task_type,
                    "variant": variant,
                    "job_priority": job_priority,
                    "start_type_id": start_conditions,
                    "active": active,

                    //1.Immediate
                    "period_immediately": period_immediately,
                    "frequency": frequency,

                    //2.date_time
                    "periodic_date_time": periodic_date_time,
                    "frequency_date_time": frequency_date_time,

                    //3. After job
                    "after_job": after_job,
                    // "predecessor_job_name": predecessor_job_name,

                    "job_start_date": job_start_date,
                    "job_end_date": job_end_date,

                    "job_last_run_date": job_last_run_date,
                    "job_start_time": job_start_time,
                    "job_end_time": job_end_time,

                    "job_last_run_time": job_last_run_time,

                },
                success: function(data) {
                    after_save();
                    $.msgBox({
                        title: "Message",
                        content: "Scheduler added successfully",
                        type: "info",
                    });
                },
                beforeSend: function() {
                    $('#msg').show();

                },
                error: function() {}
            });
        }

        function after_save() {
            reset();
        }

        function reset() {
            $('#scheduler_reports_form').trigger("reset");
            $('#start_conditions_form').trigger("reset");
            $('#immediate_form').trigger("reset");
            $('#date_time_form').trigger("reset");
            $('#date_time_form_frequency').trigger("reset");
            $('#after_job_from').trigger("reset");
        }

        $('#cancel_button').click(function() {
            reset();
        });

    });
</script>
</div>
@stop

@section('lower_band')
<div class="col-xs-12 lower_band">
   <button id="cancel_button" class="headerbuttons" type="button" style="float: right; margin-right: 20px;">Cancel</button>
   <button id="save_button" class="headerbuttons" type="button"  style="float: right; margin-right: 20px;">Save</button>
</div>
@stop
