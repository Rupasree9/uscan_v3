@extends('layouts.uscan_master_page')
@section('header')
<!-- <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css"> -->
<link rel="stylesheet" type="text/css" href="css/plants_customers.css">
<!-- <script src="js/plants_customers_jquery-1.12.4.js"></script>
<script src="js/jquery.dataTables.min.js"></script> -->
@stop
@section('upper_band')
<div class="upper_band col-xs-12">
     <div class="col-xs-8 display-title">
        Plant Customer Code
     </div>
</div>
@stop
@section('content')
<div class="col-xs-12 content">
  <div class="loading">
     <img src="images/loading.gif" height="50px;">
  </div>

  <div id="list-of-plants_customers" style=" margin-right: 10px;padding-bottom: 10px;">
    <table class="data"  border="1">
    </table>
  </div>

</div>

<script type="text/javascript">

$(document).ready(function() {

   $('#list-of-plants_customers').addClass("scroll");

   function All_plants_customer() {

       $.ajax({
           type: "GET",
           url: "get_plant_customer_code",
           // dataType: "json",
           success: function(data) {

               $('.loading').css('display', 'none');
               var plant_id = data[1];
               var plant_name = data[2];
               var customer_id = data[3];
               var customer_name = data[4];
               var supplier_code = data[5];

               $('.data tr').remove();
               var length = customer_id.length;

               var width = (length + 4) * 105 ;
              //  alert(width);
               $(".data").css("width",width);

               data = '<thead><tr> <th  colspan="2" style="text-align: center;">PLANT/CUSTOMER</th><th style="text-align: center; " colspan="' + length + '">CUSTOMER CODE</th> </tr> <tr><th>PLANT NAME</th><th>PLANT DUNS NUMBER</th>';

               for (var j = 0; j < customer_id.length; j++) {

                   data = data + "<th  class = \"columnid\" id = " + "columnid" + j + " >" + customer_name[j].toUpperCase() + "</th>";

               }
               data = data + "</tr></thead>";

               $('.data').append(data);

               for (var i = 0; i < plant_id.length; i++) {

                   var res = plant_name[i].split(",");
                   var p = plant_id[i];

                   var data = "<tr class = 'change_color'> <td><b>" + res[0].toUpperCase() + "</b></td><td>" + res[1] + "</td>";

                   for (var k = 0; k < customer_id.length; k++) {
                       var c = customer_id[k];
                       // var data = data + "<td>"+supplier_code[p][c]+"</td>";
                       //change
                       if(supplier_code[p][c])
                       {
                          var data = data + "<td data-category=\"td\" class = " + "columnid" + k + " > <input disabled type='text' id = " + p + "_" + c + " value=\"" + supplier_code[p][c] + "\"></td></div>";
                       }
                       else {
                          var data = data + "<td data-category=\"td\" class =" + "columnid" + k + " >  <input disabled type='text' id = " + p + "_" + c + " value></td>";
                       }

                   }

                   $('.data').append(data);
                   // $('.data').append("<td>"+res[1]+"</td>");
                   $('.data').append("</tr>");

               }

           },
           beforeSend: function() {

               $('.loading').css('display', 'block');


           },
           error: function() {


           }
       });
   }

   All_plants_customer();

   $(document).on('click', '#update', function() {

       jsonObj = [];
       $('.changes').each(

           function(index) {
               var input = $(this);
               var value = input.val();
               var id = input.attr('id');

               var temp = id + '_' + value;
               data = {}
               data["value"] = temp;

               jsonObj.push(data);

           });

       $.msgBox({
           title: "Confirmation",
           content: "Do you want to update the data",
           type: "confirm",
           buttons: [{
               value: "Yes"
           }, {
               value: "No"
           }],
           success: function(result) {
               if (result == "Yes") {

                   func_update_data(jsonObj);

               } else {

               }
           }
       });




   });

   function func_update_data(jsonObj) {


       $.ajax({
           type: "POST",
           url: "update_plant_customer_code",
           data: {
               "_token": "{{ csrf_token() }}",
               "data": jsonObj,
           },
           success: function(data) {


               $('input:text[value]').prop('disabled', true);
               $('input:text[value]').removeClass('changes');


               $('.loading').css('display', 'none');
               $('#update').css('display', 'block');

               $.msgBox({
                   title: "Message",
                   content: data,
                   type: "info",
               });
           },


           beforeSend: function() {
               $('.loading').css('display', 'block');
               $('#update').css('display', 'none');
           },
           error: function() {

           }

       });

   }
   var td_click_count = 0;

   $(document).on('click', '.change_color', function(){

    $("tr").removeClass('highlight_color');
    $(this).addClass('highlight_color');

   });


   $(document).on('click', 'td[data-category="td"]', function(){
       var columnid =  $(this).attr("class");
       if($(this).hasClass("highlight_color_td"))
       {
               //none

       }
       else {
         $("td").removeClass('highlight_color_td');
         $("." + columnid).addClass('highlight_color_td');
       }

   });




   $(document).on('dblclick', 'td', function(event) {

      event.stopPropagation();
     var columnid =  $(this).attr("class");
     $('input:text[value]').prop('disabled', true);
     $(this).find('input').prop('disabled', false);
     $(this).find('input').addClass('changes');

  });

  $(document).on('click', '.columnid', function() {

  var columnid = this.id;

  $("td").removeClass('highlight_color_td');
  $('.'+columnid).addClass('highlight_color_td');


 });










});

$(window).load(function() {
   $("#setups").trigger('click');
   $("#cross_reference").trigger('click');
   $("#plant_customer_code").trigger('click');
});


</script>
@stop
@section('lower_band')
<div class="col-xs-12 lower_band">
 <button class="headerbuttons" type="button" id="update">Update</button>
</div>
@stop
