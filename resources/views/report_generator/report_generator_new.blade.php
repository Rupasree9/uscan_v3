@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" href="css/bootstrap-multiselect.css" type="text/css" />
<script type="text/javascript" src="js/bootstrap-multiselect.js"></script>
<script type="text/javascript" src="js/jquery.table2excel.js"></script>
<!-- <script type="text/javascript" src="js/jspdf.js"></script>
<script type="text/javascript" src="js/base64.js"></script> -->
<link rel="stylesheet" href="css/report.css">
<link rel="stylesheet" href="css/reports.css">
<script src="js/report_generator.js"></script>
@stop

@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="col-xs-4 display-title">
    Report Generator
   </div>
   <div id="show_all_link" class="col-xs-2" style="float: right;text-align:center;line-height:30px;">
      <span  id="show_all" style="">Show All Variants</span>
   </div>
</div>
@stop



@section('content')
<div class="col-xs-12 content scrollRemove">
  <div class="loading">
     <img src="images/loading.gif" height="50px;">
  </div>
  <div id="report_gen">
    <form id="search-para" autocomplete="off">
       <table class="hide_show" style="width:80%;">
          <tr>
             <td style="background-color: #eee;">Search Parameters</td>
          </tr>
          <tr>
             <td>Report Type </td>
             <td><select id="report_type_id" name="report_type_name">
             </select>
             </td>
             <td>Start Date</td>
             <td><input type="date" id="start_date" name="start_date"></td>
          </tr>
          <tr>
            <td>Report Name </td>
            <td><select id="report_name" name="report_name1">
            </select>
            </td>

             <td>End Date</td>
             <td><input type="date" id="end_date" name="end_date"></td>
          </tr>
          <tr>
            <td>Variant </td>
            <td><input type="text" id="variant" name="variant"><select id="variant_select" name="variant_select1"></td>
          </tr>
          <tr>
            <td>Business Unit</td>
            <td><select id="business_unit" multiple size="2">
            </select></td>

          </tr>
          <tr>
            <td>Product Group</td>
            <td><select id="product_group"  multiple size="2"></select></td>

          </tr>
          <tr>
            <td>Plant Name</td>
            <td><select id="plant_name"  multiple size="2"></select></td>

          </tr>
          <!-- <tr>
            <td>Customer Type</td>
            <td><select id="cust_type"  multiple size="2"></select></td>

          </tr> -->
          <tr>
            <td>Customer Name</td>
            <td><select id="cust_name"  multiple size="2"></select></td>
          </tr>

          <tr>
            <td>Plant Status</td>
            <td><select id="plant_status"  multiple size="2"></select></td>
          </tr>
       </table>
    </form>
    <div id="list-of-variant"  class="div1">
     <table style="width:100%" class="tab">
     </table>
   </div>
  </div>
  <div id= "report_gm_div" class="hide_div" >
    <table  id = "report" style="width:80%;" border="1">
    </table>
  </div>
  <div id= "report_gm_view1_div" class="hide_div">
    <table id = "report_view1" style="width:40%;">

    </table>
 </div>

 <div id= "report_gm_view2_div" class="hide_div">
   <table id = "report_view2" style="width:40%;">

   </table>
 </div>
<div><iframe id="txtArea1" style="display:none"></iframe></div>
</div>


<script type="text/javascript">
var results;
jQuery.validator.addMethod('selectcheck', function(value) {
    return (value != '0');
}, "");

$('#business_unit,#product_group,#plant_name,#cust_name,#plant_status').multiselect({
  includeSelectAllOption: true
});

$("#report_generator").trigger('click');

function populateSelectOptions(result, selectId, valueAttr, displayAttr, isDefault,checkmultiselect) {
  $('#' + selectId).empty();
  if(!isDefault && !checkmultiselect){
     $('#'+ selectId).append("<option id =\"null\" value=\"\">--</option>");
  }


  if(result == null || result.length == 0) {
    // alert('Data not found...!!!');
  } else {
    for (var i = 0; i < result.length; i++)
    {
      var value = result[i][valueAttr];
      var display = result[i][displayAttr];

      if(!isDefault)
      {
        if(value != '' && display != "ALL Plants" && display != "ALL Customers")
        $('#'+ selectId).append("<option value='"+ value +"'>"+display+"</option>");
        console.log('Value on selected options',display);
      }
      else
      {
        $('#'+ selectId).append("<option selected value='"+ value +"'>"+display+"</option>");
      }

    }
  }
}

function populateTextBox(result, selectId, valueAttr, displayAttr) {
  if(result == null || result.length == 0) {
    // alert('Data not found...!!!');
  } else {
    for (var i = 0; i < result.length; i++)
    {
      var value = result[i][valueAttr];
      var display = result[i][displayAttr];
      $('#'+ selectId).val(display);
    }
  }
}

function populateEditOptions(result, selectId, valueAttr) {
  console.log('selectId',selectId);console.log('valueAttr',valueAttr);
  if(result != undefined && result.length != 0) {
    var valueArr = [];
    for (var i = 0; i < result.length; i++)
    {
      valueArr.push(result[i][valueAttr]);
    }
    $('#'+ selectId).val(valueArr);
  }
}

function getDataFromDB(methodType, url, dataType, selectId, valueAttr, displayAttr, data, inputType, isDefault, onChangeFunction,checkmultiselect) {

  $.ajax({
    type:methodType,
    url:url,
    dataType:dataType,
    data:data?data:{},
    success:function(result){
      if(inputType == 'textbox') {
        populateTextBox(result, selectId, valueAttr, displayAttr);
      }
      else if(inputType == 'editMode') {
        populateEditOptions(result, selectId, valueAttr)
      }
      else {
        populateSelectOptions(result, selectId, valueAttr, displayAttr, isDefault,checkmultiselect);
        console.log("result",result);
      }
      $('#business_unit,#product_group,#plant_name,#cust_type,#cust_name,#plant_status').multiselect('rebuild');
      if(onChangeFunction)
        onChangeFunction(result);
    },
    error: function() {
      alert('Error while fetching the record');
    }
  });
}

function onChangeEvent(selectedId, methodType, url, altUrl, dataType, selectId, valueAttr, displayAttr, inputType, onChangeFunction,checkmultiselect){
  $('#'+selectedId).on('change', function(){
    var selectedValue = $('#'+selectedId).val();

    if(Array.isArray(selectedValue)) {
      selectedValue = selectedValue.join('\',\'');
    }

    var data = {"_token": "{{ csrf_token() }}", filterKey:selectedValue};
    console.log('selectedValue',selectedValue);

    if(selectedValue == null){
       url1 = altUrl;
    }
    else {
      url1 = url;
    }

    getDataFromDB(methodType, url1, dataType, selectId, valueAttr, displayAttr, data, inputType, '', onChangeFunction,checkmultiselect);

  });

}

function onReportTypeChangeFunction(){
  var selectedValue = $('#report_type_id').val();
  var disableDate = selectedValue== 1? true : false;

  $('#end_date').prop('disabled', disableDate);
  $('#start_date').prop('disabled', disableDate);

  if(selectedValue == '') {
    $('#report_name').prop('disabled', true);
    $('#report_name').css("background-color","#DADBD9");
    disableDate = true;
  } else {
    $('#report_name').prop('disabled', false);
    $('#report_name').css("background-color","#ffffff");
  }
  console.log('disableDate',!disableDate);

  if(!disableDate) {
    // on select Customer specific reports

    var data = {"_token": "{{ csrf_token() }}", filterKey:selectedValue};
    $('#cust_name').prop('disabled', true);
    $('#cust_name').css("background-color","#DADBD9");

    console.log('disableDate',!disableDate);

    getDataFromDB('GET','get_custType_custName_byReportType','json', 'cust_name', 'customer_id', 'customer_name', data,'',true,'',true);
    getDataFromDB('GET','get_bu_by_report_type','json', 'business_unit', 'business_unit', 'business_unit', data,'','','',true);
    getDataFromDB('GET','get_pg_by_report_type','json', 'product_group', 'product_group', 'product_group', data,'','','',true);
    getDataFromDB('GET','get_plant_by_report_type','json', 'plant_name', 'plant_id', 'plant_name', data,'','','',true);
    getDataFromDB('GET','get_plant_status','json', 'plant_status', 'plant_status_id', 'plant_status',data,'','','',true);

  } else {

    // on select Masterdashboard reports
    $('#cust_name').prop('disabled', false);
    $('#cust_name').css("background-color","#ffffff");

    console.log('disable data ', disableDate);
    getDataFromDB('GET','get_cust_name','json', 'cust_name', 'customer_id', 'customer_name', '','','','',true);
    getDataFromDB('GET','get_business_unit','json', 'business_unit', 'business_unit', 'business_unit', '','','','',true);
    getDataFromDB('GET','get_product_group','json', 'product_group', 'product_group', 'product_group', '','','','',true);
    getDataFromDB('GET','get_plant_name','json', 'plant_name', 'plant_id', 'plant_name', '','','','',true);
    getDataFromDB('GET','get_plant_status','json', 'plant_status', 'plant_status_id', 'plant_status', '','','','',true);
  }
}

function reset() {
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1;
  var yyyy = today.getFullYear();

  if(dd<10){dd='0'+ dd} if(mm<10){mm='0'+mm}
  today = yyyy+'-'+mm+'-'+dd;

  $('#start_date').attr('value', today);
  $('#end_date').val("");


  $('#report_name').empty();
  $('#variant').val('');

  $('#report_name').prop('disabled', true);
  $('#report_name').css("background-color","#DADBD9");  //Disable report name after save

  $('#cust_name').prop('disabled', false);
  $('#cust_name').css("background-color","#ffffff");

  getDataFromDB('GET','get_report_type','json', 'report_type_id', 'report_type_id', 'report_type_name');
  getDataFromDB('GET','get_product_group','json', 'product_group', 'product_group', 'product_group', '','','','',true);
  getDataFromDB('GET','get_plant_name','json', 'plant_name', 'plant_id', 'plant_name', '','','','',true);
  getDataFromDB('GET','get_business_unit','json', 'business_unit', 'business_unit', 'business_unit', '','','','',true);
  getDataFromDB('GET','get_plant_status','json', 'plant_status', 'plant_status_id', 'plant_status', '','','','',true);

  getDataFromDB('GET','get_cust_name','json', 'cust_name', 'customer_id', 'customer_name', '','','','',true);
  getDataFromDB('GET','get_variant_name','json', 'variant_select', 'report_id', 'variant');
}

function selectFieldValueOnEdit(data) {
  report_id = data[0].report_id;
  var reportTypeId = data[0].report_type_id;
  console.log('data', data);

  $('#report_type_id').val(reportTypeId);
  $('#report_name').prop('disabled', false);
  $('#report_name').css("background-color","#ffffff");  //Disable report name after save

  var data1 = {"_token": "{{ csrf_token() }}", filterKey:reportTypeId};
  getDataFromDB('GET', 'get_report_name_by_report_type', 'json', 'report_name', 'report_name_id', 'report_name', data1);

  var data = {"_token": "{{ csrf_token() }}","report_id": report_id};

  getDataFromDB('GET','get_plant_by_reportID','json', 'plant_name', 'plant_id', 'plant_name',data,'editMode');
  getDataFromDB('GET','get_bu_by_reportID','json', 'business_unit', 'business_unit', 'business_unit',data,'editMode');
  getDataFromDB('GET','get_pg_by_reportID','json', 'product_group', 'product_group', 'product_group',data,'editMode');
  getDataFromDB('GET','get_customer_by_reportID','json', 'cust_name', 'customer_id', 'customer_name',data,'editMode');  //'',true
  getDataFromDB('GET','get_report_name_by_reportID','json', 'report_name', 'report_name_id', 'report_name',data,'editMode');
  getDataFromDB('GET','get_report_name_by_reportID','json', 'start_date', 'start_date', 'start_date',data,'editMode');
  getDataFromDB('GET','get_report_name_by_reportID','json', 'end_date', 'end_date', 'end_date',data,'editMode');
  getDataFromDB('GET','get_plant_status_by_reportID','json', 'plant_status', 'plant_status_id', 'plant_status',data,'editMode');

}

function save_update_show(){
('#update_button').hide();
('#save_variant').show();
console.log('save_update');
}

$(document).ready(function() {
$('#update_button,#cancel_button,#back_button_listScreen').hide();
    reset();

   onChangeEvent('report_type_id','GET', 'get_report_name_by_report_type','get_report_name_by_report_type', 'json', 'report_name', 'report_name_id', 'report_name', '', onReportTypeChangeFunction);
   onChangeEvent('business_unit', 'GET','get_product_group_by_business_unit','get_product_group','json', 'product_group', 'product_group', 'product_group','','',true);
   onChangeEvent('product_group', 'GET','get_plant_name_by_product_group','get_plant_name','json', 'plant_name', 'plant_id', 'plant_name','','',true);
   onChangeEvent('variant_select','GET', 'get_selected_variant_name','get_selected_variant_name', 'json', 'variant', 'report_id', 'variant', 'textbox',selectFieldValueOnEdit);


    $(document).on('click', '#save_variant', function(){
           if ($('#search-para').valid()) {
              var report_type_id = $('#report_type_id').val();
              var report_name_id = $('#report_name').val();
              var variant = $('#variant').val();
              var variant_select = $('#variant_select').find('option:selected').text();
              var business_unit = $('#business_unit').val();
              var product_group = $('#product_group').val();

              var plant_name = $('#plant_name').val();
              var cust_type = $('#cust_type').val();
              var cust_id = $('#cust_name').val();
              var plant_status_id = $('#plant_status').val();

              var start_date = $('#start_date').val();
              var end_date = $('#end_date').val();

              if(report_type_id == "" || report_name_id == "" || variant == ""){
                $.msgBox({
                    title: "Alert",
                    content: "Please enter the Report Type,<br>Report Name and variant to continue",
                    type: "alert",
                });
              }
              else if ((variant == variant_select) ) {
                  $.msgBox({
                      title: "Alert",
                      content: "Variant already exists..</br> Please enter the new variant name",
                      type: "alert",
                  });
               }

              else{
                        $.ajax({
                          type: "POST",
                          url: "save_varient",
                          data: {
                                    "_token": "{{ csrf_token() }}",
                                    "report_type_id": report_type_id,
                                    "report_name_id":report_name_id,
                                    "variant": variant,
                                    "business_unit":business_unit,
                                    "product_group": product_group,
                                    "plant_id": plant_name,
                                    "customer_type_id": cust_type,
                                    "cust_id": cust_id,
                                    "start_date":start_date,
                                    "end_date":end_date,
                                    "plant_status_id":plant_status_id
                                  },
                          success: function(data) {
                            $.msgBox({
                                title: "Message",
                                content: "Variant added successfully",
                                type: "info",
                            });
                              reset();
                              $('#start_date').prop('disabled', false);
                              $('#end_date').prop('disabled', false);              //enable cust name and cust type after save
                              $('#end_date,#start_date').css("background-color","#ffffff");
                          }
                        });
                        }
          }
          else {
          }
     });

     $(document).on('click', '#update_button', function(){
            if ($('#search-para').valid()) {
               var report_type_id = $('#report_type_id').val();
               var report_name_id = $('#report_name').val();
               var variant = $('#variant').val();
               var variant_select = $('#variant_select').find('option:selected').text();
               var business_unit = $('#business_unit').val();
               var product_group = $('#product_group').val();

               var plant_name = $('#plant_name').val();
               var cust_type = $('#cust_type').val();
               var cust_id = $('#cust_name').val();
               var plant_status_id = $('#plant_status').val();

               var start_date = $('#start_date').val();
               var end_date = $('#end_date').val();

                 $.msgBox({
                     title: "Confirm",
                     content: "Do you want to update the existing variant?",
                     type: "confirm",
                     buttons: [{
                         value: "Yes"
                     }, {
                         value: "No"
                     }],
                     success: function(result) {
                       $('.loading').css('display', 'none');
                         if (result == "Yes") {
                           $.ajax({
                             type: "POST",
                             url: "update_varient",
                             data: {
                                       "_token": "{{ csrf_token() }}",
                                       "report_id":report_id,
                                       "report_type_id": report_type_id,
                                       "report_name_id":report_name_id,
                                       "variant": variant,
                                       "business_unit":business_unit,
                                       "product_group": product_group,
                                       "plant_id": plant_name,
                                       "customer_type_id": cust_type,
                                       "cust_id": cust_id,
                                       "start_date":start_date,
                                       "end_date":end_date,
                                       "plant_status_id":plant_status_id
                                     },
                             success: function(data) {

                               $.msgBox({
                                   title: "Message",
                                   content: "Variant updated successfully",
                                   type: "info",
                               });
                               $('#update_button,#cancel_button').hide();
                               $("#show_all_link").trigger("click");
                               $("#show_all").trigger("click");
                               $('#list-of-variant').show();
                               reset();
                             }
                           });
                         }
                       }
                   });

               }
      });

     $(document).on('click', '#show_all', function() {
        console.log('showall Click');
         hide_buttons_show_all();
         show_table();
         $('#back_button_listScreen').show();

     });
     $(document).on('click', '#delete_showall', function() {
          var id = $(this).data("id");
          var report_id = results[id].report_id;

         $.msgBox({
             title: "Confirm",
             content: "Do you want to delete the data",
             type: "confirm",
             buttons: [{
                 value: "Yes"
             }, {
                 value: "No"
             }],
             success: function(result) {
                 if (result == "Yes") {
                     $.ajax({
                         type: "POST",
                         url: "delete_variant",
                         data: {
                             "_token": "{{ csrf_token() }}",
                             "report_id": report_id,
                         },
                         success: function() {
                             show_table();
                             $.msgBox({
                                 title: "Message",
                                 content: "Variant deleted successfully",
                                 type: "info",
                             });
                         },
                         beforeSend: function() {

                         },
                         error: function() {
                           $.msgBox({
                               title: "Message",
                               content: "Can't Delete the Variant,</br> Job has scheduled for this Variant ",
                               type: "info",
                           });
                         }
                     });
                 }
             }
         });
     });

     function hide_buttons_show_all(){
       reset();
       $('#list-of-variant').show();
       $('#search-para,#filter,#save_variant').hide();
      //  $('#search-para,#filter,#save_variant,#show_all').hide();
       console.log('Hide_buutons_div');

     }

     function show_table() {

         $.ajax({
             type: "GET",
             url: "get_variant_details",
             dataType: "json",
             success: function(data) {
               $('.loading').css('display', 'none');
                 results = data;
                 $('.tab tr').remove();
                 $('.tab').append('<tr style="background-color: #D3D3D3;"><th style="display:none">Report ID</th><th>Variant</th><th>Report Type</th><th>Report Name</th><th>Edit</th><th>Delete</th></tr>');
                 console.log('show_table_function results',results);

                 if (results.length > 12) {
                     $('#list-of-variant').addClass("scroll");
                 } else {
                     $('#list-of-variant').removeClass("scroll");
                 }

                 for (var i = 0; i < results.length; i++) {
                     $('.tab').append('<tr data-id="' + i + '" class="double_click" ><td class="report_id"  style="display:none">' + results[i].report_id + '</td><td>' + results[i].variant + '</td><td>' + results[i].report_name + '</td><td>' + results[i].report_type_name + '</td><td><button id="' + i + '" class="edit btn btn-default glyphicon glyphicon-edit edit_color" type="button"></button></td><td><button  data-id="' + i + '" id="delete_showall" type="button" class="btn btn-default delete_color" aria-label="Left Align"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td></tr>')
                 }
             },
             beforeSend: function() {
                $('.loading').css('display', 'block');
             },
             error: function() {
             }
         });
     }

     $('#cancel_button').click(function(){
       $('#update_button,#cancel_button,#search-para,#filter,#save_variant').hide();
       $('#list-of-variant,#back_button_listScreen,#back_button_listScreen').show();

     });

     $('#back_button_listScreen').click(function(){
       $('#update_button,#cancel_button,#list-of-variant,#back_button_listScreen').hide();
       $('#search-para,#filter,#save_variant,#show_all').show();
       reset();


     });

     $(document).on('click', '.edit', function() {
          var id = $(this).attr("id");
          report_id = results[id].report_id;
          var reportTypeId = results[id].report_type_id;

          $("#variant").val(results[id].variant);
          var data = [{'report_id':report_id, 'report_type_id': reportTypeId}];
            $('#list-of-variant,#filter,#save_variant,#back_button_listScreen').hide();
            $('#search-para,#update_button,#cancel_button').show();
          selectFieldValueOnEdit(data);
     });

     $('#filter').click(function(){
       $('.content').removeClass('scrollRemove');
       $('.content').addClass('scrollAdd');


      //  var dt = new Date();
      //  var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
      //  alert(time);

       var report_type_id = $('#report_type_id').val();
       var report_name_id = $('#report_name').val();
       var variant = $('#variant').val();
       var variant_select = $('#variant_select').find('option:selected').text();
       var business_unit = $('#business_unit').find('option:selected').map(function () {return $(this).text();}).get().join(',');
       var product_group = $('#product_group').find('option:selected').map(function () {return $(this).text();}).get().join(',');
       var plant_id = $('#plant_name').find('option:selected').map(function () {return $(this).val();}).get().join(',');
       //var cust_type = $('#cust_type').find('option:selected').map(function () {return $(this).text();}).get().join(',');
       var cust_name = $('#cust_name').find('option:selected').map(function () {return $(this).text();}).get().join(',');
       //var plant_status = $('#plant_status').find('option:selected').map(function () {return $(this).text();}).get().join(',');
       //alert(plant_status);
       var plant_status = "";
       var cust_id = $('#cust_name').find('option:selected').map(function () {return $(this).val();}).get().join(',');
       var start_date = $('#start_date').val();
       var end_date = $('#end_date').val();
       //alert(report_type_id + ".."+report_name_id+".."+variant+".."+variant_select+".."+business_unit+".."+product_group+".."+plant_name+".."+cust_type+".."+cust_id+".."+start_date+".."+end_date);
       $.ajax({

           type: "POST",
           url: "gmreport_master",
           dataType: "json",
           data:{
             "_token": "{{ csrf_token() }}",
             "report_type_id": report_type_id,
             "report_name_id": report_name_id,
             "variant": variant,
             "business_unit": business_unit,
             "product_group": product_group,
             "plant_id": plant_id,
             //"cust_type": cust_type,
             "cust_id": cust_id,
             "cust_name": cust_name,
             "start_date": start_date,
             "end_date": end_date,
             "plant_status":plant_status,
           },
           success: function(data) {
             if(data != 0)
             {
               $('#report_gen').css('display','none');
               $('#report_gen_btns').css('display','none');
               $('#report_gm_div').css('display','block');
               $('#report tr').remove();
              var results = data;
              console.log(JSON.stringify(data));
               var cust_name = results[2];
               //alert(customer_name);
             //  alert(results[0].length);
             // for (var i = 0; i < results[0].length; i++) {    //for loop starts here....

              //  var plant_name = results[i].name + "-" + results[i].location;
               // var plant_name = results[0][i].plant_name;

                if(cust_name == "General Motor Corporation")
                {
                  //alert(plant_name);
                  for (var i = 0; i < results[0].length; i++)
                  {
                    var plant_name = results[0][i].plant_name;
                    var duns_no = results[0][i].duns_no;
                    var quality = results[0][i].quality;
                    var supplychain = results[0][i].supplychain;
                    var cca = results[0][i].cca;
                    var transaction_date = results[0][i].transaction_date;
                    $('#gm_date').text(transaction_date);
                    td_value = set_gmtd(duns_no,quality,supplychain,cca,cust_name.toUpperCase(),plant_name.toUpperCase());
                    if(i == 0)
                    {
                      $('#report tr').remove();
                      $('#report').append("<tr><td class = 'report_subtitle'>CUSTOMERS / PLANTS</td><td class = 'report_subtitle'>"+results[2].toUpperCase()+"</td></tr>");
                      $('#report').append("<tr id='tr_date'><td class='report_subtitle'>LATEST DATA AS PER DATE</td><td id='gm_date' class = 'report_subtitle'></td>")
                      $('#report').append("<tr><td class = 'report_row'>"+plant_name+"</td>"+td_value+"</tr>");
                    }
                    else {
                      $('#report').append("<tr><td class = 'report_row'>"+plant_name+"</td>"+td_value+"</tr>");
                    }
                  }
                    //alert("Number of Rows : "+$('#report tr').length);
                  $('#report').append("<tr><td colspan='2'><input type ='button' value='Back' id='goto_main' class='report_gm_back' /></td></tr>");
                }
               //  else if(cust_name == "FCA Production,FCA Mopar" || cust_name == "FCA Mopar" || cust_name == "FCA Production"){
               //    for (var i = 0; i < results[0].length; i++){
               //      var plant_name = results[0][i].plant_name;
               //      var tdid_prdn = results[0][i].supplier_code+"_prdn";
               //      var tdid_mopar = results[0][i].supplier_code+"_mopar";
               //      var overall_prdn = results[0][i].overall;
               //      var overall_mopar = results[1][i].overall;
               //      var cca = results[0][i].cca;
               //      var tdcolor_prdn;
               //      var tdcolor_mopar;
               //      td_value = set_fcatd(results[0][i].supplier_code,overall_prdn.substring(0,overall_prdn.length-1),overall_mopar.substring(0,overall_mopar.length-1),cust_name.toUpperCase(),plant_name.toUpperCase());
               //      if(i == 0)
               //      {
               //        $('#report tr').remove();
               //        $('#report').append("<tr><td class='report_subtitle'>CUSTOMERS / PLANTS</td><td class = 'report_subtitle'>Fiat Chrysler Automobiles - PRODUCTION</td><td class = 'report_subtitle'>Fiat Chrysler Automobiles - MOPAR</td></tr>");
               //        $('#report').append("<tr><td class = 'report_row'>"+plant_name+"</td>"+td_value+"</tr>");
               //      }
               //      else {
               //        $('#report').append("<tr><td class = 'report_row'>"+plant_name+"</td>"+td_value+"</tr>");
               //      }
               //   }
               //    $('#report').append("<tr><td colspan='3'><input type ='button' value='Back' id='goto_main' class='report_gm_back' /></td></tr>");
               //  }
               else if(cust_name == "FCA Production,FCA Mopar" || cust_name == "FCA Mopar" || cust_name == "FCA Production"){
                 for (var i = 0; i < results[0].length; i++){
                   var plant_name = results[0][i].plant_name;
                   var customer_name = results[0][i].customer_name;
                   var tdid = "";
                   var overall = "";
                   //alert(customer_name);
                   if(customer_name == "FCA Production")
                   {
                     tdid = results[0][i].supplier_code+"_prdn";
                     overall = results[0][i].overall;
                     var transaction_date = results[0][i].transaction_date;
                     $('#fcap_date').text(transaction_date);
                     //alert("Production : "+overall);
                   }
                   else
                   {
                     tdid = results[0][i].supplier_code+"_mopar";
                     overall = results[0][i].overall;
                     var transaction_date = results[0][i].transaction_date;
                     $('#fcam_date').text(transaction_date);
                     //alert("Mopar : "+overall);
                   }
                   if(overall != "")
                   {
                     td_value = set_fcatd(results[0][i].supplier_code,overall.substring(0,overall.length-1),cust_name.toUpperCase(),plant_name.toUpperCase());
                   }
                   else {
                     td_value = "<td></td>";
                   }

                   if(i == 0)
                   {
                     $('#report tr').remove();
                     $('#report').append("<tr><td class='report_subtitle'>CUSTOMERS / PLANTS</td><td class = 'report_subtitle'>"+results[1].toUpperCase()+"</td></tr>");
                     if(customer_name == "FCA Production")
                     {
                       $('#report').append("<tr id='tr_date'><td class='report_subtitle'>LATEST DATA AS PER DATE</td><td id='fcap_date' class = 'report_subtitle'></td>")
                     }
                     else {
                       $('#report').append("<tr id='tr_date'><td class='report_subtitle'>LATEST DATA AS PER DATE</td><td id='fcam_date' class = 'report_subtitle'></td>")
                     }
                     $('#report').append("<tr><td class = 'report_row'>"+plant_name+"</td>"+td_value+"</tr>");
                   }
                   else {
                     $('#report').append("<tr><td class = 'report_row'>"+plant_name+"</td>"+td_value+"</tr>");
                   }
                }
                 $('#report').append("<tr><td colspan='2'><input type ='button' value='Back' id='goto_main' class='report_gm_back' /></td></tr>");
               }
                else if(cust_name == "Ford Motor Company")
                {
                  for (var i = 0; i < results[0].length; i++){
                    var plant_name = results[0][i].plant_name;
                    var tdid = results[0][i].supplier_code+"_ford";
                    var q1_status = results[0][i].present_q1_status;
                    var transaction_date = results[0][i].transaction_date;
                    $('#ford_date').text(transaction_date);
                    var td_value = set_fordtd(results[0][i].supplier_code,results[0][i].present_q1_status,cust_name.toUpperCase(),plant_name.toUpperCase());
                    if(i == 0)
                    {
                      $('#report tr').remove();
                      $('#report').append("<tr><td class='report_subtitle'>CUSTOMERS / PLANTS</td><td class = 'report_subtitle'>"+results[2].toUpperCase()+"</td></tr>");
                      $('#report').append("<tr id='tr_date'><td class='report_subtitle'>LATEST DATA AS PER DATE</td><td id='ford_date' class = 'report_subtitle'></td>")
                      $('#report').append("<tr><td class = 'report_row'>"+plant_name+"</td>"+td_value+"</tr>");
                    }
                    else {
                      $('#report').append("<tr><td class = 'report_row'>"+plant_name+"</td>"+td_value+"</tr>");
                    }
                  }
                  $('#report').append("<tr><td colspan='2'><input type ='button' value='Back' id='goto_main' class='report_gm_back' /></td></tr>");
                }
                else if(cust_name == "PSA"){
                  for (var i = 0; i < results[0].length; i++){
                    var plant_name = results[0][i].plant_name;
                    var quality = results[0][i].quality_psa;
                    var supplychain = results[0][i].supplychain_psa;
                    var aftersales = results[0][i].aftersales_psa;
                    var manufacturing_duns = results[0][i].supplier_code;
                    var transaction_date = results[0][i].transaction_date;
                    $('#psa_date').text(transaction_date);
                    td_value = set_psatd(manufacturing_duns,quality,supplychain,aftersales,cust_name.toUpperCase(),plant_name.toUpperCase());
                    if(i == 0)
                    {
                      $('#report tr').remove();
                      $('#report').append("<tr><td class='report_subtitle'>CUSTOMERS / PLANTS</td><td class = 'report_subtitle'>"+results[2].toUpperCase()+"</td></tr>");
                      $('#report').append("<tr id='tr_date'><td class='report_subtitle'>LATEST DATA AS PER DATE</td><td id='psa_date' class = 'report_subtitle'></td>")
                      $('#report').append("<tr><td class = 'report_row'>"+plant_name+"</td>"+td_value+"</tr>");
                    }
                    else {
                      $('#report').append("<tr><td class = 'report_row'>"+plant_name+"</td>"+td_value+"</tr>");
                    }
                  }
                  $('#report').append("<tr><td colspan='2'><input type ='button' value='Back' id='goto_main' class='report_gm_back' /></td></tr>");
                }
                else if(cust_name == "master")
                {
                  var td_value = "<tr id=tr_head><td class='report_subtitle'>CUSTOMERS / PLANTS</td>";
                 //  if(results[4].length > 2 && results[4].length < 4)
                 //  {
                 //     $('#report').attr("width", "60%");
                 //  }
                 //  else if(results[4].length >=4 )
                 //  {
                 //     $('#report').attr("width", "80%");
                 //  }
                  for (i=0;i<results[4].length;i++)
                  {
                    if(results[4][i].customer_name == "Fiat Chrysler Automobiles")
                    {
                      td_value = td_value + "<td class = 'report_subtitle'>"+results[4][i].customer_name.toUpperCase()+" - PRODUCTION</td>";
                      td_value = td_value + "<td class = 'report_subtitle'>"+results[4][i].customer_name.toUpperCase()+" - MOPAR</td>";
                    }
                    else {
                      td_value = td_value + "<td class = 'report_subtitle'>"+results[4][i].customer_name.toUpperCase()+"</td>";
                    }

                  }
                  td_value = td_value + "</tr>";

                  td_value = td_value + "<tr id='tr_date'><td class='report_subtitle'>LATEST DATA AS PER DATE</td>";
                  for (i=0;i<results[4].length;i++)
                  {
                    var date_id = results[4][i].customer_id + "_date";
                    td_value = td_value + "<td id = "+date_id+" class = 'report_subtitle'></td>";
                  }
                  td_value = td_value + "</tr>";

                  for(i=0;i<results[3].length;i++)
                  {
                    var tr_id = results[3][i].plant_id;
                    td_value = td_value + "<tr id ="+tr_id+"><td class = 'report_row'>"+results[3][i].plant_name.toUpperCase()+"</td></tr>";
                  }
                  $('#report').append(td_value);
                  $('#report').append("<tr><td colspan='6'><input type ='button' value='Back' id='goto_main' class='report_gm_back' /></td></tr>");
                  for(i=0;i<results[4].length;i++)
                  {
                    var customer_id = results[4][i].customer_id;
                    var customer_name = results[4][i].customer_name;
                    //alert("Total Plants : "+results[3].length);
                    for(j=0;j<results[3].length;j++)
                    {
                      var plant_id = results[3][j].plant_id;
                      var plant_name = results[3][j].plant_name;
                      var plant_ids = 0;
                      var plant_id_empty = "#"+plant_id;
                      for(k=0;k<results[0].length;k++)
                      {
                        if(results[0][k].plant_id == plant_id && results[0][k].customer_id == customer_id)
                        {
                          plant_ids =1;
                          //alert(customer_name+","+plant_name);
                          if(customer_name == "General Motor Corporation")
                          {
                            if(results[0][k].quality != null && results[0][k].supplychain != null && results[0][k].cca)
                            {
                              //  alert(plant_name + "," + results[0][k].duns_no);
                                var td_cvalue = set_gmtd(results[0][k].supplier_code,results[0][k].quality,results[0][k].supplychain,results[0][k].cca,customer_name.toUpperCase(),plant_name.toUpperCase());
                                var append_tr = "#"+plant_id;
                                $(append_tr).append(td_cvalue);
                                plant_ids == 0;
                                var trans_id = "#"+customer_id+"_date";
                                var transaction_date = results[0][k].trans_date_gm;
                                $(trans_id).text(transaction_date);
                            }
                            else {
                              //alert("inside null");
                                $(plant_id_empty).append("<td></td>");
                                plant_ids == 0;
                            }

                          }
                          else if(customer_name == "Ford Motor Company")
                          {
                            if(results[0][k].present_q1_status != null)
                            {
                              if(results[0][k].present_q1_status != "")
                              {
                                var td_cvalue = set_fordtd(results[0][k].supplier_code,results[0][k].present_q1_status,customer_name.toUpperCase(),plant_name.toUpperCase());
                                var append_tr = "#"+plant_id;
                                $(append_tr).append(td_cvalue);
                                plant_ids == 0;
                                var trans_id = "#"+customer_id+"_date";
                                var transaction_date = results[0][k].trans_date_fd;
                                $(trans_id).text(transaction_date);
                              }
                              else {
                                //alert("inside null");
                                  $(plant_id_empty).append("<td></td>");
                                  plant_ids == 0;
                              }

                            }
                            else {
                              //alert("inside null");
                                $(plant_id_empty).append("<td></td>");
                                plant_ids == 0;
                            }
                          }
                         //  else if(customer_name == "Fiat Chrysler Automobiles"){
                         //    var td_cvalue = set_fcatd(results[0][k].supplier_code,results[0][k].overall_prdn.substring(0,results[0][k].overall_prdn.length-1),results[0][k].overall_mopar.substring(0,results[0][k].overall_mopar.length-1),customer_name.toUpperCase(),plant_name.toUpperCase());
                         //    var append_tr = "#"+plant_id;
                         //    $(append_tr).append(td_cvalue);
                         //    var fcap_trans_id = "#"+customer_id+"_date_fcap";
                         //    var fcam_trans_id = "#"+customer_id+"_date_fcam";
                         //    var transaction_date_fcap = results[0][k].trans_date_fca;
                         //    var transaction_date_fcam = results[0][k].trans_date_fcm;
                         //    $(fcam_trans_id).text(transaction_date_fcam);
                         //    $(fcap_trans_id).text(transaction_date_fcap);
                         //  }
                         else if(customer_name == "FCA Production")
                         {
                           //alert(results[0][k].overall_prdn);
                           if(results[0][k].overall_prdn != null)
                           {
                             var td_cvalue = set_fcatd(results[0][k].supplier_code,results[0][k].overall_prdn.substring(0,results[0][k].overall_prdn.length-1),customer_name.toUpperCase(),plant_name.toUpperCase());
                             var append_tr = "#"+plant_id;
                             $(append_tr).append(td_cvalue);
                             plant_ids == 0;
                             var fcap_trans_id = "#"+customer_id+"_date";
                             //var fcam_trans_id = "#"+customer_id+"_date_fcam";
                             var transaction_date_fcap = results[0][k].trans_date_fca;
                             //var transaction_date_fcam = results[0][k].trans_date_fcm;
                             //$(fcam_trans_id).text(transaction_date_fcam);
                             $(fcap_trans_id).text(transaction_date_fcap);
                           }
                           else {
                             //alert("inside null");
                               $(plant_id_empty).append("<td></td>");
                               plant_ids == 0;
                           }
                         }
                         else if(customer_name == "FCA Mopar")
                         {
                           if(results[0][k].overall_mopar != null)
                           {
                             var td_cvalue = set_fcatd(results[0][k].supplier_code,results[0][k].overall_mopar.substring(0,results[0][k].overall_mopar.length-1),customer_name.toUpperCase(),plant_name.toUpperCase());
                             var append_tr = "#"+plant_id;
                             $(append_tr).append(td_cvalue);
                             plant_ids == 0;
                             //var fcap_trans_id = "#"+customer_id+"_date_fcap";
                             var fcam_trans_id = "#"+customer_id+"_date";
                             //var transaction_date_fcap = results[0][k].trans_date_fca;
                             var transaction_date_fcam = results[0][k].trans_date_fcm;
                             $(fcam_trans_id).text(transaction_date_fcam);
                             //$(fcap_trans_id).text(transaction_date_fcap);
                           }
                           else {
                               $(plant_id_empty).append("<td></td>");
                               plant_ids == 0;
                           }
                          }
                          else if(customer_name == "PSA")
                          {
                            if(results[0][k].quality_psa != null && results[0][k].supplychain_psa && results[0][k].aftersales_psa != null)
                            {
                              var td_cvalue = set_psatd(results[0][k].supplier_code,results[0][k].quality_psa,results[0][k].supplychain_psa,results[0][k].aftersales_psa,customer_name.toUpperCase(),plant_name.toUpperCase());
                              var append_tr = "#"+plant_id;
                              $(append_tr).append(td_cvalue);
                              plant_ids == 0;
                              var trans_id = "#"+customer_id+"_date";
                              var transaction_date = results[0][k].trans_date_psa;
                              $(trans_id).text(transaction_date);
                            }
                            else {
                                $(plant_id_empty).append("<td></td>");
                                plant_ids == 0;
                            }
                          }
                        }
                     }
                     if(plant_ids == 0 && j<=results[3].length-1)
                     {
                       //alert("inside empty td creation");
                        $(plant_id_empty).append("<td></td>");
                     }
                    }
                  }
                  //alert("Number of Rows : "+$('#report tr').length);
                }
             }
             else {
               alert("Sorry report data not available");
             }
              },
           beforeSend: function() {

           },
           error: function() {
               alert('Sorry report data not available');
           }
       });
     });
     $(document).on('click', '.td_click', function() {
       var id = this.id;
       var splt_id = id.split("_");
       var data_token = $(this).attr("data-token");
       if(splt_id[1] == "mfgduns"){
         get_plantdata(splt_id[0],splt_id[1],data_token);
       }
       else if (splt_id[1] == "quality") {
         get_plantdata(splt_id[0],splt_id[1],data_token);
       }
       else if (splt_id[1] == "supplychain") {
         get_plantdata(splt_id[0],splt_id[1],data_token);
       }
       else if (splt_id[1] == "cca") {
         get_plantdata(splt_id[0],splt_id[1],data_token);
       }
       else if(splt_id[1] == "ford"){
         get_ford_plantdata(splt_id[0],splt_id[1],data_token);
       }
       else if(splt_id[1] == "fordcapable"){
         get_ford_plantdata(splt_id[0],splt_id[1],data_token);
       }
       else if(splt_id[1] == "fordongoing"){
         get_ford_plantdata(splt_id[0],splt_id[1],data_token);
       }
       else if(splt_id[1] == "psa"){
         get_psa_plantdata(splt_id[0],splt_id[1],data_token);
       }
      });

      $(document).on('click', '.fca_click', function() {
        var id = $(this).attr("data-id");
        var splt_id = id.split("_");
        var data_token = $(this).attr("data-token");
        if(splt_id[1] == "prdn"){
          //data_token = data_token + "_PRODUCTION";
          get_fca_plantdata(splt_id[0],splt_id[1],data_token);
        }
        else if (splt_id[1] == "mopar") {
          //data_token = data_token + "_MOPAR";
          get_fca_plantdata(splt_id[0],splt_id[1],data_token);
        }

       });


      $(document).on('click', '.report_gm_back', function() {
        var id = this.id;
        report_gm_back(id);
       });


      // function set_fcatd(supplier_code,overall_prdn,overall_mopar,customer_name,plant_name)
      // {
      //   var tdid_prdn = supplier_code+"_prdn";
      //   var tdid_mopar = supplier_code+"_mopar";
      //   var data_tkn = customer_name + " : " +plant_name;
      //   if(overall_prdn <70){
      //
      //       tdcolor_prdn = "#FF0000"; // RED Color
      //      }
      //     else if (overall_prdn >= 70 && overall_prdn < 90) {
      //       tdcolor_prdn = "#FFFF00";  // YELLOW Color
      //
      //     }
      //     else if(overall_prdn >= 90) {
      //          tdcolor_prdn = "#008000";  // GREEN Color
      //     }
      //     else {
      //
      //     }
      //
      //     if(overall_mopar <70){
      //
      //         tdcolor_mopar = "#FF0000"; // RED Color
      //        }
      //       else if (overall_mopar >= 70 && overall_mopar < 90) {
      //         tdcolor_mopar = "#FFFF00";  // YELLOW Color
      //
      //       }
      //       else if(overall_mopar >= 90){
      //          tdcolor_mopar = "#008000";  // GREEN Color
      //       }
      //       else {
      //
      //       }
      //       var td_value = "<td data-id = '"+tdid_prdn+"' data-token = '"+data_tkn+"'  class='fca_click' bgcolor = '"+tdcolor_prdn+"'></td><td data-id = '"+tdid_mopar+"' data-token = '"+data_tkn+"' class='fca_click' bgcolor = "+tdcolor_mopar+"></td>";
      //       return td_value;
      // }

      function get_fcatdcolor(overall)
      {
        var fca_tdcolor;
        if(overall < 70)
        {
          fca_tdcolor = "#FF0000"; // RED Color
        }
        else if(overall >=70 && overall < 90){
           fca_tdcolor = "#FFFF00";  // YELLOW Color
        }
        else if(overall >=90){
          fca_tdcolor = "#008000";  // GREEN Color
        }
        else {
          fca_tdcolor =  "#808080";  // Grey Color
        }
        return fca_tdcolor;
      }
      function set_fcatd(supplier_code,overall,customer_name,plant_name)
      {
        var tdid = "";
        if(customer_name == "FCA PRODUCTION")
        {
          tdid = supplier_code+"_prdn";
        }
        else
        {
          tdid = supplier_code+"_mopar";
        }
        //alert(customer_name + " , " + tdid);
        var data_tkn = customer_name + " : " +plant_name;
        //alert(data_tkn);
        if(overall <70){

            tdcolor = "#FF0000"; // RED Color
           }
          else if (overall >= 70 && overall < 90) {
            tdcolor = "#FFFF00";  // YELLOW Color

          }
          else if(overall >= 90) {
               tdcolor = "#008000";  // GREEN Color
          }
          else {

          }
            var td_value = "<td data-id = '"+tdid+"' data-token = '"+data_tkn+"'  class='fca_click' bgcolor = '"+tdcolor+"'></td>";
            return td_value;
      }

      function get_gmtdcolor(quality,supplychain,cca)
      {
        var gm_tdcolor;
        var quality_td;
        var supplychain_td;
        var cca_td;
        if(quality < 80)
        {
          quality_td = "#FF0000"; // RED Color
        }
        else {
          quality_td = "#008000";  // GREEN Color
        }
        if(supplychain < 75)
        {
          supplychain_td = "#FF0000"; // RED Color
        }
        else if(supplychain >= 75 && supplychain < 85)
        {
          supplychain_td = "#FFFF00";  // YELLOW Color
        }
        else {
          supplychain_td = "#008000";  // GREEN Color
        }
        if (cca == "R")
        {
          cca_td = "#FF0000"; // RED Color
        }
        else if (cca == "G"){
          cca_td = "#008000";  // GREEN Color
        }
        else {
          cca_td = "#FFFFFF";  // White Color
        }
        return quality_td+"_"+supplychain_td+"_"+cca_td;
      }

      function set_gmtd(duns_no,quality,supplychain,cca,customer_name,plant_name)
      {
        //var tdid = results[0][i].duns_no+"_mfgduns";
        // var quality = results[0][i].quality;
        // var supplychain = results[0][i].supplychain;
        // var cca = results[0][i].cca;
        var tdid = duns_no+"_mfgduns";
        var data_tkn = customer_name + " : " + plant_name;
        var tdcolor;
        if(quality == 0 && supplychain == "NR"){
          if(cca == "R"){
            tdcolor = "#FF0000"; // RED Color
          }
          else if (cca == "G") {
              tdcolor = "#008000";  // GREEN Color
          }
          else {
              tdcolor = "#FFFF00";  // YELLOW Color
          }
        }
        else {
          if (quality < 80 || supplychain < 75 || cca == "R") {
            tdcolor = "#FF0000"; // RED Color
          }
          else if(quality >= 80 && supplychain >=85 && cca == "G")
          {
            tdcolor = "#008000";  // GREEN Color
          }
          else {
            tdcolor = "#FFFF00";  // YELLOW Color
          }
        }
        var td_value = "<td id="+tdid+" class='td_click' data-token = '"+data_tkn+"' bgcolor = "+tdcolor+"></td>";
        return td_value;

      }
      function get_fordtdcolor(q1_status)
      {
        var ford_tdcolor;
        if(q1_status == "R" || q1_status == "X")
        {
          ford_tdcolor =  "#FF0000"; // RED Color
        }
        else if(q1_status == "A" || q1_status == "D"){
          ford_tdcolor = "#FFFF00";  // YELLOW Color
        }
        else if(q1_status == "Y" || q1_status == "W" || q1_status == "2"){
          ford_tdcolor = "#008000";  // GREEN Color
        }
        else {
          ford_tdcolor = "#808080";  // Grey Color
        }
        return ford_tdcolor;
      }
      function set_fordtd(duns_no,q1_score,customer_name,plant_name)
      {
        var tdid = duns_no+"_ford";
        var data_tkn = customer_name + " : " + plant_name;
        var tdcolor;
        if(q1_score == "R" || q1_score == "X"){
          tdcolor = "#FF0000"; // RED Color
        }
        else if(q1_score == "A" || q1_score == "D"){
          tdcolor = "#FFFF00";  // YELLOW Color
        }
        else if(q1_score == "Y" || q1_score == "W" || q1_score == "2"){
          tdcolor = "#008000";  // GREEN Color
        }
        else {
          tdcolor = "#808080";  // Grey Color
        }
        var td_value = "<td id="+tdid+" data-token = '"+data_tkn+"' class='td_click' bgcolor = "+tdcolor+"></td>";
        return td_value;
      }
      function get_psatdcolor(quality,supplychain,aftersales)
      {
        var quality_td;
        var supplychain_td;
        var aftersales_td;
        if(quality == "U")
        {
          quality_td = "#FFFFFF"; //White Color
        }
        else if(quality < 80){
          quality_td  = "#FF0000"; // RED Color
        }
        else if(quality >= 80){
          quality_td =  "#008000";  // GREEN Color
        }
        else {
          quality_td = "#FFFFFF"; //White Color
        }
        if(supplychain == "U"){
          supplychain_td = "#FFFFFF"; //White Color
        }
        else if(supplychain < 75)
        {
            supplychain_td = "#FF0000"; // RED Color
        }
        else if (supplychain >= 75 && supplychain < 85) {
            supplychain_td = "#FFFF00";  // YELLOW Color
        }
        else if(supplychain >= 85){
            supplychain_td = "#008000";  // GREEN Color
        }
        else {
            supplychain_td = "#FFFFFF"; //White Color
        }
        if(aftersales == "U"){
          aftersales_td = "#FFFFFF"; //White Color
        }
        else if(aftersales < 75){
            aftersales_td = "#FF0000"; // RED Color
        }
        else if (aftersales >= 75 && aftersales < 85) {
            aftersales_td = "#FFFF00";  // YELLOW Color
        }
        else if(aftersales >= 85){
            aftersales_td = "#008000";  // GREEN Color
        }
        else {
            aftersales_td = "#FFFFFF"; //White Color
        }
        return quality_td + "_" + supplychain_td + "_" + aftersales_td;
      }
      function set_psatd(duns_no,quality,supplychain,aftersales,customer_name,plant_name)
      {
        var tdid = duns_no+"_psa";
        var data_tkn = customer_name + " : " + plant_name;
        var tdcolor;
        if(quality == "U" && supplychain == "U" && aftersales == "U")
        {
          tdcolor = "#FFFFFF"; // WHITE Color
        }
        else if(quality == "U" && supplychain == "U")
        {
          if(aftersales < 75)
          {
            tdcolor = "#FF0000"; // RED Color
          }
          else if (aftersales >= 75 && aftersales < 85) {
              tdcolor = "#FFFF00";  // YELLOW Color
          }
          else {
              tdcolor = "#008000";  // GREEN Color
          }
        }
        else if(quality == "U" && aftersales == "U")
        {
          if(supplychain < 75)
          {
            tdcolor = "#FF0000"; // RED Color
          }
          else if (supplychain >= 75 && supplychain < 85) {
              tdcolor = "#FFFF00";  // YELLOW Color
          }
          else {
              tdcolor = "#008000";  // GREEN Color
          }
        }
        else if(supplychain == "U" && aftersales == "U")
        {
          if(quality < 80){
            tdcolor = "#FF0000"; // RED Color
          }
          else {
              tdcolor = "#008000";  // GREEN Color
          }
        }
        else if(quality == "U")
        {
          if (supplychain < 75 || aftersales < 75)
          {
            tdcolor = "#FF0000"; // RED Color
          }
          else if(supplychain >= 85 && aftersales >= 85)
          {
            tdcolor = "#008000";  // GREEN Color
          }
          else
          {
            tdcolor = "#FFFF00";  // YELLOW Color
          }
        }
        else if(supplychain == "U")
        {
          if (quality < 80 || aftersales < 75)
          {
            tdcolor = "#FF0000"; // RED Color
          }
          else if(quality >= 80 && aftersales >= 85)
          {
            tdcolor = "#008000";  // GREEN Color
          }
          else
          {
            tdcolor = "#FFFF00";  // YELLOW Color
          }
        }
        else if(aftersales == "U")
        {
          if (quality < 80 || supplychain < 75)
          {
            tdcolor = "#FF0000"; // RED Color
          }
          else if(quality >= 80 && supplychain >= 85)
          {
            tdcolor = "#008000";  // GREEN Color
          }
          else
          {
            tdcolor = "#FFFF00";  // YELLOW Color
          }
        }
        else
        {
          if (quality < 80 || supplychain < 75 || aftersales < 75) {
            tdcolor = "#FF0000"; // RED Color
          }
          else if(quality >= 80 && supplychain >=85 && aftersales >= 85)
          {
            tdcolor = "#008000";  // GREEN Color
          }
          else
          {
            tdcolor = "#FFFF00";  // YELLOW Color
          }
        }
        var td_value = "<td id="+tdid+" data-token = '"+data_tkn+"' class='td_click' bgcolor = "+tdcolor+"></td>";
        return td_value;

      }

      function get_plantdata(mfg_duns,report_type,data_token)
      {
        $('#report_gm_div').css('display','none');
        $('#report_gen_btns').css('display','none');
        $('#report_gm_view2_div').css('display','none');
        $.ajax({
            type: "POST",
            url: "get_plantdata",
            data: {
                "_token": "{{ csrf_token() }}",
                "mfg_duns": mfg_duns,
                "report_type":report_type
                },
            success: function(data) {
                if(report_type == "mfgduns"){

                  $('#report_gm_view1_div').css('display','block');

                  for (var i = 0; i < data.length; i++) {
                    var customer_plant_name = data[i].customer_name.toUpperCase() + " - " + data[i].plant_name.toUpperCase();
                    var quality = data[i].quality;
                    var supplychain = data[i].supplychain;
                    var cca = data[i].cca;
                    var gm_color = get_gmtdcolor(quality,supplychain,cca);
                    var gm_colors = gm_color.split("_");
                    var country_risk = data[i].country_risk;
                    var financial_risk = data[i].financial_risk;
                    var mmog_le = data[i].mmog_le;
                    var conflict_materials = data[i].conflict_materials;
                    var diversity = data[i].diversity;
                    var ipb = data[i].ipb;
                    var severity_score = data[i].severity_score;
                    var severity_ipb = data[i].severity_ipb;
                    var quality_id = mfg_duns+"_quality";
                    var supplychain_id=mfg_duns+"_supplychain";
                    var cca_id = mfg_duns+"_cca";
                    var target_ipbid = mfg_duns+"_targetipb";
                    var actual_ipbid = mfg_duns+"_actualipb";
                    $('#report_view1 tr').remove();
                    $('#report_view1').append("<tr><td colspan='2' class = 'report_subtitle'>"+data_token+"</td></tr>");
                    $('#report_view1').append("<tr><td colspan='2' class = 'report_subtitle'>RANKINGS</td></tr>");
                    $('#report_view1').append("<tr id="+quality_id+" data-token = '"+data_token+"' class='td_click'><td>QUALITY</td><td bgcolor = "+gm_colors[0]+">"+quality+"</td></tr>");
                    $('#report_view1').append("<tr id="+supplychain_id+" data-token = '"+data_token+"' class='td_click'><td>SUPPLY CHAIN</td><td bgcolor = "+gm_colors[1]+">"+supplychain+"</td></tr>");
                    $('#report_view1').append("<tr id="+cca_id+" data-token = '"+data_token+"' class='td_click'><td>CCA</td><td bgcolor = "+gm_colors[2]+">"+cca+"</td></tr>");
                    $('#report_view1').append("<tr><td colspan='2' class = 'report_subtitle'>RATINGS</td></tr>");
                    $('#report_view1').append("<tr><td>COUNTRY RISK</td><td>"+country_risk+"</td></tr>");
                    $('#report_view1').append("<tr><td>FINANCIAL RISK</td><td>"+financial_risk+"</td></tr>");
                    $('#report_view1').append("<tr><td>MMOG / LE</td><td>"+mmog_le+"</td></tr>");
                    $('#report_view1').append("<tr><td>CONFLICT MATERIALS</td><td>"+conflict_materials+"</td></tr>");
                    $('#report_view1').append("<tr><td>DIVERSITY</td><td>"+diversity+"</td></tr>");
                    $('#report_view1').append("<tr><td colspan='2' class = 'report_subtitle'>SEVERITY IPB</td></tr>");
                    $('#report_view1').append("<tr id="+target_ipbid+"><td>TARGET FOR SEVERITY IPB</td><td>"+ipb+"</td></tr>");
                    $('#report_view1').append("<tr id="+actual_ipbid+"><td>ACTUAL SEVERITY IPB</td><td>"+severity_ipb+"</td></tr>");
                    $('#report_view1').append("<tr><td colspan='2'><input type ='button' value='Back' id='report_child1' class='report_gm_back' /></td></tr>");
                  }
                  }
                  else if(report_type == "quality") {
                    $('#report_gm_view1_div').css('display','none');
                    $('#report_gen_btns').css('display','none');
                    $('#report_gm_view2_div').css('display','block');
                    for (var i = 0; i < data.length; i++) {
                      var customer_plant_name = data[i].customer_name.toUpperCase() + " - " + data[i].plant_name.toUpperCase();
                      var ipb = data[i].ipb;
                      var severity_score = data[i].severity_score;
                      var severity_ipb = data[i].severity_ipb;
                      var prog_mgmt_rrs = data[i].prog_mgt_prrs;
                      var unauth_change = data[i].unauth_change;
                      var customer_sat_open = data[i].customer_sat_open;
                      var launch_prrs = data[i].launch_prrs;
                      var field_action = data[i].field_action;
                      var plant_disruption = data[i].plant_disruption;
                      var controlled_shipping_level1 = data[i].controlled_shipping_level1;
                      var controlled_shipping_level2 = data[i].controlled_shipping_level2;
                      var nbh = data[i].nbh;
                      var iso_14001 = data[i].iso_14001;
                      var ts16949_cert = data[i].ts16949_cert;
                      var qsb = data[i].qsb_quality_systems_basis;
                      var sqea = data[i].supplier_quality_excellence_award;
                      $('#report_view2 tr').remove();
                      $('#report_view2').append("<tr><td colspan='2' class = 'report_subtitle'>"+data_token+"</td></tr>");
                      $('#report_view2').append("<tr><td colspan='2' class = 'report_subtitle'>QUALITY DATA</td></tr>");
                      $('#report_view2').append("<tr><td>IPB</td><td>"+ipb+"</td></tr>");
                      $('#report_view2').append("<tr><td>SEVERITY SCORE</td><td>"+severity_score+"</td></tr>");
                      $('#report_view2').append("<tr><td>SEVERITY IPB</td><td>"+severity_ipb+"</td></tr>");
                      $('#report_view2').append("<tr><td>PROGRAM MANAGEMENT RRS</td><td>"+prog_mgmt_rrs+"</td></tr>");
                      $('#report_view2').append("<tr><td>UNAUTH CHANGE</td><td>"+unauth_change+"</td></tr>");
                      $('#report_view2').append("<tr><td>CUSTOMER SAT OPEN</td><td>"+customer_sat_open+"</td></tr>");
                      $('#report_view2').append("<tr><td>LAUNCH PRRS</td><td>"+launch_prrs+"</td></tr>");
                      $('#report_view2').append("<tr><td>FIELD ACTION</td><td>"+field_action+"</td></tr>");

                      $('#report_view2').append("<tr><td>PLANT DISRUPTION</td><td>"+plant_disruption+"</td></tr>");
                      $('#report_view2').append("<tr><td>CONTROLLED SHIPPING LEVEL-1</td><td>"+controlled_shipping_level1+"</td></tr>");
                      $('#report_view2').append("<tr><td>CONTROLLED SHIPPING LEVEL-2</td><td>"+controlled_shipping_level2+"</td></tr>");
                      $('#report_view2').append("<tr><td>NBH</td><td>"+nbh+"</td></tr>");

                      $('#report_view2').append("<tr><td>ISO 14001</td><td>"+iso_14001+"</td></tr>");
                      $('#report_view2').append("<tr><td>TS16949_CERT</td><td>"+ts16949_cert+"</td></tr>");
                      $('#report_view2').append("<tr><td>QUALITY SYSTEM BASICS</td><td>"+qsb+"</td></tr>");
                      $('#report_view2').append("<tr><td>SUPPLIER QUALITY EXCELLENCE AWARD</td><td>"+sqea+"</td></tr>");
                      $('#report_view2').append("<tr><td colspan='2'><input type ='button' value='Back' id='report_child2' class='report_gm_back' /></td></tr>");
                    }
                  }
                  else if(report_type == "supplychain") {
                    $('#report_gm_view1_div').css('display','none');
                    $('#report_gen_btns').css('display','none');
                    $('#report_gm_view2_div').css('display','block');
                    for (var i = 0; i < data.length; i++) {
                      var customer_plant_name = data[i].customer_name.toUpperCase() + " - " + data[i].plant_name.toUpperCase();
                      var sc_rating = data[i].sc_rating;
                      $('#report_view2 tr').remove();
                      $('#report_view2').append("<tr><td colspan='2' class = 'report_subtitle'>"+data_token+"</td></tr>");
                      $('#report_view2').append("<tr><td colspan='2' class = 'report_subtitle'>SUPPLY CHAIN DATA</td></tr>");
                      $('#report_view2').append("<tr><td>sc_rating</td><td>"+sc_rating+"</td></tr>");
                      $('#report_view2').append("<tr><td colspan='2'><input type ='button' value='Back' id='report_child2' class='report_gm_back' /></td></tr>");
                    }
                  }
                  else if(report_type == "cca") {
                    $('#report_gm_view1_div').css('display','none');
                    $('#report_gen_btns').css('display','none');
                    $('#report_gm_view2_div').css('display','block');
                    for (var i = 0; i < data.length; i++) {
                      var customer_plant_name = data[i].customer_name.toUpperCase() + " - " + data[i].plant_name.toUpperCase();
                      var quality_score = data[i].quality_score;
                      var service_score = data[i].service_score;
                      $('#report_view2 tr').remove();
                      $('#report_view2').append("<tr><td colspan='2' class = 'report_subtitle'>"+data_token+"</td></tr>");
                      $('#report_view2').append("<tr><td colspan='2' class = 'report_subtitle'>CUSTOMER CARE AND AFTERSALES</td></tr>");
                      $('#report_view2').append("<tr><td>QUALITY SCORE</td><td>"+quality_score+"</td></tr>");
                      $('#report_view2').append("<tr><td>SERVICE SCORE</td><td>"+service_score+"</td></tr>");
                      $('#report_view2').append("<tr><td colspan='2'><input type ='button' value='Back' id='report_child2' class='report_gm_back' /></td></tr>");
                    }
                  }
                  else {

                  }
            },
            beforeSend: function() {
            },
            error: function() {

            }

        });
      }

      function get_fca_plantdata(code,report_type,data_token)
      {
        $('#report_gm_div').css('display','none');
        $('#report_gen_btns').css('display','none');
        $('#report_gm_view2_div').css('display','none');
        $.ajax({
            type: "POST",
            url: "get_fca_plantdata",
            data: {
                "_token": "{{ csrf_token() }}",
                "code": code,
                "report_type":report_type
                },
            success: function(data) {
              console.log(JSON.stringify(data));

                  $('#report_gm_view1_div').css('display','block');

                  for (var i = 0; i < data.length; i++) {
                    var quality = data[i].incoming_material_quality;
                    var overall = data[i].overall;
                    var delivery = data[i].delivery;
                    var warranty = data[i].warranty;
                    var cost = data[i].cost;
                    var partnership = data[i].partnership;
                    var fca_tdcolor = get_fcatdcolor(overall.substring(0,overall.length-1));
                    $('#report_view1 tr').remove();
                    //alert(data_token);
                    $('#report_view1').append("<tr><td colspan='2' class = 'report_subtitle'>"+data_token+"</td></tr>");
                    $('#report_view1').append("<tr><td class ='report_subtitle'>CATEGORIES</td><td class ='report_subtitle'>SCORES</td></tr>");
                    $('#report_view1').append("<tr><td>OVERALL</td><td bgcolor = "+fca_tdcolor+">"+overall+"</td></tr>");
                    $('#report_view1').append("<tr><td>INCOMING MATERIAL QUALITY</td><td>"+quality+"</td></tr>");
                    $('#report_view1').append("<tr><td>DELIVERY</td><td>"+delivery+"</td></tr>");
                    $('#report_view1').append("<tr><td>WARRANTY</td><td>"+warranty+"</td></tr>");
                    $('#report_view1').append("<tr><td>COST</td><td>"+cost+"</td></tr>");
                    $('#report_view1').append("<tr><td>PARTNERSHIP</td><td>"+partnership+"</td></tr>");
                    $('#report_view1').append("<tr><td colspan='2'><input type ='button' value='Back' id='report_child1' class='report_gm_back' /></td></tr>");
                  }
              },
            beforeSend: function() {
            },
            error: function() {

            }

        });
      }

      function get_ford_plantdata(code,report_type,data_token)
      {
        $('#report_gm_div').css('display','none');
        $('#report_gen_btns').css('display','none');
        $('#report_gm_view2_div').css('display','none');
        $.ajax({
            type: "POST",
            url: "get_ford_plantdata",
            data: {
                "_token": "{{ csrf_token() }}",
                "code": code,
                "report_type":report_type
                },
            success: function(data) {
              console.log(JSON.stringify(data));

                  $('#report_gm_view1_div').css('display','block');
                  $('#report_gm_view2_div').css('display','none');
                  $('#report_gen_btns').css('display','none');
                  $('#report_gm_div').css('display','none');

                  for (var i = 0; i < data.length; i++) {
                    if(report_type == "ford")
                    {
                      var q1_status = data[i].present_q1_status;
                      var q1_status_tdcolor = get_fordtdcolor(q1_status);
                      var q1_score = data[i].q1_score;
                      var q1msa_q1_score = data[i].q1msa_q1_score;
                      var capable_system_q1_score = data[i].capable_system_q1_score;
                      var ongoing_performance_q1_score = data[i].ongoing_performance_q1_score;
                      var capable_id = code+"_fordcapable";
                      var ongoing_id = code+"_fordongoing";
                      $('#report_view1 tr').remove();
                      $('#report_view1').append("<tr><td colspan='2' class = 'report_subtitle'>"+data_token+"</td></tr>");
                      $('#report_view1').append("<tr><td class ='report_subtitle'>CATEGORIES</td><td class ='report_subtitle'>SCORES</td></tr>");
                      $('#report_view1').append("<tr><td>Q1 STATUS</td><td bgcolor = "+q1_status_tdcolor+">"+q1_status+"</td></tr>");
                      $('#report_view1').append("<tr><td>Q1 SCORE</td><td>"+q1_score+"</td></tr>");
                      $('#report_view1').append("<tr><td>MSA Q1  SCORE</td><td>"+q1msa_q1_score+"</td></tr>");
                      $('#report_view1').append("<tr id="+capable_id+" data-token = '"+data_token+"' class='td_click'><td>CAPABLE SYSTEM Q1 SCORE</td><td>"+capable_system_q1_score+"</td></tr>");
                      $('#report_view1').append("<tr id="+ongoing_id+" data-token = '"+data_token+"' class='td_click'><td>ONGOING PERFORMANCE Q1 SCORE</td><td>"+ongoing_performance_q1_score+"</td></tr>");
                      $('#report_view1').append("<tr><td colspan='2'><input type ='button' value='Back' id='report_child1' class='report_gm_back' /></td></tr>");
                    }
                    else if(report_type == "fordcapable")
                    {
                      var capable_system_q1_score = data[i].capable_system_q1_score;
                      var iso14001_q1_score = data[i].iso14001_q1_score;
                      var iso_ts16949_q1_score = data[i].iso_ts16949_q1_score;
                      var mmog_le_q1_score = data[i].mmog_le_q1_score;

                      $('#report_gm_view2_div').css('display','block');
                      $('#report_gm_view1_div').css('display','none');
                      $('#report_gen_btns').css('display','none');
                      $('#report_gm_div').css('display','none');

                      $('#report_view2 tr').remove();
                      $('#report_view2').append("<tr><td colspan='2' class = 'report_subtitle'>"+data_token+"</td></tr>");
                      $('#report_view2').append("<tr><td class ='report_subtitle'>CATEGORIES</td><td class ='report_subtitle'>SCORES</td></tr>");
                      $('#report_view2').append("<tr><td>CAPABLE SYSTEM Q1 SCORE</td><td>"+capable_system_q1_score+"</td></tr>");
                      $('#report_view2').append("<tr><td> ISO14001 Q1 SCORE</td><td>"+iso14001_q1_score+"</td></tr>");
                      $('#report_view2').append("<tr><td> ISO_TS16949 Q1 SCORE</td><td>"+iso_ts16949_q1_score+"</td></tr>");
                      $('#report_view2').append("<tr><td> MMOG/LE Q1 SCORE</td><td>"+mmog_le_q1_score+"</td></tr>");
                      $('#report_view2').append("<tr><td colspan='2'><input type ='button' value='Back' id='report_child2' class='report_gm_back' /></td></tr>");
                    }
                    else if(report_type == "fordongoing")
                    {
                      var ongoing_performance_q1_score = data[i].ongoing_performance_q1_score;
                      var site_ppm_prod_applied_q1_score = data[i].site_ppm_prod_applied_q1_score;
                      var site_ppm_svc_applied_q1_score = data[i].site_ppm_svc_applied_q1_score;
                      var comm_ppm_prod_applied_q1_score = data[i].comm_ppm_prod_applied_q1_score;
                      var comm_ppm_svc_applied_q1_score = data[i].comm_ppm_svc_applied_q1_score;
                      var dlv_prod_applied_q1_score = data[i].dlv_prod_applied_q1_score;
                      var dlv_svc_applied_q1_score = data[i].dlv_svc_applied_q1_score;
                      var field_service_actions_q1_score = data[i].field_service_actions_q1_score;
                      var stop_shipments_q1_score = data[i].stop_shipments_q1_score;
                      var violation_of_trust_production_q1_score = data[i].violation_of_trust_production_q1_score;
                      var violation_of_trust_service_q1_score = data[i].violation_of_trust_service_q1_score;

                      $('#report_view2 tr').remove();
                      $('#report_gm_view2_div').css('display','block');
                      $('#report_gm_view1_div').css('display','none');
                      $('#report_gen_btns').css('display','none');
                      $('#report_gm_div').css('display','none');

                      $('#report_view2').append("<tr><td colspan='2' class = 'report_subtitle'>"+data_token+"</td></tr>");
                      $('#report_view2').append("<tr><td class ='report_subtitle'>CATEGORIES</td><td class ='report_subtitle'>SCORES</td></tr>");
                      $('#report_view2').append("<tr><td>ONGOING PERFORMANCE Q1 SCORE</td><td>"+ongoing_performance_q1_score+"</td></tr>");
                      $('#report_view2').append("<tr><td>    SITE PPM - PROD APPLIED Q1 SCORE</td><td>"+site_ppm_prod_applied_q1_score+"</td></tr>");
                      $('#report_view2').append("<tr><td>    SITE PPM - SVC APPLIED Q1 SCORE</td><td>"+site_ppm_svc_applied_q1_score+"</td></tr>");
                      $('#report_view2').append("<tr><td>    COMM PPM PROD - APPLIED Q1 SCORE</td><td>"+comm_ppm_prod_applied_q1_score+"</td></tr>");
                      $('#report_view2').append("<tr><td>    COMM PPM SVC - APPLIED Q1 SCORE</td><td>"+comm_ppm_svc_applied_q1_score+"</td></tr>");
                      $('#report_view2').append("<tr><td>    DLV PROD - APPLIED Q1 SCORE</td><td>"+dlv_prod_applied_q1_score+"</td></tr>");
                      $('#report_view2').append("<tr><td>    DLV SVC - APPLIED Q1 SCORE</td><td>"+dlv_svc_applied_q1_score+"</td></tr>");
                      $('#report_view2').append("<tr><td>    FIELD SERVICE ACTIONS Q1 SCORE</td><td>"+field_service_actions_q1_score+"</td></tr>");
                      $('#report_view2').append("<tr><td>    STOP SHIPMENTS Q1 SCORE</td><td>"+stop_shipments_q1_score+"</td></tr>");
                      $('#report_view2').append("<tr><td>    VIOLATION OF TRUST - PRODUCTION Q1 SCORE</td><td>"+violation_of_trust_production_q1_score+"</td></tr>");
                      $('#report_view2').append("<tr><td>    VIOLATION OF TRUST - SERVICE Q1 SCORE</td><td>"+violation_of_trust_service_q1_score+"</td></tr>");
                      $('#report_view2').append("<tr><td colspan='2'><input type ='button' value='Back' id='report_child2' class='report_gm_back' /></td></tr>");
                    }
                  }
              },
            beforeSend: function() {
            },
            error: function() {

            }

        });
      }
      function get_psa_plantdata(code,report_type,data_token)
      {
        $('#report_gm_div').css('display','none');
        $('#report_gen_btns').css('display','none');
        $('#report_gm_view2_div').css('display','none');
        $.ajax({
            type: "POST",
            url: "get_psa_plantdata",
            data: {
                "_token": "{{ csrf_token() }}",
                "code": code,
                "report_type":report_type
                },
            success: function(data) {
              console.log(JSON.stringify(data));

                  $('#report_gm_view1_div').css('display','block');

                  for (var i = 0; i < data.length; i++) {
                    var quality = data[i].quality;
                    var supply_chain = data[i].supply_chain;
                    var after_sales = data[i].after_sales;
                    var psa_tdcolor = get_psatdcolor(quality,supply_chain,after_sales);
                    var psa_tdcolors = psa_tdcolor.split("_");
                    var ipb_threshold = data[i].ipb_threshold;
                    var ipb_num_of_issues = data[i].ipb_num_of_issues;
                    var car_impact_yard_hold = data[i].car_impact_yard_hold;
                    var mass_production_minor_major = data[i].mass_production_minor_major;
                    var mass_production_unauth_change = data[i].mass_production_unauth_change;
                    var escalation_level_quality = data[i].escalation_level_quality;
                    var program_management = data[i].program_management;
                    var car_impact_yard_hold_customer = data[i].car_impact_yard_hold_customer;
                    var qsb_certification_status = data[i].qsb_certification_status;
                    var isots_16049_certification_status = data[i].isots_16049_certification_status;
                    var neologistics_service_rate = data[i].neologistics_service_rate;
                    var ship_pack_delivery_status = data[i].ship_pack_delivery_status;
                    var stock_out = data[i].stock_out;
                    var plant_prod_sequence_changed = data[i].plant_prod_sequence_changed;
                    var incomplete_car_part = data[i].incomplete_car_part;
                    var production_down_time = data[i].production_down_time;
                    var escalation_level_supplychain = data[i].escalation_level_supplychain;
                    var mmog_le_self_assessment = data[i].mmog_le_self_assessment;
                    var speed_service_rate_starting_n2 = data[i].speed_service_rate_starting_n2;
                    var quality_after_sales = data[i].quality_after_sales;
                    var supplychain_after_sales = data[i].supplychain_after_sales;
                    var speed_codes_736_743_764 = data[i].speed_codes_736_743_764;
                    var speed_codes_711 = data[i].speed_codes_711;

                    var speed_codes_716_722_731_5_742_744_762_3 = data[i].speed_codes_716_722_731_5_742_744_762_3;
                    var speed_codes_753 = data[i].speed_codes_753;
                    var speed_codes_712_715_721_752_754_756 = data[i].speed_codes_712_715_721_752_754_756;
                    var speed_codes_751_757 = data[i].speed_codes_751_757;
                    var speed_codes_755 = data[i].speed_codes_755;

                    $('#report_view1 tr').remove();
                    $('#report_view1').append("<tr><td colspan='2' class = 'report_subtitle'>"+data_token+"</td></tr>");
                    $('#report_view1').append("<tr><td class ='report_subtitle'>CATEGORIES</td><td class ='report_subtitle'>PSA - SCORES</td></tr>");
                    $('#report_view1').append("<tr><td> QUALITY</td><td bgcolor = "+psa_tdcolors[0]+">"+quality+"</td></tr>");
                    $('#report_view1').append("<tr><td> SUPPLY CHAIN</td><td bgcolor = "+psa_tdcolors[1]+">"+supply_chain+"</td></tr>");
                    $('#report_view1').append("<tr><td> AFTER SALES</td><td bgcolor = "+psa_tdcolors[2]+">"+after_sales+"</td></tr>");
                    $('#report_view1').append("<tr><td> IPB THRESHOLD</td><td>"+ipb_threshold+"</td></tr>");
                    $('#report_view1').append("<tr><td> IPB / NUMBER OF ISSUES</td><td>"+ipb_num_of_issues+"</td></tr>");
                    $('#report_view1').append("<tr><td> F.G2.H CAR IMPACT ISSUES / YARD HOLD ALERTS / YARD HOLDS</td><td>"+car_impact_yard_hold+"</td></tr>");
                    $('#report_view1').append("<tr><td> MASS PRODUCTION MINOR / MAJOR</td><td>"+mass_production_minor_major+"</td></tr>");
                    $('#report_view1').append("<tr><td> MASS PRODUCTION UNAUTHORIZED CHANGE</td><td>"+mass_production_unauth_change+"</td></tr>");
                    $('#report_view1').append("<tr><td> ESCALATION  LEVEL1 / LEVEL 2 / NEW BUSINESS HOLD (QUALITY REASON)</td><td>"+ escalation_level_quality+"</td></tr>");
                    $('#report_view1').append("<tr><td> PROGRAM MANAGEMENT - MINOR / MAJOR / CRITICAL</td><td>"+program_management+"</td></tr>");
                    $('#report_view1').append("<tr><td> F.G2.H CAR IMPACT  IN FIELD ISSUES / YARD HOLDS WITH CUSTOMER IMPACT</td><td>"+car_impact_yard_hold_customer+"</td></tr>");
                    $('#report_view1').append("<tr><td> QSB+ CERTIFICATION STATUS</td><td>"+qsb_certification_status+"</td></tr>");
                    $('#report_view1').append("<tr><td> ISO TS - 16949 CERTIFICATION STATUS</td><td>"+isots_16049_certification_status+"</td></tr>");
                    $('#report_view1').append("<tr><td> NEOLOGISTICS -  SERVICE RATE</td><td>"+neologistics_service_rate+"</td></tr>");
                    $('#report_view1').append("<tr><td> SHIP. & PACK. * DELIVERY  SHEETS</td><td>"+ship_pack_delivery_status+"</td></tr>");
                    $('#report_view1').append("<tr><td> STOCK OUT - NO LINE DISR. * DELIV. SH.</td><td>"+stock_out+"</td></tr>");
                    $('#report_view1').append("<tr><td> PLANT PROD. SEQ. CHANGED</td><td>"+plant_prod_sequence_changed+"</td></tr>");
                    $('#report_view1').append("<tr><td> INCOMPLETE CAR  OR PART</td><td>"+incomplete_car_part+"</td></tr>");
                    $('#report_view1').append("<tr><td> PRODUCTION DOWN TIME</td><td>"+production_down_time+"</td></tr>");
                    $('#report_view1').append("<tr><td> ESCALATION LEVEL 1 / LEVEL 2 / NEW BUSINESS HOLD (SUPPLYCHAIN  REASON)</td><td>"+escalation_level_supplychain+"</td></tr>");
                    $('#report_view1').append("<tr><td> MMOG/LE SELF ASSESSMENT</td><td>"+mmog_le_self_assessment+"</td></tr>");
                    $('#report_view1').append("<tr><td> SPEED -  SERVICE RATE STARTING N-2</td><td>"+speed_service_rate_starting_n2+"</td></tr>");
                    $('#report_view1').append("<tr><td> QUALITY</td><td>"+quality_after_sales+"</td></tr>");
                    $('#report_view1').append("<tr><td> SUPPLY CHAIN</td><td>"+supplychain_after_sales+"</td></tr>");
                    $('#report_view1').append("<tr><td> SPEED CODES  736-743-764</td><td>"+speed_codes_736_743_764+"</td></tr>");
                    $('#report_view1').append("<tr><td> SPEED CODES - 711</td><td>"+speed_codes_711+"</td></tr>");
                    $('#report_view1').append("<tr><td> SPEED CODES 716-722-731-5 -742-744-762-3</td><td>"+speed_codes_716_722_731_5_742_744_762_3+"</td></tr>");
                    $('#report_view1').append("<tr><td> SPEED CODE -753</td><td>"+speed_codes_755+"</td></tr>");

                    $('#report_view1').append("<tr><td> SPEED CODES 712:715 - 721 - 752 - 754 - 756</td><td>"+speed_codes_712_715_721_752_754_756+"</td></tr>");
                    $('#report_view1').append("<tr><td> SPEED CODES - 751 - 757</td><td>"+speed_codes_751_757+"</td></tr>");
                    $('#report_view1').append("<tr><td> SPEED CODE -755</td><td>"+speed_codes_755+"</td></tr>");

                    $('#report_view1').append("<tr><td colspan='2'><input type ='button' value='Back' id='report_child1' class='report_gm_back' /></td></tr>");
                  }
              },
            beforeSend: function() {
            },
            error: function() {

            }

        });
      }
      function get_plantdata2(obj)
      {
        $('#report_gm_div').css('display','none');
        $('#report_gen_btns').css('display','none');
        $('#report_gm_view1_div').css('display','none');
        $.ajax({
            type: "POST",
            url: "get_plantdata2",
            data: {
                "_token": "{{ csrf_token() }}",
                "mfg_duns": mfg_duns
                },
            success: function(data) {
                $('#report_gm_view2_div').css('display','block');
                for (var i = 0; i < data.length; i++) {
                  var quality = data[i].quality;
                  var supplychain = data[i].supplychain;
                  var cca = data[i].cca;
                  var country_risk = data[i].country_risk;
                  var financial_risk = data[i].financial_risk;
                  var mmog_le = data[i].mmog_le;
                  var conflict_materials = data[i].conflict_materials;
                  var diversity = data[i].diversity;
                  var ipb = data[i].ipb;
                  var severity_score = data[i].severity_score;
                  var severity_ipb = data[i].severity_ipb;
                  var quality_id = mfg_duns+"_quality";
                  var supplychain_id=mfg_duns+"_supplychain";
                  var cca_id = mfg_duns+"_cca";
                  var target_ipbid = mfg_duns+"_targetipb";
                  var actual_ipbid = mfg_duns+"+_actualipb";
                  $('#report_view1 tr').remove();
                  $('#report_view1').append("<tr><td colspan='2'>RANKINGS</td></tr>");
                  $('#report_view1').append("<tr id="+quality_id+" class='td_click'><td>QUALITY</td><td>"+quality+"</td></tr>");
                  $('#report_view1').append("<tr id="+supplychain_id+" class='td_click'><td>SUPPLY CHAIN</td><td>"+supplychain+"</td></tr>");
                  $('#report_view1').append("<tr id="+cca_id+" class='td_click'><td>CCA</td><td>"+cca+"</td></tr>");
                  $('#report_view1').append("<tr><td colspan='2'>RATINGS</td></tr>");
                  $('#report_view1').append("<tr><td>COUNTRY RISK</td><td>"+country_risk+"</td></tr>");
                  $('#report_view1').append("<tr><td>FINANCIAL RISK</td><td>"+financial_risk+"</td></tr>");
                  $('#report_view1').append("<tr><td>MMOG / LE</td><td>"+mmog_le+"</td></tr>");
                  $('#report_view1').append("<tr><td>CONFLICT MATERIALS</td><td>"+conflict_materials+"</td></tr>");
                  $('#report_view1').append("<tr><td>DIVERSITY</td><td>"+diversity+"</td></tr>");
                  $('#report_view1').append("<tr><td colspan='2'>SEVERITY IPB</td></tr>");
                  $('#report_view1').append("<tr id="+target_ipbid+" class='td_click'><td>TARGET FOR SEVERITY IPB</td><td>"+ipb+"</td></tr>");
                  $('#report_view1').append("<tr id="+actual_ipbid+" class='td_click'><td>ACTAUAL SEVERITY IPB</td><td>"+severity_ipb+"</td></tr>");
                  $('#report_view1').append("<tr><td colspan='2'><input type ='button' value='Back' id='report_child1' class='report_gm_back' /></td></tr>");
                }
            },
            beforeSend: function() {
            },
            error: function() {
            }
        });
      }
      function report_gm_back(id)
      {
        if(id == "report_child1"){
          $('#report_gm_view1_div').css('display','none');
          $('#report_gm_view2_div').css('display','none');
          $('#report_gen_btns').css('display','none');
          $('#report_gm_div').css('display','block');
        }
        else if(id == "report_child2"){
          $('#report_gm_view2_div').css('display','none');
          $('#report_gm_view1_div').css('display','block');
          $('#report_gen_btns').css('display','none');
          $('#report_gm_div').css('display','none');
        }
        else if(id == "goto_main")
        {
          $('#report_gm_view1_div').css('display','none');
          $('#report_gm_view2_div').css('display','none');
          $('#report_gm_div').css('display','none');
          $('#report_gen').css('display','block');
          $('#report_gen_btns').css('display','block');
          $('.content').addClass('scrollRemove');
          $('.content').removeClass('scrollAdd');
        }

      }

      function generate_pdf()
      {
        var source = $('#report_gm_div').html();
        alert(source);
        $.ajax({
            type: "POST",
            url: "generate_pdf",
            data: {
                "_token": "{{ csrf_token() }}",
                "source": source,
                },
            success: function(data) {
              console.log(JSON.stringify(data));
              },
            beforeSend: function() {
            },
            error: function() {

            }

        });
      }

 });

</script>
<script type="text/javascript">
// var doc = new jsPDF();
// var specialElementHandlers = {
// '#report': function (element, renderer) {
// return true;
// }
// };
//
// $(document).ready(function() {
// $('#btn_pdf').click(function () {
//   alert($('#report_gm_div').html());
// doc.fromHTML($('#report_gm_div').html(), 15, 15, {
// 'width': 170,
// 'elementHandlers': specialElementHandlers
// });
// doc.save('sample-content.pdf');
// });
// });

function demoFromHTML() {
  var source = $('#report_gm_div').html();
  alert(source);
  $.ajax({
      type: "POST",
      url: "generate_pdf",
      data: {
          "_token": "{{ csrf_token() }}",
          "source": source,
          },
      success: function(data) {
        console.log(JSON.stringify(data));
        },
      beforeSend: function() {
      },
      error: function() {

      }
   });

  // $('#report').table2excel({
	// 				exclude: ".noExl",
	// 				name: "Excel Document Name",
	// 				filename: "myFileName",
	// 				fileext: ".xls",
	// 				exclude_img: true,
	// 				exclude_links: true,
	// 				exclude_inputs: true
	// 			});
  //       var save = document.createElement('a');
  //     save.href = "E:/";
  //     save.target = '_blank';
  //     save.download = filename+fileext || "E:/";
  //     var evt = document.createEvent('MouseEvents');
  //     evt.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0,
  //         false, false, false, false, 0, null);
  //     save.dispatchEvent(evt);
  //     (window.URL || window.webkitURL).revokeObjectURL(save.href);


}


</script>
@stop

@section('lower_band')
<div class="col-xs-12 lower_band">
  <div id="report_gen_btns">
   <button class="headerbuttons" id="save_variant" type="button">Save Variant</button>
   <button class="headerbuttons" id="filter" type="button">Generate Report</button>
    <!-- <button class="headerbuttons" id="btn_pdf" type="button" onclick="demoFromHTML();" >Generate PDF</button> -->
   <button id="cancel_button" class="headerbuttons" type="button"  style="float: right; margin-right: 20px;">Cancel</button>
   <button id="update_button" class="headerbuttons" type="button"  style=" float: right; margin-right: 20px;">Update</button>
   <button id="back_button_listScreen" class="headerbuttons" type="button"  style="float: left; margin-right: 20px;">Back</button>
 </div>
</div>
@stop
