@extends('layouts.uscan_master_page')
@section('header')
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/base/jquery-ui.css" rel="stylesheet">
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/schedule_reports.css">
<script src="js/schedule_reports_validation.js"></script>
@stop

@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="display-title">
      Change Shedule Report Job
   </div>
   <div id="show_all_link" class="col-xs-2" style="float: right;text-align:center;line-height:30px;">
      <span  id="show_all" style="">Show All Jobs</span>
   </div>
</div>
@stop

@section('content')
<div class="col-xs-12 content">
  <div id="search_page" class="search_page">
    <form id="scheduler_search_form" autocomplete="off">
       <table style="width:30%;">
          <tr>
             <td style="background-color: #eee;"><strong>Basic Details</strong></td>
             <td></td>
          </tr>
          <tr>
             <td></td>
          </tr>
          <tr>
             <td>Job Name</td>
             <td><input type="text" id="job_name_search" name="job_name_search"></td>
          </tr>

          <tr>
             <td>Variant</td>
             <!-- <td><input type="text" id="task_name_search" name="task_name_search"></td> -->
             <td><input type="text" id="variant_search" name="variant_search"></td>
          </tr>
       </table>
    </form>
  </div>

  <div class="list_of_jobs" id="list_of_jobs">
    <table style="width:100%;margin-bottom: 371px" class="tab">
    </table>
  </div>

   <div id="tab_one" class="load">
      <form id="scheduler_create_form" autocomplete="off">
         <table style="width:60%;">
            <tr>
               <td style="background-color: #eee;"><strong>Basic Details</strong></td>
               <td></td>
               <td style="background-color: #eee;"><strong>Additional Details</strong></td>
            </tr>
            <tr>
               <td></td>
            </tr>
            <tr>
               <td>Job Name</td>
               <td><input type="text" id="job_name" name="job_name"></td>
               <td>Created By</td>
               <td><input type="text" name="created_by" id="created_by" disabled></td>
            </tr>
            <tr>
               <td>Variant</td>
               <td><select id = "variant_select" name="variant" ></select>
               </td>
               <td>Created Date</td>
               <td><input type="text" name="created_date" id="created_date" disabled></td>
            </tr>
            <tr>
               <td>Job Priority</td>
               <td>
                  <select id = "job_priority" name="job_priority" >
                     <option id="0" value="0">Select Task Name</option>
                     <option id="1" value="1">High</option>
                     <option id="2" value="2">Medium</option>
                     <option id="3" value="3">Low</option>
                  </select>
               </td>
               <td>Modified By</td>
               <td><input type="text" name="modified_by" id="modified_by" disabled></td>
            </tr>
            <tr>
               <td>Active</td>
               <td><input type="checkbox" name="active" id="active"></td>
               <td>Modified Date</td>
               <td><input type="text" name="modified_date" id="modified_date" disabled></td>
            </tr>
            <tr>
               <td></td>
            </tr>
         </table>
      </form>
      <div id="start_conditions_div" class="">
         <form id="start_conditions_form" class="">
            <table style="width:30%;">
               <tr>
                  <td style="background-color: #eee;"><strong>Start Conditions</strong></td>
                  <td></td>
               </tr>
               <tr>
                  <td>Start Conditions</td>
                  <td><select id = "start_conditions" name="start_conditions">
                     </select>
                  </td>
               </tr>
               <tr>
                  <td>Communication</td>
                  <td><input type="checkbox" class = "" name="distribution_strategy" id="distribution_strategy"><button type="button" id="change_ds" style ="display:none;">Change_ds</button></td>
               </tr>
            </table>
         </form>
      </div>
      <div class="load" id="immediate">
         <form id="immediate_form" class="">
            <table style="width:30%;">
               <tr>
                  <td>Periodic</td>
                  <td><input type="checkbox" id="period_immediately" name="period_immediately" value=""></td>
               </tr>
               <tr>
                  <td>Frequency</td>
                  <td><select id="frequency" class="" name="frequency">
                     </select>
                  </td>
               </tr>
            </table>
         </form>
      </div>
      <div id="date_time_div" class="load">
         <form id="date_time_form" class="date_time_form">
            <table style="width:60%;">
              <tr>
                 <td>
                    Start Date
                 </td>
                 <td><input type="date" name="start_date" id="start_date"></td>
                 <td>
                    End Date
                 </td>
                 <td><input type="date" name="end_date" id="end_date"></td>
              </tr>
              <tr>
                 <td>Start Time</td>
                 <td><input type="time" name="start_time" id="start_time"></td>
                 <td>End Time</td>
                 <td><input type="time" name="end_time" id="end_time"></td>
              </tr>
              <tr>
                 <td>Periodic</td>
                 <td><input type="checkbox" name="periodic_date_time" id="periodic_date_time"></td>
              </tr>
            </table>
         </form>
         <form id="date_time_form_frequency">
            <table style="width:30%;">
               <tr>
                  <td id="frequency_label">Frequency</td>
                  <td><select id ="frequency_date_time" name="frequency_date_time">
                     </select>
                  </td>
               </tr>
            </table>
         </form>
      </div>
      <div id="after_job_div" class="load">
         <form id="after_job_from" class="after_job_from">
            <table style="width:30%;">
               <tr>
                  <td>Job Name</td>
                  <td>
                     <select id = "predecessor_job_name" name="predecessor_job_name">
                     </select>
                  </td>
               </tr>
            </table>
         </form>
      </div>
   </div>
   <form class="ds_form">
     <div id="ds_content">
      <table class="ds_tab"></table>
    </div>
   </form>

   <form id="set-ds" style="display:none;">
     <table style="width:100%; margin-bottom: 20px">
     <tr style="background-color: #eee;">
       <th class="set_ds_th">Task Status</th>
       <th class="set_ds_th">Communication Options</th>
       <th class="set_ds_th">Address Type</th>
       <th class="set_ds_th">Mail Address</th>
     </tr>

       <tr>
         <td><select id="report_status">
           </select></td>

         <td><select id="communication_options">
           </select></td>

           <td><select id="mail_type">
           <option value="">Select</option>
           <option value="personnel" id="personnel">Personnel</option>
           <option value="group" id="group">Group</option>
           </select></td>

         <td><select id="mail_address">
           </select></td>
       </tr>

       <tr id="success-msg" style="display: none;">
         <td></td>
         <td><p>Information has been saved successfuly!</p></td>
       </tr>
     </table>
     <div class="resetgap actiondiv1">
     <div style=" margin-bottom: 20px; ">
     <button class="headerbuttons" type="button" id="save-ds">Save</button>
     <button class="headerbuttons" type="button" id="Reset-ds">Reset</button>
     <button class="headerbuttons" type="button" id="Cancel-ds">Cancel</button>
     </div>
         </div>
   </form>

   <script type="text/javascript">
    jQuery.validator.addMethod('selectcheck', function(value) {
        return (value != '0');
    }, "");
  var job_id,job_ds_ref_data;
    $(document).ready(function() {
        //on first time load start
        var active = 0;
        var period_immediately = 0;
        var start_conditions = 0;
        var periodic_date_time = 0;
        var frequency_date_time = 0;
        var filter_clicked = false;


        $("#report_generator").trigger('click');
        $("#schedule_reports").trigger('click');
        // $('#tab_one').hide();

        function populateSelectOptions(result, selectId, valueAttr, displayAttr,isDefault) {
          $('#' + selectId).empty();
          if(!isDefault)
            $('#'+ selectId).append("<option id =\"null\" value=\"\">Select</option>");

          if(result == null || result.length == 0) {
            alert('Data not found...!!!');
          } else {
            for (var i = 0; i < result.length; i++)
            {
              var value = result[i][valueAttr];
              var display = result[i][displayAttr];

              if(!isDefault)
              $('#'+ selectId).append("<option value='"+ value +"'>"+display+"</option>");
              else
              $('#'+ selectId).append("<option selected value='"+ value +"'>"+display+"</option>");
            }
          }
        }

        function getDataFromDB(methodType, url, dataType, selectId, valueAttr, displayAttr, data,inputType, isDefault) {

          $.ajax({
            type:methodType,
            url:url,
            dataType:dataType,
            data:data?data:{},
            success:function(result){
                populateSelectOptions(result, selectId, valueAttr, displayAttr,isDefault);
            },
            error: function() {
              alert('Error while fetching the record');
            }
          });
        }

        $('#variant_select').change(function() {
            var report_id = $('#variant_select').val();
            if (report_id == 0) {
                $("#job_priority").prop('disabled', true);
                $('#job_priority').val(0);
            } else {
                $("#job_priority").prop('disabled', false);
            }
        });


        getDataFromDB('GET','frequency_for_scheduler_report','json', 'frequency_date_time', 'frequency_id', 'frequency_name');
        getDataFromDB('GET','frequency_for_scheduler_report','json', 'frequency', 'frequency_id', 'frequency_name');
        getDataFromDB('GET','get_start_conditions_for_scheduler_reports','json', 'start_conditions', 'start_type_id', 'start_type_name');
        getDataFromDB('GET','get_jobs_for_scheduler_reports','json', 'predecessor_job_name', 'job_id', 'job_name');
        getDataFromDB('GET','get_variant','json', 'variant_select', 'report_id', 'variant');

        $('#period_immediately').on('change', function() {
            if (jQuery(this).is(":checked")) {
                $("#frequency").prop('disabled', false);
            } else {
                $('#frequency').val(0);
                $("#frequency").prop('disabled', true);
            }
        });

        $('#periodic_date_time').on('change', function() {
            if (jQuery(this).is(":checked")) {
                $("#frequency_date_time").prop('disabled', false);
            } else {
                $('#frequency_date_time').val(0);
                $("#frequency_date_time").prop('disabled', true);
            }
        });


        $('#start_conditions').on('change', function() {
            if (this.value == 1) {
                //frequency_for_scheduler_report_immediately();
                getDataFromDB('GET','frequency_for_scheduler_report','json', 'frequency', 'frequency_id', 'frequency_name');
                $('#immediate').hide();
                $('#date_time_div').hide();
                $('#after_job_div').hide();
                $("#frequency").prop('disabled', true);
            } else if (this.value == 2) {
                //frequency_for_scheduler_report_date_time();
                getDataFromDB('GET','frequency_for_scheduler_report','json', 'frequency_date_time', 'frequency_id', 'frequency_name');
                $('#immediate').hide();
                $('#date_time_div').show();
                $('#after_job_div').hide();
                $("#frequency_date_time").prop('disabled', true);
            } else if (this.value == 3) {
                //get_jobs();
                getDataFromDB('GET','get_jobs_for_scheduler_reports','json', 'predecessor_job_name', 'job_id', 'job_name');
                $('#immediate').hide();
                $('#date_time_div').hide();
                $('#after_job_div').show();

            }
        });

        $(document).on('click', '#update_button', function() {
            //start_conditions = $("#start_conditions option:selected").attr('id');
            start_conditions = $("#start_conditions option:selected").val();
             console.log("start_conditions",start_conditions);
            if ($('#scheduler_create_form').valid()) {
                if ($('#start_conditions_form').valid()) {
                    if (start_conditions == "1") {
                        if ($('#period_immediately').is(":checked")) {
                            if ($('#immediate_form').valid()) {
                                update_msg(start_conditions);
                            }
                        } else {
                            update_msg(start_conditions);
                        }
                    } else if (start_conditions == "2") {
                        if ($('#periodic_date_time').is(":checked")) {
                            if ($('#date_time_form_frequency').valid()) {
                                update_msg(start_conditions);
                            }
                        } else {
                            if ($('#date_time_form').valid()) {
                                update_msg(start_conditions);
                            }
                        }

                    } else if (start_conditions == "3") {
                        if ($('#after_job_from').valid()) {
                            update_msg(start_conditions);
                        }
                    }
                }
            }

            if ($('#distribution_strategy').is(":checked")) {
                distribution_strategy = 1;

            } else {
                distribution_strategy = 0;

            }

        });

        function update_msg(start_conditions) {
            $.msgBox({
                title: "Confirm",
                content: "Do you want to update the information?",
                type: "confirm",
                buttons: [{
                    value: "Yes"
                }, {
                    value: "No"
                }],
                success: function(result) {
                    if (result == "Yes") {
                      console.log('update results',result);
                        update(start_conditions);
                    } else {

                    }
                }
            });
        }

        function update(start_conditions) {

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd
            }
            if (mm < 10) {
                mm = '0' + mm
            }
            today = yyyy + '-' + mm + '-' + dd;

            var dt = new Date();
            var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

            var job_name = $('#job_name').val();
            var variant = $("#variant_select option:selected").val();
            var job_priority = $("#job_priority option:selected").attr('id');

            var frequency = 0;

            console.log('update job_name',job_name,variant,job_priority);

            if ($('#active').is(":checked")) {
                active = 1;
            } else {
                active = 0;
            }

            if (start_conditions == "1") {
                var job_start_date = today;
                var job_end_date = today;
                var job_last_run_date = today;
                var job_start_time = time;
                var job_end_time = time;
                var job_last_run_time = time;
                if ($('#period_immediately').is(":checked")) {
                    period_immediately = 1;
                    var frequency = $("#frequency option:selected").attr('id');

                } else {
                    period_immediately = 0;
                    var frequency = 0;
                }
            } else if (start_conditions == "2") {
                var job_start_date = $('#start_date').val();
                var job_end_date = $('#end_date').val();
                var job_last_run_date = $('#start_date').val();
                var job_start_time = $('#start_time').val();
                var job_end_time = $('#end_time').val();
                var job_last_run_time = $('#start_time').val();

                if ($('#periodic_date_time').is(":checked")) {
                    var periodic_date_time = 1;
                    //var frequency_date_time = $("#frequency_date_time option:selected").attr('id');
                    var frequency_date_time = $("#frequency_date_time option:selected").val();
                } else {
                    var periodic_date_time = 0;
                    var frequency_date_time = 0;
                }
            } else if (start_conditions == "3") {
                var job_start_date = today;
                var job_end_date = today;
                var job_last_run_date = today;
                var job_start_time = time;
                var job_end_time = time;
                var job_last_run_time = time;

                var after_job = $("#predecessor_job_name option:selected").attr('id');
                console.log('after_job',after_job);
                // var predecessor_job_name = $("#predecessor_job_name option:selected").attr('id');
            }

            $.ajax({
                type: "POST",
                url: "scheduler_update_report",
                dataType:"json",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "job_id":job_id,
                    "job_name": job_name,
                    "variant": variant,
                    "job_priority": job_priority,
                    "start_type_id": start_conditions,
                    "active": active,
                    "modified_date":today,

                    //1.Immediate
                    "period_immediately": period_immediately,
                    "frequency": frequency,

                    //2.date_time
                    "periodic_date_time": periodic_date_time,
                    "frequency_date_time": frequency_date_time,

                    //3. After job
                    "after_job": after_job,

                    "job_start_date": job_start_date,
                    "job_end_date": job_end_date,
                    "job_last_run_date": job_last_run_date,
                    "job_start_time": job_start_time,
                    "job_end_time": job_end_time,
                    "job_last_run_time": job_last_run_time,
                    "distribution_strategy":myarray,
                    "distribution_strategy_check": distribution_strategy,


                },
                success: function(data) {
                   myarray.length = 0;
                     after_update();
                    $.msgBox({
                        title: "Message",
                        content: "Scheduler updated successfully",
                        type: "info",
                    });
                },
                beforeSend: function() {
                    $('#msg').show();
                },
                error: function() {}
            });
        }

        function after_update() {
          $('#tab_one').hide();
          $('#update_button').hide();
          $('#cancel_button').hide();
          $('#back_search').show();
          if(filter_clicked==true)
          {
            // filter_clicked = false;
            filter_click();
          }
          else {
            $("#show_all_link").trigger("click");
            $("#show_all").trigger("click");
            $('#list-of-user').show();
          }
        }

        function reset() {
            $('#scheduler_create_form').trigger("reset");
            $('#start_conditions_form').trigger("reset");
            $('#immediate_form').trigger("reset");
            $('#date_time_form').trigger("reset");
            $('#date_time_form_frequency').trigger("reset");
            $('#after_job_from').trigger("reset");
        }

        $('#cancel_button').click(function() {
            $('#cancel_button').hide();
            $('#list_of_jobs').show();
            $('#back_search').show();
            $('#tab_one').hide();
            $('#update_button').hide();
            $('#show_all').hide();
            $('#search_page').hide();
            $('#search_button').hide();
            myarray.length = 0;

        });

        var fetch_job_name_report;
        $('input[name="job_name_search"]').autoComplete({
            minChars: 0,
            source: function(term, response) {
                try {
                    fetch_job_name_report.abort();
                } catch (e) {}
                fetch_job_name_report = $.getJSON('fetch_job_name_report', {
                    job_name: term
                }, function(data) {
                    response(data);
                });
            }
        });

        var fetch_task_type;
        $('input[name="task_type_search"]').autoComplete({
            minChars: 0,
            source: function(term, response) {
                try {
                    fetch_task_type.abort();
                } catch (e) {}
                fetch_task_type = $.getJSON('fetch_task_type', {
                    task_type: term
                }, function(data) {
                    response(data);
                });
            }
        });

        var fetch_variant;
        $('input[name="variant_search"]').autoComplete({
            minChars: 0,
            source: function(term, response) {
                try {
                    fetch_variant.abort();
                } catch (e) {}
                fetch_variant = $.getJSON('fetch_variant', {
                    task_name: term
                }, function(data) {
                    response(data);
                });
            }
        });

        $('#search_button').click(function() {
            display_data();
        });

        function filter_click() {
            $('#list_of_jobs').show();
            $('#tab_one').hide();
            //$('#tab_one').show();
            show_all_link_clicked = false;
            var job_name_search = $('#job_name_search').val();
            //var task_type_search = $('#task_type_search').val();
            var variant_search = $('#variant_search').val();

            if ((job_name_search == "")  && (variant_search == "")) {
                  show_search_parameter_page();
                $.msgBox({
                    title: "Alert",
                    content: "Search Criteria Not Available",
                    type: "alert",
                });

            } else {
                display_data();
            }
        }

        function display_data() {
          var job_name_search = $('#job_name_search').val();
          var variant_search = $('#variant_search').val();
          console.log("variant_search",variant_search);
          console.log("job_name_search",job_name_search);

          if ((job_name_search == "")  && (variant_search == "")) {
                show_search_parameter_page();
              $.msgBox({
                  title: "Alert",
                  content: "Search Criteria Not Available",
                  type: "alert",
              });

          }
          else {

            $.ajax({
                type: "POST",
                url: "job_display_show_report",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "job_name_search": job_name_search,
                    //"task_type_search": task_type_search,
                    "variant_search":variant_search,
                },
                success: function(data) {
                    if (data != "0") {
                        results = data;
                        $('.tab tr').remove();
                        $('.tab').append('<tr style="background-color: #D3D3D3;"><th style="display:none">Job ID</th><th>Job Name</th><th>Task Name</th><th>Job Status</th><th>Active</th><th>Edit</th><th>Delete</th></tr>');
                        show_search_results_page();

                        for (var i = 0; i < results.length; i++) {
                            if (results[i].active == 0) {
                                active_check = '<input type="checkbox" disabled readonly>';
                            } else {
                                active_check = '<input type="checkbox" checked disabled readonly>';
                            }
                            $('.tab').append('<tr data-id="' + i + '" class="double_click"><td class="job_id" style="display:none">' + results[i].job_id + '</td><td>' + results[i].job_name + '</td><td>' + results[i].variant + '</td><td>' + results[i].job_status + '</td><td>' + active_check + '</td><td><button id="' + i + '" class="edit btn btn-default glyphicon glyphicon-edit edit_color" type="button"></button></td><td><button  data-id="' + i + '" id="delete_showall" type="button" class="btn btn-default delete_color" aria-label="Left Align"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td></tr>')
                        }
                    } else {
                            show_search_parameter_page();
                            $.msgBox({
                                title: "Alert",
                                content: "Data not found",
                                type: "alert",
                            });
                    }
                },
                beforeSend: function() {

                },
                error: function() {}
            });
          }
        }

        $(document).on('click', '#show_all', function() {
            show_all_link_clicked = true;
            filter_clicked = false;
            show_search_results_page();
            show_table();

        });


        function show_search_results_page() {
              $('#show_all').hide();
              $('#list_of_jobs').show();
              $('#search_page').hide();
              $('#back_search').show();
              $('#search_button').hide();
        }

        function show_search_parameter_page()
        {
          $('#back_search').hide();
          $('#show_all').show();
          $('#list_of_jobs').hide();
          $('#search_page').show();
          $('#back_search').hide();
          $('#search_button').show();
        }

        $(document).on('click', '#back_search', function() {
            show_search_parameter_page();
        });

        function show_table() {

            $.ajax({
                type: "GET",
                url: "get_jobs_reports",
                dataType: "json",
                success: function(data) {
                    if (data != "0") {
                        results = data;
                        $('.tab tr').remove();
                        $('.tab').append('<tr style="background-color: #D3D3D3;"><th style="display:none">Job ID</th><th>Job Name</th><th>Variant</th><th>Job Status</th><th>Active</th><th>Edit</th><th>Delete</th></tr>');

                        for (var i = 0; i < results.length; i++) {
                            if (results[i].active == 0) {
                                active_check = '<input type="checkbox" disabled readonly>';
                            } else {
                                active_check = '<input type="checkbox" checked disabled readonly>';
                            }
                            $('.tab').append('<tr data-id="' + i + '" class="double_click"><td class="job_id" style="display:none">' + results[i].job_id + '</td><td>' + results[i].job_name + '</td><td>' + results[i].variant + '</td><td>' + results[i].job_status + '</td><td>' + active_check + '</td><td><button id="' + i + '" class="edit btn btn-default glyphicon glyphicon-edit edit_color" type="button"></button></td><td><button  data-id="' + i + '" id="delete_showall" type="button" class="btn btn-default delete_color" aria-label="Left Align"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td></tr>')
                        }
                    } else {
                        if (filter_clicked == false) {
                            back_search();
                        } else {
                            back_search();
                            $.msgBox({
                                title: "Alert",
                                content: "Data not found",
                                type: "alert",
                            });
                        }

                    }
                },
                beforeSend: function() {

                },
                error: function() {}
            });
        }

          $(document).on('dblclick', '.double_click', function() {
            show_change_screen();
            var id = $(this).data("id");

            job_id = results[id].job_id;
            // task_id = results[id].task_id;
            $('#job_name').val(results[id].job_name);
            //$('#task_type').val(results[id].task_type_id);
            $('#job_priority').val(results[id].job_priority);
            $('#created_by').val(results[id].created_by);
            $('#created_date').val(results[id].created_at);
            $('#modified_by').val(results[id].updated_by);
            $('#modified_date').val(results[id].updated_at);

            if (results[id].active == 0) {
                $('#active').prop('checked', false);
            } else {
                $('#active').prop('checked', true);
            }



            function get_variant(report_id){

            $.ajax({
                type: "POST",
                url: "get_variant_for_scheduler",
                data: {
                    "_token": "{{ csrf_token() }}",
                     "report_id": report_id,
                },
                success: function(data) {
                    var results = data;
                    // console.log(JSON.stringify(data));
                    // console.log("results",results);

                    $('#variant_select').html('');
                    if (results.length == 0) {
                        $("#job_priority").prop('disabled', true);
                        $('#variant_select').append("<option id =\"0\" value=\"0\">Task Doesn't exist</option>");
                    } else {
                        $("#job_priority").prop('disabled', false);
                        for (var i = 0; i < results.length; i++) {
                            if (i == '0') {
                                $('#variant_select').append("<option id =\"0\" value=\"0\">Select</option>");
                            }
                            var variant = results[i].variant
                            var report_id = results[i].report_id
                            $('#variant_select').append("<option  id =" + report_id + " value=" + report_id + ">" + variant + "</option>");
                        }
                    }
                },
                beforeSend: function() {},
                error: function() {}
            });
            }
              $('#variant_select').val(results[id].task_id);
              $('#start_conditions').val(results[id].start_type_id);

            if (results[id].start_type_id == 1) {
                $('#immediate').hide();
                $('#date_time_div').hide();
                $('#after_job_div').hide();

                if (results[id].periodic == 1) {
                    $('#period_immediately').prop('checked', true);
                    $.ajax({
                        type: "POST",
                        url: "get_frequency_for_scheduler_report",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "job_id": results[id].job_id,
                        },
                        success: function(data) {
                            var results = data;
                            $('#frequency').val(results[0].frequency_id);
                        },
                        beforeSend: function() {},
                        error: function() {}
                    });
                }

            } else if (results[id].start_type_id == 2) {
                $('#immediate').hide();
                $('#date_time_div').show();
                $('#after_job_div').hide();
                $("#start_date").val(results[id].job_start_date);
                $("#start_time").val(results[id].job_start_time);
                $("#end_date").val(results[id].job_end_date);
                $("#end_time").val(results[id].job_end_time);

                if (results[id].periodic == 1) {
                    $('#periodic_date_time').prop('checked', true);
                    $.ajax({
                        type: "POST",
                        url: "get_frequency_for_scheduler_report",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "job_id": results[id].job_id,
                        },
                        success: function(data) {
                            var results = data;
                             console.log("results[0].frequency_id",results[0].frequency_id);
                             $('#frequency_date_time').val(results[0].frequency_id);
                        },
                        beforeSend: function() {},
                        error: function() {}
                    });
                }

            } else if (results[id].start_type_id == 3) {

                $('#immediate').hide();
                $('#date_time_div').hide();
                $('#after_job_div').show();

                $.ajax({
                    type: "POST",
                    url: "get_after_job_for_scheduler_report",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "job_id": results[id].job_id,
                    },
                    success: function(data) {
                        var results = data;
                        $('#predecessor_job_name').val(results[0].predecessor_job_id);
                    },
                    beforeSend: function() {},
                    error: function() {}
                });

            }

            var ds = 0;
            $.ajax({
                type: "POST",
                url: "task_ds_ref",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "task_id": job_id ,
                },

                success: function(data) {

                 console.log(data);
                  if (data != 0) {

                      job_ds_ref_data = "YES";
                      $('#distribution_strategy').prop('checked', true);
                      $("#change_ds").show();

                  } else {
                    job_ds_ref_data = "NO";
                    $('#distribution_strategy').prop('checked', false);
                      $("#change_ds").hide();
                  }
                },
                beforeSend: function() {

                },
                error: function() {

                }
            });


          });

        $(document).on('click', '.edit', function() {
            $('#tab_one,#cancel_button,#update_button').show();
            $('#list_of_jobs,#back_search').hide();//show_change_screen();

            var id = $(this).attr("id");
            // console.log("id",id);
            job_id = results[id].job_id;
            // console.log("job_id",job_id);
            $('#job_name').val(results[id].job_name);
            //$('#task_type').val(results[id].task_type_id);
            $('#job_priority').val(results[id].job_priority);
            $('#created_by').val(results[id].created_by);
            $('#created_date').val(results[id].created_at);
            $('#modified_by').val(results[id].updated_by);
            $('#modified_date').val(results[id].updated_at);

            if (results[id].active == 0) {
                $('#active').prop('checked', false);
            } else {
                $('#active').prop('checked', true);
            }

            function get_variant(report_id){
            $.ajax({
                type: "POST",
                url: "get_variant_for_scheduler",
                data: {
                    "_token": "{{ csrf_token() }}",
                     "report_id": report_id,
                },
                success: function(data) {
                    var results = data;
                    // console.log(JSON.stringify(data));
                    // console.log("results",results);

                    $('#variant_select').html('');
                    if (results.length == 0) {
                        $("#job_priority").prop('disabled', true);
                        $('#variant_select').append("<option id =\"0\" value=\"0\">Task Doesn't exist</option>");
                    } else {
                        $("#job_priority").prop('disabled', false);
                        for (var i = 0; i < results.length; i++) {
                            if (i == '0') {
                                $('#variant_select').append("<option id =\"0\" value=\"0\">Select</option>");
                            }
                            var variant = results[i].variant
                            var report_id = results[i].report_id
                            $('#variant_select').append("<option  id =" + report_id + " value=" + report_id + ">" + variant + "</option>");
                        }
                    }
                },
                beforeSend: function() {},
                error: function() {}
            });
            }
            $('#variant_select').val(results[id].task_id);
            $('#start_conditions').val(results[id].start_type_id);

            if (results[id].start_type_id == 1) {
                $('#immediate').hide();
                $('#date_time_div').hide();
                $('#after_job_div').hide();

                if (results[id].periodic == 1) {
                    $('#period_immediately').prop('checked', true);
                    $.ajax({
                        type: "POST",
                        url: "get_frequency_for_scheduler_report",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "job_id": results[id].job_id,
                        },
                        success: function(data) {
                            var results = data;
                            $('#frequency').val(results[0].frequency_id);
                        },
                        beforeSend: function() {},
                        error: function() {}
                    });
                }

            } else if (results[id].start_type_id == 2) {
                $('#immediate').hide();
                $('#date_time_div').show();
                $('#after_job_div').hide();
                $("#start_date").val(results[id].job_start_date);
                $("#start_time").val(results[id].job_start_time);
                $("#end_date").val(results[id].job_end_date);
                $("#end_time").val(results[id].job_end_time);

                if (results[id].periodic == 1) {
                    $('#periodic_date_time').prop('checked', true);
                    $.ajax({
                        type: "POST",
                        url: "get_frequency_for_scheduler",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "job_id": results[id].job_id,
                        },
                        success: function(data) {
                            var results = data;
                            // console.log("results",results);
                            // $('#frequency_date_time').val(results[0].frequency_id);
                        },
                        beforeSend: function() {},
                        error: function() {}
                    });
                }

            } else if (results[id].start_type_id == 3) {

                $('#immediate').hide();
                $('#date_time_div').hide();
                $('#after_job_div').show();

                $.ajax({
                    type: "POST",
                    url: "get_after_job_for_scheduler_report",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "job_id": results[id].job_id,
                    },
                    success: function(data) {
                        var results = data;

                        $('#predecessor_job_name').val(results[0].predecessor_job_id);
                    },
                    beforeSend: function() {},
                    error: function() {}
                });

            }


            var ds = 0;
            $.ajax({
                type: "POST",
                url: "task_ds_ref",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "task_id": job_id ,
                },

                success: function(data) {
                  if (data != 0) {

                      job_ds_ref_data = "YES";
                      $('#distribution_strategy').prop('checked', true);
                      $("#change_ds").show();

                  } else {
                    job_ds_ref_data = "NO";
                    $('#distribution_strategy').prop('checked', false);
                      $("#change_ds").hide();
                  }
                },
                beforeSend: function() {

                },
                error: function() {

                }
            });


        });

        function show_change_screen() {
            $('#tab_one').show();
            $('#list_of_jobs').hide();
            $('#cancel_button').show();
            $('#update_button').show();
            $('#back_search').hide();
        }

        $(document).on('click', '#delete_search', function() {
          var id = $(this).data("id");
          job_id = results[id].job_id;
            $.msgBox({
                title: "Confirm",
                content: "Do you want to delete the data",
                type: "confirm",
                buttons: [{
                    value: "Yes"
                }, {
                    value: "No"
                }],
                success: function(result) {
                    if (result == "Yes") {

                        $.ajax({
                            type: "POST",
                            url: "scheduler_delete_report",
                            data: {
                              "_token": "{{ csrf_token() }}",
                              "job_id": job_id,
                            },
                            success: function() {
                                display_data();
                                $.msgBox({
                                    title: "Message",
                                    content: "Job deleted successfully",
                                    type: "info",
                                });
                            },
                            beforeSend: function() {

                            },
                            error: function() {

                            }
                        });
                    }
                }
            });
        });

        $(document).on('click', '#delete_showall', function() {

          var id = $(this).data("id");
          job_id = results[id].job_id;
            $.msgBox({
                title: "Confirm",
                content: "Do you want to delete the data",
                type: "confirm",
                buttons: [{
                    value: "Yes"
                }, {
                    value: "No"
                }],
                success: function(result) {
                    if (result == "Yes") {
                        $.ajax({
                            type: "POST",
                            url: "scheduler_delete_report",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                "job_id": job_id,
                            },
                            success: function() {
                                show_table();
                                $.msgBox({
                                    title: "Message",
                                    content: "Job deleted successfully",
                                    type: "info",
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "delete_ds_ref_task",
                                    data: {
                                        "_token": "{{ csrf_token() }}",
                                        "task_id": job_id,
                                    },
                                    success: function(data) {
                                    },
                                    beforeSend: function() {

                                    },
                                    error: function() {

                                    }
                                });

                            },
                            beforeSend: function() {

                            },
                            error: function() {}
                        });
                    }
                }
            });
        });

    });

//distribution_strategy starts here
$(".ds_form").dialog({
  autoOpen: false,
  modal: true,
  width: 750,
});

$("#set-ds").dialog({
  modal: true,
  autoOpen: false,
  width:850,
  height:170
 });

 $.ajax({
     type: "GET",
     url: "task_status",
     success: function(data) {

         if (data != 0) {
             data_status = data;
             $('#report_status').empty();
             $('#report_status').append("<option id =\"null\" value=\"0\">Select</option>");

             for (var i = 0; i < data.length; i++)
                {
                 var status_id = data[i].status_id;
                 var status_name = data[i].status_name;
                 $('#report_status').append("<option id =" + status_id + " value=" + status_id + ">" + status_name + "</option>");
                }

         }

     },
     beforeSend: function() {

     },
     error: function() {

     }
 });

 $.ajax({
     type: "GET",
     url: "communication_options",
     success: function(data) {

         if (data != 0) {
             data_communication_options = data;
             $('#communication_options').empty();
             $('#communication_options').append("<option id =\"null\" value=\"0\">Select</option>");

             for (var i = 0; i < data.length; i++) {

                 var communication_option_id = data[i].communication_option_id;
                 var communication_option_name = data[i].communication_option_name;
                 $('#communication_options').append("<option id =" + communication_option_id + " value=" + communication_option_id + ">" + communication_option_name + "</option>");
             }
         }

     },
     beforeSend: function() {

     },
     error: function() {

     }
 });

 $.ajax({
     type: "GET",
     url: "mail_address",
     success: function(data) {

         if (data != 0) {
             mail_address_data = data;

         }
     },
     beforeSend: function() {

     },
     error: function() {

     }
 });

 $.ajax({
     type: "GET",
     url: "group_mail_address",
     success: function(data) {

         if (data != 0) {
             group_mail_address_data = data;
         }
     },
     beforeSend: function() {

     },
     error: function() {

     }
 });


 function populateSelectOptions_DS(result, selectId, valueAttr, displayAttr) {
   $('#' + selectId).empty();
     $('#'+ selectId).append("<option id =\"null\" value=\"\">Select Mail Address</option>");

   if(result == null || result.length == 0) {
     alert('Data not found...!!!');
   } else {
     for (var i = 0; i < result.length; i++)
     {
       var value = result[i][valueAttr];
       var display = result[i][displayAttr];
       console.log("selectId",selectId);
       $('#'+ selectId).append("<option value='"+ value +"'>"+display+"</option>");

     }
   }
 }

 function getDataFromDB_DS(methodType, url, dataType, selectId, valueAttr, displayAttr, data) {

   $.ajax({
     type:methodType,
     url:url,
     dataType:dataType,
     data:data?data:{},
     success:function(result){

         populateSelectOptions_DS(result, selectId, valueAttr, displayAttr);
         console.log("result",result);

     },
     error: function() {
       alert('Error while fetching the record');
     }
   });
 }

 function onChangeEvent_DS(selectedId, methodType, url, altUrl, dataType, selectId, valueAttr, displayAttr){
   $('#'+selectedId).on('change', function(){
     var selectedValue = $('#'+selectedId).val();

     if(Array.isArray(selectedValue)) {
       selectedValue = selectedValue.join('\',\'');

     }
     var data = {"_token": "{{ csrf_token() }}", filterKey:selectedValue};

     if(selectedValue == ""){
       url = altUrl;
     }

     getDataFromDB_DS(methodType, url, dataType, selectId, valueAttr, displayAttr, data);

   });
 }

 $('#mail_type').on('change', function() {

   if(this.value == 'personnel')
    {
      console.log(this.value);
       getDataFromDB_DS('GET', 'mail_address','json', 'mail_address', 'mail_comm_id', 'ent_user_id');
    }

   else if(this.value == 'group')
    {
        console.log(this.value);
               getDataFromDB_DS('GET', 'group_mail_address', 'json', 'mail_address', 'user_group_id', 'ent_user_group_id');
      }

    });



 function ds_data() {
     var ref_job_id = job_id;

     $.ajax({
         type: "POST",
         url: "ds_results",
         dataType: "json",
         data: {

             "_token": "{{ csrf_token() }}",
             "task_id_ref_ds": ref_job_id,
         },

         success: function(data) {
             if (data != "0") {
                             var results = data;
                             ds_results = results;
                             ds_data1 = "YES";

                             $('.ds_tab tr').remove();
                            $('.resetgap2').remove();
                             $('.ds_tab').append('<tr style="background-color: #D3D3D3;"><th class="ds_th">Task Status</th><th class="ds_th">communication_options</th><th class="ds_th">Mail Type</th><th class="ds_th">Mail Address</th><th>Delete</th></tr>');
                             $('.ds_form').append('	<div class="resetgap2 actiondiv2"><div style=" margin-bottom: 20px; "> <button id="update_ds" class="dsbuttons" type="button"><span class="btn_txt">Save</span></button><button class="dsbuttons" id="ds_close" type="button" ><span class="btn_txt">Close</span></button></div></div>');

                             for (var j = 0; j < results.length; j++) {

                                 $('.ds_tab').append('<tr data-id="' + j + '" id="' + results[j].distribution_strategy_id + '"><td><select id="report_status' + j + '"></select></td><td><select id="communication_options' + j + '"></select></td><td><select class = "mail_type_change" data-id="' + j + '"  id="mail_type' + j + '"><option>personnel</option><option>group</option></select></td><td data-id="' + results[j].distribution_strategy_id + '"><select id="mail_address' + j + '" ></select></td><td><button id="' + j + '" type="button" class="delete_ds btn btn-default" aria-label="Left Align"><span class="glyphicon glyphicon-remove delete_color" aria-hidden="true"></span></button></td></tr>');

                                 ref_ds_id = results[j].distribution_strategy_id;

                                 var task_status = results[j].status_name;
                                 var communication_options = results[j].communication_option_name;
                                 var mail_address = results[j].mail_address;
                                 var mail_type = results[j].mail_type;
                                 var mail_id = results[j].mail_id;
                                 var mail_id_address = results[j].mail_id_address;


                                 for (var i = 0; i < data_status.length; i++) {

                                     var status_name = data_status[i].status_name;
                                     var status_id = data_status[i].status_id;
                                     $('#' + "report_status" + j).append("<option id =" + status_id + " value=" + status_id + ">" + status_name + "</option>");

                                 }
                                 $('#' + "report_status" + j).val(results[j].status_id);

                                 for (var i = 0; i < data_communication_options.length; i++) {
                                     var communication_option_name = data_communication_options[i].communication_option_name;
                                     var communication_option_id = data_communication_options[i].communication_option_id;
                                     $('#' + "communication_options" + j).append("<option id =" + communication_option_id + " value=" + communication_option_id + ">" + communication_option_name + "</option>");
                                 }
                                 $('#' + "communication_options" + j).val(results[j].communication_options);
                                 $('#' + "mail_type" + j).val(results[j].mail_type);


                                 if (mail_type == "personnel") {
                                    // $('#' + "mail_address" + j).empty();
                                     for (var i = 0; i < mail_address_data.length; i++) {
                                         ent_user_id = mail_address_data[i].ent_user_id;
                                         mail_comm_id = mail_address_data[i].mail_comm_id;
                                         $('#' + "mail_address" + j).append("<option id =" + mail_comm_id + " value=" + mail_comm_id + ">" + ent_user_id + "</option>");
                                     }
                                     $('#' + "mail_address" + j).val(results[j].mail_id);
                                 } else if (mail_type == "group") {
                                     //$('#' + "mail_address" + j).empty();

                                     for (var i = 0; i < group_mail_address_data.length; i++) {
                                         ent_user_group_id = group_mail_address_data[i].ent_user_group_id;
                                         user_group_id = group_mail_address_data[i].user_group_id;
                                         $('#' + "mail_address" + j).append("<option id =" + user_group_id + " value=" + user_group_id + ">" + ent_user_group_id + "</option>");
                                     }
                                     $('#' + "mail_address" + j).val(results[j].mail_id);

                                 }
                             }
                }
                else if(data == "0")
                 {
                   job_ds_ref_data = "NO";
                 $('.ds_form').dialog("close");
                 var ds_val = $('#distribution_strategy').val();
                 if (ds_val == "on")
                 {
                    $('#distribution_strategy').prop('checked', false);
                 }
                 $('#change_ds').hide();
              }
         },
         beforeSend: function() {

         },
         error: function() {

         }
     });

 }



 $('#distribution_strategy').click(function() {
    if ($("#distribution_strategy").is(':checked'))
    {

        if(job_ds_ref_data == "NO")
                    {
                            $('#set-ds').dialog("open");
                            $(".resetgap").addClass("show");
                            $.ajax({
                               type: "GET",
                               url: "get_current_ds_val",
                               success: function(data) {
                                 for (var i = 0; i < data.length; i++)
                                 {
                                     current_ds_count = data[i].distribution_strategy_id;
                                      console.log(current_ds_count);
                                }
                               },
                               beforeSend: function() {

                               },
                               error: function() {

                               }
                           });
                      }
                     else if(job_ds_ref_data == "YES")
                         {
                            $('#change_ds').show();

                         }
                  }
                  else if ($("#distribution_strategy").is(':unchecked'))
                  {
                    $('#change_ds').hide();
                  }
       });

      $('#change_ds').on('click', function() {

           if(job_ds_ref_data == "YES")
            {
                    ds_data();
                   $('.ds_tab tr').remove();
                   $('.ds_form').dialog("open");
                   $('.resetgap2').remove();
           }
      });



        $(' #Cancel-ds').click(function(){
               myarray.length = 0;
               reset_ds();
            $('#set-ds').dialog("close");
             var ds_val = $('#distribution_strategy').val();
             if (ds_val == "on")
             {
                $('#distribution_strategy').prop('checked', false);
             }
          });


      $(document).on('change', '.mail_type_change', function() {

          var id = $(this).data("id");
          if (this.value == 'personnel') {
              $('#mail_address' + id).empty();
              $.ajax({
                  type: "GET",
                  url: "mail_address",
                  success: function(data) {

                      if (data != 0) {
                          for (var i = 0; i < data.length; i++) {
                              var ent_user_id = data[i].ent_user_id;
                              var mail_comm_id = data[i].mail_comm_id;
                              $('#mail_address' + id).append("<option id =" + mail_comm_id + " value=" + mail_comm_id + ">" + ent_user_id + "</option>");
                          }

                      }
                  },
                  beforeSend: function() {

                  },
                  error: function() {

                  }
              });
          } else if (this.value == 'group') {
              $('#mail_address' + id).empty();
              $.ajax({
                  type: "GET",
                  url: "group_mail_address",
                  success: function(data) {

                      if (data != 0) {
                          for (var i = 0; i < data.length; i++) {
                              var ent_user_group_id = data[i].ent_user_group_id;
                              var user_group_id = data[i].user_group_id;
                              $('#mail_address' + id).append("<option id =" + user_group_id + " value=" + user_group_id + ">" + ent_user_group_id + "</option>");
                          }

                      }
                  },
                  beforeSend: function() {

                  },
                  error: function() {

                  }
              });
          }
      });



      var myarray = [];
      $(document).on('click', '#update_ds', function() {
          for (var i = 0; i < ds_results.length; i++) {

              var distribution_strategy_id = ds_results[i].distribution_strategy_id;
              var status_id = $('#report_status' + i).val();
              var communication_options = $('#communication_options' + i).val();
              var mail_type = $('#mail_type' + i).val();
              var mail_id = $('#mail_address' + i).val();
              var ds_update_data = {
                  "distribution_strategy_id": distribution_strategy_id,
                  "report_status": status_id,
                  "communication_options": communication_options,
                  "mail_type": mail_type,
                  "mail_id": mail_id
              }
              myarray.push(ds_update_data);
              reset_ds();
          }
      });



      $(document).on('click', '#ds_close', function() {
          $('.ds_form').dialog("close");
      });

      function reset_ds()
        {
              $('#set-ds')[0].reset();
        }
      $('#Reset-ds').click(function(){
            reset_ds();
      });

      var myarray = [];
      $(document).on('click', '#save-ds', function() {

              var report_status = $('#report_status').val();
              var communication_options = $('#communication_options').val();
              var mail_type = $('#mail_type').val();
              var mail_address = $('#mail_address').val();
              current_ds_count = current_ds_count +1;

              var ds =  {
                      "distribution_strategy_id": current_ds_count,
                      "report_status": report_status,
                      "communication_options": communication_options,
                      "mail_type": mail_type,
                      "mail_id": mail_address
                  }
              myarray.push(ds);
              reset_ds();
      });


      $(document).on('click', '.delete_ds', function() {

          var msg = "Do you want to delete the DS";
          $.msgBox({
              title: "Confirm",
              content: msg,
              type: "confirm",
              buttons: [{
                  value: "Yes"
              }, {
                  value: "No"
              }],

              success: function(result) {
                  if (result == "Yes") {

                      $.ajax({
                          type: "POST",
                          url: "ds_delete",
                          data: {
                              "_token": "{{ csrf_token() }}",
                              "distribution_strategy_id": ref_ds_id,
                          },
                          success: function(data) {

                              $.msgBox({
                                  title: "Message",
                                  content: "DS Deleted Successfully",
                                  type: "info",
                              });

                               $(ref_ds_id).remove();
                                ds_data();

                          },
                          beforeSend: function() {

                          },
                          error: function() {

                          }
                      });
                  } else {

                  }
              }
          });

      });


//distribution_strategy ends here


</script>
</div>
@stop
@section('lower_band')
<div class="col-xs-12 lower_band">
   <button id="cancel_button" class="headerbuttons load" type="button" style="float: right; margin-right: 20px;">Cancel</button>
   <button id="update_button" class="headerbuttons load" type="button"  style="float: right; margin-right: 20px;">Update</button>
   <button id="search_button" class="headerbuttons" type="button"  style="float: left; margin-right: 20px;">Search</button>
   <button id="back_search" class="headerbuttons load" type="button"  style="float: left; margin-right: 20px;">Back</button>
</div>
@stop
