@extends('layouts.uscan_master_page')
@section('header')
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/base/jquery-ui.css" rel="stylesheet">
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/schedule_reports.css">
<script src="js/schedule_reports_validation.js"></script>
@stop

@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="display-title">
      Display Shedule Report Job
   </div>
   <div id="show_all_link" class="col-xs-2" style="float: right;text-align:center;line-height:30px;">
      <span  id="show_all" style="">Show All Jobs</span>
   </div>
</div>
@stop

@section('content')
<div class="col-xs-12 content">
  <div id="search_page" class="search_page">
    <form id="scheduler_search_form" autocomplete="off">
       <table style="width:30%;">
          <tr>
             <td style="background-color: #eee;"><strong>Basic Details</strong></td>
             <td></td>
          </tr>
          <tr>
             <td></td>
          </tr>
          <tr>
             <td>Job Name</td>
             <td><input type="text" id="job_name_search" name="job_name_search" ></td>
          </tr>
          <!-- <tr>
             <td>Task Type</td>
             <td><input type="text" id="task_type_search" name="task_type_search"></td>
          </tr> -->
          <tr>
             <td>Variant</td>
             <!-- <td><input type="text" id="task_name_search" name="task_name_search"></td> -->
             <td><input type="text" id="variant_search" name="variant_search"></td>
          </tr>
       </table>
    </form>
  </div>

  <div class="list_of_jobs" id="list_of_jobs">
    <table style="width:100%;margin-bottom: 371px" class="tab">
    </table>
  </div>

   <div id="tab_one" class="load">
      <form id="scheduler_create_form" autocomplete="off">
         <table style="width:60%;">
            <tr>
               <td style="background-color: #eee;"><strong>Basic Details</strong></td>
               <td></td>
               <td style="background-color: #eee;"><strong>Additional Details</strong></td>
            </tr>
            <tr>
               <td></td>
            </tr>
            <tr>
               <td>Job Name</td>
               <td><input type="text" id="job_name" name="job_name" disabled></td>
               <td>Created By</td>
               <td><input type="text" name="created_by" id="created_by" disabled></td>
            </tr>
            <!-- <tr>
               <td>Task Type</td>
               <td><select id = "task_type" name="task_type">
                  </select>
               </td>
               <td>Created Date</td>
               <td><input type="text" name="created_date" id="created_date" disabled></td>
            </tr> -->
            <tr>
               <td>Variant</td>
               <td><select id = "variant_select" name="variant" disabled style="background-color: #ebebe4;"></select>
               </td>
               <td>Created Date</td>
               <td><input type="text" name="created_date" id="created_date" disabled></td>
            </tr>
            <tr>
               <td>Job Priority</td>
               <td>
                  <select id = "job_priority" name="job_priority" disabled style="background-color: #ebebe4;">
                     <option id="0" value="0">Select Task Name</option>
                     <option id="1" value="1">High</option>
                     <option id="2" value="2">Medium</option>
                     <option id="3" value="3">Low</option>
                  </select>
               </td>
               <td>Modified By</td>
               <td><input type="text" name="modified_by" id="modified_by" disabled></td>
            </tr>
            <tr>
               <td>Active</td>
               <td><input type="checkbox" name="active" id="active" disabled></td>
               <td>Modified Date</td>
               <td><input type="text" name="modified_date" id="modified_date" disabled></td>
            </tr>
            <tr>
               <td></td>
            </tr>
         </table>
      </form>
      <div id="start_conditions_div" class="">
         <form id="start_conditions_form" class="">
            <table style="width:30%;">
               <tr>
                  <td style="background-color: #eee;"><strong>Start Conditions</strong></td>
                  <td></td>
               </tr>
               <tr>
                  <td>Start Conditions</td>
                  <td><select id = "start_conditions" name="start_conditions" disabled style="background-color: #ebebe4;">
                     </select>
                  </td>
               </tr>
               <tr>
                  <td>Communication</td>
                  <td><input type="checkbox" class = "" name="distribution_strategy" id="distribution_strategy" disabled><button type="button" id="show_ds" style ="display:none;">show_ds</button></td>
               </tr>
            </table>
         </form>
      </div>
      <div class="load" id="immediate">
         <form id="immediate_form" class="">
            <table style="width:30%;">
               <tr>
                  <td>Periodic</td>
                  <td><input type="checkbox" id="period_immediately" name="period_immediately" value="" disabled></td>
               </tr>
               <tr>
                  <td>Frequency</td>
                  <td><select id="frequency" class="" name="frequency" disabled style="background-color: #ebebe4;">
                     </select>
                  </td>
               </tr>
            </table>
         </form>
      </div>
      <div id="date_time_div" class="load">
         <form id="date_time_form" class="date_time_form">
           <table style="width:60%;">
             <tr>
                <td>
                   Start Date
                </td>
                <td><input type="date" name="start_date" id="start_date" disabled style="background-color: #ebebe4;"></td>
                <td>
                   End Date
                </td>
                <td><input type="date" name="end_date" id="end_date" disabled style="background-color: #ebebe4;"></td>
             </tr>
             <tr>
                <td>Start Time</td>
                <td><input type="time" name="start_time" id="start_time" disabled style="background-color: #ebebe4;"></td>
                <td>End Time</td>
                <td><input type="time" name="end_time" id="end_time" disabled style="background-color: #ebebe4;"></td>
             </tr>
             <tr>
                <td>Periodic</td>
                <td><input type="checkbox" name="periodic_date_time" id="periodic_date_time" disabled ></td>
             </tr>
           </table>
         </form>
         <form id="date_time_form_frequency">
            <table style="width:30%;">
               <tr>
                  <td id="frequency_label">Frequency</td>
                  <td><select id ="frequency_date_time" name="frequency_date_time" disabled style="background-color: #ebebe4;">
                     </select>
                  </td>
               </tr>
            </table>
         </form>
      </div>
      <div id="after_job_div" class="load">
         <form id="after_job_from" class="after_job_from">
            <table style="width:30%;">
               <tr>
                  <td>Job Name</td>
                  <td>
                     <select id = "predecessor_job_name" name="predecessor_job_name" disabled style="background-color: #ebebe4;">
                     </select>
                  </td>
               </tr>
            </table>
         </form>
      </div>
   </div>

   <form class="ds_form">
      <table class="ds_tab"></table>
   </form>

   <script type="text/javascript">
    jQuery.validator.addMethod('selectcheck', function(value) {
        return (value != '0');
    }, "");

var job_id = "";
    $(document).ready(function() {
        //on first time load start
        var active = 0;
        var period_immediately = 0;
        var start_conditions = 0;
        var periodic_date_time = 0;
        var frequency_date_time = 0;
        var filter_clicked = false;


        $("#report_generator").trigger('click');
        $("#schedule_reports").trigger('click');
        $('#tab_one').hide();

        function populateSelectOptions(result, selectId, valueAttr, displayAttr,isDefault) {
          $('#' + selectId).empty();
          if(!isDefault)
            $('#'+ selectId).append("<option id =\"null\" value=\"\">Select</option>");

          if(result == null || result.length == 0) {
            alert('Data not found...!!!');
          } else {
            for (var i = 0; i < result.length; i++)
            {
              var value = result[i][valueAttr];
              var display = result[i][displayAttr];
              if(!isDefault)
              $('#'+ selectId).append("<option value='"+ value +"'>"+display+"</option>");
              else
              $('#'+ selectId).append("<option selected value='"+ value +"'>"+display+"</option>");
            }
          }
        }

        function getDataFromDB(methodType, url, dataType, selectId, valueAttr, displayAttr, data,inputType, isDefault) {

          $.ajax({
            type:methodType,
            url:url,
            dataType:dataType,
            data:data?data:{},
            success:function(result){
                populateSelectOptions(result, selectId, valueAttr, displayAttr,isDefault);
            },
            error: function() {
              alert('Error while fetching the record');
            }
          });
        }

        $('#variant_select').change(function() {
            var report_id = $('#variant_select').val();
            if (report_id == 0) {
                $("#job_priority").prop('disabled', true);
                $('#job_priority').val(0);
            } else {
                $("#job_priority").prop('disabled', false);
            }
        });


        getDataFromDB('GET','frequency_for_scheduler_report','json', 'frequency_date_time', 'frequency_id', 'frequency_name');
        getDataFromDB('GET','frequency_for_scheduler_report','json', 'frequency', 'frequency_id', 'frequency_name');
        getDataFromDB('GET','get_start_conditions_for_scheduler_reports','json', 'start_conditions', 'start_type_id', 'start_type_name');
        getDataFromDB('GET','get_jobs_for_scheduler_reports','json', 'predecessor_job_name', 'job_id', 'job_name');
        getDataFromDB('GET','get_variant','json', 'variant_select', 'report_id', 'variant');


        $('#cancel_button').click(function() {
            $('#cancel_button').hide();
            $('#list_of_jobs').show();
            $('#back_search').show();
            $('#tab_one').hide();
            $('#update_button').hide();
            $('#show_all').hide();
            $('#search_page').hide();
            $('#search_button').hide();

        });

        var fetch_job_name_report;
        $('input[name="job_name_search"]').autoComplete({
            minChars: 0,
            source: function(term, response) {
                try {
                    fetch_job_name_report.abort();
                } catch (e) {}
                fetch_job_name_report = $.getJSON('fetch_job_name_report', {
                    job_name: term
                }, function(data) {
                    response(data);
                });
            }
        });

        var fetch_task_type;
        $('input[name="task_type_search"]').autoComplete({
            minChars: 0,
            source: function(term, response) {
                try {
                    fetch_task_type.abort();
                } catch (e) {}
                fetch_task_type = $.getJSON('fetch_task_type', {
                    task_type: term
                }, function(data) {
                    response(data);
                });
            }
        });

        var fetch_variant;
        //$('input[name="task_name_search"]').autoComplete({
        $('input[name="variant_search"]').autoComplete({
            minChars: 0,
            source: function(term, response) {
                try {
                    fetch_variant.abort();
                } catch (e) {}
                fetch_variant = $.getJSON('fetch_variant', {
                    task_name: term
                }, function(data) {
                    response(data);
                });
            }
        });

        $('#search_button').click(function() {
            display_data();
        });

        function display_data() {

            var job_name_search = $('#job_name_search').val();
            var variant_search = $('#variant_search').val();
            console.log("variant_search",variant_search);
            console.log("job_name_search",job_name_search);

            if ((job_name_search == "")  && (variant_search == "")) {
                  show_search_parameter_page();
                $.msgBox({
                    title: "Alert",
                    content: "Search Criteria Not Available",
                    type: "alert",
                });

            }
            else {

            $.ajax({
                type: "POST",
                url: "job_display_show_report",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "job_name_search": job_name_search,
                    "variant_search":variant_search,
                },
                success: function(data) {

                    if (data != "0") {
                        results = data;
                        $('.tab tr').remove();
                        $('.tab').append('<tr style="background-color: #D3D3D3;"><th style="display:none">Job ID</th><th>Job Name</th><th>Variant</th><th>Job Status</th><th>Active</th><th>Created By</th><th>Updated By</th></tr>');
                        show_search_results_page();

                        for (var i = 0; i < results.length; i++) {
                            if (results[i].active == 0) {
                                active_check = '<input type="checkbox" disabled readonly>';
                            } else {
                                active_check = '<input type="checkbox" checked disabled readonly>';
                            }
                            $('.tab').append('<tr data-id="' + i + '" class="double_click"><td class="job_id" style="display:none">' + results[i].job_id + '</td><td>' + results[i].job_name + '</td><td>' + results[i].variant + '</td><td>' + results[i].job_status + '</td><td>' + active_check + '</td><td>' + results[i].created_by + '</td><td>' + results[i].updated_by + '</td></tr>')
                        }
                    } else {
                          show_search_parameter_page();
                          $.msgBox({
                              title: "Alert",
                              content: "Data not found",
                              type: "alert",
                          });

                    }
                },
                beforeSend: function() {

                },
                error: function() {}
            });
          }
        }

        $(document).on('click', '#show_all', function() {
            show_all_link_clicked = true;
            filter_clicked = false;
            show_search_results_page();
            show_table();

        });


        function show_search_results_page() {
              $('#show_all').hide();
              $('#list_of_jobs').show();
              $('#search_page').hide();
              $('#back_search').show();
              $('#search_button').hide();
        }

        function show_search_parameter_page()
        {
          // alert("show");
          $('#back_search').hide();
          $('#show_all').show();
          $('#list_of_jobs').hide();
          $('#search_page').show();
          $('#back_search').hide();
          $('#search_button').show();
        }

        $(document).on('click', '#back_search', function() {
            show_search_parameter_page();
        });

        function show_table() {
            $.ajax({
                type: "GET",
                url: "get_jobs_reports",
                dataType: "json",
                success: function(data) {
                    if (data != "0") {
                        results = data;
                        $('.tab tr').remove();
                        $('.tab').append('<tr style="background-color: #D3D3D3;"><th style="display:none">Job ID</th><th>Job Name</th><th>Variant</th><th>Job Status</th><th>Active</th><th>Created By</th><th>Updated By</th></tr>');

                        for (var i = 0; i < results.length; i++) {
                            if (results[i].active == 0) {
                                active_check = '<input type="checkbox" disabled readonly>';
                            } else {
                                active_check = '<input type="checkbox" checked disabled readonly>';
                            }
                            $('.tab').append('<tr data-id="' + i + '" class="double_click"><td class="job_id" style="display:none">' + results[i].job_id + '</td><td>' + results[i].job_name + '</td><td>' + results[i].variant + '</td><td>' + results[i].job_status + '</td><td>' + active_check + '</td><td>' + results[i].created_by + '</td><td>' + results[i].updated_by + '</td></tr>')
                        }
                    } else {
                        if (filter_clicked == false) {
                            back_search();
                        } else {
                            back_search();
                            $.msgBox({
                                title: "Alert",
                                content: "Data not found",
                                type: "alert",
                            });
                        }

                    }
                },
                beforeSend: function() {

                },
                error: function() {}
            });
        }

          $(document).on('dblclick', '.double_click', function() {
            show_change_screen();
            var id = $(this).data("id");

            job_id = results[id].job_id;
            $('#job_name').val(results[id].job_name);
            //$('#task_type').val(results[id].task_type_id);
            $('#job_priority').val(results[id].job_priority);
            $('#created_by').val(results[id].created_by);
            $('#created_date').val(results[id].created_at);
            $('#modified_by').val(results[id].updated_by);
            $('#modified_date').val(results[id].updated_at);

            if (results[id].active == 0) {
                $('#active').prop('checked', false);
            } else {
                $('#active').prop('checked', true);
            }

            $('#variant_select').val(results[id].task_id);

            function get_variant(report_id){
            $.ajax({
                type: "POST",
                url: "get_variant_for_scheduler",
                data: {
                    "_token": "{{ csrf_token() }}",
                     "report_id": report_id,
                },
                success: function(data) {
                    var results = data;
                    console.log(JSON.stringify(data));
                    console.log("results",results);

                    $('#variant_select').html('');
                    if (results.length == 0) {
                        $("#job_priority").prop('disabled', true);
                        $('#variant_select').append("<option id =\"0\" value=\"0\">Task Doesn't exist</option>");
                    } else {
                        $("#job_priority").prop('disabled', false);
                        for (var i = 0; i < results.length; i++) {
                            if (i == '0') {
                                $('#variant_select').append("<option id =\"0\" value=\"0\">Select</option>");
                            }
                            var variant = results[i].variant
                            var report_id = results[i].report_id
                            $('#variant_select').append("<option  id =" + report_id + " value=" + report_id + ">" + variant + "</option>");
                        }
                    }
                },
                beforeSend: function() {},
                error: function() {}
            });
            }
            $('#start_conditions').val(results[id].start_type_id);

            if (results[id].start_type_id == 1) {
                $('#immediate').hide();
                $('#date_time_div').hide();
                $('#after_job_div').hide();

                if (results[id].periodic == 1) {
                    $('#period_immediately').prop('checked', true);
                    $.ajax({
                        type: "POST",
                        url: "get_frequency_for_scheduler_report",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "job_id": results[id].job_id,
                        },
                        success: function(data) {
                            var results = data;
                            $('#frequency').val(results[0].frequency_id);
                        },
                        beforeSend: function() {},
                        error: function() {}
                    });
                }

            } else if (results[id].start_type_id == 2) {
                $('#immediate').hide();
                $('#date_time_div').show();
                $('#after_job_div').hide();
                $("#start_date").val(results[id].job_start_date);
                $("#start_time").val(results[id].job_start_time);
                $("#end_date").val(results[id].job_end_date);
                $("#end_time").val(results[id].job_end_time);

                if (results[id].periodic == 1) {
                    $('#periodic_date_time').prop('checked', true);
                    $.ajax({
                        type: "POST",
                        url: "get_frequency_for_scheduler_report",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "job_id": results[id].job_id,
                        },
                        success: function(data) {
                            var results = data;
                            console.log("results[0].frequency_id",results[0].frequency_id);
                            $('#frequency_date_time').val(results[0].frequency_id);
                        },
                        beforeSend: function() {},
                        error: function() {}
                    });
                }

            } else if (results[id].start_type_id == 3) {

                $('#immediate').hide();
                $('#date_time_div').hide();
                $('#after_job_div').show();

                $.ajax({
                    type: "POST",
                    url: "get_after_job_for_scheduler_report",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "job_id": results[id].job_id,
                    },
                    success: function(data) {
                        var results = data;
                        $('#predecessor_job_name').val(results[0].predecessor_job_id);
                    },
                    beforeSend: function() {},
                    error: function() {}
                });

            }
            var ds = 0;
            $.ajax({
                type: "POST",
                url: "task_ds_ref",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "task_id": job_id ,
                },

                success: function(data) {

                 console.log(data);
                  if (data != 0) {

                      $('#distribution_strategy').prop('checked', true);
                      $("#show_ds").show();

                  } else {

                    $('#distribution_strategy').prop('checked', false);
                      $("#show_ds").hide();
                  }
                },
                beforeSend: function() {

                },
                error: function() {

                }
            });



          });

        function show_change_screen() {
            $('#tab_one').show();
            $('#list_of_jobs').hide();
            $('#cancel_button').show();
            $('#update_button').show();
            $('#back_search').hide();
        }
    });

//distribution_strategy starts here
$(".ds_tab").dialog({
 autoOpen: false,
 modal: true,
 width:450,
 height:150
 });

 $('#show_ds').on('click',function(){
$('.ds_tab').dialog("open");
ds_data();
   //}
 });

 $(' #Cancel-ds').click(function(){
   $('#set-ds').dialog("close");
 });
    function ds_data(){
      var job_id_ref_ds = job_id;

          $.ajax({
              type: "POST",
              url: "ds_results",
              dataType: "json",
              data: {

                  "_token": "{{ csrf_token() }}",
                  "task_id_ref_ds": job_id_ref_ds,
              },

              success: function(data) {
                if (data != "0") {
                var results = data;
                ds_results = results;

                $('.ds_tab tr').remove();
                $('.ds_tab').append('<tr style="background-color: #D3D3D3;"><th class="ds_th">Task Status</th><th class="ds_th">communication_options</th><th class="ds_th">Mail Type</th><th class="ds_th">Mail Address</th></tr>');


                         for(var i=0; i< results.length; i++)
                       {
                            $('.ds_tab').append('<tr class = "double_click rows" data-id="' + i + '" id="' + results[i].distribution_strategy_id  + '" ><td>' + results[i].status_name + '</td><td>' + results[i].communication_option_name + '</td><td>' +results[i].mail_type + '</td><td>' +results[i].mail_id_address +
                              '</td></tr>');
                        }

                }
                else{
                  ds_data="NO";
                  $('.ds_tab').dialog("close");
                }
                 },
                    beforeSend: function() {

                        },
                    error: function() {

                         }
               });

          }



//distribution_strategy ends here

</script>
</div>
@stop

@section('lower_band')
<div class="col-xs-12 lower_band">
   <button id="cancel_button" class="headerbuttons load" type="button" style="float: left; margin-right: 20px;">Back</button>
   <!-- <button id="update_button" class="headerbuttons load" type="button"  style="float: right; margin-right: 20px;">Update</button> -->
   <button id="search_button" class="headerbuttons" type="button"  style="float: left; margin-right: 20px;">Search</button>
   <button id="back_search" class="headerbuttons load" type="button"  style="float: left; margin-right: 20px;">Back</button>
</div>
@stop
