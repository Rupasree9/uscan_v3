@extends('layouts.uscan_master_page')
@section('header')
   <!-- <link rel="stylesheet" href="css/screen.css"> -->
   <link rel="stylesheet" type="text/css" href="css/product_administration.css">
@stop

@section('upper_band')
      <div class="col-xs-12 upper_band">
          <div class="col-xs-4 display-title">
               Administration
          </div>
           <div class="col-xs-2" style="float:right;text-align:center;line-height:30px;">
             <!-- <span  id="show_all" style="">Show All Portals</span> -->
           </div>
      </div>
@stop

@section('content')
 <div class="col-xs-12 content">
     <div class="div1">
        <table style="width:100%;" class="tab">
        </table>
      </div>

      <div class="" id ="product_admin_list">
<pre>
This product is developped to facilitate one, can generate reports across all the plants and customers.
It enables the one to easily identify the risly plants for the future and the current.
</pre>


           <ul class="">
                 <li id="product_record"><a>About the Product Record</a></li>
                 <li id ="simple_products"><a>Process of Creating Simple Products</a></li>
                 <li id="related_products"><a>Defining Related Products</a></li>
                 <li id="equivalent_products"><a>Defining Equivalent Products</a></li>
                 <li id="products_literature"><a>Associating Literature with Products</a></li>
                 <li id="products_news"><a>Associating Product News with Products</a></li>
                 <li id="exp_imp_products"><a>Exporting and Importing Products</a></li>
                 <li id="managing_products"><a>About Managing Product Records</a></li>
                 <li id="services_licencing"><a>Product Services and licencing</a></li>
           </ul>
</div>

<div class="" id ="product_record_details" style="display:none;">
  <h5>About the Product Record</h5>
  <pre style="background-color:white;">
            This will contain the fields in the product entities,it meaning and where all it will be connected.
            For example Master Data Master>>Plant Master>>customer_create
            We have field name "Plant Name" is the name of the enterprise plant.
                                "DUNS No"   is the plant dun's number
  </pre>
</div>

<div class="" id ="simple_products_detalis" style="display:none;">
    <h5>Process of Creating Simple Products</h5>
  <pre style="background-color:white;">
            This will contain the information about how to create a simple product
            without any varients of solution.
  </pre>
</div>

<div class="" id ="related_products_details" style="display:none;">
  <h5>Defining Related Products</h5>
  <pre style="background-color:white;">
            This will contain the information about how to create a product
            which is related to existing solution.
  </pre>
</div>

<div class="" id ="equivalent_products_details" style="display:none;">
    <h5>Defining Equivalent Products</h5>
  <pre style="background-color:white;">
            This will contain the information about how to create a
            equivalent product which is already existing in the solution.
  </pre>
</div>


<div class="" id ="exp_imp_products_details" style="display:none;">
    <h5>Exporting and Importing Products</h5>
  <pre style="background-color:white;">
            This will contain the information about how to import and export products.
  </pre>
</div>

<div class="" id ="products_literature_details" style="display:none;">
    <h5>Associating Literature with Products</h5>
  <pre style="background-color:white;">
    This will contain the information about how to associate literature with products so salespeople can use this literature to sell the products.
    Product literature is associated with the product as an attachment,
    so it can be used for such things as product brochures, competitive analyses, and image files.
  </pre>
</div>

<div class="" id ="products_news_details" style="display:none;">
    <h5>Associating Product News with Products</h5>
  <pre style="background-color:white;">
    Product news is information about a product that is displayed in eService and eSales as inline text associated with the product.
    Product news is not the same as product literature.
  </pre>
</div>

<div class="" id ="managing_products_details" style="display:none;">
    <h5>About Managing Product Records</h5>
  <pre style="background-color:white;">
    This will contain the information about how to manage product records:
    Editing Product Records
    Copying Product Records
    Deleting Product Records
    Exporting Product Records for Display
  </pre>
</div>
<div class="" id ="services_licencing_details" style="display:none;">
    <h5>Product Services and licencing</h5>
  <pre style="background-color:white;">
  This will contain the information about services available in the solution and
  the licencing.
  </pre>
</div>
<script type="text/javascript">

$(document).ready(function(){

               $(document).on('click', '#product_record', function() {
                  $("#product_record_details").show();
                  $("#product_admin_list").hide();
                  $("#back").show();
                });
                $(document).on('click', '#simple_products', function() {
                   $("#simple_products_detalis").show();
                   $("#product_admin_list").hide();
                   $("#back").show();
                 });

                 $(document).on('click', '#related_products', function() {
                    $("#related_products_details").show();
                    $("#product_admin_list").hide();
                    $("#back").show();
                  });

                  $(document).on('click', '#equivalent_products', function() {
                     $("#equivalent_products_details").show();
                     $("#product_admin_list").hide();
                     $("#back").show();
                   });

                  $(document).on('click', '#exp_imp_products', function() {
                    $("#exp_imp_products_details").show();
                    $("#product_admin_list").hide();
                    $("#back").show();
                  });

                  $(document).on('click', '#products_literature', function() {
                    $("#products_literature_details").show();
                    $("#product_admin_list").hide();
                    $("#back").show();
                  });

                  $(document).on('click', '#products_news', function() {
                    $("#products_news_details").show();
                    $("#product_admin_list").hide();
                    $("#back").show();
                  });
                  $(document).on('click', '#managing_products', function() {
                    $("#managing_products_details").show();
                    $("#product_admin_list").hide();
                    $("#back").show();
                  });

                  $(document).on('click', '#services_licencing', function() {
                    $("#services_licencing_details").show();
                    $("#product_admin_list").hide();
                    $("#back").show();
                  });

                $(document).on('click', '#back', function() {
                   $("#product_record_details,#simple_products_detalis,#related_products_details,#equivalent_products_details,#exp_imp_products_details,#products_literature_details,#products_news_details,#managing_products_details,#services_licencing_details").hide();
                   $("#product_admin_list").show();
                });

           });
                $(window).load(function() {
                    $("#administration").trigger('click');
                });


</script>
</div>

@stop
@section('lower_band')
<div class="col-xs-12 lower_band">
  <div  id="back_buttons">
      <button type = "button" style = "display:none" id="back">Back</button>
</div>
</div>
@stop
