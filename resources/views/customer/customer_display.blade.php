@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" type="text/css" href="css/customer_change.css">
@stop
@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="col-xs-4 display-title">
      <span>Display Customer Master</span>
   </div>
   <div class="col-xs-2" id = "show_all_customer" style="float: right;text-align:center;line-height:30px;">
      <span id="show_all_c">Show All Customers</span>
   </div>
   <div class="col-xs-3" id = "show_all_parents" style="float: right;text-align:center;line-height:30px;width:20%">
      <span id="show_all_p">Show All Parent Companies</span>
   </div>
</div>
@stop
@section('content')
<div class="col-xs-12 content">
  <div class="loading">
     <img src="images/loading.gif" height="50px;">
  </div>
   <div class="customer_type">
      <table id = "type" style="width:40%;">
         <tr>
            <td class="details_color">Search Parameters</td>
         </tr>
         <tr>
            <td>Type</td>
            <td>
               <select id="parent-child">
               </select>
            </td>
         </tr>
         <tr>
            <td></td>
         </tr>
      </table>
      <script type="text/javascript">
         $(document).ready(function() {
                 customer_master_type();
           function customer_master_type() {

                                        $('parent-child').empty();
                                        $.ajax({

                                            type: "GET",
                                            url: "customer_master_type",
                                            dataType: "json",
                                            success: function(data) {

                                                $('.loading').css('display', 'none');
                                                var results = data;
                                                console.log(JSON.stringify(data));

                                                for (var i = 0; i < results.length; i++) {
                                                    if (i == '0') {

                                                        $('#parent-child').append("<option  value = \"Select\" >Select</option>");

                                                    }

                                                    var customer_master_type = results[i].customer_master_type;
                                                    var customer_master_type_id = results[i].customer_master_type_id;
                                                    $('#parent-child').append("<option  value=" + customer_master_type_id + ">" + customer_master_type + "</option>");
                                                }

                                            },
                                            beforeSend: function() {
                                                $('.loading').css('display', 'block');
                                            },
                                            error: function() {

                                            }
                                        });

                                                     }

         $('#parent-child').on('change', function() {

                       if (this.value == "1") {

                           $("#parent_change,.parent_bottom_buttons,#parent_search_button,#show_all_parents").addClass("show");
                           $("#child_change,.child_bottom_buttons,#child_search_button,#show_all_customer").removeClass("show");
                           $("#child_change").addClass("hide");
                           $("#parent_change").removeClass("hide");

                       } else if (this.value == "2") {

                           $("#parent_change").addClass("hide");
                           $("#child_change").removeClass("hide");
                           $("#child_change,.child_bottom_buttons,#search_button,#show_all_customer").addClass("show");
                           $("#parent_change,.parent_bottom_buttons,#parent_search_button,#show_all_parents").removeClass("show");

                       } else {

                           $("#parent_change").removeClass("show");
                           $("#child_change").removeClass("show");
                       }

                   });
               });
      </script>
   </div>
   <div id="parent_change">
      <div id="search-para-parent">
         <form id="parent_search_form" autocomplete="off">
            <table style="width:40%;">
               <tr>
                  <td style="width: 139px;">Parent Company Name</td>
                  <td><input type="text" name="search_parent_name" id="search_parent_name_id"></td>
               </tr>
               <tr>
                  <td>Parent Company Code</td>
                  <td><input type="text" name="search_parent_code" id="search_parent_code_id"></td>
               </tr>
            </table>
         </form>
      </div>
      <div id="parent_table">
         <ul class="nav nav-tabs">
            <li  class="active"><a id ="details_parent" data-toggle="tab" href="#tab_one_parent">Details</a></li>
            <li><a data-toggle="tab" href="#tab_two_parent">Additional Details</a></li>
         </ul>
         <div class="tab-content">
            <div id="tab_one_parent" class="tab-pane fade in active">
               <form id = "edit_parent_screen" autocomplete="off">
                  <table style="width:40%;">
                     <tr>
                        <td>Parent Company Name</td>
                        <td><input type="text" name="search_parent_name" id="search_parent_name_id_display" disabled></td>
                     </tr>
                     <tr>
                        <td>Parent Company Code</td>
                        <td><input type="text" name="search_parent_code" id="search_parent_code_id_display" disabled></td>
                     </tr>
                  </table>
            </div>
            <div id="tab_two_parent" class="tab-pane fade">
            <table table style="width:40%; margin-bottom: 20px">
            <tr>
            <td></td>
            </tr>
            <tr>
            <td>Created By</td>
            <td><input id ="created_by_p" type="text" name="" disabled></td>
            </tr>
            <tr>
            <td>Created Date</td>
            <td><input id ="created_date_p" type="text" disabled ></td>
            </tr>
            <tr>
            <td>Modified By</td>
            <td><input id = "modified_by_p" type="text" disabled ></td>
            </tr>
            <tr>
            <td>Modified Date</td>
            <td><input id = "modified_date_p" type="text" disabled ></td>
            </tr>
            </table>
            </form>
            </div>
         </div>
      </div>
      <script type="text/javascript">
         $(document).ready(function() {
             var temp_results, parent_id_ref;
             var parent_search, parent_show_all;


             parent_search = "FALSE";
             parent_show_all = "FALSE";
             update_click = "FALSE";


             function showall_p() {


                 $.ajax({

                     type: "GET",
                     url: "parent_results",
                     dataType: "json",
                     success: function(data) {
                         $('.loading').css('display', 'none');
                         var results = data;
                         temp_results = data;
                         $('.parent_tab').append('<tr style="background-color: #D3D3D3;cursor:text;"><th>Parent Company Name</th><th>Parent Company Code</th><th>Created By</th><th>Modified By</th><th>Created Date</th><th>Modified Date</th></tr>');

                         if (results.length > 12) {

                             $('#list-of-parent').addClass("scroll");

                         } else {
                             $('#list-of-parent').removeClass("scroll");
                         }

                         for (var i = 0; i < results.length; i++) {
                             $('.parent_tab').append('<tr class = "double_click_parent" id = ' + i + '><td>' + results[i].parent_name + '</td><td>' + results[i].parent_code + '</td><td>' + results[i].created_by + '</td><td>' + results[i].updated_by +
                                 '</td><td>' + results[i].created_at + '</td><td>' + results[i].updated_at + '</td></td></tr>');
                         }

                     },
                     beforeSend: function() {

                         $('.loading').css('display', 'block');
                         $('.parent_tab tr').remove();
                         $("#list-of-parent,.parent_bottom_buttons,#parent_back_buttons").addClass("show");
                         $(".customer_type").addClass("hide");
                         $("#parent_change,#parent_search_button,#show_all_parents,#child_change,.child_bottom_buttons,#search_button,#show_all_customer").removeClass("show");
                     },
                     error: function() {

                         $.msgBox({
                             title: "Error",
                             content: "Something went wrong",
                             type: "error",
                         });

                     }
                 });

             }

             function display_parent_data() {

                 var search_parent_name_id = $('#search_parent_name_id').val();
                 var search_parent_code_id = $('#search_parent_code_id').val();

                 if ((search_parent_name_id == "") && (search_parent_code_id == "")) {

                     $.msgBox({
                         title: "Alert",
                         content: "Search Criteria Not Available",
                         type: "alert",
                     });

                 } else {


                     $.ajax({
                         type: "POST",
                         url: "parent_search",
                         dataType: "json",
                         data: {
                             "_token": "{{ csrf_token() }}",
                             "search_parent_name_id": search_parent_name_id,
                             "search_parent_code_id": search_parent_code_id,
                         },

                         success: function(data) {
                             $('.loading').css('display', 'none');
                             var results = data;
                             temp_results = data;
                             if (data != "0") {

                                 $("#list-of-parent,.parent_bottom_buttons,#parent_back_buttons").addClass("show");
                                 $(".customer_type").addClass("hide");
                                 $("#parent_change,#parent_search_button,#show_all_parents,#child_change,.child_bottom_buttons,#search_button,#show_all_customer").removeClass("show");


                                 $('.parent_tab').append('<tr style="background-color: #D3D3D3;cursor:text;"><th>Parent Company Name</th><th>Parent Company Code</th><th>Created By</th><th>Modified By</th><th>Created Date</th><th>Modified Date</th></tr>');

                                 for (var i = 0; i < results.length; i++) {
                                     $('.parent_tab').append('<tr class = "double_click_parent" id = ' + i + '><td>' + results[i].parent_name + '</td><td>' + results[i].parent_code + '</td><td>' + results[i].created_by + '</td><td>' + results[i].updated_by +
                                         '</td><td>' + results[i].created_at + '</td><td>' + results[i].updated_at + '</td></tr>');
                                 }
                             } else {


                                 if (update_click == "FALSE") {

                                     $.msgBox({
                                         title: "Alert",
                                         content: "Result Not available",
                                         type: "alert",
                                     });
                                     update_click = "TRUE";

                                 }


                                 $("#list-of-parent,#parent_back_buttons").removeClass("show");
                                 $(".customer_type").removeClass("hide");
                                 $("#parent_change,#parent_search_button,#show_all_parents").addClass("show");


                             }


                         },
                         beforeSend: function() {
                             $('.loading').css('display', 'block');
                             $('.parent_tab tr').remove();

                         },
                         error: function() {

                             $.msgBox({
                                 title: "Error",
                                 content: "Something went wrong",
                                 type: "error",
                             });

                         }
                     });

                 }
             }

             $(document).on('dblclick', '.double_click_parent', function() {

                 var id = $(this).attr('id');
                 parent_id_ref = temp_results[id].parent_id;
                 $('#search_parent_name_id_display').val(temp_results[id].parent_name);
                 $('#search_parent_code_id_display').val(temp_results[id].parent_code);
                 $('#created_by_p').val(temp_results[id].created_by);
                 $('#created_date_p').val(temp_results[id].created_at);
                 $('#modified_by_p').val(temp_results[id].updated_by);
                 $('#modified_date_p').val(temp_results[id].updated_at);
                 $("#list-of-parent,#parent_back_buttons").removeClass("show");
                 $("#parent_update_button,#parent_table,#parent_change").addClass("show");
                 $("#search-para-parent").addClass("hide");
                 $("#details_parent").click();


             });


             $(document).on('click', '#show_all_p', function() {

                 parent_search = "FALSE";
                 parent_show_all = "TRUE";
                 showall_p();

             });

             $(document).on('click', '#parent_back', function() {

                 $("#list-of-parent,#parent_back_buttons").removeClass("show");
                 $(".customer_type").removeClass("hide");
                 $("#parent_change,#show_all_parents,#parent_search_button").addClass("show");
                 $('#parent_search_form')[0].reset();

             });

             $(document).on('click', '#parent_filter', function() {
                 update_click = "FALSE";
                 parent_search = "TRUE";
                 parent_show_all = "FALSE";
                 display_parent_data();
             });

             $(document).on('click', '#parent_cancel', function() {

                 $("#list-of-parent,#parent_back_buttons").addClass("show");
                 $("#parent_update_button,#parent_table,#parent_change").removeClass("show");
                 $("#search-para-parent").removeClass("hide");

             });

         });

         $(window).load(function() {

             $("#master_data").trigger('click');
             $("#customer_master").trigger('click');

             var fetch_search_parent_name_id_display;
             $('input[name="search_parent_name"]').autoComplete({
                 minChars: 1,
                 source: function(term, response) {
                     try {
                         fetch_search_parent_name_id_display.abort();
                     } catch (e) {}
                     fetch_search_parent_name_id_display = $.getJSON('customer_child_controller/autocomplete_parent_name', {
                         search_parent_company_name_id: term
                     }, function(data) {
                         response(data);
                     });
                 }
             });

             var fetch_search_parent_code_id_display;
             $('input[name="search_parent_code"]').autoComplete({
                 minChars: 1,
                 source: function(term, response) {
                     try {
                         fetch_search_parent_code_id_display.abort();
                     } catch (e) {}
                     fetch_search_parent_code_id_display = $.getJSON('customer_child_controller/autocomplete_parent_code', {
                         search_parent_company_code_id: term
                     }, function(data) {
                         response(data);
                     });
                 }
             });

         });
      </script>
   </div>
   <div id="list-of-parent">
      <table style="width:100%;" class="parent_tab">
      </table>
   </div>
   <div id="child_change">
      <div id="search-para">
         <form autocomplete="off">
            <table style="width:40%;">
               <tr>
                  <td>Parent Company Name</td>
                  <td><input type="text" name="search_parent_company_name" id="search_parent_company_name_id"></td>
               </tr>
               <tr>
                  <td>Customer Name</td>
                  <td><input type="text" name="search_customer_name_id" id="search_customer_name_id"></td>
               </tr>
               <tr>
                  <td>Customer Code</td>
                  <td><input type="text" name="search_customer_code" id="search_customer_code_id"></td>
               </tr>
               <tr>
                  <td>DUNS No</td>
                  <td><input type="text" name="search_duns_number" id="search_duns_number_id"></td>
               </tr>
            </table>
         </form>
      </div>
      <div id="child-table">
         <ul class="nav nav-tabs">
            <li   class="active"><a id = "details_customer" data-toggle="tab" href="#tab_one">Details</a></li>
            <li id = "additional_customer" ><a data-toggle="tab" href="#tab_two">Additional Details</a></li>
         </ul>
         <div class="tab-content">
            <div id="tab_one" class="tab-pane fade in active">
               <form id="child-table-form" autocomplete="off">
                  <table style="width:80%; margin-bottom: 20px">
                     <tr>
                        <td></td>
                     </tr>
                     <tr>
                        <td>Parent Company Name</td>
                        <td><input type="text" id="parent_company_name_id" disabled></td>
                        <td>Primary Contact Name </td>
                        <td><input type="text" id="p_contact_name" disabled></td>
                     </tr>
                     <tr>
                        <td>Customer Name</td>
                        <td><input type="text"  id="customer_name" disabled></td>
                        <td>Primary Contact Mail ID</td>
                        <td><input type="email" id="p_contact_email" disabled ></td>
                     </tr>
                     <tr>
                        <td>Customer Code</td>
                        <td><input type="text"  id="customer_code" disabled></td>
                        <td>Primary Contact Number</td>
                        <td><input type="text" id="p_contact_number" disabled></td>
                     </tr>
                     <tr>
                        <td>DUNS No</td>
                        <td><input type="text" id="dun_no" disabled></td>
                        <td>Secondary Contact Name </td>
                        <td><input type="text" id="s_contact_name" disabled></td>
                     </tr>
                     <tr>
                        <td>Customer Type</td>
                        <td><input type="text" id="customer_type" disabled></td>
                        <td>Secondary Contact Mail ID</td>
                        <td><input type="email" id="s_contact_email" disabled></td>
                     </tr>
                     <tr>
                        <td>Active</td>
                        <td><input type="checkbox" id="customer_active" disabled></td>
                        <td>Secondary Contact Number</td>
                        <td><input type="text" id="s_contact_number" disabled></td>
                     </tr>
                  </table>
            </div>
            <div id="tab_two" class="tab-pane fade">
            <table table style="width:40%; margin-bottom: 20px">
            <tr>
            <td></td>
            </tr>
            <tr>
            <td>Created By</td>
            <td><input id ="created_by" type="text" name="" disabled></td>
            </tr>
            <tr>
            <td>Created Date</td>
            <td><input id ="created_date" type="text" disabled></td>
            </tr>
            <tr>
            <td>Modified By</td>
            <td><input id = "modified_by" type="text" disabled></td>
            </tr>
            <tr>
            <td>Modified Date</td>
            <td><input id = "modified_date" type="text" disabled></td>
            </tr>
            </table>
            </form>
            </div>
         </div>
         <script type="text/javascript">
            $(document).ready(function() {


              var customer_results,no_results,search_click,show_all_click,show_no_results;
              search_click = "NO";
              show_all_click = "YES";
              show_no_results = "TRUE";


              function parent_company_name() {

                                 $.ajax({

                                     type: "GET",
                                     url: "parent_results",
                                     dataType: "json",
                                     success: function(data) {

                                         $('.loading').css('display', 'none');
                                         var results = data;


                                         for (var i = 0; i < results.length; i++) {
                                             if (i == '0') {

                                                 $('#parent_company_name_id').append("<option id =\"null\" value=\"\">Select</option>");

                                             }

                                             var parent_company_name = results[i].parent_name
                                             var parent_id = results[i].parent_id
                                             $('#parent_company_name_id').append("<option id =" + parent_id + " value=" + parent_id + ">" + parent_company_name + "</option>");
                                         }

                                     },
                                     beforeSend: function() {
                                          $('.loading').css('display', 'block');
                                     },
                                     error: function() {

                                       $.msgBox({
                                           title: "Alert",
                                           content: "Something went wrong",
                                           type: "alert",
                                       });

                                     }
                                 });

                             }

              function customer_type() {

                                 $('#customer_type').empty();
                                 $.ajax({

                                     type: "GET",
                                     url: "customer_type",
                                     dataType: "json",
                                     success: function(data) {


                                         var results = data;


                                         for (var i = 0; i < results.length; i++) {
                                             if (i == '0') {

                                                 $('#customer_type').append("<option id =\"null\" value=\"\">Select</option>");

                                             }

                                             var customer_type_id = results[i].customer_type_id;
                                             var customer_type_name = results[i].customer_type;
                                             $('#customer_type').append("<option id =" + customer_type_id + " value=" + customer_type_id + ">" + customer_type_name + "</option>");
                                         }

                                     },
                                     beforeSend: function() {

                                     },
                                     error: function() {

                                     }
                                 });

                             }

              parent_company_name();

              customer_type();

              function showall_customer() {
                                $.ajax({
                                    type: "GET",
                                    url: "customer_results",
                                    dataType: "json",
                                    success: function(data) {

                                        $('.loading').css('display', 'none');
                                        var results = data;
                                        customer_results = data;

                                        $('.tab tr').remove();
                                        $('.tab').append('<tr style="background-color: #D3D3D3;"><th>Parent Company Name</th><th>Customer Name</th><th>Customer Code</th><th>DUNS No</th><th>Active</th><th>Created By</th><th>Modified By</th><th>Created Date</th><th>Modified Date</th></tr>');

                                        if (results.length > 12)
                                        {

                                          $('#list-of-cust').addClass("scroll");

                                        }
                                        else {
                                           $('#list-of-cust').removeClass("scroll");
                                        }


                                        for (var i = 0; i < results.length; i++) {
                                            if (results[i].active == 0) {
                                                active_check = '<input type="checkbox" disabled readonly>';
                                            } else {
                                                active_check = '<input type="checkbox" checked disabled readonly>';
                                            }
                                            $('.tab').append('<tr class = "double_click rows" data-id="' + i + '" id="' + results[i].customer_id + '" ><td>' + results[i].parent_name + '</td><td>' + results[i].customer_name + '</td><td>' + results[i].customer_code + '</td><td>' + results[i].duns_no + '</td><td>' + active_check + '</td><td>' + results[i].created_by + '</td><td>' + results[i].updated_by +
                                                '</td><td>' + results[i].created_at + '</td><td>' + results[i].updated_at + '</td></tr>');
                                        }



                                    },
                                    beforeSend: function() {
                                      $('.loading').css('display', 'block');
                                      $( "#list-of-cust,#back_buttons,#back" ).addClass( "show" );
                                      $( ".customer_type,#search-para" ).addClass( "hide" );
                                      $( "#show_all_customer,#search_button" ).removeClass( "show" );



                                    },
                                    error: function() {

                                        $.msgBox({
                                            title: "Alert",
                                            content: "Something went wrong",
                                            type: "alert",
                                        });



                                    }
                                });
                            }

              function display_customer_data() {

               var search_parent_company_name_id = $('#search_parent_company_name_id').val();
               var search_customer_name_id = $('#search_customer_name_id').val();
               var search_customer_code_id = $('#search_customer_code_id').val();
               var search_duns_number_id = $('#search_duns_number_id').val();


               if ((search_parent_company_name_id == "") && (search_customer_name_id == "") && (search_customer_code_id == "") && (search_duns_number_id == "")) {

                 $.msgBox({
                     title: "Alert",
                     content: "Search Criteria Not Available",
                     type: "alert",
                 });

               } else {

                   $.ajax({
                       type: "POST",
                       url: "customer_search",
                       dataType: "json",
                       data: {
                           "_token": "{{ csrf_token() }}",
                           "search_parent_company_name_id": search_parent_company_name_id,
                           "search_customer_name_id": search_customer_name_id,
                           "search_customer_code_id": search_customer_code_id,
                           "search_duns_number_id": search_duns_number_id,
                       },

                       success: function(data) {

                           $('.loading').css('display', 'none');
                           if (data != "0") {

                             $( "#list-of-cust,#back_buttons,#back" ).addClass( "show" );
                             $( ".customer_type,#search-para" ).addClass( "hide" );
                             $( "#show_all_customer,#search_button" ).removeClass( "show" );


                             var results = data;
                             customer_results = data;


                             console.log(JSON.stringify(results));
                             $('.tab tr').remove();
                             $('.tab').append('<tr style="background-color: #D3D3D3;"><th>Parent Company Name</th><th>Customer Name</th><th>Customer Code</th><th>DUNS No</th><th>Active</th><th>Created By</th><th>Modified By</th><th>Created Date</th><th>Modified Date</th></tr>');



                             for (var i = 0; i < results.length; i++) {
                                 if (results[i].active == 0) {
                                     active_check = '<input type="checkbox" disabled readonly>';
                                 } else {
                                     active_check = '<input type="checkbox" checked disabled readonly>';
                                 }
                                 $('.tab').append('<tr class = "double_click rows" data-id="' + i + '" id="' + results[i].customer_id + '" ><td>' + results[i].parent_name + '</td><td>' + results[i].customer_name + '</td><td>' + results[i].customer_code + '</td><td>' + results[i].duns_no + '</td><td>' + active_check + '</td><td>' + results[i].created_by + '</td><td>' + results[i].updated_by +
                                     '</td><td>' + results[i].created_at + '</td><td>' + results[i].updated_at + '</td></tr>');
                             }



                           }
                           else {

                               if (show_no_results == "TRUE") {

                               $.msgBox({
                                   title: "Alert",
                                   content: "Result Not available",
                                   type: "alert",
                               });

                               $( "#list-of-cust,#back_buttons,#back" ).removeClass( "show" );
                               $( ".customer_type,#search-para" ).removeClass( "hide" );
                               $( "#show_all_customer,#search_button" ).addClass( "show" );


                             }
                             else {

                               $( "#list-of-cust,#back_buttons,#back" ).removeClass( "show" );
                               $( ".customer_type,#search-para" ).removeClass( "hide" );
                               $( "#show_all_customer,#search_button" ).addClass( "show" );

                             }

                           }

                       },
                       beforeSend: function() {
                          $('.tab tr').remove();
                          $('.loading').css('display', 'block');
                        },
                       error: function() {

                         $.msgBox({
                             title: "Error",
                             content: "Something went wrong",
                             type: "error",
                         });
                       }
                   });
               };

               }

              $(document).on('click', '#show_all_c', function() {
                            search_click = "NO";
                            show_all_click = "YES";
                            showall_customer();
                          });

              $(document).on('click', '#back', function() {

                            $('.tab tr').remove();
                            $( "#list-of-cust,#back_buttons,#back" ).removeClass( "show" );
                            $( ".customer_type,#search-para" ).removeClass( "hide" );
                            $( "#show_all_customer,#search_button" ).addClass( "show" );

              });


              $(document).on('click', '#filter', function() {

                               search_click = "YES";
                               show_all_click = "NO";
                               show_no_results = "TRUE";
                               display_customer_data();

                            });

              $(document).on('click', '#cancel', function() {

                  $( "#child-table,#update_button" ).removeClass( "show" );
                  $( "#back,#list-of-cust" ).addClass( "show" );

               });


             $(document).on('dblclick', '.double_click', function() {

                     var id = $(this).data('id');
                     $( "#child-table,#update_button" ).addClass( "show" );
                     $( "#back,#list-of-cust" ).removeClass( "show" );
                     $("#details_customer").click();
                     customer_id_ref = customer_results[id].customer_id;
                     $('#parent_company_name_id').val(customer_results[id].parent_name);
                     $('#customer_name').val(customer_results[id].customer_name);
                     $('#customer_code').val(customer_results[id].customer_code);
                     $('#dun_no').val(customer_results[id].duns_no);
                     $('#customer_type').val(customer_results[id].customer_type);
                     $('#p_contact_name').val(customer_results[id].primary_contact_name);
                     $('#p_contact_email').val(customer_results[id].primary_contact_email_id);
                     $('#p_contact_number').val(customer_results[id].primary_contact_number);
                     $('#s_contact_name').val(customer_results[id].secondary_contact_name);
                     $('#s_contact_email').val(customer_results[id].secondary_contact_email_id);
                     $('#s_contact_number').val(customer_results[id].secondary_contact_number);
                     $('#created_by').val(customer_results[id].created_by);
                     $('#created_date').val(customer_results[id].created_at);
                     $('#modified_by').val(customer_results[id].updated_by);
                     $('#modified_date').val(customer_results[id].updated_at);

                     if (customer_results[id].active == 0) {
                         $('#customer_active').prop('checked', false);
                     } else {
                         $('#customer_active').prop('checked', true);
                     }

             });

            });

            $(window).load(function() {

                            $("#master_data").trigger('click');
                            $("#customer_master").trigger('click');

                            var fetch_customer_name;
                            $('input[name="search_customer_name_id"]').autoComplete({
                                minChars: 1,
                                source: function(term, response) {
                                    try {
                                        fetch_customer_name.abort();
                                    } catch (e) {}
                                    fetch_customer_name = $.getJSON('customer_child_controller/autocomplete_customer_name', {
                                        search_customer_name_id: term
                                    }, function(data) {
                                        response(data);
                                    });
                                }
                            });

                            var fetch_customer_code;
                            $('input[name="search_customer_code"]').autoComplete({
                                minChars: 1,
                                source: function(term, response) {
                                    try {
                                        fetch_customer_code.abort();
                                    } catch (e) {}
                                    fetch_customer_code = $.getJSON('customer_child_controller/autocomplete_customer_code', {
                                        search_customer_code_id: term
                                    }, function(data) {
                                        response(data);
                                    });
                                }
                            });

                            var fetch_duns_number;
                            $('input[name="search_duns_number"]').autoComplete({
                                minChars: 1,
                                source: function(term, response) {
                                    try {
                                        fetch_duns_number.abort();
                                    } catch (e) {}
                                    fetch_duns_number = $.getJSON('customer_child_controller/autocomplete_duns_no', {
                                        search_duns_number_id: term
                                    }, function(data) {
                                        response(data);
                                    });
                                }
                            });

                            var fetch_search_parent_company_name;
                            $('input[name="search_parent_company_name"]').autoComplete({
                                minChars: 1,
                                source: function(term, response) {
                                    try {
                                        fetch_search_parent_company_name.abort();
                                    } catch (e) {}
                                    fetch_search_parent_company_name = $.getJSON('customer_child_controller/autocomplete_parent_name', {
                                        search_parent_company_name_id: term
                                    }, function(data) {
                                        response(data);
                                    });
                                }
                            });

                        });

         </script>
      </div>
      <div id="list-of-cust">
         <table style="width:100%;" class="tab">
         </table>
      </div>
   </div>
</div>
@stop
@section('lower_band')
<div class="col-xs-12 lower_band">
   <div class="parent_bottom_buttons">
      <div  id="parent_search_button">
         <button class="headerbuttons" id="parent_filter" type="button">Search</button>
      </div>
      <div  id="parent_back_buttons">
         <button class="headerbuttons" id="parent_back" type="button">Back</button>
      </div>
      <div id="parent_update_button">
         <button class="headerbuttons" id="parent_cancel" type="button">BACK</button>
      </div>
   </div>
   <div class="child_bottom_buttons">
      <div  id="search_button">
         <button class="headerbuttons" id="filter" type="button">Search</button>
      </div>
      <div  id="back_buttons">
         <button class="headerbuttons" id="back" type="button">Back</button>
      </div>
      <div id="update_button">
         <button class="headerbuttons" id="cancel" type="button">Back</button>
      </div>
   </div>
</div>
@stop
