@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" type="text/css" href="css/customer_change.css">
<script src="js/customer_validation.js"></script>
@stop
@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="col-xs-4 display-title">
      <span>Change Customer Master</span>
   </div>
   <div class="col-xs-2" id = "show_all_customer" style="float: right;text-align:center;line-height:30px;">
      <span id="show_all_c">Show All Customers</span>
   </div>
   <div class="col-xs-3" id = "show_all_parents" style="float: right;text-align:center;line-height:30px;width:20%;">
      <span id="show_all_p">Show All Parent Companies</span>
   </div>
</div>
@stop
@section('content')
<div class="col-xs-12 content">
  <div class="loading">
     <img src="images/loading.gif" height="50px;">
  </div>
   <div class="customer_type">
      <table id = "type" style="width:40%;">
         <tr>
            <td class="details_color">Search Parameters</td>
         </tr>
         <tr>
            <td>Type</td>
            <td>
               <select id="parent-child">
               </select>
            </td>
         </tr>
         <tr>
            <td></td>
         </tr>
      </table>
       <script type="text/javascript">
          $(document).ready(function() {

            customer_master_type();

            function customer_master_type() {

                $('parent-child').empty();
                $.ajax({

                    type: "GET",
                    url: "customer_master_type",
                    dataType: "json",
                    success: function(data) {

                        $('.loading').css('display', 'none');
                        var results = data;
                        console.log(JSON.stringify(data));

                        for (var i = 0; i < results.length; i++) {
                            if (i == '0') {

                                $('#parent-child').append("<option  value = \"Select\" >Select</option>");

                            }

                            var customer_master_type = results[i].customer_master_type;
                            var customer_master_type_id = results[i].customer_master_type_id;
                            $('#parent-child').append("<option  value=" + customer_master_type_id + ">" + customer_master_type + "</option>");
                        }

                    },
                    beforeSend: function() {
                      $('.loading').css('display', 'block');
                    },
                    error: function() {

                    }
                });

            }

                 $('#parent-child').on('change', function() {

                     if (this.value == "1") {

                         $("#parent_change,.parent_bottom_buttons,#parent_search_button,#show_all_parents").addClass("show");
                         $("#child_change,.child_bottom_buttons,#child_search_button,#show_all_customer").removeClass("show");
                         $("#child_change").addClass("hide");
                         $("#parent_change").removeClass("hide");

                     } else if (this.value == "2") {

                         $("#parent_change").addClass("hide");
                         $("#child_change").removeClass("hide");
                         $("#child_change,.child_bottom_buttons,#search_button,#show_all_customer").addClass("show");
                         $("#parent_change,.parent_bottom_buttons,#parent_search_button,#show_all_parents").removeClass("show");

                     } else {

                         $("#parent_change").removeClass("show");
                         $("#child_change").removeClass("show");
                     }

                  });
            });
       </script>
   </div>
   <div id="parent_change">
      <div id="search-para-parent">
         <form id="parent_search_form" autocomplete="off">
            <table style="width:40%;">
               <tr>
                  <td style="width: 139px;">Parent Company Name</td>
                  <td><input type="text" name="search_parent_name" id="search_parent_name_id"></td>
               </tr>
               <tr>
                  <td>Parent Company Code</td>
                  <td><input type="text" name="search_parent_code" id="search_parent_code_id"></td>
               </tr>
            </table>
         </form>
      </div>
      <div id="parent_table">
         <ul class="nav nav-tabs">
            <li class="active"><a id ="details_parent" data-toggle="tab" href="#tab_one_parent">Details</a></li>
            <li ><a data-toggle="tab" href="#tab_two_parent">Additional Details</a></li>
         </ul>
         <div class="tab-content">
            <div id="tab_one_parent" class="tab-pane fade in active">
               <form id = "parent-table-form-change" autocomplete="off">
                  <table style="width:40%;">
                    <tr><td></td></tr>
                     <tr>
                        <td>Parent Company Name</td>
                        <td><input type="text"  data-id = "test"  name="parent_name" id="search_parent_name_id_display">
                          <input id = "customer_parent_name_token_id" name="_token" value="{{ csrf_token() }}" style="display:none">
                          <input id = "customer_parent_name_valid_id" name="customer_parent_name_valid" value=" " style="display:none">
                        </td>
                     </tr>
                     <tr>
                        <td>Parent Company Code</td>
                        <td><input type="text" name="parent_code" id="search_parent_code_id_display"></td>
                     </tr>
                  </table>
            </div>
            <div id="tab_two_parent" class="tab-pane fade">
            <table table style="width:40%; margin-bottom: 20px">
            <tr>
            <td></td>
            </tr>
            <tr>
            <td>Created By</td>
            <td><input id ="created_by_p" type="text" name="" disabled readonly></td>
            </tr>
            <tr>
            <td>Created Date</td>
            <td><input id ="created_date_p" type="text" disabled readonly></td>
            </tr>
            <tr>
            <td>Modified By</td>
            <td><input id = "modified_by_p" type="text" disabled readonly></td>
            </tr>
            <tr>
            <td>Modified Date</td>
            <td><input id = "modified_date_p" type="text" disabled readonly></td>
            </tr>
            </table>
            </form>
            </div>
         </div>
      </div>
      <script type="text/javascript">
                $(document).ready(function() {
              var temp_results, parent_id_ref;
              var parent_search, parent_show_all;


              parent_search = "FALSE";
              parent_show_all = "FALSE";
              update_click = "FALSE";

              function showall_p() {


                  $.ajax({

                      type: "GET",
                      url: "parent_results",
                      dataType: "json",
                      success: function(data) {

                          $('.loading').css('display', 'none');
                          var results = data;
                          temp_results = data;
                          $('.parent_tab').append('<tr style="background-color: #D3D3D3;cursor:text;"><th>Parent Company Name</th><th>Parent Company Code</th><th>Created By</th><th>Modified By</th><th>Created Date</th><th>Modified Date</th><th>Edit</th><th>Delete</th></tr>');

                          if (results.length > 12) {

                              $('#list-of-parent').addClass("scroll");

                          } else {
                              $('#list-of-parent').removeClass("scroll");
                          }

                          for (var i = 0; i < results.length; i++) {
                              $('.parent_tab').append('<tr class = "double_click_parent" id = ' + i + '><td>' + results[i].parent_name + '</td><td>' + results[i].parent_code + '</td><td>' + results[i].created_by + '</td><td>' + results[i].updated_by +
                                  '</td><td>' + results[i].created_at + '</td><td>' + results[i].updated_at + '</td><td><button id="' + i + '" type="button" class="edit_parent btn btn-default glyphicon glyphicon-edit edit_color" aria-label="Left Align"></button></td><td><button id="' + results[i].parent_id + '" type="button" class=" delete_parent btn btn-default delete_color" aria-label="Left Align"><span class="glyphicon glyphicon-remove delete_color" aria-hidden="true"></span></button></td></tr>');
                          }

                      },
                      beforeSend: function() {
                          $('.loading').css('display', 'block');
                          $('.parent_tab tr').remove();
                          $("#list-of-parent,.parent_bottom_buttons,#parent_back_buttons").addClass("show");
                          $(".customer_type").addClass("hide");
                          $("#parent_change,#parent_search_button,#show_all_parents,#child_change,.child_bottom_buttons,#search_button,#show_all_customer").removeClass("show");
                      },
                      error: function() {

                          $.msgBox({
                              title: "Error",
                              content: "Something went wrong",
                              type: "error",
                          });

                      }
                  });

              }

              function display_parent_data() {

                  var search_parent_name_id = $('#search_parent_name_id').val();
                  var search_parent_code_id = $('#search_parent_code_id').val();

                  if ((search_parent_name_id == "") && (search_parent_code_id == "")) {

                      $.msgBox({
                          title: "Alert",
                          content: "Search Criteria Not Available",
                          type: "alert",
                      });

                  } else {


                      $.ajax({
                          type: "POST",
                          url: "parent_search",
                          dataType: "json",
                          data: {
                              "_token": "{{ csrf_token() }}",
                              "search_parent_name_id": search_parent_name_id,
                              "search_parent_code_id": search_parent_code_id,
                          },

                          success: function(data) {

                              var results = data;
                              temp_results = data;
                              $('.loading').css('display', 'none');


                              if (data != "0") {

                                  $("#list-of-parent,.parent_bottom_buttons,#parent_back_buttons").addClass("show");
                                  $(".customer_type").addClass("hide");
                                  $("#parent_change,#parent_search_button,#show_all_parents,#child_change,.child_bottom_buttons,#search_button,#show_all_customer").removeClass("show");


                                  $('.parent_tab').append('<tr style="background-color: #D3D3D3;cursor:text;"><th>Parent Company Name</th><th>Parent Company Code</th><th>Created By</th><th>Modified By</th><th>Created Date</th><th>Modified Date</th><th>Edit</th><th>Delete</th></tr>');

                                  for (var i = 0; i < results.length; i++) {
                                      $('.parent_tab').append('<tr class = "double_click_parent" id = ' + i + '><td>' + results[i].parent_name + '</td><td>' + results[i].parent_code + '</td><td>' + results[i].created_by + '</td><td>' + results[i].updated_by +
                                          '</td><td>' + results[i].created_at + '</td><td>' + results[i].updated_at + '</td><td><button id="' + i + '" type="button" class="edit_parent btn btn-default glyphicon glyphicon-edit edit_color" aria-label="Left Align"></button></td><td><button id="' + results[i].parent_id + '" type="button" class=" delete_parent btn btn-default delete_color" aria-label="Left Align"><span class="glyphicon glyphicon-remove delete_color" aria-hidden="true"></span></button></td></tr>');
                                  }
                              } else {


                                  if (update_click == "FALSE") {

                                      $.msgBox({
                                          title: "Alert",
                                          content: "Result Not available",
                                          type: "alert",
                                      });
                                      update_click = "TRUE";

                                  }


                                  $("#list-of-parent,#parent_back_buttons").removeClass("show");
                                  $(".customer_type").removeClass("hide");
                                  $("#parent_change,#parent_search_button,#show_all_parents").addClass("show");


                              }


                          },
                          beforeSend: function() {

                              $('.parent_tab tr').remove();
                              $('.loading').css('display', 'block');

                          },
                          error: function() {

                              $.msgBox({
                                  title: "Error",
                                  content: "Something went wrong",
                                  type: "error",
                              });

                          }
                      });

                  }


              }

              function update_parent_data() {

                  var search_parent_name_id_display = $('#search_parent_name_id_display').val();
                  var search_parent_code_id_display = $('#search_parent_code_id_display').val();
                  update_click = "TRUE";

                  $.ajax({
                      type: "POST",
                      url: "parent_update",
                      data: {
                          "_token": "{{ csrf_token() }}",
                          "search_parent_name_id_display": search_parent_name_id_display,
                          "search_parent_code_id_display": search_parent_code_id_display,
                          "parent_id": parent_id_ref,

                      },
                      success: function(data) {

                          $('.loading').css('display', 'none');
                          $.msgBox({
                              title: "Message",
                              content: "Parent company updated successfully",
                              type: "info",
                          });


                          $('#parent-table-form-change')[0].reset();

                          $("#parent_table,#parent_update_button").removeClass("show");
                          $("#search-para-parent").removeClass("hide");

                          if (parent_search == "TRUE") {
                              display_parent_data();

                          }

                          if (parent_show_all == "TRUE") {
                              showall_p();
                          }



                      },
                      beforeSend: function() {
                        $('.loading').css('display', 'block');
                      },
                      error: function(xhr, ajaxOptions, thrownError) {

                        jsonValue = jQuery.parseJSON( xhr.responseText );
                        error_data ="";

                        $.each(jsonValue, function (key, val) {

                            if(error_data)
                            {
                              error_data = error_data +  "</br>" + val;
                            }
                            else {
                              error_data = val;
                            }


                        });

                        $.msgBox({
                            title: "Error",
                            content: error_data,
                            type: "error",
                        });



                      }
                  });
              }

              $(document).on('dblclick', '.double_click_parent', function() {

                  var id = $(this).attr('id');
                  // $("#search_parent_name_id_display").attr("data-id", temp_results[id].parent_name);
                  parent_id_ref = temp_results[id].parent_id;
                  $('#search_parent_name_id_display').val(temp_results[id].parent_name);
                  $('#customer_parent_name_valid_id').val(temp_results[id].parent_name);

                  $('#search_parent_code_id_display').val(temp_results[id].parent_code);
                  $('#created_by_p').val(temp_results[id].created_by);
                  $('#created_date_p').val(temp_results[id].created_at);
                  $('#modified_by_p').val(temp_results[id].updated_by);
                  $('#modified_date_p').val(temp_results[id].updated_at);
                  $("#list-of-parent,#parent_back_buttons").removeClass("show");
                  $("#parent_update_button,#parent_table,#parent_change").addClass("show");
                  $("#search-para-parent").addClass("hide");
                  $("#details_parent").click();

              });


              $(document).on('click', '#parent_update', function() {
                  if ($('#parent-table-form-change').valid()) {

                      $.msgBox({
                          title: "Confirmation",
                          content: "Do you want to update the customer",
                          type: "confirm",
                          buttons: [{
                              value: "Yes"
                          }, {
                              value: "No"
                          }],
                          success: function(result) {
                              if (result == "Yes") {

                                  update_parent_data();

                              } else {

                              }
                          }
                      });
                  }
              });

              $(document).on('click', '#show_all_p', function() {

                  parent_search = "FALSE";
                  parent_show_all = "TRUE";
                  showall_p();

              });

              $(document).on('click', '#parent_back', function() {

                  $("#list-of-parent,#parent_back_buttons").removeClass("show");
                  $(".customer_type").removeClass("hide");
                  $("#parent_change,#show_all_parents,#parent_search_button").addClass("show");


              });

              $(document).on('click', '#parent_filter', function() {
                  update_click = "FALSE";
                  parent_search = "TRUE";
                  parent_show_all = "FALSE";
                  display_parent_data();
              });

              $(document).on('click', '.delete_parent', function() {
                  var msg = "Do you want to delete the </br> Parent Company";
                  var ref_parent_id = $(this).attr('id');

                  $.msgBox({
                      title: "Confirm",
                      content: msg,
                      type: "confirm",
                      buttons: [{
                          value: "Yes"
                      }, {
                          value: "No"
                      }],
                      success: function(result) {
                          if (result == "Yes") {

                              $.ajax({
                                  type: "POST",
                                  url: "parent_delete",
                                  data: {
                                      "_token": "{{ csrf_token() }}",
                                      "parent_id": ref_parent_id,
                                  },
                                  success: function(data) {
                                      $('.loading').css('display', 'none');
                                      $.msgBox({
                                          title: "Message",
                                          content: data,
                                          type: "info",
                                      });


                                      if (parent_search == "TRUE") {
                                          display_parent_data();

                                      }
                                      if (parent_show_all == "TRUE") {
                                          showall_p();
                                      }

                                  },
                                  beforeSend: function() {
                                      $('.loading').css('display', 'block');
                                  },
                                  error: function() {

                                      // $.msgBox({
                                      //     title: "Message",
                                      //     content: "Unable to delete the Parent Company</br>Please delete the dependent customers",
                                      //     type: "info",
                                      // });

                                  }
                              });


                          } else {

                          }
                      }
                  });

              });

              $(document).on('click', '.edit_parent', function() {

                  var id = $(this).attr('id');
                  parent_id_ref = temp_results[id].parent_id;
                  // $("#search_parent_name_id_display").attr("data-id", temp_results[id].parent_name);
                  $('#search_parent_name_id_display').val(temp_results[id].parent_name);
                  $('#customer_parent_name_valid_id').val(temp_results[id].parent_name);
                  $('#search_parent_code_id_display').val(temp_results[id].parent_code);
                  $('#created_by_p').val(temp_results[id].created_by);
                  $('#created_date_p').val(temp_results[id].created_at);
                  $('#modified_by_p').val(temp_results[id].updated_by);
                  $('#modified_date_p').val(temp_results[id].updated_at);
                  $("#list-of-parent,#parent_back_buttons").removeClass("show");
                  $("#parent_update_button,#parent_table,#parent_change").addClass("show");
                  $("#search-para-parent").addClass("hide");
                  $("#details_parent").click();

              });

              $(document).on('click', '#parent_cancel', function() {

                  $("#list-of-parent,#parent_back_buttons").addClass("show");
                  $("#parent_update_button,#parent_table,#parent_change").removeClass("show");
                  $("#search-para-parent").removeClass("hide");
                  $('#search_parent_name_id_display-error,#search_parent_code_id_display-error').remove();
                  $('#search_parent_name_id_display,#search_parent_code_id_display').removeClass("error");

              });




          });

          $(window).load(function() {

              $("#master_data").trigger('click');
              $("#customer_master").trigger('click');

              var fetch_search_parent_name_id_display;
              $('input[name="search_parent_name"]').autoComplete({
                  minChars: 1,
                  source: function(term, response) {
                      try {
                          fetch_search_parent_name_id_display.abort();
                      } catch (e) {}
                      fetch_search_parent_name_id_display = $.getJSON('customer_child_controller/autocomplete_parent_name', {
                          search_parent_company_name_id: term
                      }, function(data) {
                          response(data);
                      });
                  }
              });

              var fetch_search_parent_code_id_display;
              $('input[name="search_parent_code"]').autoComplete({
                  minChars: 1,
                  source: function(term, response) {
                      try {
                          fetch_search_parent_code_id_display.abort();
                      } catch (e) {}
                      fetch_search_parent_code_id_display = $.getJSON('customer_child_controller/autocomplete_parent_code', {
                          search_parent_company_code_id: term
                      }, function(data) {
                          response(data);
                      });
                  }
              });

          });
      </script>
   </div>
   <div id="list-of-parent">
      <table style="width:100%;" class="parent_tab">
      </table>
   </div>
   <div id="child_change">
      <div id="search-para">
         <form  autocomplete="off">
            <table style="width:40%;">
               <tr>
                  <td>Parent Company Name</td>
                  <td><input type="text" name="search_parent_company_name" id="search_parent_company_name_id"></td>
               </tr>
               <tr>
                  <td>Customer Name</td>
                  <td><input type="text" name="search_customer_name_id" id="search_customer_name_id"></td>
               </tr>
               <tr>
                  <td>Customer Code</td>
                  <td><input type="text" name="search_customer_code" id="search_customer_code_id"></td>
               </tr>
               <tr>
                  <td>DUNS No</td>
                  <td><input type="text" name="search_duns_number" id="search_duns_number_id"></td>
               </tr>
            </table>
         </form>
      </div>
      <div id="child-table">
         <ul class="nav nav-tabs">
            <li  class="active"><a id = "details_customer" data-toggle="tab" href="#tab_one">Details</a></li>
            <li><a data-toggle="tab" href="#tab_two">Additional Details</a></li>
         </ul>
         <div class="tab-content">
            <div id="tab_one" class="tab-pane fade in active">
               <form id="child-table-form-change" autocomplete="off">
                  <table style="width:80%; margin-bottom: 20px">
                     <tr>
                        <!-- <td class="details_color"><strong>Basic Details</strong></td> -->
                        <td></td>
                        <!-- <td class="details_color"><strong>Primary Contact Details</strong></td> -->
                        <td></td>
                     </tr>
                     <tr>
                        <td>Parent Company Name</td>
                        <td><select name = "parent_company_name_id" id = "parent_company_name_id" >
                           </select>
                        </td>
                        <td>Primary Contact Name </td>
                        <td><input type="text" id="p_contact_name"></td>
                     </tr>
                     <tr>
                        <td>Customer Name</td>
                        <td><input type="text"  data-id = "" id="customer_name" name = "customer_name" >
                        <input id = "customer_name_token_id" name="_token" value="{{ csrf_token() }}" style="display:none">
                        <input id = "customer_name_valid_id" name="customer_name_valid" value=" " style="display:none">
                        <td>Primary Contact Mail ID</td>
                        <td><input type="email" name = "p_contact_email"  id="p_contact_email" ></td>
                     </tr>
                     <tr>
                        <td>Customer Code</td>
                        <td><input type="text"  id="customer_code" ></td>
                        <td>Primary Contact Number</td>
                        <td><input type="text" name = "p_contact_no" id="p_contact_number" ></td>
                     </tr>
                     <tr>
                        <td>DUNS No</td>
                        <td><input type="text" id="dun_no" name ="dun_no"></td>
                        <td>Secondary Contact Name </td>
                        <td><input type="text" id="s_contact_name"></td>
                     </tr>
                     <tr>
                        <td>Customer Type</td>
                        <td>
                           <select id="customer_type" name = "customer_type">
                           </select>
                        </td>
                        <td>Secondary Contact Mail ID</td>
                        <td><input type="email" name = "s_contact_email" id="s_contact_email"></td>
                     </tr>
                     <tr>
                        <td>Active</td>
                        <td><input type="checkbox" id="customer_active"></td>
                        <td>Secondary Contact Number</td>
                        <td><input type="text" name = "s_contact_no"  id="s_contact_number"></td>
                     </tr>
                  </table>
               </form>
            </div>
            <div id="tab_two" class="tab-pane fade">
               <form>
                  <table table style="width:40%; margin-bottom: 20px">
                     <tr>
                        <!-- <td class="details_color"><strong>Details</strong></td> -->
                        <td></td>
                     </tr>
                     <tr>
                        <td>Created By</td>
                        <td><input id ="created_by" type="text" name="" disabled></td>
                     </tr>
                     <tr>
                        <td>Created Date</td>
                        <td><input id ="created_date" type="text" disabled></td>
                     </tr>
                     <tr>
                        <td>Modified By</td>
                        <td><input id = "modified_by" type="text" disabled></td>
                     </tr>
                     <tr>
                        <td>Modified Date</td>
                        <td><input id = "modified_date" type="text" disabled></td>
                     </tr>
                  </table>
               </form>
            </div>
         </div>
  <script type="text/javascript">
    jQuery.validator.addMethod('selectcheck', function(value) {
        return (value != '0');
    }, "");

    $(document).ready(function() {


        var customer_results, no_results, search_click, show_all_click, show_no_results;
        search_click = "NO";
        show_all_click = "YES";
        show_no_results = "TRUE";


        function parent_company_name() {

            $.ajax({

                type: "GET",
                url: "parent_results",
                dataType: "json",
                success: function(data) {

                    $('.loading').css('display', 'none');
                    var results = data;


                    for (var i = 0; i < results.length; i++) {
                        if (i == '0') {

                            $('#parent_company_name_id').append("<option id =\"null\" value=\"0\">Select</option>");

                        }

                        var parent_company_name = results[i].parent_name
                        var parent_id = results[i].parent_id
                        $('#parent_company_name_id').append("<option id =" + parent_id + " value=" + parent_id + ">" + parent_company_name + "</option>");
                    }

                },
                beforeSend: function() {
                  $('.loading').css('display', 'block');
                },
                error: function() {

                    $.msgBox({
                        title: "Alert",
                        content: "Something went wrong",
                        type: "alert",
                    });

                }
            });

        }

        function customer_type() {

            $('#customer_type').empty();
            $.ajax({

                type: "GET",
                url: "customer_type",
                dataType: "json",
                success: function(data) {

                    $('.loading').css('display', 'none');
                    var results = data;


                    for (var i = 0; i < results.length; i++) {
                        if (i == '0') {

                            $('#customer_type').append("<option id =\"null\" value=\"0\">Select</option>");

                        }

                        var customer_type_id = results[i].customer_type_id;
                        var customer_type_name = results[i].customer_type;
                        $('#customer_type').append("<option id =" + customer_type_id + " value=" + customer_type_id + ">" + customer_type_name + "</option>");
                    }

                },
                beforeSend: function() {
                    $('.loading').css('display', 'block');
                },
                error: function() {

                }
            });

        }

        parent_company_name();

        customer_type();

        function showall_customer() {
            $.ajax({
                type: "GET",
                url: "customer_results",
                dataType: "json",
                success: function(data) {


                    var results = data;
                    customer_results = data;
                    $('.loading').css('display', 'none');

                    $('.tab tr').remove();
                    $('.tab').append('<tr style="background-color: #D3D3D3;"><th>Parent Company Name</th><th>Customer Name</th><th>Customer Code</th><th>DUNS No</th><th>Active</th><th>Created By</th><th>Modified By</th><th>Created Date</th><th>Modified Date</th><th>Edit</th><th>Delete</th></tr>');

                    if (results.length > 12) {

                        $('#list-of-cust').addClass("scroll");

                    } else {
                        $('#list-of-cust').removeClass("scroll");
                    }

                    for (var i = 0; i < results.length; i++) {
                        if (results[i].active == 0) {
                            active_check = '<input type="checkbox" disabled readonly>';
                        } else {
                            active_check = '<input type="checkbox" checked disabled readonly>';
                        }
                        $('.tab').append('<tr class = "double_click rows" data-id="' + i + '" id="' + results[i].customer_id + '" ><td>' + results[i].parent_name + '</td><td>' + results[i].customer_name + '</td><td>' + results[i].customer_code + '</td><td>' + results[i].duns_no + '</td><td>' + active_check + '</td><td>' + results[i].created_by + '</td><td>' + results[i].updated_by +
                            '</td><td>' + results[i].created_at + '</td><td>' + results[i].updated_at + '</td><td><button id="' + i + '" type="button" class="edit_customer btn btn-default glyphicon glyphicon-edit edit_color" aria-label="Left Align"></button></td><td><button id="' + results[i].customer_id + '" type="button" class="delete_customer btn btn-default" aria-label="Left Align"><span class="glyphicon glyphicon-remove delete_color" aria-hidden="true"></span></button></td></tr>');
                    }



                },
                beforeSend: function() {

                    $("#list-of-cust,#back_buttons,#back").addClass("show");
                    $(".customer_type,#search-para").addClass("hide");
                    $("#show_all_customer,#search_button").removeClass("show");
                    $('.loading').css('display', 'block');


                },
                error: function() {

                    $.msgBox({
                        title: "Alert",
                        content: "Something went wrong",
                        type: "alert",
                    });



                }
            });
        }

        function display_customer_data() {

            var search_parent_company_name_id = $('#search_parent_company_name_id').val();
            var search_customer_name_id = $('#search_customer_name_id').val();
            var search_customer_code_id = $('#search_customer_code_id').val();
            var search_duns_number_id = $('#search_duns_number_id').val();


            if ((search_parent_company_name_id == "") && (search_customer_name_id == "") && (search_customer_code_id == "") && (search_duns_number_id == "")) {

                $.msgBox({
                    title: "Alert",
                    content: "Search Criteria Not Available",
                    type: "alert",
                });

            } else {

                $.ajax({
                    type: "POST",
                    url: "customer_search",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "search_parent_company_name_id": search_parent_company_name_id,
                        "search_customer_name_id": search_customer_name_id,
                        "search_customer_code_id": search_customer_code_id,
                        "search_duns_number_id": search_duns_number_id,
                    },

                    success: function(data) {

                        $('.loading').css('display', 'none');
                        if (data != "0") {

                            $("#list-of-cust,#back_buttons,#back").addClass("show");
                            $(".customer_type,#search-para").addClass("hide");
                            $("#show_all_customer,#search_button").removeClass("show");


                            var results = data;
                            customer_results = data;


                            console.log(JSON.stringify(results));
                            $('.tab tr').remove();
                            $('.tab').append('<tr style="background-color: #D3D3D3;"><th>Parent Company Name</th><th>Customer Name</th><th>Customer Code</th><th>DUNS No</th><th>Active</th><th>Created By</th><th>Modified By</th><th>Created Date</th><th>Modified Date</th><th>Edit</th><th>Delete</th></tr>');



                            for (var i = 0; i < results.length; i++) {
                                if (results[i].active == 0) {
                                    active_check = '<input type="checkbox" disabled readonly>';
                                } else {
                                    active_check = '<input type="checkbox" checked disabled readonly>';
                                }
                                $('.tab').append('<tr class = "double_click rows" data-id="' + i + '" id="' + results[i].customer_id + '" ><td>' + results[i].parent_name + '</td><td>' + results[i].customer_name + '</td><td>' + results[i].customer_code + '</td><td>' + results[i].duns_no + '</td><td>' + active_check + '</td><td>' + results[i].created_by + '</td><td>' + results[i].updated_by +
                                    '</td><td>' + results[i].created_at + '</td><td>' + results[i].updated_at + '</td><td><button id="' + i + '" type="button" class="edit_customer btn btn-default glyphicon glyphicon-edit edit_color" aria-label="Left Align"></button></td><td><button id="' + results[i].customer_id + '" type="button" class="delete_customer btn btn-default" aria-label="Left Align"><span class="glyphicon glyphicon-remove delete_color" aria-hidden="true"></span></button></td></tr>');
                            }



                        } else {

                            if (show_no_results == "TRUE") {

                                $.msgBox({
                                    title: "Alert",
                                    content: "Result Not available",
                                    type: "alert",
                                });

                                $("#list-of-cust,#back_buttons,#back").removeClass("show");
                                $(".customer_type,#search-para").removeClass("hide");
                                $("#show_all_customer,#search_button").addClass("show");


                            } else {

                                $("#list-of-cust,#back_buttons,#back").removeClass("show");
                                $(".customer_type,#search-para").removeClass("hide");
                                $("#show_all_customer,#search_button").addClass("show");

                            }

                        }

                    },
                    beforeSend: function() {
                        $('.tab tr').remove();
                        $('.loading').css('display', 'block');
                    },
                    error: function() {

                        $.msgBox({
                            title: "Error",
                            content: "Something went wrong",
                            type: "error",
                        });
                    }
                });
            };

        }

        $(document).on('click', '#show_all_c', function() {
            search_click = "NO";
            show_all_click = "YES";
            showall_customer();
        });

        $(document).on('click', '#back', function() {

            $('.tab tr').remove();
            $("#list-of-cust,#back_buttons,#back").removeClass("show");
            $(".customer_type,#search-para").removeClass("hide");
            $("#show_all_customer,#search_button").addClass("show");

        });

        $(document).on('click', '.delete_customer', function() {

            var msg = "Do you want to delete the customer";
            var ref_customer_id = $(this).attr('id');
            $.msgBox({
                title: "Confirm",
                content: msg,
                type: "confirm",
                buttons: [{
                    value: "Yes"
                }, {
                    value: "No"
                }],
                success: function(result) {
                    if (result == "Yes") {

                        $.ajax({
                            type: "POST",
                            url: "customer_delete",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                "customer_id": ref_customer_id,
                            },
                            success: function(data) {
                                $('.loading').css('display', 'none');
                                $.msgBox({
                                    title: "Message",
                                    content: "Customer Deleted Successfully",
                                    type: "info",
                                });

                                if (search_click == "YES") {
                                    show_no_results = "FALSE";
                                    display_customer_data();


                                }

                                if (show_all_click == "YES") {
                                    show_no_results = "FALSE";
                                    showall_customer();

                                }
                            },
                            beforeSend: function() {
                                $('.loading').css('display', 'block');
                            },
                            error: function() {

                              $.msgBox({
                                  title: "Alert",
                                  content: "Unable to delete the Customer</br>Please delete the dependent plants",
                                  type: "alert",
                              });

                            }
                        });


                    } else {

                    }
                }
            });

        });

        $(document).on('click', '#filter', function() {

            search_click = "YES";
            show_all_click = "NO";
            show_no_results = "TRUE";
            display_customer_data();

        });

        $(document).on('click', '.edit_customer', function() {

            var id = $(this).attr('id');

            // $("#customer_name").attr("data-id", customer_results[id].customer_name);

            $("#child-table,#update_button").addClass("show");
            $("#back,#list-of-cust").removeClass("show");



            $("#details_customer").click();
            customer_id_ref = customer_results[id].customer_id;
            $('#parent_company_name_id').val(customer_results[id].parent_id);
            $('#customer_name').val(customer_results[id].customer_name);
            $('#customer_name_valid_id').val(customer_results[id].customer_name);
            $('#customer_code').val(customer_results[id].customer_code);
            $('#dun_no').val(customer_results[id].duns_no);
            $('#customer_type').val(customer_results[id].customer_type_id);
            $('#p_contact_name').val(customer_results[id].primary_contact_name);
            $('#p_contact_email').val(customer_results[id].primary_contact_email_id);
            $('#p_contact_number').val(customer_results[id].primary_contact_number);
            $('#s_contact_name').val(customer_results[id].secondary_contact_name);
            $('#s_contact_email').val(customer_results[id].secondary_contact_email_id);
            $('#s_contact_number').val(customer_results[id].secondary_contact_number);
            $('#created_by').val(customer_results[id].created_by);
            $('#created_date').val(customer_results[id].created_at);
            $('#modified_by').val(customer_results[id].updated_by);
            $('#modified_date').val(customer_results[id].updated_at);

            if (customer_results[id].active == 0) {
                $('#customer_active').prop('checked', false);
            } else {
                $('#customer_active').prop('checked', true);
            }


        });

        $(document).on('click', '#cancel', function() {

            $("#child-table,#update_button").removeClass("show");
            $("#back,#list-of-cust").addClass("show");
            $('#parent_company_name_id-error,#customer_name-error,#customer_type-error').remove();
            $('#parent_company_name_id,#customer_name,#customer_type').removeClass("error");

        });

        function update_customer_data() {

            show_no_results = "FALSE";
            var parent_company_name_id = $('#parent_company_name_id').val();
            var customer_name = $('#customer_name').val();
            var customer_code = $('#customer_code').val();
            var dun_no = $('#dun_no').val();
            var customer_type = $('#customer_type').val();
            var p_contact_name = $('#p_contact_name').val();
            var p_contact_email = $('#p_contact_email').val();
            var p_contact_number = $('#p_contact_number').val();
            var s_contact_name = $('#s_contact_name').val();
            var s_contact_email = $('#s_contact_email').val();
            var s_contact_number = $('#s_contact_number').val();

            if ($('#customer_active').is(":checked")) {
                customer_active = 1;

            } else {
                customer_active = 0;

            }


            $.ajax({
                type: "POST",
                url: "customer_update",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "parent_company_name_id": parent_company_name_id,
                    "customer_name": customer_name,
                    "customer_code": customer_code,
                    "duns_no": dun_no,
                    "customer_type": customer_type,
                    "p_contact_name": p_contact_name,
                    "p_contact_email": p_contact_email,
                    "p_contact_number": p_contact_number,
                    "s_contact_name": s_contact_name,
                    "s_contact_email": s_contact_email,
                    "s_contact_number": s_contact_number,
                    "customer_active": customer_active,
                    "customer_id": customer_id_ref,

                },
                success: function(data) {
                    $('.loading').css('display', 'none');
                    $("#search_parent_name_id_display").attr("data-id","");

                    if (search_click == "YES") {
                        display_customer_data();

                    }

                    if (show_all_click == "YES") {
                        show_no_results = "TRUE";
                        showall_customer();
                    }

                    $.msgBox({
                        title: "Message",
                        content: "Customer updated successfully",
                        type: "info",
                    });




                    $("#child-table,#update_button").removeClass("show");
                    $("#back,#list-of-cust").addClass("show");



                },
                beforeSend: function() {
                  $('.loading').css('display', 'block');
                },
                error: function(xhr, ajaxOptions, thrownError) {

                  $('.loading').css('display', 'none');
                  jsonValue = jQuery.parseJSON( xhr.responseText );
                  error_data ="";

                  $.each(jsonValue, function (key, val) {

                      if(error_data)
                      {
                        error_data = error_data +  "</br>" + val;
                      }
                      else {
                        error_data = val;
                      }


                  });

                  $.msgBox({
                      title: "Error",
                      content: error_data,
                      type: "error",
                  });



                }
            });
        }



        $(document).on('click', '#update', function() {
            if ($('#child-table-form-change').valid()) {
                $.msgBox({
                    title: "Confirmation",
                    content: "Do you want to update the customer",
                    type: "confirm",
                    buttons: [{
                        value: "Yes"
                    }, {
                        value: "No"
                    }],
                    success: function(result) {
                        if (result == "Yes") {

                            update_customer_data();

                        } else {

                        }
                    }
                });

            }
        });

        $(document).on('dblclick', '.double_click', function() {

            var id = $(this).data('id');

          //  $("#customer_name").attr("data-id", customer_results[id].customer_name);


            $("#child-table,#update_button").addClass("show");
            $("#back,#list-of-cust").removeClass("show");
            customer_id_ref = customer_results[id].customer_id;
            $('#parent_company_name_id').val(customer_results[id].parent_id);
            $('#customer_name').val(customer_results[id].customer_name);
            $('#customer_name_valid_id').val(customer_results[id].customer_name);

            $('#customer_code').val(customer_results[id].customer_code);
            $('#dun_no').val(customer_results[id].duns_no);
            $('#customer_type').val(customer_results[id].customer_type_id);
            $('#p_contact_name').val(customer_results[id].primary_contact_name);
            $('#p_contact_email').val(customer_results[id].primary_contact_email_id);
            $('#p_contact_number').val(customer_results[id].primary_contact_number);
            $('#s_contact_name').val(customer_results[id].secondary_contact_name);
            $('#s_contact_email').val(customer_results[id].secondary_contact_email_id);
            $('#s_contact_number').val(customer_results[id].secondary_contact_number);
            $('#created_by').val(customer_results[id].created_by);
            $('#created_date').val(customer_results[id].created_at);
            $('#modified_by').val(customer_results[id].updated_by);
            $('#modified_date').val(customer_results[id].updated_at);
            $("#details_customer").click();

            if (customer_results[id].active == 0) {
                $('#customer_active').prop('checked', false);
            } else {
                $('#customer_active').prop('checked', true);
            }

        });

    });

    $(window).load(function() {

        $("#master_data").trigger('click');
        $("#customer_master").trigger('click');

        var fetch_customer_name;
        $('input[name="search_customer_name_id"]').autoComplete({
            minChars: 1,
            source: function(term, response) {
                try {
                    fetch_customer_name.abort();
                } catch (e) {}
                fetch_customer_name = $.getJSON('customer_child_controller/autocomplete_customer_name', {
                    search_customer_name_id: term
                }, function(data) {
                    response(data);
                });
            }
        });

        var fetch_customer_code;
        $('input[name="search_customer_code"]').autoComplete({
            minChars: 1,
            source: function(term, response) {
                try {
                    fetch_customer_code.abort();
                } catch (e) {}
                fetch_customer_code = $.getJSON('customer_child_controller/autocomplete_customer_code', {
                    search_customer_code_id: term
                }, function(data) {
                    response(data);
                });
            }
        });

        var fetch_duns_number;
        $('input[name="search_duns_number"]').autoComplete({
            minChars: 1,
            source: function(term, response) {
                try {
                    fetch_duns_number.abort();
                } catch (e) {}
                fetch_duns_number = $.getJSON('customer_child_controller/autocomplete_duns_no', {
                    search_duns_number_id: term
                }, function(data) {
                    response(data);
                });
            }
        });

        var fetch_search_parent_company_name;
        $('input[name="search_parent_company_name"]').autoComplete({
            minChars: 1,
            source: function(term, response) {
                try {
                    fetch_search_parent_company_name.abort();
                } catch (e) {}
                fetch_search_parent_company_name = $.getJSON('customer_child_controller/autocomplete_parent_name', {
                    search_parent_company_name_id: term
                }, function(data) {
                    response(data);
                });
            }
        });

    });
  </script>
      </div>
      <div id="list-of-cust">
         <table style="width:100%;" class="tab">
         </table>
      </div>
   </div>

</div>
@stop
@section('lower_band')
<div class="col-xs-12 lower_band">
   <div class="parent_bottom_buttons">
      <div  id="parent_search_button">
         <button class="headerbuttons" id="parent_filter" type="button">Search</button>
      </div>
      <div  id="parent_back_buttons">
         <button class="headerbuttons" id="parent_back" type="button">Back</button>
      </div>
      <div id="parent_update_button" style="float:right">
         <button class="headerbuttons" id="parent_update" type="button">Update</button>
         <button class="headerbuttons" id="parent_cancel" type="button">Cancel</button>
      </div>
   </div>
   <div class="child_bottom_buttons">
      <div  id="search_button">
         <button class="headerbuttons" id="filter" type="button">Search</button>
      </div>
      <div  id="back_buttons">
         <button class="headerbuttons" id="back" type="button">Back</button>
      </div>
      <div id="update_button" style="float:right">
         <button class="headerbuttons" id="update" type="button">Update</button>
         <button class="headerbuttons" id="cancel" type="button">Cancel</button>
      </div>
   </div>
</div>
@stop
