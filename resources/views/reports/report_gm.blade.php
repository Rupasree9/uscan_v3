@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" type="text/css" href="css/reports.css">
@stop
@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="col-xs-4 display-title">
      <span>Report For General Motors</span>
   </div>
</div>
@stop
@section('content')
<div  class="col-xs-12 content">
    <div id= "report_gm_div" class="customer_type scroll" >
      <table  id = "report" style="width:40%;">
        <tr><td>Customers / Plants</td><td>General Motors</td></tr>
      </table>
   </div>
   <div id= "report_gm_view1_div" class="customer_type scroll">
     <table id = "report_view1" style="width:40%;">

     </table>
  </div>

  <div id= "report_gm_view2_div" class="customer_type scroll">
    <table id = "report_view2" style="width:40%;">

    </table>
 </div>

   <script type="text/javascript">
      jQuery.validator.addMethod('selectcheck', function(value) {
          return (value != '0');
      }, "");

      $(document).ready(function() {

        $('#report_gm_view1_div').css('display','none');
        $('#report_gm_view2_div').css('display','none');
          get_gmreport_master();
          function get_gmreport_master() {

              $.ajax({

                  type: "GET",
                  url: "get_gmreport_master",
                  dataType: "json",
                  success: function(data) {

                      var results = data;
                    //  console.log(JSON.stringify(data));
                      for (var i = 0; i < results.length; i++) {
                      //  var plant_name = results[i].name + "-" + results[i].location;
                        var plant_name = results[i].name;
                        var tdid = results[i].mfg_duns+"_mfgduns";
                        var quality = results[i].quality;
                        var supplychain = results[i].supplychain;
                        var cca = results[i].cca;

                        var tdcolor;
                        if(quality == 0 && supplychain == "NR"){
                          if(cca == "R"){
                            tdcolor = "#FF0000"; // RED Color
                          }
                          else if (cca == "G") {
                              tdcolor = "#008000";  // GREEN Color
                          }
                          else {
                              tdcolor = "#FFFF000";  // YELLOW Color
                          }
                        }
                        else {
                          if (quality < 80 || supplychain < 75 || cca == "R") {
                            tdcolor = "#FF0000"; // RED Color
                          }
                          else if(quality >= 80 && supplychain >=85 && cca == "G")
                          {
                            tdcolor = "#008000";  // GREEN Color
                          }
                        }


                        // else if (cca == "Y") {
                        //   tdcolor = "#FFFF00";
                        // }
                         $('#report').append("<tr><td>"+plant_name+"</td><td id="+tdid+" class='td_click' bgcolor = "+tdcolor+"></td></tr>");
                      }

                  },
                  beforeSend: function() {

                  },
                  error: function() {
                      alert('Sorry report data not available');
                  }
              });

          }

          $(document).on('click', '.td_click', function() {
            var id = this.id;
            var splt_id = id.split("_");
            if(splt_id[1] == "mfgduns"){
              get_plantdata(splt_id[0],splt_id[1]);
            }
            else if (splt_id[1] == "quality") {
              get_plantdata(splt_id[0],splt_id[1]);
            }
            else if (splt_id[1] == "supplychain") {
              get_plantdata(splt_id[0],splt_id[1]);
            }
            else if (splt_id[1] == "cca") {
              get_plantdata(splt_id[0],splt_id[1]);
            }
           });

           $(document).on('click', '.report_gm_back', function() {
             var id = this.id;
             report_gm_back(id);
            });

           function get_plantdata(mfg_duns,report_type)
           {
             $('#report_gm_div').css('display','none');
             $('#report_gm_view2_div').css('display','none');
             $.ajax({
                 type: "POST",
                 url: "get_plantdata",
                 data: {
                     "_token": "{{ csrf_token() }}",
                     "mfg_duns": mfg_duns,
                     "report_type":report_type
                     },
                 success: function(data) {
                     if(report_type == "mfgduns"){
                       $('#report_gm_view1_div').css('display','block');

                       for (var i = 0; i < data.length; i++) {
                         var quality = data[i].quality;
                         var supplychain = data[i].supplychain;
                         var cca = data[i].cca;
                         var country_risk = data[i].country_risk;
                         var financial_risk = data[i].financial_risk;
                         var mmog_le = data[i].mmog_le;
                         var conflict_materials = data[i].conflict_materials;
                         var diversity = data[i].diversity;
                         var ipb = data[i].ipb;
                         var severity_score = data[i].severity_score;
                         var severity_ipb = data[i].severity_ipb;
                         var quality_id = mfg_duns+"_quality";
                         var supplychain_id=mfg_duns+"_supplychain";
                         var cca_id = mfg_duns+"_cca";
                         var target_ipbid = mfg_duns+"_targetipb";
                         var actual_ipbid = mfg_duns+"_actualipb";
                         $('#report_view1 tr').remove();
                         $('#report_view1').append("<tr><td colspan='2' class = 'report_subtitle'>RANKINGS</td></tr>");
                         $('#report_view1').append("<tr id="+quality_id+" class='td_click'><td>QUALITY</td><td>"+quality+"</td></tr>");
                         $('#report_view1').append("<tr id="+supplychain_id+" class='td_click'><td>SUPPLY CHAIN</td><td>"+supplychain+"</td></tr>");
                         $('#report_view1').append("<tr id="+cca_id+" class='td_click'><td>CCA</td><td>"+cca+"</td></tr>");
                         $('#report_view1').append("<tr><td colspan='2' class = 'report_subtitle'>RATINGS</td></tr>");
                         $('#report_view1').append("<tr><td>COUNTRY RISK</td><td>"+country_risk+"</td></tr>");
                         $('#report_view1').append("<tr><td>FINANCIAL RISK</td><td>"+financial_risk+"</td></tr>");
                         $('#report_view1').append("<tr><td>MMOG / LE</td><td>"+mmog_le+"</td></tr>");
                         $('#report_view1').append("<tr><td>CONFLICT MATERIALS</td><td>"+conflict_materials+"</td></tr>");
                         $('#report_view1').append("<tr><td>DIVERSITY</td><td>"+diversity+"</td></tr>");
                         $('#report_view1').append("<tr><td colspan='2' class = 'report_subtitle'>SEVERITY IPB</td></tr>");
                         $('#report_view1').append("<tr id="+target_ipbid+" class='td_click'><td>TARGET FOR SEVERITY IPB</td><td>"+ipb+"</td></tr>");
                         $('#report_view1').append("<tr id="+actual_ipbid+" class='td_click'><td>ACTUAL SEVERITY IPB</td><td>"+severity_ipb+"</td></tr>");
                         $('#report_view1').append("<tr><td colspan='2'><input type ='button' value='Back' id='report_child1' class='report_gm_back' /></td></tr>");
                       }
                       }
                       else if(report_type == "quality") {
                         $('#report_gm_view1_div').css('display','none');
                         $('#report_gm_view2_div').css('display','block');
                         for (var i = 0; i < data.length; i++) {
                           var ipb = data[i].ipb;
                           var severity_score = data[i].severity_score;
                           var severity_ipb = data[i].severity_ipb;
                           var prog_mgmt_rrs = data[i].prog_mgt_prrs;
                           var unauth_change = data[i].unauth_change;
                           var customer_sat_open = data[i].customer_sat_open;
                           var launch_prrs = data[i].launch_prrs;
                           var field_action = data[i].field_action;
                           var plant_disruption = data[i].plant_disruption;
                           var controlled_shipping_level1 = data[i].controlled_shipping_level1;
                           var controlled_shipping_level2 = data[i].controlled_shipping_level2;
                           var nbh = data[i].nbh;
                           var iso_14001 = data[i].iso_14001;
                           var ts16949_cert = data[i].ts16949_cert;
                           var qsb = data[i].qsb;
                           var sqea = data[i].sqea;
                           $('#report_view2 tr').remove();
                           $('#report_view2').append("<tr><td colspan='2' class = 'report_subtitle'>QUALITY DATA</td></tr>");
                           $('#report_view2').append("<tr><td>IPB</td><td>"+ipb+"</td></tr>");
                           $('#report_view2').append("<tr><td>SEVERITY SCORE</td><td>"+severity_score+"</td></tr>");
                           $('#report_view2').append("<tr><td>SEVERITY IPB</td><td>"+severity_ipb+"</td></tr>");
                           $('#report_view2').append("<tr><td>PROGRAM MANAGEMENT RRS</td><td>"+prog_mgmt_rrs+"</td></tr>");
                           $('#report_view2').append("<tr><td>UNAUTH CHANGE</td><td>"+unauth_change+"</td></tr>");
                           $('#report_view2').append("<tr><td>CUSTOMER SAT OPEN</td><td>"+customer_sat_open+"</td></tr>");
                           $('#report_view2').append("<tr><td>LAUNCH PRRS</td><td>"+launch_prrs+"</td></tr>");
                           $('#report_view2').append("<tr><td>FIELD ACTION</td><td>"+field_action+"</td></tr>");

                           $('#report_view2').append("<tr><td>PLANT DISRUPTION</td><td>"+plant_disruption+"</td></tr>");
                           $('#report_view2').append("<tr><td>CONTROLLED SHIPPING LEVEL-1</td><td>"+controlled_shipping_level1+"</td></tr>");
                           $('#report_view2').append("<tr><td>CONTROLLED SHIPPING LEVEL-2</td><td>"+controlled_shipping_level2+"</td></tr>");
                           $('#report_view2').append("<tr><td>NBH</td><td>"+nbh+"</td></tr>");

                           $('#report_view2').append("<tr><td>ISO 14001</td><td>"+iso_14001+"</td></tr>");
                           $('#report_view2').append("<tr><td>TS16949_CERT</td><td>"+ts16949_cert+"</td></tr>");
                           $('#report_view2').append("<tr><td>QSB</td><td>"+qsb+"</td></tr>");
                           $('#report_view2').append("<tr><td>SQEA</td><td>"+sqea+"</td></tr>");
                           $('#report_view2').append("<tr><td colspan='2'><input type ='button' value='Back' id='report_child2' class='report_gm_back' /></td></tr>");
                         }
                       }
                       else if(report_type == "supplychain") {
                         $('#report_gm_view1_div').css('display','none');
                         $('#report_gm_view2_div').css('display','block');
                         for (var i = 0; i < data.length; i++) {
                           var sc_rating = data[i].sc_rating;
                           $('#report_view2 tr').remove();
                           $('#report_view2').append("<tr><td colspan='2' class = 'report_subtitle'>SUPPLY CHAIN DATA</td></tr>");
                           $('#report_view2').append("<tr><td>sc_rating</td><td>"+sc_rating+"</td></tr>");
                           $('#report_view2').append("<tr><td colspan='2'><input type ='button' value='Back' id='report_child2' class='report_gm_back' /></td></tr>");
                         }
                       }
                       else if(report_type == "cca") {
                         $('#report_gm_view1_div').css('display','none');
                         $('#report_gm_view2_div').css('display','block');
                         for (var i = 0; i < data.length; i++) {
                           var quality_score = data[i].quality_score;
                           var service_score = data[i].service_score;
                           $('#report_view2 tr').remove();
                           $('#report_view2').append("<tr><td colspan='2' class = 'report_subtitle'>CUSTOMER CARE AND AFTERSALES</td></tr>");
                           $('#report_view2').append("<tr><td>QUALITY SCORE</td><td>"+quality_score+"</td></tr>");
                           $('#report_view2').append("<tr><td>SERVICE SCORE</td><td>"+service_score+"</td></tr>");
                           $('#report_view2').append("<tr><td colspan='2'><input type ='button' value='Back' id='report_child2' class='report_gm_back' /></td></tr>");
                         }
                       }
                       else {

                       }


                 },
                 beforeSend: function() {
                 },
                 error: function() {

                 }

             });
           }

           function get_plantdata2(obj)
           {
             $('#report_gm_div').css('display','none');
             $('#report_gm_view1_div').css('display','none');
             $.ajax({
                 type: "POST",
                 url: "get_plantdata2",
                 data: {
                     "_token": "{{ csrf_token() }}",
                     "mfg_duns": mfg_duns
                     },
                 success: function(data) {
                     $('#report_gm_view2_div').css('display','block');
                     for (var i = 0; i < data.length; i++) {
                       var quality = data[i].quality;
                       var supplychain = data[i].supplychain;
                       var cca = data[i].cca;
                       var country_risk = data[i].country_risk;
                       var financial_risk = data[i].financial_risk;
                       var mmog_le = data[i].mmog_le;
                       var conflict_materials = data[i].conflict_materials;
                       var diversity = data[i].diversity;
                       var ipb = data[i].ipb;
                       var severity_score = data[i].severity_score;
                       var severity_ipb = data[i].severity_ipb;
                       var quality_id = mfg_duns+"_quality";
                       var supplychain_id=mfg_duns+"_supplychain";
                       var cca_id = mfg_duns+"_cca";
                       var target_ipbid = mfg_duns+"_targetipb";
                       var actual_ipbid = mfg_duns+"+_actualipb";
                       $('#report_view1 tr').remove();
                       $('#report_view1').append("<tr><td colspan='2'>RANKINGS</td></tr>");
                       $('#report_view1').append("<tr id="+quality_id+" class='td_click'><td>QUALITY</td><td>"+quality+"</td></tr>");
                       $('#report_view1').append("<tr id="+supplychain_id+" class='td_click'><td>SUPPLY CHAIN</td><td>"+supplychain+"</td></tr>");
                       $('#report_view1').append("<tr id="+cca_id+" class='td_click'><td>CCA</td><td>"+cca+"</td></tr>");
                       $('#report_view1').append("<tr><td colspan='2'>RATINGS</td></tr>");
                       $('#report_view1').append("<tr><td>COUNTRY RISK</td><td>"+country_risk+"</td></tr>");
                       $('#report_view1').append("<tr><td>FINANCIAL RISK</td><td>"+financial_risk+"</td></tr>");
                       $('#report_view1').append("<tr><td>MMOG / LE</td><td>"+mmog_le+"</td></tr>");
                       $('#report_view1').append("<tr><td>CONFLICT MATERIALS</td><td>"+conflict_materials+"</td></tr>");
                       $('#report_view1').append("<tr><td>DIVERSITY</td><td>"+diversity+"</td></tr>");
                       $('#report_view1').append("<tr><td colspan='2'>SEVERITY IPB</td></tr>");
                       $('#report_view1').append("<tr id="+target_ipbid+" class='td_click'><td>TARGET FOR SEVERITY IPB</td><td>"+ipb+"</td></tr>");
                       $('#report_view1').append("<tr id="+actual_ipbid+" class='td_click'><td>ACTAUAL SEVERITY IPB</td><td>"+severity_ipb+"</td></tr>");
                       $('#report_view1').append("<tr><td colspan='2'><input type ='button' value='Back' id='report_child1' class='report_gm_back' /></td></tr>");
                     }
                 },
                 beforeSend: function() {
                 },
                 error: function() {
                 }
             });
           }

          function get_businessunit() {

              $('#bunit').empty();
              $('#bunit').append("<option id =\"null\" value=\"0\">Select</option>");
              $('#prdgrp').empty();
              $('#prdgrp').append("<option id =\"null\" value=\"0\">Select</option>");
              $.ajax({

                  type: "GET",
                  url: "get_bunit",
                  dataType: "json",
                  success: function(data) {

                      var results = data;
                      console.log(JSON.stringify(data));

                      for (var i = 0; i < results.length; i++) {
                          var bunit_name = results[i].business_unit;
                          var bunit_id = results[i].business_unit;
                          var prdgrp_name = results[i].product_group;
                          var prdgrp_id = results[i].product_group;
                          $('#bunit').append("<option id =" + bunit_id + " value=" + bunit_id + ">" + bunit_name + "</option>");
                          $('#prdgrp').append("<option id =" + prdgrp_id + " value=" + prdgrp_id + ">" + prdgrp_name + "</option>");
                      }

                  },
                  beforeSend: function() {

                  },
                  error: function() {
                      alert('Sorry Bunit not available');
                  }
              });

          }
          function report_gm_back(id)
          {
            if(id == "report_child1"){
              $('#report_gm_view1_div').css('display','none');
              $('#report_gm_view2_div').css('display','none');
              $('#report_gm_div').css('display','block');
            }
            else if(id == "report_child2"){
              $('#report_gm_view2_div').css('display','none');
              $('#report_gm_view1_div').css('display','block');
              $('#report_gm_div').css('display','none');
            }

          }
      });

      $(window).load(function() {
          //$("#report_gm").trigger('click');
          //$("#customer_master").trigger('click');
      });
   </script>
</div>
@stop
@section('lower_band')
<div class="col-xs-12 lower_band">

</div>
@stop
