<!doctype html>
<html>
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  <!-- Scripts -->
  <script>
      window.Laravel = {!! json_encode([
          'csrfToken' => csrf_token(),
      ]) !!};
  </script>

<title>USCAN</title>

<link rel="stylesheet"  href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<link rel="stylesheet"  href="{{ asset('css/general.css') }}" rel="stylesheet">

<style>
  /*.has-error .checkbox, .has-error .checkbox-inline, .has-error .help-block, .has-error .radio, .has-error .radio-inline, .has-error.checkbox label, .has-error.checkbox-inline label, .has-error.radio label, .has-error.radio-inline label
  {
    color: red;
    font-size: 10px;
  }
  .has-error .control-label
  {
    color :black;
  }*/

  .has-error .checkbox, .has-error .checkbox-inline, .has-error .control-label,.has-error .radio, .has-error .radio-inline, .has-error.checkbox label, .has-error.checkbox-inline label, .has-error.radio label, .has-error.radio-inline label
  {
    color :black;
  }
  .has-error .help-block
  {
    color : #d43c3c;
    font-size: 10px;
  }
  .has-error .form-control
  {
        border-color : #d43c3c;
  }
  input[type=email], input[type=password] {
    width: 80%;
  }

</style>

</head>
<body>
  <div class="container-fluid">

    <header class="row" style="background: -webkit-linear-gradient(top, #295070, white);">
      <div class="col-md-3" onclick="window.location='{{ url("/")}}'" style="cursor:pointer">
        <div id="mainheader" style="text-align:center;">
         <p style="color:#295070; background: -webkit-linear-gradient(white,#99b9c9);
        -webkit-background-clip: text;-webkit-text-fill-color: transparent; font-weight:bold;font-size:30px; margin-bottom:0px;margin-top:0px;">U.&nbspS&nbspC&nbspA&nbspN </p>
         <span style="color:#07253e;font-weight:bold;font-size:12px;"> Supply Chain Analytics </span>
       </div>
      </div>
    </header>
    <div class="row" style="height:515px;position: relative;">
        <div style="height: 240px;width: 450px; position: absolute; top: 50%; left: 50%; margin-left: -225px; margin-top: -120px; ">

          <div class="panel panel-default">
            <div class="panel-heading"><b>Reset Password<b></div>
            <div class="panel-body">
               @if (session('status'))
                   <div class="alert alert-success">
                       {{ session('status') }}
                   </div>
               @endif

               <form class="form-horizontal" role="form" method="POST" style=" margin-top: 15px;" action="{{ route('password.email') }}">
                   {{ csrf_field() }}

                   <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                       <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                       <div class="col-md-8">
                           <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                           @if ($errors->has('email'))
                               <span class="help-block">
                                   <strong>{{ $errors->first('email') }}</strong>
                               </span>
                           @endif
                       </div>
                   </div>

                   <div class="form-group">
                       <div class="col-md-8 col-md-offset-4">
                           <button type="submit" class="btn btn-primary" style="width: 20%;font-size:11px;padding-left: 0px;padding-right: 0px;">
                               <span > Reset  </span>
                           </button>
                           <button type="button" onclick="window.location.href='{{ url('/') }}'"  class="btn btn-primary" style="width: 20%;font-size:11px;padding-left: 0px;padding-right: 0px;">
                             <span >Cancel</span>
                           </button>
                       </div>
                   </div>
               </form>
           </div>
             </div>

        </div>

    </div>
    <footer class="row" style="height:40px;background: -webkit-linear-gradient(top,white, #295070);">
        <p style="text-align:center; margin-top: 0.8%; margin-bottom: 0.8%; "> <span style="color:#295070;">U.SCAN   &copy 2017-2021</span></p>
    </footer>
</div>
</body>
</html>
