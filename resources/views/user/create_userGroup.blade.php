
@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" href="css/user_master.css">
@stop

@section('upper_band')
<div class="col-xs-12 upper_band">
     <div class="col-xs-4 display-title">
     <span>Create User Group</span>
     </div>
</div>
@stop

@section('content')
<div class="col-xs-12 content">
      <table class="user_grp_table">
        <tr>
           <td style="background-color: #eee;"><strong>User Group Details</strong></td>
           <td></td>
        </tr>

        <tr>
          <td>Group Name</td>
          <td><input type="text" name="group_name" id="group_name"></td>
        </tr>
        <tr>
          <td>Group ID</td>
          <td><input type="text" name="group_id" id="group_id"></td>
        </tr>

     </table>

     <table class="role_table">
       <tr>
         <td style="background-color: #eee;"><strong>Assign Roles</strong></td>
         <td></td>
       </tr>
       <tr>
         <td>
           <select  id="display_roles"></select>
         </td>
         <td></td>
       </tr>
     </table>

     <table class="right_table">
       <tr>
         <td  style="background-color: #eee;"><strong>Associated Rights</strong></td>
         <td></td>
       </tr>
       <tr>
         <td>
           <ul id="display_rights"></ul>
         </td>
         <td></td>
       </tr>
     </table>
  </div>

  <script type="text/javascript">
    $(document).ready(function(){
      $.ajax({
        type:"GET",
        url:"get_roles",
        dataType:"json",
        data:{
          "_token": "{{ csrf_token() }}"
        },
        success:function(rights){
          for (var i = 0; i < rights.length; i++)
          {
            if(i == '0'){
              $('#display_roles').append("<option id =\"null\" value=\"\">Select</option>");
            }
            var role_name = rights[i].role_name
            var role_id   = rights[i].role_id
            $('#display_roles').append("<option id ="+ role_id +" value="+ role_id +">"+role_name+"</option>");
          }
        }
      });

      $('#display_roles').change(function(){

      var role_id = $('#display_roles').val();
      $.ajax({
        type:"GET",
        url:"get_rights_for_role",
        dataType:"json",
        data:{
          "_token": "{{ csrf_token() }}",
          "role_id": role_id
        },
        success:function(rights){
          $('#display_rights').html('');
          for (var i = 0; i < rights.length; i++)
          {
            var right_name = rights[i].right_name
            var right_id   = rights[i].right_id
            $('#display_rights').append("<li id ="+ right_id +" value="+ right_id +">"+right_name+"</li>");
          }
        }
      });
    });
      $(document).on('click', '#user_grp_save_id', function(){

                    var group_name = $('#group_name').val();
                    var group_id = $('#group_id').val();
                    var role_id = $('#display_roles').val();

                    if ((group_name == "") || (group_id == "") || (role_id == "")) {
                      alert("Please Enter All the values to Continue");
                    } else

                    {
                      $.ajax({
                        type: "POST",
                        url: "insert_usergrp",
                        data: {
                                      "_token": "{{ csrf_token() }}",
                                      "user_group_name": group_name,
                                      "user_group_id": group_id,
                                      "role_id": role_id
                                },
                        success: function(data) {
                            alert(data);
                            reset();
                        }
                      });
                    }
    });

  });

  function reset() {
    window.location='{{ url("create_userGroup")}}';
  }

  $(window).load(function() {
      console.log("uder load funciton");
      $("#master_data").trigger('click');
      $("#user_master").trigger('click');
      $("#user_group").trigger('click');
  });

  </script>
@stop

@section('lower_band')
  	<div class="col-xs-12 lower_band">
  		<input class="headerbuttons" type="button" value="Save" id="user_grp_save_id"/>
  		<input class="headerbuttons" type="button" onclick="reset()" id="CancelButton" value="Cancel"/>
  	</div>
@stop
