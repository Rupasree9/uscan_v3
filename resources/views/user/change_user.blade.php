@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" href="css/user_master.css">
<script src="js/user_validation.js"></script>
@stop

@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="display-title">
      Change Users
   </div>
   <div id="show_all_link" class="col-xs-2" style="float: right;text-align:center;line-height:30px;">
      <span  id="show_all" style="">Show All Users</span>
   </div>
</div>
@stop

@section('content')
<div class="col-xs-12 content no_scroll" id="content no_scroll">
  <table id="display_user_table">  </table>

  <form id="search-para" class="no_scroll">
     <table class="hide_show" style="width:40%;">
        <tr>
           <td style="background-color: #eee;">Search Parameters</td>
        </tr>
        <tr>
           <td>User Email ID</td>
           <td><input type="text" id="user_id_search" name="user_id_search"></td>
        </tr>
        <tr>
           <td>First Name</td>
           <td><input type="text" id="first_name_search" name="first_name_search"></td>
        </tr>
        <tr>
           <td>Last Name</td>
           <td><input type="text" id="last_name_search"  name="last_name_search"></td>
        </tr>
        <!-- <tr>
           <td>DUN'S Number</td>
           <td><input type="text" id="duns_no_search"  name="duns_no_search"></td>
        </tr>
        <tr>
           <td>Supplier Cde</td>
           <td><input type="text" id="supplier_code_search"  name="supplier_code_search"></td>
        </tr> -->
     </table>
  </form>
  <div id="list-of-user"  class="div1 overflow table_height">
     <table style="width:100%;" class="tab">
     </table>
  </div>
  <div class="col-xs-12" style=" padding-left: 0px; ">
     <ul class="nav nav-tabs" id="myTabs">
        <li id="1" class="active"><span id="span_one" class="tab_one "  data-toggle="tab" >User Details</span></li>
        <li id="2"><span id="span_two" class="tab_two "  data-toggle="tab" >Additional Details</span></li>
     </ul>
     <div class="tab-content">
        <div id="tab_one" class="">
           <form id="user_table_form"  autocomplete="off">
             <table class="user_table_form" style="width:40%;">
                 <tr>
                    <td></td>
                    <td></td>
                 </tr>
                 <tr>
                    <td>User ID</td>
                    <td><input type="email" data-id = "" name="user_id" id="user_id"></td>
                    <input id = "user_name_token_id" name="_token" value="{{ csrf_token() }}" style="display:none">
                 </tr>
                 <tr>
                    <td>First Name</td>
                    <td><input type="text" name="first_name" id="first_name"></td>
                 </tr>
                 <tr>
                    <td>Last Name</td>
                    <td><input type="text" name="last_name" id="last_name"></td>
                 </tr>
                 <tr>
                    <td>User Group Name</td>
                    <td><select id="group_name" name="group_name"></select>
                    </td>
                 </tr>
                 <tr>
                    <td>Password</td>
                    <td><input type="Password" name="password" id="password"></td>
                 </tr>
                 <tr>
                    <td>Repeat Password</td>
                    <td><input type="Password" name="repeat_password" id="repeat_password"></td>
                 </tr>
                 <tr>
                    <td>Password Expires</td>
                    <td><input type="date" name="password_expires" id="password_expires"></td>
                 </tr>
                 <tr>
                    <td>Valid Till</td>
                    <td><input type="date" name="valid_till" id="valid_till"></td>
                 </tr>
                 <tr>
                    <td>Active</td>
                    <td> <input type="checkbox" name="active" id="active"> </td>
                 </tr>

            </table>
           </form>

           <form id="role_table">
                <table class="role_table" style="width:30%;">
                <tr>
                   <td style="background-color: #eee;"><strong>Assigned Roles</strong></td>
                   <td></td>
                </tr>
                <tr>
                   <td>
                      <select  id="display_roles" disabled></select>
                   </td>
                   <td></td>
                </tr>
             </table>
           </form>
           <form id="right_table">
             <table class="right_table">
                <tr>
                   <td  style="background-color: #eee;"><strong>Associated Rights</strong></td>
                   <td></td>
                </tr>
                <tr>
                   <td>
                      <ul id="display_rights"></ul>
                   </td>
                   <td></td>
                </tr>
             </table>
           </form>
        </div>
        <div id="tab_two" class="load">
           <form id="additional_details" autocomplete="off">
              <table style="width:40%; margin-bottom: 20px">
                 <tr>
                   <td></td>
                 </tr>
                 <tr>
                    <td>Created By</td>
                    <td><input type="text" name="created_by" id="created_by" disabled></td>
                 </tr>
                 <tr>
                    <td>Created Date</td>
                    <td><input type="text" name="created_date" id="created_date" disabled></td>
                 </tr>
                 <tr>
                    <td>Modified By</td>
                    <td><input type="text" name="modified_by" id="modified_by" disabled></td>
                 </tr>
                 <tr>
                    <td>Modified Date</td>
                    <td><input type="text" name="modified_date" id="modified_date" disabled></td>
                 </tr>
              </table>
           </form>
        </div>
     </div>
  </div>

</div>


<script type="text/javascript">
jQuery.validator.addMethod('selectcheck', function (value) {
               return (value != '0');
       }, "");
$(window).load(function() {
    $("#master_data").trigger('click');
    $("#user_master").trigger('click');
    $("#user").trigger('click');
});

$(document).ready(function() {

    var filter_clicked=false;
    var show_all_link_clicked=false;
    var results = "";
    var contact_id = "";
    var address_id = "";
    var user_id_search = "";
    var user_id = "";
    var active_check;
    var first_name_search,last_name_search,supplier_code,duns_no;

    $("#user_id").prop('required',true);

    $.ajax({
      type:"GET",
      url:"get_user_group",
      dataType:"json",
      data:{
        "_token": "{{ csrf_token() }}"
      },
      success:function(user_grp){
        for (var i = 0; i < user_grp.length; i++)
        {
          if(i == '0'){
            $('#group_name').append("<option id =\"0\" value=\"0\">Select</option>");
          }
          var group_name = user_grp[i].group_name
          var group_id   = user_grp[i].group_id
          $('#group_name').append("<option id ="+ group_id +" value="+ group_id +">"+group_name+"</option>");
        }
      }
    });

    $.ajax({
      type:"GET",
      url:"get_role",
      dataType:"json",
      data:{
       //  "_token": "{{ csrf_token() }}"
      },
      success:function(user_role){
        for (var i = 0; i < user_role.length; i++)
        {
          if(i == '0'){
            $('#display_roles').append("<option id =\"0\" value=\"0\">Select Group Name</option>");
          }
          var role_name = user_role[i].role_name
          var role_id   = user_role[i].role_id
          $('#display_roles').append("<option id ="+ role_id +" value="+ role_id +">"+role_name+"</option>");
        }
      }
    });

    $('#group_name').change(function(){
      var group_id = $('#group_name').val();
      var group_id = $(this).children(":selected").attr("id");
      $.ajax({
        type:"GET",
        url:"get_role_by_groupId",
        dataType:"json",
        data:{
          "_token": "{{ csrf_token() }}",
          "group_id": group_id
        },
        success:function(roles){
          if (roles.length==0) {
            $('#display_roles').html('');
            $('#display_rights').html('');
             $('#display_roles').append("<option id =\"0\" value=\"0\">Select Group Name</option>");
          }
          else {
            $('#display_roles').html('');
            getRightsByRoloId(roles[0].role_id);
            for (var i = 0; i < roles.length; i++)
            {
              var role_name = roles[i].role_name
              var role_id   = roles[i].role_id
              $('#display_roles').append("<option id ="+ role_id +" value="+ role_id +">"+role_name+"</option>");
            }
          }
        }
      });
    });

    $('#display_roles').change(function(){
    var role_id = $('#display_roles').val();
      getRightsByRoloId(role_id);
    });



    function getRightsByRoloId(role_id) {
              $.ajax({
                type:"GET",
                url:"get_right_for_role",
                dataType:"json",
                data:{
                  "_token": "{{ csrf_token() }}",
                  "role_id": role_id
                },
                success:function(rights){
                  $('#display_rights').html('');
                  for (var i = 0; i < rights.length; i++)
                  {
                    var right_name = rights[i].right_name
                    var right_id   = rights[i].right_id
                    $('#display_rights').append("<li id ="+ right_id +" value="+ right_id +">"+right_name+"</li>");
                  }
                }
              });
    }

    $('#next').click(function() {
        // $('#additional').show();
        $('#user_table_form').hide();
        // $('#back_edit').show();
        $('#next').hide();
    });

    function filter_click(){
                $('#list-of-user').show();
                show_all_link_clicked=false;
                user_id_search = $('#user_id_search').val();
                first_name_search = $('#first_name_search').val();
                last_name_search = $('#last_name_search').val();
                if ((user_id_search == "") && (first_name_search == "") && (last_name_search == "")) {
                    $.msgBox({
                        title: "Alert",
                        content: "Search Criteria Not Available",
                        type: "alert",
                    });
                } else            {
                  $('#search_page_buttons').show();
                  $('#edit').show();
                  $('#delete').show();
                  $('#export').show();
                  $('#search-para').hide();
                  display_data();
                  $('.tab').show();
                  $('#filter').hide();
                  $('#back_search').show();
                  $('#show_all').hide();
                }
    }

    $('#filter').click(function() {
        filter_clicked=true;
          filter_click();
    });

    $(document).on('click', '#back_search', function() {
          back_search();
    });

    function back_search(){
      $('#search_page_buttons').hide();
      $('.tab').hide();
      $('#search-para').show();
      $('.tab tr').remove();
      $('.hide_show').show();
      $('#filter').show();
      $('#show_all').show();
      $('#back_search').hide();
      // $("#show_all,#search-para,#filter").addClass("show");
      // $("#back_search,#list-of-user").addClass("hide");
    }

    $(document).on('click', '#show_all', function() {
      show_all_link_clicked=true;
      filter_clicked=false;
        hide_buttons_show_all();
        show_table();

    });

    function hide_div_after_update() {
              $('#fourth_set_buttons').hide();
              $('#fifth_set_buttons').hide();
              $('#cancelButton').hide();
              $('#update_button').hide();
              $('#next_button_first').hide();
              $('#myTabs').hide();
              $('#role_table').hide();
              $('#right_table').hide();
              // $('#span_one').addClass('custom_click_tab_active');
              $('#span_two,#span_three,#span_four').removeClass('custom_click_tab_active');
              // $('#span_two,#span_three,#span_four').addClass('custom_click_tab');
              if(filter_clicked==true)
              {
                filter_clicked = false;
                filter_click();
              }
              else {
                $("#show_all_link").trigger("click");
                $("#show_all").trigger("click");
                $('#list-of-user').show();
              }
    }

    function hide_buttons_show_all() {
        $('.hide_show').hide();
        $('#search_page_buttons').show();
        $('#edit').show();
        $('#delete').show();
        $('#export').show();
        $('#search-para').hide();
        $('.tab').show();
        $('#Create').css("display", "none");
        $('#filter').hide();
        $('#Create').hide();
        $('#user_table_form').hide();
        $('#Create').hide();
        $('#user_table_form').hide();
        $('#list-of-user').show();
        $('#next').hide();
        $('#cancelButton').hide();
        $('#back_search').show();
        $('#show_all').hide();
    }

    $(document).on('click', '#delete_search', function() {
      var d = new Date();
      var month = d.getMonth()+1;
      var day = d.getDate();
      var deleted_at = d.getFullYear() + '-' +
          (month<10 ? '0' : '') + month + '-' +
          (day<10 ? '0' : '') + day;
      var id = $(this).data("id");
          user_id = results[id].user_id;
        $.msgBox({
            title: "Confirm",
            content: "Do you want to delete the data",
            type: "confirm",
            buttons: [{
                value: "Yes"
            }, {
                value: "No"
            }],
            success: function(result) {
                if (result == "Yes") {
                    $.ajax({
                        type: "POST",
                        url: "user_delete",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "user_id": user_id,
                            "deleted_at":deleted_at,
                        },
                        success: function() {
                            display_data();
                            $.msgBox({
                                title: "Message",
                                content: "User deleted successfully",
                                type: "info",
                            });
                        },
                        beforeSend: function() {

                        },
                        error: function() {}
                    });
                }
            }
        });
    });

    $(document).on('click', '#delete_showall', function() {
      var d = new Date();

      var month = d.getMonth()+1;
      var day = d.getDate();

      var deleted_at = d.getFullYear() + '-' +
          (month<10 ? '0' : '') + month + '-' +
          (day<10 ? '0' : '') + day;
      var id = $(this).data("id");
          user_id = results[id].user_id;
        $.msgBox({
            title: "Confirm",
            content: "Do you want to delete the data",
            type: "confirm",
            buttons: [{
                value: "Yes"
            }, {
                value: "No"
            }],
            success: function(result) {
                if (result == "Yes") {
                    $.ajax({
                        type: "POST",
                        url: "user_delete",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "user_id": user_id,
                            "deleted_at":deleted_at,
                        },
                        success: function() {
                            show_table();
                            $.msgBox({
                                title: "Message",
                                content: "User deleted successfully",
                                type: "info",
                            });
                        },
                        beforeSend: function() {

                        },
                        error: function() {}
                    });
                }
            }
        });
    });

    $(document).on('click', '#cancelButton', function() {
        $('#back_search').show();
        $('#myTabs').hide();
        $('#role_table').hide();
        $('#right_table').hide();
        $('.tab-content').hide();




        $('#Create').hide();
        $('#user_table_form').hide();
        $('#list-of-user').show();
        $('#search_page_buttons').show();
        $('#next').hide();
        $('#cancelButton').hide();
        $('#first_set_buttons').hide();
        $('#second_set_buttons').hide();
        $('#third_set_buttons').hide();
        $('#fourth_set_buttons').hide();
        $('#fifth_set_buttons').hide();
        $('#span_one,#span_two,#span_three,#span_four,#span_five').removeClass('custom_click_tab_active');
    });

    $(document).on('click', '.edit', function() {
        {
            show_change_screen();
            $('#Create').show();
            $('#myTabs').show();
            $('#next_button_first').show();

            $('#user_table_form').show();
            $('#role_table').show();
            $('#right_table').show();
            $('.tab-content').show();
            $('#search_page_buttons').hide();
            $('#cancelButton').show();
            $('#update_button').show();
            $('#list-of-user').hide();
            $('#next').show();
            $('.tab-content').show();
            $('#back_search').hide();
            $('#show_all').hide();

            var id = $(this).attr("id");
            // user_id = $(this).parents('tr:first').find('td:first').text();
            user_id = results[id].user_id;
            $('#user_id').val(results[id].user);
            $("#first_name").val(results[id].first_name);
            $("#last_name").val(results[id].last_name);
            $("#password").val(results[id].password);
            $("#repeat_password").val(results[id].password);
            $("#password_expires").val(results[id].password_expires);
            $("#valid_till").val(results[id].valid_till);
            $('#created_by').val(results[id].created_by);
            $('#created_date').val(results[id].created_at);
            $('#modified_by').val(results[id].updated_by);
            $('#modified_date').val(results[id].updated_at);

            $("#group_name").val(results[id].group_id);
            $("#display_roles").val(results[id].role_name);

            if (results[id].active == 0) {
                $('#active').prop('checked', false);
            } else {
                $('#active').prop('checked', true);
            }

            var group_id = results[id].group_id;
            $.ajax({
              type:"GET",
              url:"get_role_by_groupId",
              dataType:"json",
              data:{
                "_token": "{{ csrf_token() }}",
                "group_id": group_id
              },
              success:function(roles){
                $('#display_roles').html('');
                getRightsByRoloId(roles[0].role_id);
                for (var i = 0; i < roles.length; i++)
                {
                  var role_name = roles[i].role_name
                  var role_id   = roles[i].role_id
                  $('#display_roles').append("<option id ="+ role_id +" value="+ role_id +">"+role_name+"</option>");
                }
              }
            });


        }
    });

    function show_table() {
        $.ajax({
            type: "GET",
            url: "get_user",
            dataType: "json",
            success: function(data) {
                results = data;
                console.log(JSON.stringify(results));
                $('.tab tr').remove();
                $('.tab').append('<tr style="background-color: #D3D3D3;"><th style="display:none">User ID</th><th>User ID</th><th>First Name</th><th>Last Name</th><th>Group Name</th><th>Password Expires</th><th>Active</th><th>Edit</th><th>Delete</th></tr>');

                for (var i = 0; i < results.length; i++) {
                  if (results[i].active == 0) {
                      active_check = '<input type="checkbox" disabled readonly>';
                  } else {
                      active_check = '<input type="checkbox" checked disabled readonly>';
                  }
                    $('.tab').append('<tr data-id="' + i + '" class="double_click"><td class="user_id"  style="display:none">' + results[i].user_id + '</td><td>' + results[i].user + '</td><td>' + results[i].first_name + '</td><td>' + results[i].last_name + '</td><td>' + results[i].group_name + '</td><td>' + results[i].password_expires + '</td><td>' + active_check + '</td><td><button id="' + i + '" class="edit btn btn-default glyphicon glyphicon-edit edit_color" type="button"></button></td><td><button  data-id="' + i + '" id="delete_showall" type="button" class="btn btn-default delete_color" aria-label="Left Align"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td></tr>')
                }
            },
            beforeSend: function() {

            },
            error: function() {
            }
        });
    }

    function show_change_screen(){

          $("#first_set_buttons").show();
          $("#second_set_buttons").hide();
          $("#third_set_buttons").hide();
          $("#fourth_set_buttons").hide();
          $("#fifth_set_buttons").hide();

          $('#tab_one').show();
          $('#tab_two').hide();
          $('#tab_three').hide();
          $('#tab_four').hide();
          $('#tab_two').hide();
          $('#span_one').addClass('custom_click_tab_active');
          $('#span_two,#span_three,#span_four,#span_five').addClass('custom_click_tab');
    }

    $(document).on('dblclick', '.double_click', function() {

        show_change_screen();
        $('.tab-content').show();
        $('#myTabs').show();
        $('#next_button_first').show();
        $('#back_search').hide();
        $('#show_all').hide();
        $('#Create').show();
        $('#user_table_form').show();
        $('#role_table').show();
        $('#right_table').show();
        $('#search_page_buttons').hide();
        $('#cancelButton').show();
        $('#list-of-user').hide();
        $('#next').show();

        var id = $(this).data("id");
        user_id = results[id].user_id;
        $("#user_id").attr("data-id", results[id].user);
        $('#user_id').val(results[id].user);
        $("#first_name").val(results[id].first_name);
        $("#last_name").val(results[id].last_name);
        $("#password").val(results[id].password);
        $("#repeat_password").val(results[id].password);
        $("#password_expires").val(results[id].password_expires);
        $("#valid_till").val(results[id].valid_till);
        $('#created_by').val(results[id].created_by);
        $('#created_date').val(results[id].created_at);
        $('#modified_by').val(results[id].updated_by);
        $('#modified_date').val(results[id].updated_at);

        $("#group_name").val(results[id].group_id);
        $("#display_roles").val(results[id].role_name);

        if (results[id].active == 0) {
            $('#active').prop('checked', false);
        } else {
            $('#active').prop('checked', true);
        }

        var group_id = results[id].group_id;
        $.ajax({
          type:"GET",
          url:"get_role_by_groupId",
          dataType:"json",
          data:{
            "_token": "{{ csrf_token() }}",
            "group_id": group_id
          },
          success:function(roles){
            $('#display_roles').html('');
            getRightsByRoloId(roles[0].role_id);
            for (var i = 0; i < roles.length; i++)
            {
              var role_name = roles[i].role_name
              var role_id   = roles[i].role_id
              $('#display_roles').append("<option id ="+ role_id +" value="+ role_id +">"+role_name+"</option>");
            }
          }
        });
        $('#cancelButton').show();
        $('#update_button').show();

    });

    function display_data(){
        var user_id_search = $('#user_id_search').val();
        var first_name_search = $('#first_name_search').val();
        var last_name_search = $('#last_name_search').val();
        $.ajax({
            type: "POST",
            url: "user_display_show",
            data: {
                "_token": "{{ csrf_token() }}",
                "user_id_search": user_id_search,
                "first_name_search": first_name_search,
                "last_name_search": last_name_search,
            },
            success: function(data) {
                if (data != "0") {
                    results = data;
                    $('.tab tr').remove();
                    $('.tab').append('<tr style="background-color: #D3D3D3;"><th style="display:none">User ID</th><th>User ID</th><th>First Name</th><th>Last Name</th><th>Group Name</th><th>Password Expires</th><th>Active</th><th>Edit</th><th>Delete</th></tr>');

                    for (var i = 0; i < results.length; i++) {
                      if (results[i].active == 0) {
                          active_check = '<input type="checkbox" disabled readonly>';
                      } else {
                          active_check = '<input type="checkbox" checked disabled readonly>';
                      }
                        $('.tab').append('<tr data-id="' + i + '" class="double_click"><td class="user_id" style="display:none">' + results[i].user_id + '</td><td>' + results[i].user + '</td><td>' + results[i].first_name + '</td><td>' + results[i].last_name + '</td><td>' + results[i].group_name + '</td><td>' + results[i].password_expires + '</td><td>' + active_check + '</td><td><button id="' + i + '" class="edit btn btn-default glyphicon glyphicon-edit edit_color" type="button"></button></td><td><button  data-id="' + i + '" id="delete_search" type="button" class="btn btn-default delete_color" aria-label="Left Align"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td></tr>')
                    }
                } else {
                    if(filter_clicked == false){
                        back_search();
                    }
                    else {
                        back_search();
                        $.msgBox({
                            title: "Alert",
                            content: "Data not found",
                            type: "alert",
                        });
                    }

                }
            },
            beforeSend: function() {

            },
            error: function() {
            }
        });
    }

    $(document).on('click', '#update_button', function() {
      if ($('#user_table_form').valid()) {
                  $.msgBox({
                      title: "Confirm",
                      content: "Do you want to update the information?",
                      type: "confirm",
                      buttons: [{
                          value: "Yes"
                      }, {
                          value: "No"
                      }],
                      success: function(result) {
                          if (result == "Yes") {
                                    $('#list-of-user').hide();
                                    fun_update();
                                  }
                                  else {

                                  }
                                }
                            });
              }
    });

    function fun_update(){
          $('#list-of-user').hide();
          $('.tab-content').hide();
          var user = $('#user_id').val();
          var first_name = $('#first_name').val();
          var last_name = $('#last_name').val();
          var role_id = $('#display_roles').val();
          var group_id = $('#group_name').val();
          var password = $('#password').val();
          var password_expires = $('#password_expires').val();
          var active = $('#active').val();
          if ($('#active').is(":checked")) {
              active = 1;
          } else {
              active = 0;
          }
          var valid_till = $('#valid_till').val();

          $.ajax({
              type: "POST",
              url: "user_update",
              data: {
                  "_token": "{{ csrf_token() }}",
                  "user_id": user_id,
                  "user": user,
                  "first_name": first_name,
                  "last_name": last_name,
                  "role_id":role_id,
                  "group_id": group_id,
                  "password": password,
                  "password_expires": password_expires,
                  "active": active,
                  "valid_till":valid_till
              },
              success: function(data) {
                  $.msgBox({
                      title: "Alert",
                      content: "Updated successfully",
                      type: "info",
                  });
                  hide_div_after_update();
              },
              beforeSend: function() {
                  $('#msg').show();
              },
              error: function() {
                  $('#msg').text('Sorry we are not available');
              }
          });
    }

    var fetch_user_id;
    $('input[name="user_id_search"]').autoComplete({
        minChars: 0,
        source: function(term, response) {
            try {
                fetch_user_id.abort();
            } catch (e) {}
            fetch_user_id = $.getJSON('fetch_user_id', {
                user_id: term
            }, function(data) {
                response(data);
            });
        }
    });

    var fetch_first_name;
    $('input[name="first_name_search"]').autoComplete({
        minChars: 0,
        source: function(term, response) {
            try {
                fetch_first_name.abort();
            } catch (e) {}
            fetch_first_name = $.getJSON('fetch_first_name', {
                first_name: term
            }, function(data) {
                response(data);
            });
        }
    });


    var fetch_last_name;
    $('input[name="last_name_search"]').autoComplete({
        minChars: 0,
        source: function(term, response) {
            try {
                fetch_last_name.abort();
            } catch (e) {}
            fetch_last_name = $.getJSON('fetch_last_name', {
                last_name: term
            }, function(data) {
                response(data);
            });
        }
    });



    $('#next_button_first').click(function() {
      if ($('#user_table_form').valid()) {
          $('#first_set_buttons').hide();
          $('#tab_one').hide();
          $('#tab_two').show();
          $('#tab_three').hide();
          $('#tab_four').hide();
          $('#second_set_buttons').show();

          $('#span_two').addClass('custom_click_tab_active');
          $('#span_one').removeClass('custom_click_tab_active');
          $('#span_one,#span_three,#span_four,#span_five').addClass('custom_click_tab');
        }
    });

    $('#back_button_second').click(function() {
          $('#first_set_buttons').show();
          $('#tab_one').show();
          $('#tab_two').hide();
          $('#tab_three').hide();
          $('#tab_four').hide();
          $('#tab_two').hide();
          $('#second_set_buttons').hide();

          $('#span_one').addClass('custom_click_tab_active');
          $('#span_two').removeClass('custom_click_tab_active');
          $('#span_two,#span_three,#span_four,#span_five').addClass('custom_click_tab');
    });

});


</script>
@stop
@section('lower_band')
<div class="col-xs-12 lower_band">
   <button class="headerbuttons" id="back_search" type="button">Back</button>
   <button class="headerbuttons" id="filter" type="button">Search</button>
   <button class="headerbuttons" type="button" id="cancelButton" style="float: right; margin-right: 20px; display:none;">Cancel</button>
   <div class="first_set_buttons load" id="first_set_buttons">
      <button id="next_button_first" class="headerbuttons" type="button"  style="float: left; margin-right: 20px;">Next</button>
      <button id="update_button" class="headerbuttons" type="button"  style=" float: right; margin-right: 20px;">Update</button>
   </div>
   <div class="second_set_buttons load" id="second_set_buttons">
      <button id="back_button_second" class="headerbuttons" type="button"  style="float: left; margin-right: 20px;">Back</button>
      <button id="update_button" class="headerbuttons" type="button"  style=" float: right; margin-right: 20px;">Update</button>

   </div>
   <div class="third_set_buttons load" id="third_set_buttons">
      <button id="back_button_third" class="headerbuttons" type="button"  style="float: left; margin-right: 20px;">Back</button>
      <button id="next_button_third" class="headerbuttons" type="button"  style="float: left; margin-right: 20px;">Next</button>
   </div>
</div>
@stop
