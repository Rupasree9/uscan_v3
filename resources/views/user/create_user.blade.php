@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" href="css/user_master.css">
<script src="js/user_validation.js"></script>
@stop
@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="col-xs-4 display-title">
      <span>Create User</span>
   </div>
</div>
@stop
@section('content')
<div class="col-xs-12 content">
  <ul class="nav nav-tabs" id="">
     <li id="1" class="active"><span id="span_one" class="tab_one custom_click_tab_active"  data-toggle="tab" >Basic Details</span></li>
     <!-- <li id="2"><span id="span_two" class="tab_two custom_click_tab"  data-toggle="tab" >Plant Details</span></li>
     <li id="3"><span id="span_three" class="tab_three custom_click_tab"  data-toggle="tab" >Address</span></li>
     <li id="4"><span id="span_four" class="tab_four custom_click_tab" data-toggle="tab" >Contact</span></li> -->
  </ul>
   <form id="user_table">
      <table class="user_table" style="width:40%;">
         <!-- <tr>
            <td style="background-color: #eee;"><strong>User Details</strong></td>
            <td></td>
         </tr> -->
         <tr>
            <td>User Email ID</td>
            <td><input type="email" name="user_id" id="user_id"></td>
            <input id = "user_name_token_id" name="_token" value="{{ csrf_token() }}" style="display:none"></td>
         </tr>
         <tr>
            <td>First Name</td>
            <td><input type="text" name="first_name" id="first_name" required="required"></td>
         </tr>
         <tr>
            <td>Last Name</td>
            <td><input type="text" name="last_name" id="last_name"></td>
         </tr>
         <tr>
            <td>User Group Name</td>
            <td><select id="group_name" name="group_name">
            </select>
            </td>
         </tr>
         <tr>
            <td>Password</td>
            <td><input type="Password" name="password" id="password"></td>
         </tr>
         <tr>
            <td>Repeat Password</td>
            <td><input type="Password" name="repeat_password" id="repeat_password"></td>
         </tr>
         <tr>
            <td>Password Expires</td>
            <td><input type="date" name="pwd_expires" id="pwd_expires"></td>
         </tr>
         <tr>
            <td>Valid Till</td>
            <td><input type="date" name="valid_till" id="valid_till"></td>
         </tr>
         <tr>
            <td>Active</td>
            <td> <input type="checkbox" name="active" id="active"> </td>
         </tr>
      </table>
   <div id="role_table">
      <table class="role_table" style="width:30%;">
         <tr>
            <td style="background-color: #eee;"><strong>Role</strong></td>
            <td></td>
         </tr>
         <tr>
            <td>
               <select  id="display_roles" name="display_roles" disabled></select>
            </td>
            <td></td>
         </tr>
      </table>
   </div>
   <div id="right_table">
      <table class="right_table">
         <tr>
            <td  style="background-color: #eee;"><strong>Associated Rights</strong></td>
            <td></td>
         </tr>
         <tr>
            <td>
               <ul id="display_rights"></ul>
            </td>
            <td></td>
         </tr>
      </table>
   </div>

</form>
</div>

<script type="text/javascript">

jQuery.validator.addMethod('selectcheck', function (value) {
               return (value != '0');
       }, "");

   $(document).ready(function(){

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+7; //January is 0!

    var yyyy = today.getFullYear();
    if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm} today = yyyy+'-'+mm+'-'+dd;

    $('#pwd_expires').attr('value', today);


     $("#user_id").prop('required',true);

     $.ajax({
       type:"GET",
       url:"get_user_group",
       dataType:"json",
       data:{
         "_token": "{{ csrf_token() }}"
       },
       success:function(user_grp){
         for (var i = 0; i < user_grp.length; i++)
         {
           if(i == '0'){
             $('#group_name').append("<option id =\"0\" value=\"0\">Select</option>");
           }
           var group_name = user_grp[i].group_name
           var group_id   = user_grp[i].group_id
           $('#group_name').append("<option id ="+ group_id +" value="+ group_id +">"+group_name+"</option>");
         }
       }
     });

     $.ajax({
       type:"GET",
       url:"get_role",
       dataType:"json",
       data:{
        //  "_token": "{{ csrf_token() }}"
       },
       success:function(user_role){
         for (var i = 0; i < user_role.length; i++)
         {
           if(i == '0'){
             $('#display_roles').append("<option id =\"0\" value=\"0\">Select Group Name</option>");
           }
           var role_name = user_role[i].role_name
           var role_id   = user_role[i].role_id
           $('#display_roles').append("<option id ="+ role_id +" value="+ role_id +">"+role_name+"</option>");
         }
       }
     });

     $('#group_name').change(function(){
       var group_id = $('#group_name').val();
       $.ajax({
         type:"GET",
         url:"get_role_by_groupId",
         dataType:"json",
         data:{
           "_token": "{{ csrf_token() }}",
           "group_id": group_id
         },
         success:function(roles){

           if (roles.length==0) {
             $('#display_roles').html('');
             $('#display_rights').html('');
              $('#display_roles').append("<option id =\"0\" value=\"0\">Select Group Name</option>");

           }
           else {
             $('#display_roles').html('');
             getRightsByRoloId(roles[0].role_id);
             for (var i = 0; i < roles.length; i++)
             {
               var role_name = roles[i].role_name
               var role_id   = roles[i].role_id
               $('#display_roles').append("<option id ="+ role_id +" value="+ role_id +">"+role_name+"</option>");
             }
           }


         }
       });
     });

     $('#display_roles').change(function(){
             var role_id = $('#display_roles').val();
               getRightsByRoloId(role_id);
     });

     $(document).on('click', '#user_save_id', function(){
            if ($('#user_table').valid()) {
              $.msgBox({
                  title: "Confirm",
                  content: "Do you want to save the information?",
                  type: "confirm",
                  buttons: [{
                      value: "Yes"
                  }, {
                      value: "No"
                  }],
                  success: function(result) {
                      if (result == "Yes") {

               var user_id = $('#user_id').val();
               var first_name = $('#first_name').val();
               var last_name = $('#last_name').val();
               var role_id = $('#display_roles').val();
               var group_id = $('#group_name').val();
               var password = $('#password').val();
               var repeat_password = $('#repeat_password').val();
               var password_expires = $('#pwd_expires').val();
               var active = $('#active').val();
               if ($('#active').is(":checked")) {
                   active = 1;
               } else {
                   active = 0;
               }
               var valid_till = $('#valid_till').val();

               if ((user_id == "")) {
                 $.msgBox({
                     title: "Alert",
                     content: "User Email ID is mandatory",
                     type: "alert",
                 });
               } else

                       {
                         $.ajax({
                           type: "POST",
                           url: "insert_user",
                           data: {
                                         "_token": "{{ csrf_token() }}",
                                         "user_id": user_id,
                                         "first_name": first_name,
                                         "last_name": last_name,
                                         "role_id":role_id,
                                         "group_id": group_id,
                                         "password": password,
                                        //  "repeat_password":repeat_password,
                                         "password_expires": password_expires,
                                         "active": active,
                                         "valid_till":valid_till

                                   },
                           success: function(data) {
                             $.msgBox({
                                 title: "Message",
                                 content: "User added successfully",
                                 type: "info",
                             });
                               reset();
                           }
                         });
                       }
                      } else {

                      }
                  }
              });

                 }
      });

     function getRightsByRoloId(role_id) {
               $.ajax({
                 type:"GET",
                 url:"get_right_for_role",
                 dataType:"json",
                 data:{
                   "_token": "{{ csrf_token() }}",
                   "role_id": role_id
                 },
                 success:function(rights){
                   $('#display_rights').html('');

                   for (var i = 0; i < rights.length; i++)
                   {
                     var right_name = rights[i].right_name
                     var right_id   = rights[i].right_id
                     $('#display_rights').append("<li id ="+ right_id +" value="+ right_id +">"+right_name+"</li>");
                   }
                 }
               });
   }

     $(document).on('click', '#cancel_button', function(){
      reset();
   });

     function reset() {
         $('#user_table').trigger("reset");
         $('#role_table').trigger("reset");
         $('#right_table').trigger("reset");
         $('#display_roles').html('');
         $('#display_rights').html('');

         $.ajax({
           type:"GET",
           url:"get_role",
           dataType:"json",
           data:{
            //  "_token": "{{ csrf_token() }}"
           },
           success:function(user_role){
             for (var i = 0; i < user_role.length; i++)
             {
               if(i == '0'){
                 $('#display_roles').append("<option id =\"0\" value=\"0\">Select Group Name</option>");
               }
               var role_name = user_role[i].role_name
               var role_id   = user_role[i].role_id
               $('#display_roles').append("<option id ="+ role_id +" value="+ role_id +">"+role_name+"</option>");
             }
           }
         });
   }

   });

   $(window).load(function() {
     $("#master_data").trigger('click');
     $("#user_master").trigger('click');
     $("#user").trigger('click');
   });

</script>
@stop
@section('lower_band')
<div class="col-xs-12 lower_band">
   <button id="cancel_button" class="headerbuttons" type="button" style="float: right; margin-right: 20px;">Cancel</button>
   <button id="user_save_id" class="headerbuttons" type="button"  style="float: right; margin-right: 20px;">Save</button>
   <!-- <input class="headerbuttons" type="button" value="Save" id="user_save_id"/> -->
   <!-- <input class="headerbuttons" type="button" onclick="reset()" id="CancelButton" value="Cancel"/> -->
</div>
@stop
