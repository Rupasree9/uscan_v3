@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" href="css/user_master.css">
@stop

@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="display-title">
      Display User Roles
   </div>
</div>
@stop

@section('content')
<div class="col-xs-12 content">
  <table id="display_role_table">  </table>
</div>


<script type="text/javascript">
  $(document).ready(function(){
    $.ajax({
      type:"GET",
      url:"get_roles",
      dataType:"json",
      data:{
        "_token": "{{ csrf_token() }}"
      },

      success:function(roles){
        for (var i = 0; i < roles.length; i++)
        {
          if(i == 0){
            $('#display_role_table').append('<tr style="background-color: #D3D3D3;"><th>Role Name</th><th>Role Description</th><th>Edit</th><th>Delete</th></tr>');
          }
          var role = roles[i];
          $('#display_role_table').append('<tr data-id="' + i + '" class="double_click"><td>' + role.role_name + '</td><td>' + role.role_desc + '</td><td><a href="#">Edit </a> </td><td><a href="#" id="delete_role">Delete </a></td></tr>')
        }
      }
    });

// $('#delete_role').click(function(){
//   var role_id = $(this).val();
//   $.ajax({
//         type:'POST',
//         url:'delete_role',
//         data:{
//           "_token": "{{ csrf_token() }}",
//           'role_id':role_id
//         },
//         success: function(data){
//           alert(data);
//          }
//   });
// })

});

  $(window).load(function() {
      console.log("uder load funciton");
      $("#master_data").trigger('click');
      $("#user_master").trigger('click');
      $("#user_roles").trigger('click');
  });
</script>
@stop
