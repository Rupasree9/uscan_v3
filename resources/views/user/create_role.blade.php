
@extends('layouts.uscan_master_page')
@section('header')
<!-- <link rel="stylesheet" href="css/user_master.css"> -->
@stop

@section('upper_band')
<div class="col-xs-12 upper_band">
     <div class="col-xs-4 display-title">
     <span>Create User Role</span>
     </div>
</div>
@stop

@section('content')
<div class="col-xs-12 content">
      <table style="width:50%;" >
        <tr>
           <td style="background-color: #eee;"><strong>User Role Details</strong></td>
           <td></td>
        </tr>

        <tr>
          <td>Role Name</td>
          <td><input type="text" name="role_name" id="role_name"></td>
        </tr>
        <tr>
          <td>Role Description</td>
          <td><input type="text" name="role_desc" id="role_desc"></td>
        </tr>
        <tr>
          <td>Assign Rights</td>
          <td>
            <select multiple id="display_rights"></select>
          </td>
        </tr>

     </table>
  </div>

  <script type="text/javascript">
    $(document).ready(function(){
      $.ajax({
        type:"GET",
        url:"get_rights",
        dataType:"json",
        data:{
          "_token": "{{ csrf_token() }}"
        },
        success:function(rights){
          for (var i = 0; i < rights.length; i++)
          {
            var right_name = rights[i].right_name
            var right_id   = rights[i].right_id
            $('#display_rights').append("<option id ="+ right_id +" value="+ right_id +">"+right_name+"</option>");
          }
        }
      });

      $(document).on('click', '#role_button_save_id', function(){

                    var role_name = $('#role_name').val();
                    var role_desc = $('#role_desc').val();
                    var assign_rights = $('#display_rights').val();

                    if ((role_desc == "") || (role_name == "") || (assign_rights == "")) {
                      alert("Please Enter All the values to Continue");
                    } else

                    {
                      $.ajax({
                        type: "POST",
                        url: "insert_role",
                        data: {
                                      "_token": "{{ csrf_token() }}",
                                      "role_name": role_name,
                                      "role_desc": role_desc,
                                      "assigned_rights": assign_rights
                                },
                        success: function(data) {
                          alert(data);
                          // $('#parent-table')[0].reset();
                        },
                        beforeSend: function() {
                        },
                        error: function() {
                        }
                      });
                    }
    });

    // if(getUrlVars()["role_id"] != null) {
    //     getRoleByRoleId();
    // }

    function getRoleByRoleId() {
      $.ajax({
        type:"GET",
        url:"get_role_by_id",
        dataType:"json",
        data:{
          "_token": "{{ csrf_token() }}",
          role_id : roleId
        },
        success:function(role){
          var role_name = $('#role_name').val() = role.role_name;
          var role_desc = $('#role_desc').val() = role.role_desc;
          var assign_rights = $('#display_rights') = role.assinged_rights;
        }
      });
    }

  });

  $(window).load(function() {
      console.log("uder load funciton");
      $("#master_data").trigger('click');
      $("#user_master").trigger('click');
      $("#user_roles").trigger('click');
  });

  // function getUrlVars() {
  //   var vars = [], hash;
  //   var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
  //   for(var i = 0; i < hashes.length; i++)
  //   {
  //       hash = hashes[i].split('=');
  //       vars.push(hash[0]);
  //       vars[hash[0]] = hash[1];
  //   }
  //   return vars;
  // }

  </script>
@stop

@section('lower_band')
  	<div class="col-xs-12 lower_band">
  		<input class="headerbuttons" type="button" value="Save" id="role_button_save_id">
  		<input class="headerbuttons" type="button" id="CancelButton" value="Cancel">
  	</div>
@stop
