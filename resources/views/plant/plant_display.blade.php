@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" type="text/css" href="css/plant.css">
<link rel="stylesheet" type="text/css" href="css/plant_change.css">
@stop
@section('display_screen_name')
@stop
@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="display-title">
      Display Plant Master
   </div>
   <div class="col-xs-2" style="float: right;text-align:center;line-height:30px;">
      <span  id="show_all" style="">Show All Plants</span>
   </div>
</div>
@stop
@section('content')
<div class="col-xs-12 content no_scroll" id="content no_scroll">
  <div class="loading">
     <img src="images/loading.gif" height="50px;">
  </div>
   <div id="search-para no_scroll">
      <table class="hide_show" style="width:55%;">
         <tr>
            <td style="background-color: #eee;">Search Parameters</td>
         </tr>
         <tr style="display:none;">
            <td>Plant ID</td>
            <td><input type="text" id="plant_id" name="plant_id" autocomplete="off"></td>
         </tr>
         <tr>
            <td>Plant Name</td>
            <td>
               <input type="text" id="plant_name" name="plant_name" autocomplete="off" style="position:relative">
               <!-- <div id="result" class="result groove"></div> -->
            </td>
         </tr>
         <tr>
            <td>Plant Code</td>
            <td><input type="text" id="plant_code" autocomplete="off"  name="plant_code"></td>
         </tr>
         <tr>
            <td>DUNS Number</td>
            <td><input type="text" id="duns_no"  autocomplete="off" name="duns_no"></td>
         </tr>
      </table>
   </div>
   <div id="list-of-plant"  class="div1 overflow table_height">
      <table style="width:100%;" class="tab">
      </table>
   </div>
   <div class="tab-content load">
      <ul class="nav nav-tabs  " id="myTabs">
         <li id="1" class="active"><span id="span_one" class="tab_one "  data-toggle="tab" >Basic Details</span></li>
         <li id="2"><span id="span_two" class="tab_two "  data-toggle="tab" >Plant Details</span></li>
         <li id="3"><span id="span_three" class="tab_three "  data-toggle="tab" >Address</span></li>
         <li id="4"><span id="span_four" class="tab_four" data-toggle="tab" >Contact</span></li>
         <li id="5"><span id="span_five" class="tab_five" data-toggle="tab" >Additional Details</span></li>
      </ul>
      <div id="tab_one" >
         <div id="Create " >
            <form id="plant-table" autocomplete="off">
               <table style="width:40%;">
                  <tr>
                     <td></td>
                  </tr>

                  <tr>
                     <td>Plant Name</td>
                     <td><input type="text" id = "plant_name_display"  disabled></td>
                  </tr>
                  <tr>
                     <td>Plant Code</td>
                     <td><input type="text" id = "plant_code_display"  disabled></td>
                  </tr>
                  <tr>
                     <td>DUN's No</td>
                     <td><input type="text" id = "duns_no_display"  disabled></td>
                  </tr>
                  <tr>
                     <td>QMIS ID</td>
                     <td><input type="text" id = "qmis_id" disabled></td>
                  </tr>
                  <tr>
                     <td>Active</td>
                     <td><input type="checkbox" id="activeid"  disabled></td>
                  </tr>
               </table>
            </form>
         </div>
      </div>
      <div id="tab_two" class="load">
         <form id="second_form" >
            <table style="width:80%; margin-bottom: 20px">
               <tr>
                  <td></td>
               </tr>
               <tr>
                  <td>Business Unit</td>
                  <td><input type="text" name=""  id = "business_unit" disabled></td>
                  <td>Logistics Manager</td>
                  <td><input type="text"  id = "logistics_manager" disabled></td>
               </tr>
               <tr>
                  <td>Product Group</td>
                  <td><input type="text"  name="" id = "product_group" disabled></td>
                  <td>Finance Manager (Controller)</td>
                  <td><input type="text"  id = "finance_manager" disabled></td>
               </tr>
               <tr>
                  <td>Function</td>
                  <td><input type="text"  name="" id = "function_plant" disabled></td>
                  <td>Human Resource Manager (HR)</td>
                  <td><input type="text"   id = "hr_manager" disabled></td>
               </tr>
               <tr>
                  <td>ERP System</td>
                  <td><input type="text"  name="" id = "erp_system" disabled></td>
                  <td>Plant Warranty Champion (PWC)</td>
                  <td><input type="text"   id = "plant_warranty_champion" disabled></td>
               </tr>
               <tr>
                  <td>Quality Manager</td>
                  <td><input type="text"  name="" id = "quality_manager" disabled></td>
                  <td>Energy Champion</td>
                  <td><input type="text"  id = "energy_champion" disabled></td>
               </tr>
               <tr>
                  <td>Plant Manager</td>
                  <td><input type="text"  id = "plant_manager" disabled></td>
                  <td>Health and Safety Coordinator</td>
                  <td><input type="text"   id = "health_and_safety_coordinator" disabled></td>
               </tr>
               <tr>
                  <td>Environmental Coordinator</td>
                  <td><input type="text"  id = "environmental_coordinator" disabled></td>
               </tr>
            </table>
         </form>
      </div>
      <div id="tab_three" class="load">
         <form id="third_form" >
            <table class="table-responsive" style="width:40%; margin-bottom: 20px">
               <tr >
                  <!-- <td class="details_color"><strong>Address </strong></td> -->
                  <td></td>
               </tr>
               <tr>
                  <td>Street </td>
                  <td><input type="text"  id = "street" disabled></td>
               </tr>
               <tr>
                  <td>City</td>
                  <td><input type="text"  id = "city" disabled></td>
               </tr>
               <tr>
                  <td>State</td>
                  <td><input type="text"  id = "state" disabled></td>
               </tr>
               <tr>
                  <td>Country</td>
                  <td><input type="text"   id = "country" disabled></td>
               </tr>
               <tr>
                  <td>Region</td>
                  <td><input type="text"  id = "region" disabled></td>
               </tr>
               <tr>
                  <td>Zip code</td>
                  <td><input type="text"   id = "zip_code" disabled></td>
               </tr>
            </table>
         </form>
      </div>
      <div id="tab_four" class="load">
         <form id= "additional">
            <table style="width:40%; margin-bottom: 20px">
               <tr>
                  <td></td>
               </tr>
               <tr>
                  <td>Name </td>
                  <td><input type="text"  id="contact_name" disabled></td>
               </tr>
               <tr>
                  <td>Mail ID</td>
                  <td><input type="email"  id="contact_email" disabled></td>
               </tr>
               <tr>
                  <td>Contact Number</td>
                  <td><input type="text"  id="contact_no" disabled></td>
               </tr>
            </table>
         </form>
      </div>
      <div id="tab_five" class="load">
         <form id= "additional_details">
            <table style="width:40%; margin-bottom: 20px">
               <tr>
                  <td></td>
               </tr>
               <tr>
                  <td>Created By</td>
                  <td><input id ="created_by" type="text" name="" disabled></td>
               </tr>
               <tr>
                  <td>Created Date</td>
                  <td><input id ="created_date" type="text" disabled></td>
               </tr>
               <tr>
                  <td>Modified By</td>
                  <td><input id = "modified_by" type="text" disabled></td>
               </tr>
               <tr>
                  <td>Modified Date</td>
                  <td><input id = "modified_date" type="text" disabled></td>
               </tr>
            </table>
         </form>
      </div>
   </div>
   <script type="text/javascript">
      $(window).load(function() {
          $("#master_data").trigger('click');
          $("#plant_master").trigger('click');
          $("#back_display").hide();
           $('.tab-content').hide();

          $("#first_set_buttons").hide();
          $("#second_set_buttons").hide();
          $("#third_set_buttons").hide();
          $("#fourth_set_buttons").hide();
          $("#fifth_set_buttons").hide();

          $('#tab_one').show();
          $('#tab_two').hide();
          $('#tab_three').hide();
          $('#tab_four').hide();
          $('#tab_five').hide();

          $('#span_one').addClass('custom_click_tab_active');
          $('#span_two,#span_three,#span_four,#span_five').addClass('custom_click_tab');
      });

      $(document).ready(function() {
          var results = "";
          var active_check;




          $(document).on('click', '#show_all', function() {
              $('.loading').css('display', 'block');
              $('.tab-content').hide();
              $('#search-para').hide();
              $('.hide_show').hide();
              $('#back').show();
              $('#filter').hide();
              $('.tab').show();
              $('#show_all').hide();
              $.ajax({
                  type: "GET",
                  url: "plant_display_show_all",
                  dataType: "json",
                  success: function(data) {
                      results = data;
                      console.log(JSON.stringify(results));
                      $('.tab tr').remove();
                      $('.tab').append('<tr style="background-color: #D3D3D3;"><th>Plant Name</th><th>Plant Code</th><th>DUNS No.</th><th>QMIS ID</th><th>Business Unit</th><th>Plant Manager</th><th>Finance Manager</th><th>Active</th></tr>');

                      for (var i = 0; i < results.length; i++) {
                          if (results[i].active == 0) {
                              active_check = '<input type="checkbox" disabled readonly>';
                          } else {
                              active_check = '<input type="checkbox" checked disabled readonly>';
                          }
                          $('.tab').append('<tr data-id="' + i + '" class="double_click"><td style="display:none;">' + results[i].plant_id + '</td><td>' + results[i].plant_name + '</td><td>' + results[i].plant_code + '</td><td>' + results[i].plant_duns_no + '</td><td>' + results[i].qmis_id + '</td><td>' + results[i].business_unit + '</td><td>' + results[i].plant_manager + '</td><td>' + results[i].finance_manager + '</td><td>' + active_check + '</td></tr>')
                      }
                      $('.loading').css('display', 'none');
                  },
                  beforeSend: function() {
                    $('.loading').css('display', 'block');
                  },
                  error: function() {
                      $('.loading').css('display', 'none');
                  }
              });
              //$('.loading').css('display', 'none');
          });

          $(document).on('click', '#filter', function() {
            $('.tab-content').hide();
            $('#list-of-plant').show();

            var plant_id = $('#plant_id').val();
            var plant_name = $('#plant_name').val();
            var plant_code = $('#plant_code').val();
            var duns_no = $('#duns_no').val();

            if ((plant_id == "") && (plant_name == "") && (plant_code == "") && (duns_no == "")) {

                $.msgBox({
                    title: "Alert",
                    content: "Search Criteria Not Available",
                    type: "alert",
                });
            } else            {
              $('#search-para').hide();
              display_data();
              // $('#search-para').hide();
              $('.hide_show').hide();
              $('#back').show();
              $('.tab').show();
              $('#filter').hide();
              $('#show_all').hide();
            }

          });

          $(document).on('click', '#back', function() {
              $('#back').hide();
              $('#filter').show();
              $('.tab').hide();
              $('.hide_show').show()
              $('.tab tr').remove();
              $('#show_all').show();
              $('.tab-content').hide();
          });

          $(document).on('click', '#back_display', function() {
              $('#back').show();
              $('#back_display').hide();
              $('.tab').show();
              $('#show_all').hide();
              $('#list-of-plant').show();
              $('.tab-content').hide();

              $("#first_set_buttons").hide();
          });

          function display_data() {
              var plant_id = $('#plant_id').val();
              var plant_name = $('#plant_name').val();
              var plant_code = $('#plant_code').val();
              var duns_no = $('#duns_no').val();
              $.ajax({
                  type: "POST",
                  url: "plant_display_show",
                  data: {
                      "_token": "{{ csrf_token() }}",
                      "plant_id": plant_id,
                      "plant_name": plant_name,
                      "plant_code": plant_code,
                      "duns_no": duns_no,
                  },
                  success: function(data) {
                      if (data != "0") {
                          results = data;

                          $('.tab tr').remove();
                          $('.tab').append('<tr style="background-color: #D3D3D3;"><th>Plant Name</th><th>Plant Code</th><th>DUNS No.</th><th>QMIS ID</th><th>Business Unit</th><th>Plant Manager</th><th>Finance Manager</th><th>Active</th></tr>');

                          for (var i = 0; i < results.length; i++) {
                              if (results[i].active == 0) {
                                  active_check = '<input type="checkbox" disabled readonly>';
                              } else {
                                  active_check = '<input type="checkbox" checked disabled readonly>';
                              }
                              $('.tab').append('<tr data-id="' + i + '" class="double_click"><td style="display:none;">' + results[i].plant_id + '</td><td>' + results[i].plant_name + '</td><td>' + results[i].plant_code + '</td><td>' + results[i].plant_duns_no + '</td><td>' + results[i].qmis_id + '</td><td>' + results[i].business_unit + '</td><td>' + results[i].plant_manager + '</td><td>' + results[i].finance_manager + '</td><td>' + active_check + '</td></tr>')
                          }
                      } else {
                        $.msgBox({
                            title: "Alert",
                            content: "Data not found!!!",
                            type: "alert",
                        });
                        $('#search-para').show();
                        $('.hide_show').show();
                        $('#back').hide();
                        $('#filter').show();
                        $('#show_all').show();
                      }
                  },
                  beforeSend: function() {

                  },
                  error: function() {
                      alert("error!!");
                  }
              });
          }

          $(document).on('click', '.search_result', function() {
              var name = $(this).closest('tr').find('.search_result').text();
              $('#plant_name').val(name);
              $('#result').hide();
          });

          $('#next').click(function() {
              $('#change-table').hide();
              $('#back').show();
              $('#next').hide();
          });

          $('#back').click(function() {
              $('#change-table').show();
              $('.actiondiv').show();
              $('#back').hide();
              $('#next').show();
          });

          $('#search').click(function() {
              $('#change-table').show();
              $('#copy').hide();
              $('#Change').show();
              $('#export').show();
              $('.resetgap').show();
              $('#next').show();
              $('#search').hide();
              $('#search-para').hide();
              $('.save-cancel').show();
              $('#save, #CancelButton').hide();
              $('#change-table input[type=text], input[type=date],input[type=checkbox],input[type=email]').prop('readonly', true);
              $('#change-table input[type=text], input[type=date],input[type=checkbox],input[type=email]').css('background-color', '#eee');
              $('#additional input[type=text], input[type=date],input[type=checkbox],input[type=email]').prop('readonly', true);
              $('#additional input[type=text], input[type=date],input[type=checkbox],input[type=email]').css('background-color', '#eee');
              $('.main-div').css('height', '420px');
          });

          $('#Change').click(function() {
              $('#change-table input[type=text], input[type=date],input[type=checkbox],input[type=email]').prop('readonly', false);
              $('#change-table input[type=text], input[type=date],input[type=checkbox],input[type=email]').css('background-color', '#fff');
              $('#additional input[type=text], input[type=date],input[type=checkbox],input[type=email]').prop('readonly', false);
              $('#additional input[type=text], input[type=date],input[type=checkbox],input[type=email]').css('background-color', '#fff');
              $('#plant-id').attr('readonly', true);
              $('#plant-id').css('background-color', '#eee');
              $('.save-cancel').show();
              $('#delete').show();
              $('#copy').show();
              $('#save, #CancelButton').show();
          });

          $('#CancelButton').click(function() {
              $('#search-para').show();
              $('#search').show();
              $('.resetgap').show();
              $('#change-table').hide();
              $('#copy').hide();
              $('#Change').hide();
              $('#delete').hide();
              $('#export').hide();
              $('.save-cancel').hide();
              $('.main-div').css('height', '0px');
              $('#span_one,#span_two,#span_three,#span_four,#span_five').removeClass('custom_click_tab_active');
          });

          var fetch_plant_name;
          $('input[name="plant_name"]').autoComplete({
              minChars: 0,
              source: function(term, response) {
                  try {
                      fetch_plant_name.abort();
                  } catch (e) {}
                  fetch_plant_name = $.getJSON('fetch_plant_name', {
                      plant_name: term
                  }, function(data) {
                      response(data);
                  });
              }
          });

          var fetch_plant_id;
          $('input[name="plant_id"]').autoComplete({
              minChars: 0,
              source: function(term, response) {
                  try {
                      fetch_plant_id.abort();
                  } catch (e) {}
                  fetch_plant_id = $.getJSON('fetch_plant_id', {
                      plant_id: term
                  }, function(data) {
                      response(data);
                  });
              }
          });

          // var fetch_plant_code;
          // $('input[name="plant_code"]').autoComplete({
          //
          //   // alert(bla);
          //     minChars: 0,
          //     source: function(term, response) {
          //         try {
          //             fetch_plant_code.abort();
          //         } catch (e) {}
          //         fetch_plant_code = $.getJSON('fetch_plant_code', {
          //             // var bla =
          //             plant_code: term,
          //             plant_name_for_fetch : $('#plant_name').val()
          //         }, function(data) {
          //             response(data);
          //         });
          //     }
          // });

          $('input[name="plant_code"]').autoComplete({
               minChars: 0,
              source: function(term, response){
                  $.getJSON('fetch_plant_code', {
                    plant_code: term,
                    plant_name_for_fetch : $('#plant_name').val()
                  }, function(data){ response(data); });
              }
          });

          var fetch_plant_duns_no;
          $('input[name="duns_no"]').autoComplete({
              minChars: 0,
              source: function(term, response) {
                  try {
                      fetch_plant_duns_no.abort();
                  } catch (e) {}
                  fetch_plant_duns_no = $.getJSON('fetch_plant_duns_no', {
                      duns_no: term
                  }, function(data) {
                      response(data);
                  });
              }
          });



          $(document).on('dblclick', '.double_click', function() {
              show_change_screen();
              $('.tab-content').show();
              $('.tab_one').trigger('click');
              $('#back').hide();
              $('#show_all').hide();
              $('#back_display').show();
              var id = $(this).data("id");
              $('#Create').show();
              $('#plant-table').show();
              // $('#additional').hide();
              $('#search_page_buttons').hide();
              $('#cancelButton').show();
              plant_id = $(this).parents('tr:first').find('td:first').text();
              $('#Create').show();
              $('#list-of-plant').hide();
              $('#next').show();
              $('#business_unit').val(results[id].business_unit);
              $('#plant_name_display').val(results[id].plant_name);
              $('#product_group').val(results[id].product_group);
              $('#plant_code_display').val(results[id].plant_code);
              $('#function_plant').val(results[id].function);
              $('#duns_no_display').val(results[id].plant_duns_no);
              $('#qmis_id').val(results[id].qmis_id);
              $('#erp_system').val(results[id].erp_system);
              $('#quality_manager').val(results[id].quality_manager);
              $('#plant_manager').val(results[id].plant_manager);
              $('#logistics_manager').val(results[id].logistics_manager);
              $('#finance_manager').val(results[id].finance_manager);
              $('#hr_manager').val(results[id].hr_manager);
              $('#plant_warranty_champion').val(results[id].plant_warranty_champion);
              $('#energy_champion').val(results[id].energy_champion);
              $('#health_and_safety_coordinator').val(results[id].health_and_safety_coordinator);
              $('#environmental_coordinator').val(results[id].environmental_coordinator);
              $('#contact_name').val(results[id].contact_name);
              $('#contact_email').val(results[id].contact_email_id);
              $('#contact_no').val(results[id].contact_number);
              $('#street').val(results[id].street);
              $('#city').val(results[id].city);
              $('#state').val(results[id].state);
              $('#country').val(results[id].country);
              $('#region').val(results[id].region);
              $('#created_by').val(results[id].created_by);
              $('#created_date').val(results[id].created_at);
              $('#modified_by').val(results[id].updated_by);
              $('#modified_date').val(results[id].updated_at);
              $('#zip_code').val(results[id].zip_code);
              plant_id = results[id].plant_id;

              if (results[id].active == 0) {
                  $('#activeid').prop('checked', false);
              } else {
                  $('#activeid').prop('checked', true);
              }
          });

          function show_change_screen()
          {
                $("#first_set_buttons").show();
                $("#second_set_buttons").hide();
                $("#third_set_buttons").hide();
                $("#fourth_set_buttons").hide();
                $("#fifth_set_buttons").hide();

                $('#tab_one').show();
                $('#tab_two').hide();
                $('#tab_three').hide();
                $('#tab_four').hide();
                $('#tab_five').hide();
                $('#span_one').addClass('custom_click_tab_active');
                $('#span_two,#span_three,#span_four,#span_five').addClass('custom_click_tab');
          }

          $('#next_button_first').click(function() {
                $('#first_set_buttons').hide();
                $('#tab_one').hide();
                $('#tab_two').show();
                $('#tab_three').hide();
                $('#tab_four').hide();
                $('#tab_five').hide();
                $('#second_set_buttons').show();

                $('#span_two').addClass('custom_click_tab_active');
                $('#span_one').removeClass('custom_click_tab_active');
                $('#span_one,#span_three,#span_four,#span_five').addClass('custom_click_tab');
          });

          $('#back_button_second').click(function() {
                $('#first_set_buttons').show();
                $('#tab_one').show();
                $('#tab_two').hide();
                $('#tab_three').hide();
                $('#tab_four').hide();
                $('#tab_five').hide();
                $('#second_set_buttons').hide();

                $('#span_one').addClass('custom_click_tab_active');
                $('#span_two').removeClass('custom_click_tab_active');
                $('#span_two,#span_three,#span_four,#span_five').addClass('custom_click_tab');
          });

          $('#next_button_second').click(function() {
                $('#third_set_buttons').show();
                $('#tab_one').hide();
                $('#tab_two').hide();
                $('#tab_three').show();
                $('#tab_four').hide();
                $('#tab_five').hide();
                $('#second_set_buttons').hide();

                $('#span_three').addClass('custom_click_tab_active');
                $('#span_two').removeClass('custom_click_tab_active');
                $('#span_two,#span_one,#span_four,#span_five').addClass('custom_click_tab');
          });

          $('#back_button_third').click(function() {
                $('#third_set_buttons').hide();
                $('#tab_one').hide();
                $('#tab_two').show();
                $('#tab_three').hide();
                $('#tab_four').hide();
                $('#tab_five').hide();
                $('#second_set_buttons').show();

                $('#span_two').addClass('custom_click_tab_active');
                $('#span_three').removeClass('custom_click_tab_active');
                $('#span_one,#span_three,#span_four,#span_five').addClass('custom_click_tab');
          });

          $('#next_button_third').click(function() {
                $('#fourth_set_buttons').show();
                $('#tab_one').hide();
                $('#tab_two').hide();
                $('#tab_three').hide();
                $('#tab_four').show();
                $('#tab_five').hide();
                $('#third_set_buttons').hide();

                $('#span_four').addClass('custom_click_tab_active');
                $('#span_three').removeClass('custom_click_tab_active');
                $('#span_two,#span_one,#span_three,#span_five').addClass('custom_click_tab');
          });

          $('#back_button_fourth').click(function() {
                $('#fourth_set_buttons').hide();
                $('#tab_one').hide();
                $('#tab_two').hide();
                $('#tab_three').show();
                $('#tab_four').hide();
                $('#tab_five').hide();
                $('#third_set_buttons').show();

                $('#span_three').addClass('custom_click_tab_active');
                $('#span_four').removeClass('custom_click_tab_active');
                $('#span_one,#span_two,#span_four,#span_five').addClass('custom_click_tab');
          });

          $('#next_button_fourth').click(function() {
                $('#fifth_set_buttons').show();
                $('#tab_one').hide();
                $('#tab_two').hide();
                $('#tab_three').hide();
                $('#tab_four').hide();
                $('#tab_five').show();
                $('#fourth_set_buttons').hide();

                $('#span_five').addClass('custom_click_tab_active');
                $('#span_four').removeClass('custom_click_tab_active');
                $('#span_two,#span_one,#span_three,#span_four').addClass('custom_click_tab');
          });

          $('#back_button_fifth').click(function() {
                $('#fourth_set_buttons').show();
                $('#tab_one').hide();
                $('#tab_two').hide();
                $('#tab_three').hide();
                $('#tab_four').show();
                $('#tab_five').hide();
                $('#fifth_set_buttons').hide();

                $('#span_four').addClass('custom_click_tab_active');
                $('#span_five').removeClass('custom_click_tab_active');
                $('#span_one,#span_two,#span_four,#span_five').addClass('custom_click_tab');
          });

          $("#activeid").on('keydown', '#textbox', function(e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode == 9) {
                  e.preventDefault();
                  alert("pavan tab")
                }
              });

              $(document).on('click', '#cancelButton', function() {
                  $('#back').show();
                  $('#Create').hide();
                  $('#plant-table').hide();
                  $('.tab-content').hide();
                  $('#list-of-plant').show();
                  $('#search_page_buttons').show();
                  $('#next').hide();
                  $('#cancelButton').hide();
                  $('#first_set_buttons').hide();
                  $('#second_set_buttons').hide();
                  $('#third_set_buttons').hide();
                  $('#fourth_set_buttons').hide();
                  $('#fifth_set_buttons').hide();
                  $('#span_one,#span_two,#span_three,#span_four,#span_five').removeClass('custom_click_tab_active');
              });

      });
   </script>
</div>
@stop
@section('lower_band')
<div class="col-xs-12 lower_band">
   <button class="headerbuttons" id="back" type="button">Back</button>
   <button class="headerbuttons" id="filter" type="button" style="float: Left;">Search</button>
   <button class="headerbuttons" type="button" id="cancelButton" style="float: right; margin-right: 20px; display:none;">Cancel</button>
   <div class="first_set_buttons load" id="first_set_buttons">
      <button class="headerbuttons" id="back_display" type="button" style=" margin-right: 20px;">Back</button>
      <button id="next_button_first" class="headerbuttons" type="button"  style="margin-right: 20px;">Next</button>
   </div>
   <div class="second_set_buttons load" id="second_set_buttons">
      <button id="back_button_second" class="headerbuttons" type="button"  style=" margin-right: 20px;">Back</button>
      <button id="next_button_second" class="headerbuttons" type="button"  style="margin-right: 20px;">Next</button>
   </div>
   <div class="third_set_buttons load" id="third_set_buttons">
      <button id="back_button_third" class="headerbuttons" type="button"  style="margin-right: 20px;">Back</button>
      <button id="next_button_third" class="headerbuttons" type="button"  style="margin-right: 20px;">Next</button>
   </div>
   <div class="fourth_set_buttons load" id="fourth_set_buttons">
      <button id="back_button_fourth" class="headerbuttons" type="button"  style=" margin-right: 20px;">Back</button>
      <button id="next_button_fourth" class="headerbuttons" type="button"  style="margin-right: 20px;">Next</button>
   </div>
   <div class="fifth_set_buttons load" id="fifth_set_buttons">
      <button id="back_button_fifth" class="headerbuttons" type="button"  style=" margin-right: 20px;">Back</button>
   </div>
</div>
@stop
