@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" type="text/css" href="css/plant.css">
<script src="js/plant_validation.js"></script>
@stop
@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="display-title" >
      <p>Create Plant Master</p>
   </div>
</div>
@stop
@section('content')
<script type="text/javascript">

</script>
<div class="col-xs-12 content">
   <ul class="nav nav-tabs" id="myTabs">
      <li id="1" class="active"><span id="span_one" class="tab_one custom_click_tab_active"  data-toggle="tab" >Basic Details</span></li>
      <li id="2"><span id="span_two" class="tab_two custom_click_tab"  data-toggle="tab" >Plant Details</span></li>
      <li id="3"><span id="span_three" class="tab_three custom_click_tab"  data-toggle="tab" >Address</span></li>
      <li id="4"><span id="span_four" class="tab_four custom_click_tab" data-toggle="tab" >Contact</span></li>
   </ul>
   <div class="tab-content">
      <div id="tab_one" class="">
         <form id="plant-table" autocomplete="off">
            <table style="width:40%;">
               <tr>
                  <td></td>
               </tr>
               <!-- <tr>
                  <td>Customer Name</td>
                  <td><select id = "customer_id" name="customer_id">
                     </select>
                  </td>
               </tr> -->
               <tr>
                  <td>Plant Name</td>
                  <td><input type="text" id = "plant_name" name="plant_name"></td>
               </tr>
               <tr>
                  <td>Plant Code</td>
                  <td><input type="text" id = "plant_code" name="plant_code"></td>
               </tr>
               <tr>
                  <td>DUNS No</td>
                  <td><input type="text" id = "duns_no"></td>
               </tr>
               <tr>
                  <td>QMIS ID</td>
                  <td><input type="text" id = "qmis_id"></td>
               </tr>
               <tr>
                  <td>Active</td>
                  <td><input type="checkbox" id="activeid"></td>
               </tr>
            </table>
         </form>
      </div>
      <div id="tab_two" class="load">
         <form id="second_form" class="" autocomplete="off">
            <table style="width:80%; margin-bottom: 20px">
               <tr>
                  <td></td>
               </tr>
               <tr>
                  <td>Business Unit</td>
                  <td><input type="text" name="" id = "business_unit"></td>
                  <td>Logistics Manager</td>
                  <td><input type="text" id = "logistics_manager"></td>
               </tr>
               <tr>
                  <td>Product Group</td>
                  <td><input type="text" name="" id = "product_group"></td>
                  <td>Finance Manager (Controller)</td>
                  <td><input type="text" id = "finance_manager"></td>
               </tr>
               <tr>
                  <td>Function</td>
                  <td><input type="text" name="" id = "function_plant"></td>
                  <td>Human Resource Manager (HR)</td>
                  <td><input type="text" id = "hr_manager"></td>
               </tr>
               <tr>
                  <td>ERP System</td>
                  <td><input type="text" name="" id = "erp_system"></td>
                  <td>Plant Warranty Champion (PWC)</td>
                  <td><input type="text" id = "plant_warranty_champion"></td>
               </tr>
               <tr>
                  <td>Quality Manager</td>
                  <td><input type="text" name="" id = "quality_manager" ></td>
                  <td>Energy Champion</td>
                  <td><input type="text" id = "energy_champion"></td>
               </tr>
               <tr>
                  <td>Plant Manager</td>
                  <td><input type="text" id = "plant_manager"></td>
                  <td>Health and Safety Coordinator</td>
                  <td><input type="text" id = "health_and_safety_coordinator"></td>
               </tr>
               <tr>
                  <td>Environmental Coordinator</td>
                  <td><input type="text" id = "environmental_coordinator"></td>
               </tr>
            </table>
         </form>
      </div>
      <div id="tab_three" class="load">
         <form id="third_form" class="" autocomplete="off">
            <table class="table-responsive" style="width:40%; margin-bottom: 20px">
               <tr>
                  <td></td>
               </tr>
               <tr>
                  <td>Street </td>
                  <td><input type="text" id = "street"></td>
               </tr>
               <tr>
                  <td>City</td>
                  <td><input type="text" id = "city"></td>
               </tr>
               <tr>
                  <td>State</td>
                  <td><input type="text" id = "state"></td>
               </tr>
               <tr>
                  <td>Country</td>
                  <td><input type="text" id = "country"></td>
               </tr>
               <tr>
                  <td>Region</td>
                  <td><input type="text" id = "region"></td>
               </tr>
               <tr>
                  <td>Zip code</td>
                  <td><input type="text" id = "zip_code"></td>
               </tr>
            </table>
         </form>
      </div>
      <div id="tab_four" class="load">
         <form id= "additional" autocomplete="off">
            <table style="width:40%; margin-bottom: 20px">
               <tr>
                  <td></td>
               </tr>
               <tr>
                  <td>Name</td>
                  <td><input type="text" id="contact_name" name="contact_name"></td>
               </tr>
               <tr>
                  <td>Mail ID</td>
                  <td><input type="email" id="contact_email" name="contact_email"></td>
               </tr>
               <tr>
                  <td>Contact Number</td>
                  <td><input type="text" id="contact_no" name="contact_no"></td>
               </tr>
            </table>
         </form>
      </div>
   </div>
   <script type="text/javascript">
      jQuery.validator.addMethod('selectcheck', function (value) {
                     return (value != '0');
             }, "");
         $(window).load(function() {


            //  $('#span_one').addClass('custom_click_tab_active');
            //  $('#span_two,#span_three,#span_four').addClass('custom_click_tab');
         });

         $(document).ready(function() {
           $("#master_data").trigger('click');
           $("#plant_master").trigger('click');
           $("#first_set_buttons").show();
           $("#second_set_buttons").hide();
           $("#third_set_buttons").hide();
           $("#fourth_set_buttons").hide();

            //  $.ajax({
            //      type: "GET",
            //      url: "cust_name",
            //      dataType: "json",
            //      success: function(data) {
            //          var results = data;
            //          console.log(JSON.stringify(data));
            //          if(results.length==0)
            //          {
            //               $('#customer_id').append("<option id =\"0\" value=\"0\">Customer doesn't exist</option>");
            //          }
            //          else {
            //                for (var i = 0; i < results.length; i++) {
            //                    if (i == '0') {
            //                        $('#customer_id').append("<option id =\"0\" value=\"0\">Select</option>");
            //                    }
            //                    var customer_name = results[i].customer_name
            //                    var customer_id = results[i].customer_id
            //                    $('#customer_id').append("<option id =" + customer_id + " value=" + customer_name + ">" + customer_name + "</option>");
            //                }
            //          }
            //      },
            //      beforeSend: function() {},
            //      error: function() {
            //      }
            //  });

             function reset() {
                 $('#plant-table').trigger("reset");
                 $('#second_form').trigger("reset");
                 $('#third_form').trigger("reset");
                 $('#additional').trigger("reset");
                 $('#back_button_second').trigger("click");

                 $("#first_set_buttons").show();
                 $("#second_set_buttons").hide();
                 $("#third_set_buttons").hide();
                 $("#fourth_set_buttons").hide();

                 $('#span_one').addClass('custom_click_tab_active');
                 $('#span_two,#span_three,#span_four').removeClass('custom_click_tab_active');
                 $('#span_two,#span_three,#span_four').addClass('custom_click_tab');
             }

             $('#cancel_button').click(function() {
                 reset();
             });

             var is_valid_array = [];
             var active = 0;

             $(document).on('click', '#save_button', function() {
                  if ($('#additional').valid()) {
                         $.msgBox({
                             title: "Confirm",
                             content: "Do you want to save the information?",
                             type: "confirm",
                             buttons: [{
                                 value: "Yes"
                             }, {
                                 value: "No"
                             }],
                             success: function(result) {
                                 if (result == "Yes") {
                                     save();
                                 } else {

                                 }
                             }
                         });
                       }
             });

             function save() {
                //  var customer_id = $("#customer_id option:selected").attr('id');
                 var plant_name = $('#plant_name').val();
                 var plant_code = $('#plant_code').val();
                 var duns_no = $('#duns_no').val();
                 var qmis_id = $('#qmis_id').val();
                 var business_unit = $('#business_unit').val();
                 var product_group = $('#product_group').val();
                 var function_plant = $('#function_plant').val();
                 var erp_system = $('#erp_system').val();
                 var quality_manager = $('#quality_manager').val();
                 var plant_manager = $('#plant_manager').val();
                 var logistics_manager = $('#logistics_manager').val();
                 var finance_manager = $('#finance_manager').val();
                 var hr_manager = $('#hr_manager').val();
                 var plant_warranty_champion = $('#plant_warranty_champion').val();
                 var energy_champion = $('#energy_champion').val();
                 var health_and_safety_coordinator = $('#health_and_safety_coordinator').val();
                 var environmental_coordinator = $('#environmental_coordinator').val();
                 var street = $('#street').val();
                 var city = $('#city').val();
                 var state = $('#state').val();
                 var country = $('#country').val();
                 var region = $('#region').val();
                 var zip_code = $('#zip_code').val();
                 var contact_name = $('#contact_name').val();
                 var contact_email = $('#contact_email').val();
                 var contact_no = $('#contact_no').val();
                 var created_by = $('#created_by').val();
                 var created_date = $('#created_date').val();
                 var modified_by = $('#modified_by').val();
                 var modified_date = $('#modified_date').val();

                 if ($('#activeid').is(":checked")) {
                     active = 1;
                 } else {
                     active = 0;
                 }

                 $.ajax({
                     type: "POST",
                     url: "plant_save",
                     data: {
                         "_token": "{{ csrf_token() }}",
                        //  "customer_id": customer_id,
                         "plant_name": plant_name,
                         "plant_code": plant_code,
                         "duns_no": duns_no,
                         "qmis_id": qmis_id,
                         "business_unit": business_unit,
                         "product_group": product_group,
                         "function_plant": function_plant,
                         "erp_system": erp_system,
                         "quality_manager": quality_manager,
                         "plant_manager": plant_manager,
                         "logistics_manager": logistics_manager,
                         "finance_manager": finance_manager,
                         "hr_manager": hr_manager,
                         "plant_warranty_champion": plant_warranty_champion,
                         "energy_champion": energy_champion,
                         "health_and_safety_coordinator": health_and_safety_coordinator,
                         "environmental_coordinator": environmental_coordinator,
                         "street": street,
                         "city": city,
                         "state": state,
                         "country": country,
                         "region": region,
                         "zip_code": zip_code,
                         "contact_name": contact_name,
                         "contact_email": contact_email,
                         "contact_no": contact_no,
                         "created_by": created_by,
                         "created_date": created_date,
                         "modified_by": modified_by,
                         "modified_date": modified_date,
                         "active": active,
                     },
                     success: function(data) {
                       after_save();
                             $.msgBox({
                                 title: "Message",
                                 content: "Plant added successfully",
                                 type: "info",
                             });
                     },
                     beforeSend: function() {
                         $('#msg').show();

                     },
                     error: function() {
                     }
                 });
             }

             function after_save(){
               reset();
             }

               $('#next_button_first').click(function() {
                 if ($('#plant-table').valid()) {
                     $('#first_set_buttons').hide();
                     $('#tab_one').hide();
                     $('#tab_two').show();
                     $('#tab_three').hide();
                     $('#tab_four').hide();
                     $('#second_set_buttons').show();

                     $('#span_two').addClass('custom_click_tab_active');
                     $('#span_one').removeClass('custom_click_tab_active');
                     $('#span_one,#span_three,#span_four').addClass('custom_click_tab');
                   }
               });

               $('#back_button_second').click(function() {
                     $('#first_set_buttons').show();
                     $('#tab_one').show();
                     $('#tab_two').hide();
                     $('#tab_three').hide();
                     $('#tab_four').hide();
                     $('#second_set_buttons').hide();

                     $('#span_one').addClass('custom_click_tab_active');
                     $('#span_two').removeClass('custom_click_tab_active');
                     $('#span_two,#span_three,#span_four').addClass('custom_click_tab');
               });

               $('#next_button_second').click(function() {
                     $('#third_set_buttons').show();
                     $('#tab_one').hide();
                     $('#tab_two').hide();
                     $('#tab_three').show();
                     $('#tab_four').hide();
                     $('#second_set_buttons').hide();

                     $('#span_three').addClass('custom_click_tab_active');
                     $('#span_two').removeClass('custom_click_tab_active');
                     $('#span_two,#span_one,#span_four').addClass('custom_click_tab');
               });

               $('#back_button_third').click(function() {
                     $('#third_set_buttons').hide();
                     $('#tab_one').hide();
                     $('#tab_two').show();
                     $('#tab_three').hide();
                     $('#tab_four').hide();
                     $('#second_set_buttons').show();

                     $('#span_two').addClass('custom_click_tab_active');
                     $('#span_three').removeClass('custom_click_tab_active');
                     $('#span_one,#span_three,#span_four').addClass('custom_click_tab');
               });

               $('#next_button_third').click(function() {
                     $('#fourth_set_buttons').show();
                     $('#tab_one').hide();
                     $('#tab_two').hide();
                     $('#tab_three').hide();
                     $('#tab_four').show();
                     $('#third_set_buttons').hide();

                     $('#span_four').addClass('custom_click_tab_active');
                     $('#span_three').removeClass('custom_click_tab_active');
                     $('#span_two,#span_one,#span_three').addClass('custom_click_tab');
               });

               $('#back_button_fourth').click(function() {
                     $('#fourth_set_buttons').hide();
                     $('#tab_one').hide();
                     $('#tab_two').hide();
                     $('#tab_three').show();
                     $('#tab_four').hide();
                     $('#third_set_buttons').show();

                     $('#span_three').addClass('custom_click_tab_active');
                     $('#span_four').removeClass('custom_click_tab_active');
                     $('#span_one,#span_two,#span_four').addClass('custom_click_tab');
               });

               $("#tab_one").on('keydown', '#activeid', function(e) {
                     var keyCode = e.keyCode || e.which;
                     if (keyCode == 9) {
                       e.preventDefault();
                       $( "#next_button_first" ).trigger( "click" );
                     }
                   });



         });
   </script>
</div>
@stop
@section('lower_band')
<div class="col-xs-12 lower_band">
   <button id="cancel_button" class="headerbuttons" type="button" style="float: right; margin-right: 20px;">Cancel</button>
   <div class="first_set_buttons load" id="first_set_buttons">
      <button id="next_button_first" class="headerbuttons" type="button"  style="float: left; margin-right: 20px;">Next</button>
   </div>
   <div class="second_set_buttons load" id="second_set_buttons">
     <button id="back_button_second" class="headerbuttons" type="button"  style="float: left; margin-right: 20px;">Back</button>
      <button id="next_button_second" class="headerbuttons" type="button"  style="float: left; margin-right: 20px;">Next</button>
   </div>
   <div class="third_set_buttons load" id="third_set_buttons">

      <button id="back_button_third" class="headerbuttons" type="button"  style="float: left; margin-right: 20px;">Back</button>
      <button id="next_button_third" class="headerbuttons" type="button"  style="float: left; margin-right: 20px;">Next</button>
   </div>
   <div class="fourth_set_buttons load" id="fourth_set_buttons">
      <button id="save_button" class="headerbuttons" type="button"  style="float: right; margin-right: 20px;">Save</button>
      <button id="back_button_fourth" class="headerbuttons" type="button"  style="float: left; margin-right: 20px;">Back</button>
   </div>
</div>
@stop
