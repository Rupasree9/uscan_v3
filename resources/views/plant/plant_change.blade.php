@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" type="text/css" href="css/plant.css">
<link rel="stylesheet" type="text/css" href="css/plant_change.css">
<script src="js/plant_validation.js"></script>
@stop
@section('display_screen_name')
<div class="col-md-9" style="position:relative;"></div>
@stop
@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="display-title">
      Change Plant Master
   </div>
   <div id="show_all_link" class="col-xs-2" style="float: right;text-align:center;line-height:30px;">
      <span  id="show_all" style="">Show All Plants</span>
   </div>
</div>
@stop
@section('content')
<div class="col-xs-12 content no_scroll" id="content no_scroll">
  <div class="loading">
     <img src="images/loading.gif" height="50px;">
  </div>
   <form id="search-para" class="no_scroll">
      <table class="hide_show" style="width:40%;">
         <tr>
            <td style="background-color: #eee;">Search Parameters</td>
         </tr>
         <tr  style="display:none;">
            <td>Plant ID</td>
            <td><input type="text" id="plant_id_search" name="plant_id_search"></td>
         </tr>
         <tr>
            <td>Plant Name</td>
            <td><input type="text" id="plant_name_search" name="plant_name_search"></td>
         </tr>
         <tr>
            <td>Plant Code</td>
            <td><input type="text" id="plant_code_search"  name="plant_code_search"></td>
         </tr>
         <tr>
            <td>DUNS Number</td>
            <td><input type="text" id="duns_no_search"  name="duns_no_search"></td>
         </tr>
      </table>
   </form>
   <div id="list-of-plant"  class="div1 overflow table_height">
      <table style="width:100%;" class="tab" id="test_datatable">
      </table>
   </div>
   <div class="col-xs-12 hide_div">
      <ul class="nav nav-tabs  load" id="myTabs">
         <li id="1" class="active"><span id="span_one" class="tab_one "  data-toggle="tab" >Basic Details</span></li>
         <li id="2"><span id="span_two" class="tab_two "  data-toggle="tab" >Plant Details</span></li>
         <li id="3"><span id="span_three" class="tab_three "  data-toggle="tab" >Address</span></li>
         <li id="4"><span id="span_four" class="tab_four" data-toggle="tab" >Contact</span></li>
         <li id="5"><span id="span_five" class="tab_five" data-toggle="tab" >Additional Details</span></li>
      </ul>
      <div class="tab-content">
         <div id="tab_one" class="load">
            <form id="plant-table" autocomplete="off">
               <table style="width:40%;">
                  <tr>
                     <td></td>
                  </tr>

                  <tr>
                     <td>Plant Name</td>
                     <td><input type="text" id = "plant_name" name="plant_name"></td>
                  </tr>
                  <tr>
                     <td>Plant Code</td>
                     <td><input type="text" id = "plant_code" name="plant_code"></td>
                  </tr>
                  <tr>
                     <td>DUNS No</td>
                     <td><input type="text" id = "duns_no"></td>
                  </tr>
                  <tr>
                     <td>QMIS ID</td>
                     <td><input type="text" id = "qmis_id"></td>
                  </tr>
                  <tr>
                     <td>Active</td>
                     <td><input type="checkbox" id="active"></td>
                  </tr>
               </table>
            </form>
         </div>
         <div id="tab_two" class="load">
            <form id="second_form" class="" autocomplete="off">
               <table style="width:80%; margin-bottom: 20px">
                  <tr>
                     <td></td>
                  </tr>
                  <tr>
                     <td>Business Unit</td>
                     <td><input type="text" name="" id = "business_unit"></td>
                     <td>Logistics Manager</td>
                     <td><input type="text" id = "logistics_manager"></td>
                  </tr>
                  <tr>
                     <td>Product Group</td>
                     <td><input type="text" name="" id = "product_group"></td>
                     <td>Finance Manager (Controller)</td>
                     <td><input type="text" id = "finance_manager"></td>
                  </tr>
                  <tr>
                     <td>Function</td>
                     <td><input type="text" name="" id = "function_plant"></td>
                     <td>Human Resource Manager (HR)</td>
                     <td><input type="text" id = "hr_manager"></td>
                  </tr>
                  <tr>
                     <td>ERP System</td>
                     <td><input type="text" name="" id = "erp_system"></td>
                     <td>Plant Warranty Champion (PWC)</td>
                     <td><input type="text" id = "plant_warranty_champion"></td>
                  </tr>
                  <tr>
                     <td>Quality Manager</td>
                     <td><input type="text" name="" id = "quality_manager" ></td>
                     <td>Energy Champion</td>
                     <td><input type="text" id = "energy_champion"></td>
                  </tr>
                  <tr>
                     <td>Plant Manager</td>
                     <td><input type="text" id = "plant_manager"></td>
                     <td>Health and Safety Coordinator</td>
                     <td><input type="text" id = "health_and_safety_coordinator"></td>
                  </tr>
                  <tr>
                     <td>Environmental Coordinator</td>
                     <td><input type="text" id = "environmental_coordinator"></td>
                  </tr>
               </table>
            </form>
         </div>
         <div id="tab_three" class="load">
            <form id="third_form" class="" autocomplete="off">
               <table class="table-responsive" style="width:40%; margin-bottom: 20px">
                  <tr>
                     <td></td>
                  </tr>
                  <tr>
                     <td>Street </td>
                     <td><input type="text" id = "street"></td>
                  </tr>
                  <tr>
                     <td>City</td>
                     <td><input type="text" id = "city"></td>
                  </tr>
                  <tr>
                     <td>State</td>
                     <td><input type="text" id = "state"></td>
                  </tr>
                  <tr>
                     <td>Country</td>
                     <td><input type="text" id = "country"></td>
                  </tr>
                  <tr>
                     <td>Region</td>
                     <td><input type="text" id = "region"></td>
                  </tr>
                  <tr>
                     <td>Zip code</td>
                     <td><input type="text" id = "zip_code"></td>
                  </tr>
               </table>
            </form>
         </div>
         <div id="tab_four" class="load">
            <form id= "additional" autocomplete="off">
               <table style="width:40%; margin-bottom: 20px">
                  <tr>
                     <td></td>
                  </tr>
                  <tr>
                     <td>Name </td>
                     <td><input type="text" id="contact_name" name="contact_name"></td>
                  </tr>
                  <tr>
                     <td>Mail ID</td>
                     <td><input type="email" id="contact_email" name="contact_email"></td>
                  </tr>
                  <tr>
                     <td>Contact Number</td>
                     <td><input type="text" id="contact_no" name="contact_no"></td>
                  </tr>
               </table>
            </form>
         </div>
         <div id="tab_five" class="load">
            <form id="additional_details" autocomplete="off">
               <table style="width:40%; margin-bottom: 20px">
                  <tr>
                    <td></td>
                  </tr>
                  <tr>
                     <td>Created By</td>
                     <td><input type="text" name="created_by" id="created_by" disabled></td>
                  </tr>
                  <tr>
                     <td>Created Date</td>
                     <td><input type="text" name="created_date" id="created_date" disabled></td>
                  </tr>
                  <tr>
                     <td>Modified By</td>
                     <td><input type="text" name="modified_by" id="modified_by" disabled></td>
                  </tr>
                  <tr>
                     <td>Modified Date</td>
                     <td><input type="text" name="modified_date" id="modified_date" disabled></td>
                  </tr>
               </table>
            </form>
         </div>
      </div>
   </div>
   <script type="text/javascript">
      jQuery.validator.addMethod('selectcheck', function (value) {
                     return (value != '0');
             }, "");
       $(window).load(function() {
           $("#master_data").trigger('click');
           $("#plant_master").trigger('click');
           $('.tab-content').hide();

           $("#first_set_buttons").hide();
           $("#second_set_buttons").hide();
           $("#third_set_buttons").hide();
           $("#fourth_set_buttons").hide();
           $("#fifth_set_buttons").hide();

           $('#tab_one').show();
           $('#tab_two').hide();
           $('#tab_three').hide();
           $('#tab_four').hide();
           $('#tab_five').hide();

           $('#span_one').addClass('custom_click_tab_active');
           $('#span_two,#span_three,#span_four,#span_five').addClass('custom_click_tab');
       });

       $(document).ready(function() {
           var filter_clicked=false;
           var show_all_link_clicked=false;
           var results = "";
           var contact_id = "";
           var address_id = "";
           var plant_id = "";
           var active_check;
           var plant_name,plant_code,duns_no;



           $('#next').click(function() {
               // $('#additional').show();
               $('#plant-table').hide();
               // $('#back_edit').show();
               $('#next').hide();
           });

           function filter_click(){
                       $('#list-of-plant').show();
                       show_all_link_clicked=false;
                       plant_id = $('#plant_id_search').val();
                       plant_name = $('#plant_name_search').val();
                       plant_code = $('#plant_code_search').val();
                       duns_no = $('#duns_no_search').val();
                       if ((plant_id == "") && (plant_name == "") && (plant_code == "")  && (duns_no == "")) {
                           $.msgBox({
                               title: "Alert",
                               content: "Search Criteria Not Available",
                               type: "alert",
                           });
                       } else            {
                         $('#search_page_buttons').show();
                         $('#edit').show();
                         $('#delete').show();
                         $('#export').show();
                         $('#search-para').hide();
                         display_data();
                         $('.tab').show();
                         $('#filter').hide();
                         $('#back_search').show();
                         $('#show_all').hide();
                       }
           }

           $('#filter').click(function() {
               filter_clicked=true;
                 filter_click();

           });

           $(document).on('click', '#back_search', function() {
                 back_search();
           });

           function back_search(){
             $('#search_page_buttons').hide();
             $('.tab').hide();
             $('#search-para').show();
             $('.tab tr').remove();
             $('.hide_show').show();
             $('#filter').show();
             $('#show_all').show();
             $('#back_search').hide();
             // $("#show_all,#search-para,#filter").addClass("show");
             // $("#back_search,#list-of-plant").addClass("hide");
           }

           $(document).on('click', '#show_all', function() {
             $('.loading').css('display', 'block');
             show_all_link_clicked=true;
             filter_clicked=false;
             hide_buttons_show_all();
             show_table();
             //$('.loading').css('display', 'none');
           });

           function hide_div_after_update() {
                     $('#fourth_set_buttons').hide();
                     $('#fifth_set_buttons').hide();
                     $('#cancelButton').hide();
                     $('#myTabs').hide();
                     // $('#span_one').addClass('custom_click_tab_active');
                     $('#span_two,#span_three,#span_four').removeClass('custom_click_tab_active');
                     // $('#span_two,#span_three,#span_four').addClass('custom_click_tab');
                     if(filter_clicked==true)
                     {
                       filter_clicked = false;
                       filter_click();
                     }
                     else {
                       $("#show_all_link").trigger("click");
                       $("#show_all").trigger("click");
                       $('#list-of-plant').show();
                     }
           }

           function hide_buttons_show_all() {
               $('.hide_show').hide();
               $('#search_page_buttons').show();
               $('#edit').show();
               $('#delete').show();
               $('#export').show();
               $('#search-para').hide();
               $('.tab').show();
               $('#Create').css("display", "none");
               $('#filter').hide();
               $('#Create').hide();
               $('#plant-table').hide();
               $('#Create').hide();
               $('#plant-table').hide();
               $('#list-of-plant').show();
               $('#next').hide();
               $('#cancelButton').hide();
               $('#back_search').show();
               $('#show_all').hide();
           }

           $(document).on('click', '#delete_search', function() {
               plant_id = $(this).parents('tr:first').find('td:first').text();
               $.msgBox({
                   title: "Confirm",
                   content: "Do you want to delete the data",
                   type: "confirm",
                   buttons: [{
                       value: "Yes"
                   }, {
                       value: "No"
                   }],
                   success: function(result) {
                       if (result == "Yes") {
                           $.ajax({
                               type: "POST",
                               url: "plant_delete",
                               data: {
                                   "_token": "{{ csrf_token() }}",
                                   "plant_id": plant_id,
                               },
                               success: function() {
                                   display_data();
                                   $.msgBox({
                                       title: "Message",
                                       content: "Plant deleted successfully",
                                       type: "info",
                                   });
                               },
                               beforeSend: function() {

                               },
                               error: function() {
                                 $.msgBox({
                                     title: "Alert",
                                     content: "Unable to delete the Plant</br>Please delete the dependent portals",
                                     type: "alert",
                                 });
                               }
                           });
                       }
                   }
               });
           });

           $(document).on('click', '#delete_showall', function() {
               plant_id = $(this).parents('tr:first').find('td:first').text();
               $.msgBox({
                   title: "Confirm",
                   content: "Do you want to delete the data",
                   type: "confirm",
                   buttons: [{
                       value: "Yes"
                   }, {
                       value: "No"
                   }],
                   success: function(result) {
                       if (result == "Yes") {
                           $.ajax({
                               type: "POST",
                               url: "plant_delete",
                               data: {
                                   "_token": "{{ csrf_token() }}",
                                   "plant_id": plant_id,
                               },
                               success: function() {
                                   show_table();
                                   $.msgBox({
                                       title: "Message",
                                       content: "Plant deleted successfully",
                                       type: "info",
                                   });
                               },
                               beforeSend: function() {
                                   //$('#msg').show();

                               },
                               error: function() {
                                 $.msgBox({
                                     title: "Alert",
                                     content: "Unable to delete the Plant</br>Please delete the dependent portals",
                                     type: "alert",
                                 });
                               }
                           });
                       }
                   }
               });
           });

           $(document).on('click', '#cancelButton', function() {
               $('#back_search').show();
               $('#Create').hide();
               $('#plant-table').hide();
               $('#list-of-plant').show();
               $('.hide_div').hide();
               $('#search_page_buttons').show();
               $('#next').hide();
               $('#myTabs').hide();
               $('#cancelButton').hide();
               $('#first_set_buttons').hide();
               $('#second_set_buttons').hide();
               $('#third_set_buttons').hide();
               $('#fourth_set_buttons').hide();
               $('#fifth_set_buttons').hide();
               $('#span_one,#span_two,#span_three,#span_four,#span_five').removeClass('custom_click_tab_active');
           });

           $(document).on('click', '.edit', function() {
               {
                    show_change_screen();
                    $('#myTabs').show();
                   $('#Create').show();
                   $('#plant-table').show();
                   $('.tab-content,#list-of-plant,.hide_div').show();
                   $('#search_page_buttons').hide();
                   $('#cancelButton').show();
                   $('#list-of-plant').hide();
                   $('#next').show();
                  //  $('.tab-content').show();
                   $('#back_search').hide();
                   $('#show_all').hide();


                   var id = $(this).attr('id');
                   plant_id = $(this).parents('tr:first').find('td:first').text();
                   $('#business_unit').val(results[id].business_unit);
                   $('#plant_name').val(results[id].plant_name);
                   $('#product_group').val(results[id].product_group);
                   $('#plant_code').val(results[id].plant_code);
                   $('#function_plant').val(results[id].function);
                   $('#duns_no').val(results[id].plant_duns_no);
                   $('#qmis_id').val(results[id].qmis_id);
                   $('#erp_system').val(results[id].erp_system);
                   $('#quality_manager').val(results[id].quality_manager);
                   $('#plant_manager').val(results[id].plant_manager);
                   $('#logistics_manager').val(results[id].logistics_manager);
                   $('#finance_manager').val(results[id].finance_manager);
                   $('#hr_manager').val(results[id].hr_manager);
                   $('#plant_warranty_champion').val(results[id].plant_warranty_champion);
                   $('#energy_champion').val(results[id].energy_champion);
                   $('#health_and_safety_coordinator').val(results[id].health_and_safety_coordinator);
                   $('#environmental_coordinator').val(results[id].environmental_coordinator);
                   $('#contact_name').val(results[id].contact_name);
                   $('#contact_email').val(results[id].contact_email_id);
                   $('#contact_no').val(results[id].contact_number);
                   $('#street').val(results[id].street);
                   $('#city').val(results[id].city);
                   $('#state').val(results[id].state);
                   $('#country').val(results[id].country);
                   $('#region').val(results[id].region);
                   $('#created_by').val(results[id].created_by);
                   $('#created_date').val(results[id].created_at);
                   $('#modified_by').val(results[id].updated_by);
                   $('#modified_date').val(results[id].updated_at);
                   $('#zip_code').val(results[id].zip_code);
                   plant_id = results[id].plant_id;

                   if (results[id].active == 0) {
                       $('#active').prop('checked', false);
                   } else {
                       $('#active').prop('checked', true);
                   }
               }
           });

           function show_table() {
               $.ajax({
                   type: "GET",
                   url: "plant_display_show_all",
                   dataType: "json",
                   success: function(data) {
                       results = data;
                       console.log(JSON.stringify(results));
                       $('.tab tr').remove();
                       $('.tab').append('<tr style="background-color: #D3D3D3;"><th>Plant Name</th><th>Plant Code</th><th>DUNS No.</th><th>QMIS ID</th><th>Business Unit</th><th>Plant Manager</th><th>Active</th><th>Edit</th><th>Delete</th></tr>');

                       for (var i = 0; i < results.length; i++) {
                         if (results[i].active == 0) {
                             active_check = '<input type="checkbox" disabled readonly>';
                         } else {
                             active_check = '<input type="checkbox" checked disabled readonly>';
                         }
                           $('.tab').append('<tr data-id="' + i + '" class="double_click"><td class"plant_id" style="display:none;">' + results[i].plant_id + '</td><td>' + results[i].plant_name + '</td><td>' + results[i].plant_code + '</td><td>' + results[i].plant_duns_no + '</td><td>' + results[i].qmis_id + '</td><td>' + results[i].business_unit + '</td><td>' + results[i].plant_manager + '</td><td>' + active_check + '</td><td><button id="' + i + '" class="edit btn btn-default glyphicon glyphicon-edit edit_color" type="button"></button></td><td><button id="delete_showall" type="button" class="btn btn-default delete_color" aria-label="Left Align"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td></tr>')
                       }
                       $('.loading').css('display', 'none');
                   },
                   beforeSend: function() {
                      $('.loading').css('display', 'block');
                   },
                   error: function() {
                     $('.loading').css('display', 'none');
                   }
               });
           }

           function show_change_screen(){
                 $("#first_set_buttons").show();
                 $("#second_set_buttons").hide();
                 $("#third_set_buttons").hide();
                 $("#fourth_set_buttons").hide();
                 $("#fifth_set_buttons").hide();

                 $('#tab_one').show();
                 $('#tab_two').hide();
                 $('#tab_three').hide();
                 $('#tab_four').hide();
                 $('#tab_five').hide();
                 $('#span_two,#span_three,#span_four,#span_five').removeClass('custom_click_tab_active');
                 $('#span_one').addClass('custom_click_tab_active');
                 $('#span_two,#span_three,#span_four,#span_five').addClass('custom_click_tab');
           }

           $(document).on('dblclick', '.double_click', function() {
               show_change_screen();
               $('.tab-content,#list-of-plant,.hide_div').show();
               $('#myTabs').show();
               $('#back_search').hide();
               $('#show_all').hide();
               $('#Create').show();
               $('#plant-table').show();
               $('#search_page_buttons').hide();
               $('#cancelButton').show();
               $('#list-of-plant').hide();
               $('#next').show();

               var id = $(this).data("id");
               plant_id = $(this).parents('tr:first').find('td:first').text();
               $('#business_unit').val(results[id].business_unit);
               $('#plant_name').val(results[id].plant_name);
               $('#product_group').val(results[id].product_group);
               $('#plant_code').val(results[id].plant_code);
               $('#function_plant').val(results[id].function);
               $('#duns_no').val(results[id].plant_duns_no);
               $('#qmis_id').val(results[id].qmis_id);
               $('#erp_system').val(results[id].erp_system);
               $('#quality_manager').val(results[id].quality_manager);
               $('#plant_manager').val(results[id].plant_manager);
               $('#logistics_manager').val(results[id].logistics_manager);
               $('#finance_manager').val(results[id].finance_manager);
               $('#hr_manager').val(results[id].hr_manager);
               $('#plant_warranty_champion').val(results[id].plant_warranty_champion);
               $('#energy_champion').val(results[id].energy_champion);
               $('#health_and_safety_coordinator').val(results[id].health_and_safety_coordinator);
               $('#environmental_coordinator').val(results[id].environmental_coordinator);
               $('#contact_name').val(results[id].contact_name);
               $('#contact_email').val(results[id].contact_email_id);
               $('#contact_no').val(results[id].contact_number);
               $('#street').val(results[id].street);
               $('#city').val(results[id].city);
               $('#state').val(results[id].state);
               $('#country').val(results[id].country);
               $('#region').val(results[id].region);
               $('#created_by').val(results[id].created_by);
               $('#created_date').val(results[id].created_at);
               $('#modified_by').val(results[id].updated_by);
               $('#modified_date').val(results[id].updated_at);
               $('#zip_code').val(results[id].zip_code);
               plant_id = results[id].plant_id;
               if (results[id].active == 0) {
                   $('#active').prop('checked', false);
               } else {
                   $('#active').prop('checked', true);
               }
           });

           function display_data() {
               plant_id = $('#plant_id_search').val();
               var plant_name = $('#plant_name_search').val();
               var plant_code = $('#plant_code_search').val();
               var duns_no = $('#duns_no_search').val();
               $.ajax({
                   type: "POST",
                   url: "plant_display_show",
                   data: {
                       "_token": "{{ csrf_token() }}",
                       "plant_id": plant_id,
                       "plant_name": plant_name,
                       "plant_code": plant_code,
                       "duns_no": duns_no,
                   },
                   success: function(data) {
                       if (data != "0") {
                           results = data;
                           $('.tab tr').remove();
                           $('.tab').append('<tr style="background-color: #D3D3D3;"><th>Plant Name</th><th>Plant Code</th><th>DUNS No.</th><th>QMIS ID</th><th>Business Unit</th><th>Plant Manager</th><th>Active</th><th>Edit</th><th>Delete</th></tr>');

                           for (var i = 0; i < results.length; i++) {
                             if (results[i].active == 0) {
                                 active_check = '<input type="checkbox" disabled readonly>';
                             } else {
                                 active_check = '<input type="checkbox" checked disabled readonly>';
                             }
                               $('.tab').append('<tr data-id="' + i + '" class="double_click"><td  class="id" style="display:none;">' + results[i].plant_id + '</td><td>' + results[i].plant_name + '</td><td>' + results[i].plant_code + '</td><td>' + results[i].plant_duns_no + '</td><td>' + results[i].qmis_id + '</td><td>' + results[i].business_unit + '</td><td>' + results[i].plant_manager + '</td><td>' + active_check + '</td><td><button id="' + i + '" class="edit btn btn-default glyphicon glyphicon-edit edit_color" type="button"></button></td><td><button id="delete_search" type="button" class="btn btn-default delete_color" aria-label="Left Align"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td></tr>')
                           }
                       } else {
                           if(filter_clicked == false){
                               back_search();
                           }
                           else {
                               back_search();
                               $.msgBox({
                                   title: "Alert",
                                   content: "Data not found",
                                   type: "alert",
                               });
                           }

                       }
                   },
                   beforeSend: function() {

                   },
                   error: function() {
                   }
               });
           }

           $(document).on('click', '#update_button', function() {
             if ($('#additional').valid()) {
                         $.msgBox({
                             title: "Confirm",
                             content: "Do you want to update the information?",
                             type: "confirm",
                             buttons: [{
                                 value: "Yes"
                             }, {
                                 value: "No"
                             }],
                             success: function(result) {
                                 if (result == "Yes") {
                                           $('#list-of-plant').hide();
                                           fun_update();
                                         }
                                         else {

                                         }
                                       }
                                   });
                     }
           });

           function fun_update(){
                 $('#list-of-plant').hide();
                 $('.tab-content').hide();
                 var plant_name = $('#plant_name').val();
                 var plant_code = $('#plant_code').val();
                 var duns_no = $('#duns_no').val();
                 var qmis_id = $('#qmis_id').val();
                 var business_unit = $('#business_unit').val();
                 var product_group = $('#product_group').val();
                 var function_plant = $('#function_plant').val();
                 var erp_system = $('#erp_system').val();
                 var quality_manager = $('#quality_manager').val();
                 var plant_manager = $('#plant_manager').val();
                 var logistics_manager = $('#logistics_manager').val();
                 var finance_manager = $('#finance_manager').val();
                 var hr_manager = $('#hr_manager').val();
                 var plant_warranty_champion = $('#plant_warranty_champion').val();
                 var energy_champion = $('#energy_champion').val();
                 var health_and_safety_coordinator = $('#health_and_safety_coordinator').val();
                 var environmental_coordinator = $('#environmental_coordinator').val();
                 var street = $('#street').val();
                 var city = $('#city').val();
                 var state = $('#state').val();
                 var country = $('#country').val();
                 var region = $('#region').val();
                 var zip_code = $('#zip_code').val();
                 var contact_name = $('#contact_name').val();
                 var contact_email = $('#contact_email').val();
                 var contact_no = $('#contact_no').val();
                 var created_by = $('#created_by').val();
                 var created_date = $('#created_date').val();
                 var modified_by = $('#modified_by').val();
                 var modified_date = $('#modified_date').val();

                 if ($('#active').is(":checked")) {
                     active = 1;
                 } else {
                     active = 0;
                 }
                 $.ajax({
                     type: "POST",
                     url: "plant_update",
                     data: {
                         "_token": "{{ csrf_token() }}",
                         "plant_id": plant_id,
                         "plant_name": plant_name,
                         "plant_code": plant_code,
                         "duns_no": duns_no,
                         "qmis_id": qmis_id,
                         "business_unit": business_unit,
                         "product_group": product_group,
                         "function_plant": function_plant,
                         "erp_system": erp_system,
                         "quality_manager": quality_manager,
                         "plant_manager": plant_manager,
                         "logistics_manager": logistics_manager,
                         "finance_manager": finance_manager,
                         "hr_manager": hr_manager,
                         "plant_warranty_champion": plant_warranty_champion,
                         "energy_champion": energy_champion,
                         "health_and_safety_coordinator": health_and_safety_coordinator,
                         "environmental_coordinator": environmental_coordinator,
                         "street": street,
                         "city": city,
                         "state": state,
                         "country": country,
                         "region": region,
                         "zip_code": zip_code,
                         "contact_name": contact_name,
                         "contact_email": contact_email,
                         "contact_no": contact_no,
                         "created_by": created_by,
                         "created_date": created_date,
                         "modified_by": modified_by,
                         "modified_date": modified_date,
                         "active": active,
                     },
                     success: function(data) {
                         $.msgBox({
                             title: "Alert",
                             content: "Updated successfully",
                             type: "info",
                         });
                         hide_div_after_update();
                     },
                     beforeSend: function() {
                         $('#msg').show();
                     },
                     error: function() {
                         $('#msg').text('Sorry we are not available');
                     }
                 });
           }

           var fetch_plant_name;
           $('input[name="plant_name_search"]').autoComplete({
               minChars: 0,
               source: function(term, response) {
                   try {
                       fetch_plant_name.abort();
                   } catch (e) {}
                   fetch_plant_name = $.getJSON('fetch_plant_name', {
                       plant_name: term
                   }, function(data) {
                       response(data);
                   });
               }
           });

           var fetch_plant_id;
           $('input[name="plant_id_search"]').autoComplete({
               minChars: 0,
               source: function(term, response) {
                   try {
                       fetch_plant_id.abort();
                   } catch (e) {}
                   fetch_plant_id = $.getJSON('fetch_plant_id', {
                       plant_id: term
                   }, function(data) {
                       response(data);
                   });
               }
           });


           var fetch_plant_code;
           $('input[name="plant_code_search"]').autoComplete({
               minChars: 0,
               source: function(term, response) {
                   try {
                       fetch_plant_code.abort();
                   } catch (e) {}
                   fetch_plant_code = $.getJSON('fetch_plant_code', {
                       plant_code: term
                   }, function(data) {
                       response(data);
                   });
               }
           });

           var fetch_plant_duns_no;
           $('input[name="duns_no_search"]').autoComplete({
               minChars: 0,
               source: function(term, response) {
                   try {
                       fetch_plant_duns_no.abort();
                   } catch (e) {}
                   fetch_plant_duns_no = $.getJSON('fetch_plant_duns_no', {
                       duns_no: term
                   }, function(data) {
                       response(data);
                   });
               }
           });



           $('#next_button_first').click(function() {
             if ($('#plant-table').valid()) {
                 $('#first_set_buttons').hide();
                 $('#tab_one').hide();
                 $('#tab_two').show();
                 $('#tab_three').hide();
                 $('#tab_four').hide();
                 $('#tab_five').hide();
                 $('#second_set_buttons').show();

                 $('#span_two').addClass('custom_click_tab_active');
                 $('#span_one').removeClass('custom_click_tab_active');
                 $('#span_one,#span_three,#span_four,#span_five').addClass('custom_click_tab');
               }
           });

           $('#back_button_second').click(function() {
                 $('#first_set_buttons').show();
                 $('#tab_one').show();
                 $('#tab_two').hide();
                 $('#tab_three').hide();
                 $('#tab_four').hide();
                 $('#tab_five').hide();
                 $('#second_set_buttons').hide();

                 $('#span_one').addClass('custom_click_tab_active');
                 $('#span_two').removeClass('custom_click_tab_active');
                 $('#span_two,#span_three,#span_four,#span_five').addClass('custom_click_tab');
           });

           $('#next_button_second').click(function() {
                 $('#third_set_buttons').show();
                 $('#tab_one').hide();
                 $('#tab_two').hide();
                 $('#tab_three').show();
                 $('#tab_four').hide();
                 $('#tab_five').hide();
                 $('#second_set_buttons').hide();

                 $('#span_three').addClass('custom_click_tab_active');
                 $('#span_two').removeClass('custom_click_tab_active');
                 $('#span_two,#span_one,#span_four,#span_five').addClass('custom_click_tab');
           });

           $('#back_button_third').click(function() {
                 $('#third_set_buttons').hide();
                 $('#tab_one').hide();
                 $('#tab_two').show();
                 $('#tab_three').hide();
                 $('#tab_four').hide();
                 $('#tab_five').hide();
                 $('#second_set_buttons').show();

                 $('#span_two').addClass('custom_click_tab_active');
                 $('#span_three').removeClass('custom_click_tab_active');
                 $('#span_one,#span_three,#span_four,#span_five').addClass('custom_click_tab');
           });

           $('#next_button_third').click(function() {
                 $('#fourth_set_buttons').show();
                 $('#tab_one').hide();
                 $('#tab_two').hide();
                 $('#tab_three').hide();
                 $('#tab_four').show();
                 $('#tab_five').hide();
                 $('#third_set_buttons').hide();

                 $('#span_four').addClass('custom_click_tab_active');
                 $('#span_three').removeClass('custom_click_tab_active');
                 $('#span_two,#span_one,#span_three,#span_five').addClass('custom_click_tab');
           });

           $('#back_button_fourth').click(function() {
                 $('#fourth_set_buttons').hide();
                 $('#tab_one').hide();
                 $('#tab_two').hide();
                 $('#tab_three').show();
                 $('#tab_four').hide();
                 $('#tab_five').hide();
                 $('#third_set_buttons').show();

                 $('#span_three').addClass('custom_click_tab_active');
                 $('#span_four').removeClass('custom_click_tab_active');
                 $('#span_one,#span_two,#span_four,#span_five').addClass('custom_click_tab');
           });

           $('#next_button_fourth').click(function() {
                 $('#fifth_set_buttons').show();
                 $('#tab_one').hide();
                 $('#tab_two').hide();
                 $('#tab_three').hide();
                 $('#tab_four').hide();
                 $('#tab_five').show();
                 $('#fourth_set_buttons').hide();

                 $('#span_five').addClass('custom_click_tab_active');
                 $('#span_four').removeClass('custom_click_tab_active');
                 $('#span_two,#span_one,#span_three,#span_four').addClass('custom_click_tab');
           });

           $('#back_button_fifth').click(function() {
                 $('#fourth_set_buttons').show();
                 $('#tab_one').hide();
                 $('#tab_two').hide();
                 $('#tab_three').hide();
                 $('#tab_four').show();
                 $('#tab_five').hide();
                 $('#fifth_set_buttons').hide();

                 $('#span_four').addClass('custom_click_tab_active');
                 $('#span_five').removeClass('custom_click_tab_active');
                 $('#span_one,#span_two,#span_four,#span_five').addClass('custom_click_tab');
           });

       });
   </script>
</div>
@stop
@section('lower_band')
<div class="col-xs-12 lower_band">
   <button class="headerbuttons" id="back_search" type="button">Back</button>
   <button class="headerbuttons" id="filter" type="button">Search</button>
   <button class="headerbuttons" type="button" id="cancelButton" style="float: right; margin-right: 20px; display:none;">Cancel</button>
   <div class="first_set_buttons load" id="first_set_buttons">
      <button id="next_button_first" class="headerbuttons" type="button"  style="float: left; margin-right: 20px;">Next</button>
   </div>
   <div class="second_set_buttons load" id="second_set_buttons">
      <button id="back_button_second" class="headerbuttons" type="button"  style="float: left; margin-right: 20px;">Back</button>
      <button id="next_button_second" class="headerbuttons" type="button"  style="float: left; margin-right: 20px;">Next</button>
   </div>
   <div class="third_set_buttons load" id="third_set_buttons">
      <button id="back_button_third" class="headerbuttons" type="button"  style="float: left; margin-right: 20px;">Back</button>
      <button id="next_button_third" class="headerbuttons" type="button"  style="float: left; margin-right: 20px;">Next</button>
   </div>
   <div class="fourth_set_buttons load" id="fourth_set_buttons">
      <button id="update_button" class="headerbuttons" type="button"  style=" float: right; margin-right: 20px;">Update</button>
      <button id="back_button_fourth" class="headerbuttons" type="button"  style="float: left; margin-right: 20px;">Back</button>
      <button id="next_button_fourth" class="headerbuttons" type="button"  style="float: left; margin-right: 20px;">Next</button>
   </div>
   <div class="fifth_set_buttons load" id="fifth_set_buttons">
      <button id="update_button" class="headerbuttons" type="button"  style=" float: right; margin-right: 20px;">Update</button>
      <button id="back_button_fifth" class="headerbuttons" type="button"  style="float: left; margin-right: 20px;">Back</button>
   </div>
</div>
@stop
