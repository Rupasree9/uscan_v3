@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" type="text/css" href="css/communication.css">
<script src="js/communication_validation.js"></script>
@stop

@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="col-xs-4 display-title">
      Display FTP Address
   </div>
   <div class="col-xs-3" id = "show_all_addresses" style="float: right;text-align:center;line-height:30px;">
      <span id="show_all">Show All FTP Addresses</span>
   </div>
</div>
@stop

@section('content')
<div class="col-xs-12 content">
   <div id="connection-table-search">
      <div id="search-para-address">
         <form id="address_search_form" autocomplete="off">
            <table style="width:40%;">
               <tr>
                 <td>Address Name</td>
                 <td><input type="text"  id = "search_address_name" name="search_address_name"></td>
               </tr>
               <tr>
                 <td>User Name</td>
                 <td><input type="text" name="search_user_name" id="search_user_name"></td>
               </tr>
               <!-- <tr>
                  <td>Connection</td>
                  <td><input type="text" name="search_connection" id="search_connection"></td>
               </tr> -->
            </table>
         </form>
      </div>

      <div id="connection_table"  style="display:none">
         <ul class="nav nav-tabs">
            <li class="active"><a id ="details_connection" data-toggle="tab" href="#tab_one_connection">Details</a></li>
            <li ><a data-toggle="tab" href="#tab_two_connection">Additional Details</a></li>
         </ul>
         <div class="tab-content">
            <div id="tab_one_connection" class="tab-pane fade in active">
               <form id = "address-table-form" autocomplete="off">
                  <table style="width:40%;">
                    <tr>
                       <td>Address Name</td>
                       <td><input type="text"  id = "address_name" name="address_name" disabled></td>
                    </tr>
                    <tr>
                       <td>User Name</td>
                       <td><input type="text" id="user_name" name="user_name" disabled></td>
                    </tr>
                    <tr>
                       <td>Password</td>
                       <td><input type="text" name="pwd" id="pwd" disabled></td>
                    </tr>
                    <tr>
                       <td>File Mask</td>
                       <td><input type="text" name="file_mask" id="file_mask" disabled></td>
                    </tr>

                    <tr>
                      <td>Source Path</td>
                      <td><input type="text" name="source_path" id="source_path" disabled></td>
                    </tr>
                    <tr>
                      <td>Destination Path</td>
                      <td><input type="text" name="dest_path" id="dest_path" value="" disabled></td>
                    </tr>
                    <tr>
                      <td>Encoding</td>
                      <td><select id="encoding" name="encoding" disabled style="background-color: #ebebe4;">
                        <option value="0">Select</option>
                        <option value="Binary">Binary</option>
                        <option value="ASCII">ASCII</option>
                      </select></td>
                    </tr>
                    <tr>
                      <td>File Name</td>
                      <td><input type="text" name="file_name" id="file_name" value="" disabled></td>
                    </tr>
                    <tr>
                      <td>Connection</td>
                      <td><select id="ftp_connection_name" name="ftp_connection_name" disabled style="background-color: #ebebe4;"></select></td>
                    </tr>
                  </table>
            </div>
            <div id="tab_two_connection" class="tab-pane fade">
            <table table style="width:40%; margin-bottom: 20px">
            <tr>
            <td></td>
            </tr>
            <tr>
            <td>Created By</td>
            <td><input id ="created_by" type="text" name="" disabled readonly></td>
            </tr>
            <tr>
            <td>Created Date</td>
            <td><input id ="created_date" type="text" disabled readonly></td>
            </tr>
            <tr>
            <td>Updated By</td>
            <td><input id = "updated_by" type="text" disabled readonly></td>
            </tr>
            <tr>
            <td>Updated Date</td>
            <td><input id = "updated_at" type="text" disabled readonly></td>
            </tr>
            </table>
            </form>
            </div>
         </div>
      </div>
      <div id="list-of-address">
         <table style="width:100%;margin-bottom: 371px" class="connection_tab">
         </table>
      </div>

      <script type="text/javascript">
          $(document).ready(function() {

              $('#connection_table,#list-of-address,#back,#update,#cancel').hide();
              $('#search-para-address').show();

               function showall() {

                  $.ajax({
                      type: "GET",
                      url: "get_ftp_address_details",
                      dataType: "json",
                      success: function(data) {
                          $('#show_all').hide();
                          var results = data;
                          temp_results = data;
                          $('.connection_tab').append('<tr style="background-color: #D3D3D3;cursor:text;"><th>Address Name</th><th>User Name</th><th>File Name</th><th>Connection</th><th>Created By</th><th>Updated By</th></tr>');

                          if (results.length > 12) {

                              $('#list-of-address').addClass("scroll");

                          } else {
                              $('#list-of-address').removeClass("scroll");
                          }

                          for (var i = 0; i < results.length; i++) {
                              $('.connection_tab').append('<tr class = "double_click_connection" id = ' + i + '><td>' + results[i].address_name + '</td><td>' + results[i].user_name + '</td><td>' + results[i].file_name + '</td><td>' + results[i].connection_name +
                                  '</td><td>' + results[i].created_by + '</td><td>' + results[i].updated_by + '</td></tr>');
                          }

                      },
                      beforeSend: function() {
                          $('.connection_tab tr').remove();
                        },
                      error: function() {

                          $.msgBox({
                              title: "Error",
                              content: "Something went wrong",
                              type: "error",
                          });

                      }
                  });

              }

              function display_ftp_address() {

                  var search_address_name = $('#search_address_name').val();
                  var search_user_name = $('#search_user_name').val();
                  var search_connection = $('#search_connection').val();

                  if ((search_address_name == "") && (search_user_name == "") && (search_connection == "")) {

                      $.msgBox({
                          title: "Alert",
                          content: "Search Criteria Not Available",
                          type: "alert",
                      });

                  } else {

                      $.ajax({
                          type: "POST",
                          url: "ftp_address_search",
                          dataType: "json",
                          data: {
                              "_token": "{{ csrf_token() }}",
                              "search_address_name": search_address_name,
                              "search_user_name": search_user_name,

                          },

                          success: function(data) {

                              var results = data;
                              temp_results = data;
                              $('#connection_table,#search-para-address,#search,#update,#cancel').hide();
                              $('#list-of-address,#back').show();

                              if (data != "0") {

                                  $('.connection_tab').append('<tr style="background-color: #D3D3D3;cursor:text;"><th>Address Name</th><th>User Name</th><th>File Name</th><th>Connection</th><th>Created By</th><th>Updated By</th></tr>');
                                  for (var i = 0; i < results.length; i++) {
                                      $('.connection_tab').append('<tr class = "double_click_connection" id = ' + i + '><td>' + results[i].address_name + '</td><td>' + results[i].user_name + '</td><td>' + results[i].file_name + '</td><td>' + results[i].connection_name +
                                          '</td><td>' + results[i].created_by + '</td><td>' + results[i].updated_by + '</td></tr>');
                                  }

                              }
                              else
                              {
                                $('#search-para-address,#search').show();
                                $('#list-of-address,#back').hide();

                                $.msgBox({
                                    title: "Alert",
                                    content: "Result Not available",
                                    type: "alert",
                                });

                              }

                          },
                          beforeSend: function() {

                              $('.connection_tab tr').remove();

                          },
                          error: function() {

                              $.msgBox({
                                  title: "Error",
                                  content: "Something went wrong",
                                  type: "error",
                              });

                          }
                      });

                  }
              }


              $(document).on('dblclick', '.double_click_connection', function() {

                var id = $(this).attr('id');
                ftp_addr_id = temp_results[id].ftp_addr_id;
                console.log("ftp_addr_id edit",ftp_addr_id);
                $('#address_name').val(temp_results[id].address_name);
                $('#user_name').val(temp_results[id].user_name);
                $('#pwd').val(temp_results[id].password);
                $('#file_mask').val(temp_results[id].file_mask);
                $('#source_path').val(temp_results[id].source_path);
                $('#dest_path').val(temp_results[id].destination_path);
                // $('#ftp_connection_name').val(temp_results[id].connection_name);
                $('#encoding').val(temp_results[id].encoding);
                $('#file_name').val(temp_results[id].file_name);

                $('#created_by').val(temp_results[id].created_by);
                $('#created_date').val(temp_results[id].created_at);
                $('#updated_by').val(temp_results[id].updated_by);
                $('#updated_at').val(temp_results[id].updated_at);

                $.ajax({
                    type: "GET",
                    url: "get_connection_for_address",
                    dataType: "json",
                    success: function(data) {
                        var results = data;
                        console.log("results",results);
                        for (var i = 0; i < results.length; i++) {
                            if (i == '0') {
                                $('#ftp_connection_name').append("<option id =\"0\" value=\"0\">Select</option>");
                            }
                            var connection_name = results[i].connection_name
                            var ftp_conn_id = results[i].ftp_conn_id
                            console.log("connection_name",connection_name);
                            $('#ftp_connection_name').append("<option selected id =" + ftp_conn_id + " value=" + ftp_conn_id + ">" + connection_name + "</option>");
                        }
                    },
                    beforeSend: function() {},
                    error: function() {}
                });

                console.log('temp_results[id].connection_name',temp_results[id].connection_name);
                $('#connection_table,#update,#cancel').show();
                $('#list-of-address,#back,#search,#search-para-address').hide();

              });


              $(document).on('click', '#back', function() {
                  $('#search-para-address,#search').show();
                  $('#list-of-address,#connection_table,#back').hide();
                  $('#show_all').show();
              });

              $(document).on('click', '#search', function() {
                display_ftp_address();

              });

              $(document).on('click', '#show_all', function() {
                $('#connection_table,#search-para-address,#search').hide();
                $('#list-of-address,#back').show();
                showall();
              });

              $(document).on('click', '#cancel', function() {
                  $('#connection_table,#update,#cancel').hide();
                  $('#list-of-address,#back').show();

              });
          });

          $(window).load(function() {

            $("#setups").trigger('click');
            $("#communication").trigger('click');
            $("#ftp").trigger('click');
            $("#ftp_address").trigger('click');

              var fetch_search_address_name_display;
              $('input[name="search_address_name"]').autoComplete({
                  minChars: 1,
                  source: function(term, response) {
                      try {
                          fetch_search_address_name_display.abort();
                      } catch (e) {}
                      fetch_search_address_name_display = $.getJSON('communication_controller/autocomplete_address_name', {
                          fetch_search_address_name: term
                      }, function(data) {
                          response(data);
                      });
                  }
              });

              var fetch_search_user_name_display;
              $('input[name="search_user_name"]').autoComplete({
                  minChars: 1,
                  source: function(term, response) {
                      try {
                          fetch_search_user_name_display.abort();
                      } catch (e) {}
                      fetch_search_user_name_display = $.getJSON('communication_controller/autocomplete_search_user_name', {
                          fetch_search_user_name: term
                      }, function(data) {
                          response(data);
                      });
                  }
              });

              // var fetch_search_connection_display;
              // $('input[name="search_connection"]').autoComplete({
              //     minChars: 1,
              //     source: function(term, response) {
              //         try {
              //             fetch_search_connection_display.abort();
              //         } catch (e) {}
              //         fetch_search_connection_display = $.getJSON('communication_controller/autocomplete_search_connection', {
              //             fetch_search_connection: term
              //         }, function(data) {
              //             response(data);
              //         });
              //     }
              // });

          });
      </script>
   </div>

</div>
@stop

@section('lower_band')
<div class="col-xs-12 lower_band">
   <div class="bottom_buttons">
      <div  id="search_button">
         <button class="headerbuttons" id="search" type="button">Search</button>
      </div>
      <div  id="back_buttons">
         <button class="headerbuttons" id="back" type="button">Back</button>
      </div>
      <div id="update_button" style="float:left">
         <!-- <button class="headerbuttons" id="update" type="button">Update</button> -->
         <button class="headerbuttons" id="cancel" type="button">Back</button>
      </div>
   </div>
</div>
@stop
