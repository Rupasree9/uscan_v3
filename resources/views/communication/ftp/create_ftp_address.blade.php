@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" type="text/css" href="css/communication.css">
<script src="js/communication_validation.js"></script>
@stop

@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="col-xs-4 display-title">
      Create FTP Address
   </div>
</div>
@stop

@section('content')
<div class="col-xs-12 content">
   <ul class="nav nav-tabs">
      <li class="active"><a class="tab_one"  data-toggle="tab" href="#tab_one">Basic Details</a></li>
   </ul>
   <div class="tab-content">
      <div id="tab_one" class="tab-pane fade in active custom_tab">
         <form id="ftp-address-table-form" autocomplete="off" class="connection-table">
            <table style="width:40%;">
               <tr>
                  <td>Address Name</td>
                  <td><input type="text"  id = "address_name" name="address_name"></td>
               </tr>
               <tr>
                  <td>User Name</td>
                  <td><input type="text" id="user_name" name="user_name"></td>
               </tr>
               <tr>
                  <td>Password</td>
                  <td><input type="password" name="pwd" id="pwd"></td>
               </tr>
               <tr>
                  <td>File Mask</td>
                  <td><input type="text" name="file_mask" id="file_mask"></td>
               </tr>

               <tr>
                 <td>Source Path</td>
                 <td><input type="text" name="source_path" id="source_path"></td>
               </tr>
               <tr>
                 <td>Destination Path</td>
                 <td><input type="text" name="dest_path" id="dest_path" value=""></td>
               </tr>
               <tr>
                 <td>Encoding</td>
                 <td><select id="encoding" name="encoding">
                   <option value="0">Select</option>
                   <option value="Binary">Binary</option>
                   <option value="ASCII">ASCII</option>
                 </select></td>
               </tr>
               <tr>
                 <td>File Name</td>
                 <td><input type="text" name="file_name" id="file_name" value=""></td>
               </tr>
               <tr>
                 <td>Connection</td>
                 <td><select id="ftp_connection_name" name="ftp_connection_name"></select></td>
               </tr>

            </table>
         </form>
      </div>

   </div>

   <script type="text/javascript">
      jQuery.validator.addMethod('selectcheck', function(value) {
          return (value != '0');
      }, "");

      get_ftp_connection_data();

      function get_ftp_connection_data(){
        $.ajax({
          type:"GET",
          url:"get_ftp_connection",
          dataType:"json",

          success:function(data){
            var results = data;
            console.log("results",results);

            for(var i = 0;i < results.length; i++){
              if(i == '0'){
                $('#ftp_connection_name').append("<option id=\"0\" value=\"0\">Select</option> ");
              }
              var connection_name = results[i].connection_name
              var ftp_conn_id = results[i].ftp_conn_id
              $('#ftp_connection_name').append("<option id =" + ftp_conn_id + " value=" + ftp_conn_id + ">" + connection_name + "</option>");
            }

          },

          beforeSend:function(){},

          error:function(){}
        });
      }

      $(".initial_disable").prop('disabled', true);

      $(document).on('click', '#save_button', function() {

          if ($('#ftp-address-table-form').valid()) {

              var address_name = $('#address_name').val();
              var user_name = $('#user_name').val();
              var pwd = $('#pwd').val();

              var file_mask = $('#file_mask').val();
              var source_path = $('#source_path').val();
              var dest_path = $('#dest_path').val();
              var encoding = $('#encoding').val();
              var file_name = $('#file_name').val();
              var ftp_connection_name = $('#ftp_connection_name').val();

              $.ajax({
                  type: "POST",
                  url: "save_ftp_address",
                  data: {
                      "_token": "{{ csrf_token() }}",
                      "address_name":address_name,
                      "user_name": user_name,
                      "pwd": pwd,
                      "file_mask": file_mask,
                      "source_path": source_path,
                      "dest_path":dest_path,
                      "encoding": encoding,
                      "file_name": file_name,
                      "ftp_connection_name":ftp_connection_name,

                  },
                  success: function(data) {

                      $.msgBox({
                          title: "Message",
                          content: data,
                          type: "info",
                      });

                      $('#ftp-address-table-form')[0].reset();
                      $('#ftp_connection_name').html('<option value="0">Select</option>');
                      $('#ftp_connection_name').empty();
                      get_ftp_connection_data();

                  },
                  beforeSend: function() {

                  },
                  error: function() {

                  }

              });
          } else {}
      });

      $(document).on('click', '#cancel', function(){

        $('#ftp-address-table-form')[0].reset();
        $('#ftp_connection_name').html('<option value="0">Select</option>');

             });

      $(window).load(function() {
          $("#setups").trigger('click');
          $("#communication").trigger('click');
          $("#ftp").trigger('click');
          $("#ftp_address").trigger('click');

      });
   </script>
</div>
@stop
@section('lower_band')
<div class="col-xs-12 lower_band">
   <button class="headerbuttons" type="button" style="float:right;" id="cancel">Cancel</button>
   <button id="save_button" type="button" class="headerbuttons" style="float:right;" type="submit">Save</button>
</div>
@stop
