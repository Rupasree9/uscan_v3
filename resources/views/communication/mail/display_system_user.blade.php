@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" type="text/css" href="css/communication.css">
<script src="js/communication_validation.js"></script>
<style media="screen">
  #smtp_password{
    background-color: white;
  }
</style>
@stop

@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="col-xs-4 display-title">
      <span>Display System User</span>
   </div>
   <div class="col-xs-2" id = "show_all_system_users" style="float: right;text-align:center;line-height:30px;">
      <span id="show_all">Show All System Users</span>
   </div>
</div>
@stop

@section('content')
<div class="col-xs-12 content">
   <div id="connection-table-search">
      <div id="search-para-system-users">
         <form id="system-user-table-form" autocomplete="off">
            <table style="width:40%;">
              <tr>
                 <td>System User Name</td>
                 <td><input type="text"  id = "search_system_user_name" name="search_system_user_name"></td>
              </tr>
              <tr>
                 <td>Email Address</td>
                 <td><input type="email" name="search_email_id" id="search_email_id"></td>
              </tr>
              <tr>
                 <td>SMTP User Name</td>
                 <td><input type="text" name="search_smtp_user_name" id="search_smtp_user_name"></td>
              </tr>
            </table>
         </form>
      </div>
      <div id="connection_table"  style="display:none">
         <ul class="nav nav-tabs">
            <li class="active"><a id ="details_connection" data-toggle="tab" href="#tab_one_connection">Details</a></li>
            <li ><a data-toggle="tab" href="#tab_two_connection">Additional Details</a></li>
         </ul>
         <div class="tab-content">
            <div id="tab_one_connection" class="tab-pane fade in active">
               <form id = "connection-table-form" autocomplete="off">
                  <table style="width:40%;">
                    <tr>
                       <td>System User Name</td>
                       <td><input type="text"  id = "system_user_name" name="system_user_name" disabled></td>
                    </tr>
                    <tr>
                       <td>Email Address</td>
                       <td><input type="email" name="email_id" id="email_id" disabled></td>
                    </tr>
                    <tr>
                       <td>SMTP User Name</td>
                       <td><input type="text" name="smtp_user_name" id="smtp_user_name" disabled></td>
                    </tr>
                    <tr>
                       <td>SMTP Password</td>
                       <td><input type="text" name="smtp_password" id="smtp_password" disabled style="background-color: #ebebe4;"></td>
                    </tr>
                    <tr>
                      <td>Connection</td>
                      <td><select id="mail_connection" name="mail_connection" disabled style="background-color: #ebebe4;">
                          </select></td>
                    </tr>
                  </table>
            </div>
            <div id="tab_two_connection" class="tab-pane fade">
            <table table style="width:40%; margin-bottom: 20px">
            <tr>
            <td></td>
            </tr>
            <tr>
            <td>Created By</td>
            <td><input id ="created_by" type="text" name="" disabled readonly></td>
            </tr>
            <tr>
            <td>Created Date</td>
            <td><input id ="created_date" type="text" disabled readonly></td>
            </tr>
            <tr>
            <td>Updated By</td>
            <td><input id = "updated_by" type="text" disabled readonly></td>
            </tr>
            <tr>
            <td>Updated Date</td>
            <td><input id = "updated_at" type="text" disabled readonly></td>
            </tr>
            </table>
            </form>
            </div>
         </div>
      </div>
      <div id="list-of-system-users">
         <table style="width:100%;margin-bottom: 371px" class="connection_tab">
         </table>
      </div>


      <script type="text/javascript">
          $(document).ready(function() {
              $('#connection_table,#list-of-system-users,#back,#update,#cancel').hide();
              $('#search-para-system-users').show();

               function showall() {

                  $.ajax({
                      type: "GET",
                      url: "get_system_user_details",
                      dataType: "json",
                      success: function(data) {
                          $('#show_all').hide();
                          var results = data;
                          temp_results = data;
                          $('.connection_tab').append('<tr style="background-color: #D3D3D3;cursor:text;"><th>System User Name</th><th>Email Address</th><th>SMTP User Name</th><th>Connection</th><th>Created By</th><th>Updated By</th></tr>');

                          if (results.length > 12) {

                              $('#list-of-system-users').addClass("scroll");

                          } else {
                              $('#list-of-system-users').removeClass("scroll");
                          }

                          for (var i = 0; i < results.length; i++) {
                              $('.connection_tab').append('<tr class = "double_click" id = ' + i + '><td>' + results[i].conn_system_user_name + '</td><td>' + results[i].conn_system_user_email + '</td><td>' + results[i].conn_smtp_user_name + '</td><td>' + results[i].connection_name +
                                  '</td><td>' + results[i].created_by + '</td><td>' + results[i].updated_by + '</td></tr>');
                          }

                      },
                      beforeSend: function() {
                          $('.connection_tab tr').remove();
                          //$("#list-of-system-users,.parent_bottom_buttons,#parent_back_buttons").addClass("show");
                          $(".customer_type").addClass("hide");
                          $("#parent_change,#parent_search_button,#show_all_parents,#child_change,.child_bottom_buttons,#search_button,#show_all_customer").removeClass("show");
                      },
                      error: function() {

                          $.msgBox({
                              title: "Error",
                              content: "Something went wrong",
                              type: "error",
                          });

                      }
                  });

              }

              function display_system_users() {

                  var search_system_user_name = $('#search_system_user_name').val();
                  var search_email_id = $('#search_email_id').val();
                  var search_smtp_user_name = $('#search_smtp_user_name').val();

                  if ((search_system_user_name == "") && (search_email_id == "") && (search_smtp_user_name == "")) {

                      $.msgBox({
                          title: "Alert",
                          content: "Search Criteria Not Available",
                          type: "alert",
                      });

                  } else {

                      $.ajax({
                          type: "POST",
                          url: "search_system_user",
                          dataType: "json",
                          data: {
                              "_token": "{{ csrf_token() }}",
                              "search_system_user_name": search_system_user_name,
                              "search_email_id": search_email_id,
                              "search_smtp_user_name": search_smtp_user_name,
                          },

                          success: function(data) {

                              var results = data;
                              temp_results = data;

                              $('#connection_table,#search-para-system-users,#search,#update,#cancel').hide();
                              $('#list-of-system-users,#back').show();

                              if (data != "0") {

                                  $('.connection_tab').append('<tr style="background-color: #D3D3D3;cursor:text;"><th>System User Name</th><th>Email Address</th><th>SMTP User Name</th><th>Connection</th><th>Created By</th><th>Updated By</th></tr>');
                                  for (var i = 0; i < results.length; i++) {
                                      $('.connection_tab').append('<tr class = "double_click" id = ' + i + '><td>' + results[i].conn_system_user_name + '</td><td>' + results[i].conn_system_user_email + '</td><td>' + results[i].conn_smtp_user_name + '</td><td>' + results[i].connection_name +
                                          '</td><td>' + results[i].created_by + '</td><td>' + results[i].updated_by + '</td></tr>');
                                  }
                              } else {

                                $('#search-para-system-users,#search').show();
                                $('#list-of-system-users,#back').hide();

                                $.msgBox({
                                    title: "Alert",
                                    content: "Result Not available",
                                    type: "alert",
                                });

                                  $("#list-of-system-users,#parent_back_buttons").removeClass("show");
                                  $(".customer_type").removeClass("hide");
                                  $("#parent_change,#parent_search_button,#show_all_parents").addClass("show");

                              }

                          },
                          beforeSend: function() {

                              $('.connection_tab tr').remove();

                          },
                          error: function() {

                              $.msgBox({
                                  title: "Error",
                                  content: "Something went wrong",
                                  type: "error",
                              });

                          }
                      });

                  }
              }

              $(document).on('dblclick', '.double_click', function() {

                var id = $(this).attr('id');
                system_user_id = temp_results[id].system_user_id;
                console.log("system_user_id edit",system_user_id);
                $('#system_user_name').val(temp_results[id].conn_system_user_name);
                $('#email_id').val(temp_results[id].conn_system_user_email);
                $('#smtp_user_name').val(temp_results[id].conn_smtp_user_name);
                $('#smtp_password').val(temp_results[id].conn_smtp_pwd);

                console.log("system_user_id edit",system_user_id);
                $.ajax({
                    type: "GET",
                    url: "get_connection",
                    dataType: "json",
                    success: function(data) {
                        var results = data;
                        console.log("results",results);
                        for (var i = 0; i < results.length; i++) {
                            if (i == '0') {
                                $('#mail_connection').append("<option id =\"0\" value=\"0\">Select</option>");
                            }
                            var connection_name = results[i].connection_name
                            var mail_conn_id = results[i].mail_conn_id
                            console.log("connection_name",connection_name);
                            $('#mail_connection').append("<option selected id =" + mail_conn_id + " value=" + mail_conn_id + ">" + connection_name + "</option>");
                        }
                    },
                    beforeSend: function() {},
                    error: function() {}
                });
                // $('#mail_connection').val(temp_results[id].mail_connection);
                $('#created_by').val(temp_results[id].created_by);
                $('#created_date').val(temp_results[id].created_at);
                $('#updated_by').val(temp_results[id].updated_by);
                $('#updated_at').val(temp_results[id].updated_at);

                $('#connection_table,#update,#cancel').show();
                $('#list-of-system-users,#back,#search,#search-para-system-users').hide();

              });


              $(document).on('click', '#back', function() {
                  $('#search-para-system-users,#search').show();
                  $('#list-of-system-users,#connection_table,#back').hide();
                  $('#show_all').show();
              });

              $(document).on('click', '#search', function() {
                display_system_users();
              });

              $(document).on('click', '#show_all', function() {
                $('#connection_table,#search-para-system-users,#search').hide();
                $('#list-of-system-users,#back').show();
                showall();
              });

              $(document).on('click', '#cancel', function() {
                  $('#connection_table,#update,#cancel').hide();
                  $('#list-of-system-users,#back').show();

              });
          });

          $(window).load(function() {
            $("#setups").trigger('click');
            $("#communication").trigger('click');
            $("#mail").trigger('click');
            $("#address").trigger('click');
            $("#system_users").trigger('click');

              var fetch_search_system_user_name_display;
              $('input[name="search_system_user_name"]').autoComplete({
                  minChars: 1,
                  source: function(term, response) {
                      try {
                          fetch_search_system_user_name_display.abort();
                      } catch (e) {}
                      fetch_search_system_user_name_display = $.getJSON('communication_controller/autocomplete_system_user_name', {
                          fetch_search_system_user_name: term
                      }, function(data) {
                          response(data);
                      });
                  }
              });

              var fetch_search_email_id_display;
              $('input[name="search_email_id"]').autoComplete({
                  minChars: 1,
                  source: function(term, response) {
                      try {
                          fetch_search_email_id_display.abort();
                      } catch (e) {}
                      fetch_search_email_id_display = $.getJSON('communication_controller/autocomplete_email_id', {
                          fetch_search_email_id_display: term
                      }, function(data) {
                          response(data);
                      });
                  }
              });
              var fetch_search_smtp_user_name_display;
              $('input[name="search_smtp_user_name"]').autoComplete({
                  minChars: 1,
                  source: function(term, response) {
                      try {
                          fetch_search_smtp_user_name_display.abort();
                      } catch (e) {}
                      fetch_search_smtp_user_name_display = $.getJSON('communication_controller/autocomplete_smtp_user_name', {
                          fetch_search_smtp_user_name_display: term
                      }, function(data) {
                          response(data);
                      });
                  }
              });

          });
      </script>
   </div>

</div>
@stop

@section('lower_band')
<div class="col-xs-12 lower_band">
   <div class="bottom_buttons">
      <div  id="search_button">
         <button class="headerbuttons" id="search" type="button">Search</button>
      </div>
      <div  id="back_buttons">
         <button class="headerbuttons" id="back" type="button">Back</button>
      </div>
      <div id="update_button" style="float:left">
         <!-- <button class="headerbuttons" id="update" type="button">Update</button> -->
         <button class="headerbuttons" id="cancel" type="button">Back</button>
      </div>
   </div>
</div>
@stop
