@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" type="text/css" href="css/communication.css">
<script src="js/communication_validation.js"></script>
@stop

@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="col-xs-4 display-title">
      <span>Display Mail Connection</span>
   </div>
   <div class="col-xs-2" id = "show_all_connections" style="float: right;text-align:center;line-height:30px;">
      <span id="show_all_p">Show All Connections</span>
   </div>
</div>
@stop

@section('content')
<div class="col-xs-12 content">
   <div id="connection-table-search">
      <div id="search-para-connection">
         <form id="connection_search_form" autocomplete="off">
            <table style="width:40%;">
               <tr>
                 <td>Connection Name</td>
                 <td><input type="text"  id = "search_connection_name" name="search_connection_name"></td>
               </tr>
               <tr>
                 <td>Protocol</td>
                 <td><input type="text" name="search_protocol" id="search_protocol"></td>
               </tr>
               <tr>
                  <td>Server</td>
                  <td><input type="text" name="search_server_name" id="search_server_name"></td>
               </tr>
            </table>
         </form>
      </div>
      <div id="connection_table"  style="display:none">
         <ul class="nav nav-tabs">
            <li class="active"><a id ="details_connection" data-toggle="tab" href="#tab_one_connection">Details</a></li>
            <li ><a data-toggle="tab" href="#tab_two_connection">Additional Details</a></li>
         </ul>
         <div class="tab-content">
            <div id="tab_one_connection" class="tab-pane fade in active">
               <form id = "connection-table-form" autocomplete="off">
                  <table style="width:40%;">
                    <tr>
                       <td>Connection Name</td>
                       <td><input type="text"  id = "connection_name" name="connection_name" disabled></td>
                    </tr>
                    <tr>
                       <td>Protocol</td>
                       <td><input type="text" name="protocol" id="protocol" disabled></td>
                    </tr>
                    <tr>
                       <td>Server</td>
                       <td><input type="text" name="server" id="server" disabled></td>
                    </tr>
                    <tr>
                       <td>Port</td>
                       <td><input type="text" name="port" id="port" disabled></td>
                    </tr>
                    <tr>
                      <td>Request Notification</td>
                      <td><input type="checkbox" name="request_notification" id="request_notification" value="" disabled></td>
                    </tr>
                    <tr>
                      <td>Proxy Mode</td>
                      <td><select id="proxy_mode" name="proxy_mode" disabled style="background-color: #ebebe4;">
                        <option value="0">Select</option>
                        <option value="No Proxy">No Proxy</option>
                        <option value="Specific Proxy">Specific Proxy</option>
                        <option value="Proxy Group">Proxy Group</option>
                      </select></td>
                    </tr>
                    <tr>
                      <td>Dump Messages</td>
                      <td><input type="checkbox" name="dump_message" id="dump_message" value="" disabled></td>
                    </tr>
                  </table>
            </div>
            <div id="tab_two_connection" class="tab-pane fade">
            <table table style="width:40%; margin-bottom: 20px">
            <tr>
            <td></td>
            </tr>
            <tr>
            <td>Created By</td>
            <td><input id ="created_by" type="text" name="" disabled readonly></td>
            </tr>
            <tr>
            <td>Created Date</td>
            <td><input id ="created_date" type="text" disabled readonly></td>
            </tr>
            <tr>
            <td>Updated By</td>
            <td><input id = "updated_by" type="text" disabled readonly></td>
            </tr>
            <tr>
            <td>Updated Date</td>
            <td><input id = "updated_at" type="text" disabled readonly></td>
            </tr>
            </table>
            </form>
            </div>
         </div>
      </div>
      <div id="list-of-connection">
         <table style="width:100%;margin-bottom: 371px" class="connection_tab">
         </table>
      </div>


      <script type="text/javascript">
          $(document).ready(function() {

              $('#connection_table,#list-of-connection,#back,#update,#cancel').hide();
              $('#search-para-connection').show();


               function showall_p() {

                  $.ajax({
                      type: "GET",
                      url: "get_connection_details",
                      dataType: "json",
                      success: function(data) {
                          $('#show_all_p').hide();
                          var results = data;
                          temp_results = data;
                          $('.connection_tab').append('<tr style="background-color: #D3D3D3;cursor:text;"><th>Connection Name</th><th>Protocol</th><th>Server</th><th>Port</th><th>Created by</th><th>Updated by</th></tr>');

                          if (results.length > 12) {

                              $('#list-of-connection').addClass("scroll");

                          } else {
                              $('#list-of-connection').removeClass("scroll");
                          }

                          for (var i = 0; i < results.length; i++) {
                              $('.connection_tab').append('<tr class = "double_click_parent" id = ' + i + '><td>' + results[i].connection_name + '</td><td>' + results[i].protocol + '</td><td>' + results[i].server + '</td><td>' + results[i].port +
                                  '</td><td>' + results[i].created_by + '</td><td>' + results[i].updated_by + '</td></tr>');
                          }

                      },
                      beforeSend: function() {
                          $('.connection_tab tr').remove();
                      },
                      error: function() {

                          $.msgBox({
                              title: "Error",
                              content: "Something went wrong",
                              type: "error",
                          });

                      }
                  });

              }

              function display_parent_data() {

                  var search_connection_name = $('#search_connection_name').val();
                  var search_protocol = $('#search_protocol').val();
                  var search_server_name = $('#search_server_name').val();

                  if ((search_connection_name == "") && (search_protocol == "") && (search_server_name == "")) {

                      $.msgBox({
                          title: "Alert",
                          content: "Search Criteria Not Available",
                          type: "alert",
                      });

                  } else {

                      $.ajax({
                          type: "POST",
                          url: "connection_search",
                          dataType: "json",
                          data: {
                              "_token": "{{ csrf_token() }}",
                              "search_connection_name": search_connection_name,
                              "search_protocol": search_protocol,
                              "search_server_name": search_server_name,
                          },

                          success: function(data) {

                              var results = data;
                              temp_results = data;

                              $('#connection_table,#search-para-connection,#search,#update,#cancel').hide();
                              $('#list-of-connection,#back').show();

                              if (data != "0") {

                                  $('.connection_tab').append('<tr style="background-color: #D3D3D3;cursor:text;"><th>Connection Name</th><th>Protocol</th><th>Server</th><th>Port</th><th>Created by</th><th>Updated by</th></tr>');
                                  for (var i = 0; i < results.length; i++) {
                                      $('.connection_tab').append('<tr class = "double_click_parent" id = ' + i + '><td>' + results[i].connection_name + '</td><td>' + results[i].protocol + '</td><td>' + results[i].server + '</td><td>' + results[i].port +
                                          '</td><td>' + results[i].created_by + '</td><td>' + results[i].updated_by + '</td></tr>');
                                  }
                              } else {
                                $('#search-para-connection,#search').show();
                                $('#list-of-connection,#back').hide();

                                      $.msgBox({
                                          title: "Alert",
                                          content: "Result Not available",
                                          type: "alert",
                                      });

                              }

                          },
                          beforeSend: function() {

                              $('.connection_tab tr').remove();

                          },
                          error: function() {

                              $.msgBox({
                                  title: "Error",
                                  content: "Something went wrong",
                                  type: "error",
                              });

                          }
                      });

                  }
              }

              $(document).on('dblclick', '.double_click_parent', function() {

                var id = $(this).attr('id');
                mail_conn_id = temp_results[id].mail_conn_id;
                console.log("mail_conn_id edit",mail_conn_id);
                $('#connection_name').val(temp_results[id].connection_name);
                $('#protocol').val(temp_results[id].protocol);
                $('#server').val(temp_results[id].server);
                $('#port').val(temp_results[id].port);
                $('#proxy_mode').val(temp_results[id].proxy_mode);
                $('#created_by').val(temp_results[id].created_by);
                $('#created_date').val(temp_results[id].created_at);
                $('#updated_by').val(temp_results[id].updated_by);
                $('#updated_at').val(temp_results[id].updated_at);

                if (temp_results[id].request_notification == 0) {
                    $('#request_notification').prop('checked', false);
                } else {
                    $('#request_notification').prop('checked', true);
                }

                if (temp_results[id].dump_message == 0) {
                    $('#dump_message').prop('checked', false);
                } else {
                    $('#dump_message').prop('checked', true);
                }

                $('#connection_table,#cancel').show();
                $('#list-of-connection,#search,#search-para-connection,#update,#back').hide();

              });

              $(document).on('click', '#back', function() {
                  $('#search-para-connection,#search').show();
                  $('#list-of-connection,#connection_table,#back').hide();
                  $('#show_all_p').show();
              });

              $(document).on('click', '#search', function() {
                display_parent_data();
              });

              $(document).on('click', '#show_all_p', function() {
                $('#connection_table,#search-para-connection,#search').hide();
                $('#list-of-connection,#back').show();
                showall_p();
              });

              $(document).on('click', '#cancel', function() {
                  $('#connection_table,#update,#cancel').hide();
                  $('#list-of-connection,#back').show();

              });
    });

          $(window).load(function() {
            $("#setups").trigger('click');
            $("#communication").trigger('click');
            $("#mail").trigger('click');
            $("#connection").trigger('click');

              var fetch_search_connection_name_display;
              $('input[name="search_connection_name"]').autoComplete({
                  minChars: 1,
                  source: function(term, response) {
                      try {
                          fetch_search_connection_name_display.abort();
                      } catch (e) {}
                      fetch_search_connection_name_display = $.getJSON('communication_controller/autocomplete_connection_name', {
                          fetch_search_connection_name: term
                      }, function(data) {
                          response(data);
                      });
                  }
              });

              var fetch_search_protocol_display;
              $('input[name="search_protocol"]').autoComplete({
                  minChars: 1,
                  source: function(term, response) {
                      try {
                          fetch_search_protocol_display.abort();
                      } catch (e) {}
                      fetch_search_protocol_display = $.getJSON('communication_controller/autocomplete_protocol', {
                          fetch_search_protocol_display: term
                      }, function(data) {
                          response(data);
                      });
                  }
              });
              var fetch_search_server_display;
              $('input[name="search_server_name"]').autoComplete({
                  minChars: 1,
                  source: function(term, response) {
                      try {
                          fetch_search_server_display.abort();
                      } catch (e) {}
                      fetch_search_server_display = $.getJSON('communication_controller/autocomplete_server', {
                          fetch_search_server_display: term
                      }, function(data) {
                          response(data);
                      });
                  }
              });

          });
      </script>
   </div>

</div>
@stop

@section('lower_band')
<div class="col-xs-12 lower_band">
   <div class="bottom_buttons">
      <div  id="search_button">
         <button class="headerbuttons" id="search" type="button">Search</button>
      </div>
      <div  id="back_buttons">
         <button class="headerbuttons" id="back" type="button">Back</button>
      </div>
      <div id="update_button" style="float:left">
         <!-- <button class="headerbuttons" id="update" type="button">Update</button> -->
         <button class="headerbuttons" id="cancel" type="button">Back</button>
      </div>
   </div>
</div>
@stop
