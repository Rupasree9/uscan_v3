@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" type="text/css" href="css/communication.css">
<script src="js/communication_validation.js"></script>
@stop

@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="col-xs-4 display-title">
      <span>Create Enterprise Users</span>
   </div>
</div>
@stop

@section('content')
<div class="col-xs-12 content">
   <ul class="nav nav-tabs">
      <li class="active"><a class="tab_one" data-toggle="tab" href="#tab_one">Details</a></li>
   </ul>
   <div class="user_type">
      <table id = "type" style="width:55%;">
         <tr>
            <td>Type</td>
            <td>
               <select id="user-usergroup">
                 <option value="0">Select</option>
                 <option value="1">User Group</option>
                 <option value="2">User</option>
               </select>
            </td>
         </tr>
         <tr>
            <td></td>
         </tr>
      </table>
   </div>
   <div id="usergroup-table" style="display:none">
      <div class="tab-content">
         <div id="tab_one_usergroup" class="tab-pane fade in active">
            <form id="usergroup-table-form" autocomplete="off">
               <table style="margin-bottom: 20px;width:55%;">
                  <tr>
                     <td>Enterprise User Group Name</td>
                     <td><input type="text" name="user_group_name" id="user_group_name" value=""></td>
                  </tr>
                  <tr>
                     <td>Enterprise User Group ID</td>
                     <td><input type="text" name="user_group_id" id="user_group_id" value=""></td>
                  </tr>
                  <tr>
                     <td><input type="hidden" id = "usergroup_token_id" name="_token" value="{{ csrf_token() }}"></td>
                  </tr>
               </table>
            </form>
         </div>
      </div>
   </div>
   <div id="user-table" style="display:none">
      <div class="tab-content">
         <div id="tab_one" class="tab-pane fade in active">
            <form id="user-table-form" autocomplete="off">
            <table style="margin-bottom: 20px;width:55%;">
               <tr>
                  <td></td>
               </tr>
               <tr>
                  <td>Enterprise User Name</td>
                  <td><input class="initial_disable" type="text" name="ent_user_name" id="ent_user_name" >
                  </td>
               </tr>
               <tr>
                 <td>Email ID </td>
                 <td><input class="initial_disable" type="email" id="ent_email_id"></td>
               </tr>
               <tr>
                  <td>Enterprise User Group Name</td>
                  <td><select name = "ent_user_group_id" id = "ent_user_group_id">
                     </select></td>
                  </tr>
            </table>
          </form>
         </div>
      </div>
   </div>

   <script type="text/javascript">
      jQuery.validator.addMethod('selectcheck', function(value) {
          return (value != '0');
      }, "");

      $(document).ready(function() {

          $('#ent_user_group_id').empty();
          $('#ent_user_group_id').append("<option id =\"null\" value=\"0\">Select</option>");

          get_user_group_data();

          function get_user_group_data(){

            $.ajax({
                type: "GET",
                url: "get_enterprise_user_group",
                dataType: "json",
                success: function(data) {

                    var results = data;
                    console.log(JSON.stringify(data));

                    for (var i = 0; i < results.length; i++) {
                    var ent_user_group_name = results[i].ent_user_group_name;
                    var user_group_id = results[i].user_group_id;
                    $('#ent_user_group_id').append("<option id =" + user_group_id + " value=" + user_group_id + ">" + ent_user_group_name + "</option>");
                    }

                },
                beforeSend: function() {

                },
                error: function() {
                    alert('Sorry we are not available');
                }
            });
          }

          $('#user-usergroup').on('change', function() {

              if (this.value == 1) {

                  $("#usergroup-table,#usergroup_button").show();
                  $('#user-table,#user_button,#back_button_for_usergroup_results_div').hide();

              } else if (this.value == 2) {
                $("#usergroup-table,#usergroup_button,#back_button_for_usergroup_results_div").hide();
                $('#user-table,#user_button').show();
                $('#ent_user_group_id').empty();
                $('#ent_user_group_id').append("<option id =\"null\" value=\"0\">Select</option>");
                get_user_group_data();

            } else {
                $("#usergroup-table,#usergroup_button,#back_button_for_usergroup_results_div").hide();
                $('#user-table,#user_button').hide();
              }

          });

          $(document).on('click', '#back_button_for_usergroup_results_div', function() {

              $("#list-of-parents,#back_button_for_usergroup_results_div").removeClass("show");
              $(".customer_type").removeClass("hide");
              $("#usergroup-table,#usergroup_button,#show_all_usergroup").addClass("show");

          });

          $(document).on('click', '#usergroup_button_save_id', function() {
              if ($('#usergroup-table-form').valid()) {
                  var user_group_name = $('#user_group_name').val();
                  var user_group_id = $('#user_group_id').val();

                  if ((user_group_name == "") || (user_group_id == "")) {

                      $.msgBox({
                          title: "Message",
                          content: "Please Enter All the values to Continue",
                          type: "info",
                      });

                  } else

                  {
                      $.ajax({
                          type: "POST",
                          url: "user_group_insert",
                          data: {
                              "_token": "{{ csrf_token() }}",
                              "user_group_name": user_group_name,
                              "user_group_id": user_group_id,
                          },
                          success: function(data) {
                              $.msgBox({
                                  title: "Message",
                                  content: "New Enterprise User Group added<br> successfully",
                                  type: "info",
                              });

                              $('#usergroup-table-form')[0].reset();
                              $('#user-usergroup').val("0");
                              $("#usergroup-table,#usergroup_button,#back_button_for_usergroup_results_div").hide();
                              $('#user-table,#user_button').hide();

                          },

                          beforeSend: function() {

                          },
                          error: function() {

                          }

                      });

                  }

              } else {

              }

          });

          $(document).on('click', '#user_button_save_id', function() {

              if ($('#user-table-form').valid()) {

                  var ent_user_name = $('#ent_user_name').val();
                  var ent_email_id = $('#ent_email_id').val();
                  var ent_user_group_id = $('#ent_user_group_id').val();

                  $.ajax({
                      type: "POST",
                      url: "user_insert",
                      data: {
                          "_token": "{{ csrf_token() }}",
                          "ent_user_name": ent_user_name,
                          "ent_email_id": ent_email_id,
                          "ent_user_group_id": ent_user_group_id,

                      },
                      success: function(data) {

                          $.msgBox({
                              title: "Message",
                              content: data,
                              type: "info",
                          });

                          $('#user-table-form')[0].reset();
                          $('#user-usergroup').val("0");
                          $("#usergroup-table,#usergroup_button,#back_button_for_usergroup_results_div").hide();
                          $('#user-table,#user_button').hide();


                      },


                      beforeSend: function() {

                      },
                      error: function() {}

                  });
              } else {

              }

          });

          $('#usergroup_button_cancel_id,#user_button_cancel_id').on('click', function() {

            $('#user-table-form')[0].reset();
            $('#user-usergroup').val("0");
            $("#usergroup-table,#usergroup_button,#back_button_for_usergroup_results_div").hide();
            $('#user-table,#user_button').hide();

          });

      });

      $(window).load(function() {
        $("#setups").trigger('click');
        $("#communication").trigger('click');
        $("#mail").trigger('click');
        $("#address").trigger('click');
        $("#enterprise_users").trigger('click');
      });
   </script>
</div>
@stop

@section('lower_band')
<div class="col-xs-12 lower_band">
   <div id="usergroup_button" style="float:right; display:none">
      <button id="usergroup_button_save_id" class="headerbuttons" type="button">Save</button>
      <button id="usergroup_button_cancel_id"class="headerbuttons" type="button">Cancel</button>
   </div>
   <div id="user_button" style="float:right;display:none">
      <button id="user_button_save_id" class="headerbuttons" type="button">Save</button>
      <button id="user_button_cancel_id"class="headerbuttons" type="button">Cancel</button>
   </div>
   <div id="back_button_for_usergroup_results_div" style="display:none">
      <button id="back_button_for_parent_results" class="headerbuttons" type="button">BACK</button>
   </div>
</div>
@stop
