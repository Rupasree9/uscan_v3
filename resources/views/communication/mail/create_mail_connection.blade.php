@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" type="text/css" href="css/communication.css">
<script src="js/communication_validation.js"></script>
@stop

@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="col-xs-4 display-title">
      Create Mail Connection
   </div>
</div>
@stop

@section('content')
<div class="col-xs-12 content">
   <ul class="nav nav-tabs">
      <li class="active"><a class="tab_one"  data-toggle="tab" href="#tab_one">Basic Details</a></li>
   </ul>
   <div class="tab-content">
      <div id="tab_one" class="tab-pane fade in active custom_tab">
         <form id="connection-table-form" autocomplete="off" class="connection-table">
            <table style="width:40%;">
               <tr>
                  <td>Connection Name</td>
                  <td><input type="text"  id = "connection_name" name="connection_name"></td>
               </tr>
               <tr>
                  <td>Protocol</td>
                  <td><input type="text" name="protocol" id="protocol"></td>
               </tr>
               <tr>
                  <td>Server</td>
                  <td><input type="text" name="server" id="server"></td>
               </tr>
               <tr>
                  <td>Port</td>
                  <td><input type="text" name="port" id="port"></td>
               </tr>
               <tr>
                 <td>Request Notification</td>
                 <td><input type="checkbox" name="request_notification" id="request_notification" value=""></td>
               </tr>
               <tr>
                 <td>Proxy Mode</td>
                 <td><select id="proxy_mode" name="proxy_mode">
                   <option value="0">Select</option>
                   <option value="No Proxy">No Proxy</option>
                   <option value="Specific Proxy">Specific Proxy</option>
                   <option value="Proxy Group">Proxy Group</option>
                 </select></td>
               </tr>
               <tr>
                 <td>Dump Messages</td>
                 <td><input type="checkbox" name="dump_message" id="dump_message" value=""></td>
               </tr>
            </table>
         </form>
      </div>
      <!-- <div class="">
        <form class="" action="index.html" method="post">
          <table>
            <tr>
              <td>Request Notification</td>
              <td><input type="checkbox" name="request_notification" id="request_notification" value=""></td>
            </tr>
            <tr>
              <td>Proxy Mode</td>
              <td><select class="proxy_mode" name="proxy_mode">
                <option value="">Select</option>
                <option value="">No Proxy</option>
                <option value="">Specific Proxy</option>
                <option value="">Proxy Group</option>
              </select></td>
            </tr>
            <tr>
              <td>Dump Messages</td>
              <td><input type="checkbox" name="dump_message" id="dump_message" value=""></td>
            </tr>
          </table>
        </form>
      </div> -->
   </div>

   <script type="text/javascript">
      jQuery.validator.addMethod('selectcheck', function(value) {
          return (value != '0');
      }, "");

      $(".initial_disable").prop('disabled', true);

      $(document).on('click', '#save_button', function() {

          if ($('#connection-table-form').valid()) {

              var connection_name = $('#connection_name').val();
              var protocol = $('#protocol').val();
              var server = $('#server').val();
              var port = $('#port').val();
              var proxy_mode = $('#proxy_mode').val();

              if ($('#request_notification').is(":checked")) {
                  request_notification = 1;
              } else {
                  request_notification = 0;
              }

              if ($('#dump_message').is(":checked")) {
                  dump_message = 1;
              } else {
                  dump_message = 0;
              }

              console.log("connection_name",connection_name,protocol,server,port,proxy_mode);
              
              $.ajax({
                  type: "POST",
                  url: "save_communication",
                  data: {
                      "_token": "{{ csrf_token() }}",
                      "connection_name": connection_name,
                      "protocol": protocol,
                      "server_name": server,
                      "port": port,
                      "proxy_mode": proxy_mode,
                      "request_notification": request_notification,
                      "dump_message": dump_message,
                  },
                  success: function(data) {

                      $.msgBox({
                          title: "Message",
                          content: data,
                          type: "info",
                      });

                      $('#connection-table-form')[0].reset();
                      $('#proxy_mode').html('<option value="0">Select</option>');

                  },


                  beforeSend: function() {

                  },
                  error: function() {

                  }

              });




          } else {}
      });

      $(document).on('click', '#cancel', function(){

                $('#connection-table-form')[0].reset();
                // $("#plant_id,#portal_id,#program_id,#distribution_strategy,#activeid").addClass("initial_disable_color");
                // $(".initial_disable").prop('disabled', true);
                // $('#plant_id').html('<option value="0">Select</option>');
                $('#proxy_mode').html('<option value="0">Select</option>');
                // $('#plant_id-error,#customer_id-error,#task_type-error,#portal_id-error,#program_id-error,#task_name-error').remove();
                // $('#plant_id,#customer_id,#task_type,#portal_id,#program_id,#task_name').removeClass("error");

             });

      $(window).load(function() {
          $("#setups").trigger('click');
          $("#communication").trigger('click');
          $("#mail").trigger('click');
          $("#connection").trigger('click');

      });
   </script>
</div>
@stop
@section('lower_band')
<div class="col-xs-12 lower_band">
   <button class="headerbuttons" type="button" style="float:right;" id="cancel">Cancel</button>
   <button id="save_button" type="button" class="headerbuttons" style="float:right;" type="submit">Save</button>
</div>
@stop
