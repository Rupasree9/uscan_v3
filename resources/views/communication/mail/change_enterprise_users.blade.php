@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" type="text/css" href="css/communication.css">
<script src="js/communication_validation.js"></script>
@stop

@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="col-xs-4 display-title">
      <span>Change Enterprise Users</span>
   </div>

   <div class="col-xs-3" id = "show_all_users" style="float: right;text-align:center;line-height:30px;width:20%;" >
      <span id="show_all">Show All Enterprise Users</span>
   </div>
</div>
@stop

@section('content')
<div class="col-xs-12 content">
   <div id="connection-table-search">
      <div id="search-para-enterprise-users">
         <form id="user-table-form" autocomplete="off">
            <table style="width:55%;">
              <tr>
                 <td>Enterprise User Name</td>
                 <td><input type="text"  id = "search_ent_user_name" name="search_ent_user_name"></td>
              </tr>
              <tr>
                 <td>Email ID</td>
                 <td><input type="email" name="search_ent_email_id" id="search_ent_email_id"></td>
              </tr>
              <tr>
                 <td>Enterprise User Group Name</td>
                 <td><input type="text" name="search_ent_user_group_id" id="search_ent_user_group_id"></td>
              </tr>
            </table>
         </form>
      </div>
      <div id="connection_table"  style="display:none">
         <ul class="nav nav-tabs">
            <li class="active"><a id ="details_connection" data-toggle="tab" href="#tab_one_connection">Details</a></li>
            <li ><a data-toggle="tab" href="#tab_two_connection">Additional Details</a></li>
         </ul>
         <div class="tab-content">
            <div id="tab_one_connection" class="tab-pane fade in active">
               <form id = "connection-table-form" autocomplete="off">
                  <table style="width:55%;">
                    <tr>
                       <td></td>
                    </tr>
                    <tr>
                       <td>Enterprise User Name</td>
                       <td><input class="initial_disable" type="text" name="ent_user_name" id="ent_user_name" >
                       </td>
                    </tr>
                    <tr>
                      <td>Email ID </td>
                      <td><input class="initial_disable" type="email" id="ent_email_id"></td>
                    </tr>
                    <tr>
                       <td>Enterprise User Group Name</td>
                       <td><select name = "ent_user_group_id" id = "ent_user_group_id">
                          </select></td>
                       </tr>
                  </table>
            </div>
            <div id="tab_two_connection" class="tab-pane fade">
            <table table style="width:55%; margin-bottom: 20px">
            <tr>
            <td></td>
            </tr>
            <tr>
            <td>Created By</td>
            <td><input id ="created_by" type="text" name="" disabled readonly></td>
            </tr>
            <tr>
            <td>Created Date</td>
            <td><input id ="created_date" type="text" disabled readonly></td>
            </tr>
            <tr>
            <td>Updated By</td>
            <td><input id = "updated_by" type="text" disabled readonly></td>
            </tr>
            <tr>
            <td>Updated Date</td>
            <td><input id = "updated_at" type="text" disabled readonly></td>
            </tr>
            </table>
            </form>
            </div>
         </div>
      </div>
      <div id="list-of-enterprise-users">
         <table style="width:100%;margin-bottom: 371px" class="connection_tab">
         </table>
      </div>


      <script type="text/javascript">
          $(document).ready(function() {
              $('#connection_table,#list-of-enterprise-users,#back,#update,#cancel').hide();
              $('#search-para-enterprise-users').show();

               function showall() {

                  $.ajax({
                      type: "GET",
                      url: "user_results",
                      dataType: "json",
                      success: function(data) {
                          $('#show_all').hide();
                          var results = data;
                          temp_results = data;
                          $('.connection_tab').append('<tr style="background-color: #D3D3D3;cursor:text;"><th>Enterprise User Name</th><th>Email ID</th><th>Enterprise User Group Name</th><th>Edit</th><th>Delete</th></tr>');

                          if (results.length > 12) {

                              $('#list-of-enterprise-users').addClass("scroll");

                          } else {
                              $('#list-of-enterprise-users').removeClass("scroll");
                          }

                          for (var i = 0; i < results.length; i++) {
                            if(results[i].ent_user_group_name == null){
                              results[i].ent_user_group_name = '--';
                            }
                              $('.connection_tab').append('<tr class = "double_click" id = ' + i + '><td>' + results[i].ent_user_name + '</td><td>' + results[i].ent_user_id + '</td><td>' + results[i].ent_user_group_name +
                                  '</td><td><button id="' + i + '" type="button" class="edit btn btn-default glyphicon glyphicon-edit edit_color" aria-label="Left Align"></button></td><td><button id="' + results[i].mail_comm_id + '" type="button" class=" delete btn btn-default delete_color" aria-label="Left Align"><span class="glyphicon glyphicon-remove delete_color" aria-hidden="true"></span></button></td></tr>');
                          }

                      },
                      beforeSend: function() {
                          $('.connection_tab tr').remove();
                      },
                      error: function() {

                          $.msgBox({
                              title: "Error",
                              content: "Something went wrong",
                              type: "error",
                          });

                      }
                  });

              }

              function display_usergroup_users() {

                  var search_ent_user_name = $('#search_ent_user_name').val();
                  var search_ent_email_id = $('#search_ent_email_id').val();
                  var search_ent_user_group_id = $('#search_ent_user_group_id').val();

                  if ((search_ent_user_name == "") && (search_ent_email_id == "") && (search_ent_user_group_id == "")) {

                      $.msgBox({
                          title: "Alert",
                          content: "Search Criteria Not Available",
                          type: "alert",
                      });

                  } else {

                      $.ajax({
                          type: "POST",
                          url: "enterprise_user_search",
                          dataType: "json",
                          data: {
                              "_token": "{{ csrf_token() }}",
                              "search_ent_user_name": search_ent_user_name,
                              "search_ent_email_id": search_ent_email_id,
                              "search_ent_user_group_id": search_ent_user_group_id,
                          },

                          success: function(data) {

                              var results = data;
                              temp_results = data;
                              $('#connection_table,#search-para-enterprise-users,#search,#update,#cancel').hide();
                              $('#list-of-enterprise-users,#back').show();

                              if (data != "0") {

                                  $('.connection_tab').append('<tr style="background-color: #D3D3D3;cursor:text;"><th>Enterprise User Name</th><th>Email ID</th><th>Enterprise User Group Name</th><th>Edit</th><th>Delete</th></tr>');

                                  for (var i = 0; i < results.length; i++) {
                                    if(results[i].ent_user_group_name == null){
                                      results[i].ent_user_group_name = '--';
                                    }

                                      $('.connection_tab').append('<tr class = "double_click" id = ' + i + '><td>' + results[i].ent_user_name + '</td><td>' + results[i].ent_user_id + '</td><td>' + results[i].ent_user_group_name +
                                          '</td><td><button id="' + i + '" type="button" class="edit btn btn-default glyphicon glyphicon-edit edit_color" aria-label="Left Align"></button></td><td><button id="' + results[i].mail_comm_id + '" type="button" class=" delete btn btn-default delete_color" aria-label="Left Align"><span class="glyphicon glyphicon-remove delete_color" aria-hidden="true"></span></button></td></tr>');
                                  }
                              }
                              else {

                              $('#search-para-enterprise-users,#search').show();
                              $('#list-of-enterprise-users,#back').hide();

                                      $.msgBox({
                                          title: "Alert",
                                          content: "Result Not available",
                                          type: "alert",
                                      });

                              }

                          },
                          beforeSend: function() {

                              $('.connection_tab tr').remove();

                          },
                          error: function() {

                              $.msgBox({
                                  title: "Error",
                                  content: "Something went wrong",
                                  type: "error",
                              });

                          }
                      });

                  }
              }

              function update_user_data() {
                //  var mail_comm_id;

                var ent_user_name = $('#ent_user_name').val();
                var ent_email_id = $('#ent_email_id').val();
                var ent_user_group_id = $('#ent_user_group_id').val();

                  $.ajax({
                      type: "POST",
                      url: "update_enterprise_user",
                      data: {
                          "_token": "{{ csrf_token() }}",
                          "mail_comm_id":mail_comm_id,
                          "ent_user_name": ent_user_name,
                          "ent_email_id": ent_email_id,
                          "ent_user_group_id": ent_user_group_id,
                      },
                      success: function(data) {

                          $.msgBox({
                              title: "Message",
                              content: data,
                              type: "info",
                          });
                           $('#connection-table-form')[0].reset();
                           $('#ent_user_group_id').val('0');
                           $('#update,#cancel').hide();
                           $("#show_all_users").trigger("click");
                           $("#show_all").trigger("click");
                           $('#list-of-user').show();

                      },

                      beforeSend: function() {},
                      error: function() {
                          $.msgBox({
                              title: "Alert",
                              content: "Something went wrong while update",
                              type: "alert",
                          });
                      }
                  });
              }

              $(document).on('dblclick', '.double_click', function() {

                var id = $(this).attr('id');
                mail_comm_id = temp_results[id].mail_comm_id;
                console.log("mail_comm_id edit",mail_comm_id);
                $('#ent_email_id').val(temp_results[id].ent_user_id);
                $('#ent_user_name').val(temp_results[id].ent_user_name);
                $('#ent_user_group_id').val(temp_results[id].user_group_id);
                $('#created_by').val(temp_results[id].created_by);
                $('#created_date').val(temp_results[id].created_at);
                $('#updated_by').val(temp_results[id].updated_by);
                $('#updated_at').val(temp_results[id].updated_at);

                $('#connection_table,#update,#cancel').show();
                $('#list-of-enterprise-users,#back,#search,#search-para-enterprise-users').hide();

              });


              $(document).on('click', '#update', function() {
                  if ($('#user-table-form').valid()) {

                    $.msgBox({
                          title: "Confirmation",
                          content: "Do you want to update the mail connection",
                          type: "confirm",
                          buttons: [{
                              value: "Yes"
                          }, {
                              value: "No"
                          }],
                          success: function(result) {
                              if (result == "Yes") {
                                $('#list-of-enterprise-users').hide();
                                  update_user_data();
                              } else {

                              }
                          }
                      });
                  }
              });

              $(document).on('click', '#back', function() {
                  $('#search-para-enterprise-users,#search').show();
                  $('#list-of-enterprise-users,#connection_table,#back').hide();
                  $('#show_all').show();
              });

              $(document).on('click', '#search', function() {
                display_usergroup_users();
              });

              $(document).on('click', '#show_all', function() {
                $('#connection_table,#search-para-enterprise-users,#search').hide();
                $('#list-of-enterprise-users,#back').show();
                showall();
              });

              $(document).on('click', '.delete', function() {
                  var msg = "Do you want to delete the </br> Enterprise User";
                  var mail_comm_id = $(this).attr('id');

                  $.msgBox({
                      title: "Confirm",
                      content: msg,
                      type: "confirm",
                      buttons: [{
                          value: "Yes"
                      }, {
                          value: "No"
                      }],
                      success: function(result) {
                          if (result == "Yes") {

                              $.ajax({
                                  type: "POST",
                                  url: "delete_enterprise_user",
                                  data: {
                                      "_token": "{{ csrf_token() }}",
                                      "mail_comm_id": mail_comm_id,
                                  },
                                  success: function(data) {
                                    if(data!="0")
                                    {
                                        showall();
                                          $.msgBox({
                                              title: "Message",
                                              content: data,
                                              type: "info",
                                          });
                                     }
                                   else if(data=="0")
                                   {
                                     $.msgBox({
                                         title: "Error",
                                         content: "User is refered in Distribution Strategy",
                                         type: "error",
                                     });
                                   }

                                  },
                                  beforeSend: function() {

                                  },
                                  error: function() {

                                      $.msgBox({
                                          title: "Message",
                                          content: "Something went wrong ",
                                          type: "info",
                                      });

                                  }
                              });


                          } else {

                          }
                      }
                  });

              });

              $(document).on('click', '.edit', function() {

                var id = $(this).attr('id');
                mail_comm_id = temp_results[id].mail_comm_id;
                console.log("mail_comm_id edit",mail_comm_id);
                $('#ent_email_id').val(temp_results[id].ent_user_id);
                $('#ent_user_name').val(temp_results[id].ent_user_name);
                $('#ent_user_group_id').val(temp_results[id].user_group_id);

                $('#created_by').val(temp_results[id].created_by);
                $('#created_date').val(temp_results[id].created_at);
                $('#updated_by').val(temp_results[id].updated_by);
                $('#updated_at').val(temp_results[id].updated_at);

                $('#connection_table,#update,#cancel').show();
                $('#list-of-enterprise-users,#back,#search,#search-para-enterprise-users').hide();

              });

              $(document).on('click', '#cancel', function() {
                  $('#connection_table,#update,#cancel').hide();
                  $('#list-of-enterprise-users,#back').show();

              });
          });

          $(window).load(function() {

            $("#setups").trigger('click');
            $("#communication").trigger('click');
            $("#mail").trigger('click');
            $("#address").trigger('click');
            $("#enterprise_users").trigger('click');

            $.ajax({
                type: "GET",
                url: "usergroup_results",
                dataType: "json",

                success: function(data) {
                    var results = data;
                    console.log("results",results);
                    $('#ent_user_group_id').html('');
                    for (var i = 0; i < results.length; i++) {
                        if (i == '0') {
                            $('#ent_user_group_id').append("<option id =\"0\" value=\"0\">Select</option>");
                        }
                        var ent_user_group_name = results[i].ent_user_group_name
                        var user_group_id = results[i].user_group_id

                        console.log('ent_user_group_name',ent_user_group_name);
                        $('#ent_user_group_id').append("<option  id =" + user_group_id + " value=" + user_group_id + ">" + ent_user_group_name + "</option>");
                    }
                },
                beforeSend: function() {},
                error: function() {}
            });

            $("#setups").trigger('click');
            $("#communication").trigger('click');
            $("#mail").trigger('click');
            $("#address").trigger('click');
            $("#enterprise_users").trigger('click');

              var fetch_search_ent_user_name_display;
              $('input[name="search_ent_user_name"]').autoComplete({
                  minChars: 1,
                  source: function(term, response) {
                      try {
                          fetch_search_ent_user_name_display.abort();
                      } catch (e) {}
                      fetch_search_ent_user_name_display = $.getJSON('communication_controller/autocomplete_search_ent_user_name', {
                          fetch_search_ent_user_name: term
                      }, function(data) {
                          response(data);
                      });
                  }
              });

              var fetch_search_ent_email_id_display;
              $('input[name="search_ent_email_id"]').autoComplete({
                  minChars: 1,
                  source: function(term, response) {
                      try {
                          fetch_search_ent_email_id_display.abort();
                      } catch (e) {}
                      fetch_search_ent_email_id_display = $.getJSON('communication_controller/autocomplete_search_ent_email_id', {
                          fetch_search_ent_email_id: term
                      }, function(data) {
                          response(data);
                      });
                  }
              });
              var fetch_search_ent_user_group_id_display;
              $('input[name="search_ent_user_group_id"]').autoComplete({
                  minChars: 1,
                  source: function(term, response) {
                      try {
                          fetch_search_ent_user_group_id_display.abort();
                      } catch (e) {}
                      fetch_search_ent_user_group_id_display = $.getJSON('communication_controller/autocomplete_search_ent_user_group_id', {
                          fetch_search_ent_user_group_id: term
                      }, function(data) {
                          response(data);
                      });
                  }
              });

          });

      </script>
   </div>

</div>
@stop

@section('lower_band')
<div class="col-xs-12 lower_band">
   <div class="bottom_buttons">
      <div  id="search_button">
         <button class="headerbuttons" id="search" type="button">Search</button>
      </div>
      <div  id="back_buttons">
         <button class="headerbuttons" id="back" type="button">Back</button>
      </div>
      <div id="update_button" style="float:right">
         <button class="headerbuttons" id="update" type="button">Update</button>
         <button class="headerbuttons" id="cancel" type="button">Cancel</button>
      </div>
   </div>
</div>
@stop
