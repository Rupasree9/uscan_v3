@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" type="text/css" href="css/communication.css">
<script src="js/communication_validation.js"></script>
@stop

@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="col-xs-4 display-title">
      Create System User
   </div>
</div>
@stop

@section('content')
<div class="col-xs-12 content">
   <ul class="nav nav-tabs">
      <li class="active"><a class="tab_one"  data-toggle="tab" href="#tab_one">Basic Details</a></li>
   </ul>
   <div class="tab-content">
      <div id="tab_one" class="tab-pane fade in active custom_tab">
         <form id="system-user-table-form" autocomplete="off" class="connection-table">
            <table style="width:40%;">
               <tr>
                  <td>System User Name</td>
                  <td><input type="text"  id = "system_user_name" name="system_user_name"></td>
               </tr>
               <tr>
                  <td>Email Address</td>
                  <td><input type="email" name="email_id" id="email_id"></td>
               </tr>
               <tr>
                  <td>SMTP User Name</td>
                  <td><input type="text" name="smtp_user_name" id="smtp_user_name"></td>
               </tr>
               <tr>
                  <td>SMTP Password</td>
                  <td><input type="password" name="smtp_password" id="smtp_password"></td>
               </tr>
               <tr>
                 <td>Connection</td>
                 <td><select id="mail_connection" name="mail_connection">
                     </select></td>
               </tr>

            </table>
         </form>
      </div>
   </div>

   <script type="text/javascript">
      jQuery.validator.addMethod('selectcheck', function(value) {
          return (value != '0');
      }, "");

      $(".initial_disable").prop('disabled', true);

      function get_connection_data(){
        $.ajax({
            type: "GET",
            url: "get_connection",
            dataType: "json",
            success: function(data) {
                var results = data;
                console.log("results",results);
                for (var i = 0; i < results.length; i++) {
                    if (i == '0') {
                        $('#mail_connection').append("<option id =\"0\" value=\"0\">Select</option>");
                    }
                    var connection_name = results[i].connection_name
                    var mail_conn_id = results[i].mail_conn_id
                    console.log("connection_name",connection_name);
                    $('#mail_connection').append("<option id =" + mail_conn_id + " value=" + mail_conn_id + ">" + connection_name + "</option>");
                }
            },
            beforeSend: function() {},
            error: function() {}
        });
      }

      get_connection_data();

      $(document).on('click', '#save_button', function() {

          if ($('#system-user-table-form').valid()) {

              var system_user_name = $('#system_user_name').val();
              var email_id = $('#email_id').val();
              var smtp_user_name = $('#smtp_user_name').val();
              var smtp_password = $('#smtp_password').val();
              var mail_connection = $('#mail_connection').val();
              console.log("mail_connection",mail_connection);
              // console.log("connection_name",connection_name,protocol,server,port,proxy_mode);

              $.ajax({
                  type: "POST",
                  url: "save_system_user",
                  data: {
                      "_token": "{{ csrf_token() }}",
                      "system_user_name": system_user_name,
                      "email_id": email_id,
                      "smtp_user_name": smtp_user_name,
                      "smtp_password": smtp_password,
                      "mail_connection": mail_connection,

                  },
                  success: function(data) {

                      $.msgBox({
                          title: "Message",
                          content: data,
                          type: "info",
                      });

                      $('#system-user-table-form')[0].reset();
                      $('#mail_connection').empty();
                      get_connection_data();
                  },

                  beforeSend: function() {

                  },
                  error: function() {
                  }
              });
          } else {}
      });

      $(document).on('click', '#cancel', function(){

                $('#system-user-table-form')[0].reset();
                $('#mail_connection').html('<option value="0">Select</option>');

             });

      $(window).load(function() {
          $("#setups").trigger('click');
          $("#communication").trigger('click');
          $("#mail").trigger('click');
          $("#address").trigger('click');
          $("#system_users").trigger('click');

      });
   </script>
</div>
@stop
@section('lower_band')
<div class="col-xs-12 lower_band">
   <button class="headerbuttons" type="button" style="float:right;" id="cancel">Cancel</button>
   <button id="save_button" type="button" class="headerbuttons" style="float:right;" type="submit">Save</button>
</div>
@stop
