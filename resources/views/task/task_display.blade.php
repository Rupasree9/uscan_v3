@extends('layouts.uscan_master_page')
@section('header')
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/base/jquery-ui.css" rel="stylesheet">
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/task_change.css">
<script src="js/task_validation.js"></script>
@stop
@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="col-xs-4 display-title">
      <span>Display Task</span>
   </div>
   <div class="col-xs-2" id = "show_all_task" style="float: right;text-align:center;line-height:30px;">
      <span id="show_all_t">Show All Tasks</span>
   </div>
</div>
@stop
@section('content')
<div class="col-xs-12 content">

      <div id="list-of-task">
        <table style="width:100%;" class="task_tab">
        </table>
      </div>
      <div id="search-para">
         <form  autocomplete="off">
            <table style="width:40%;">
              <tr>
               <td class="details_color">Search Parameters</td>
              </tr>
               <tr>
                  <td>Task Type</td>
                  <td><input type="text" name="search_task_type" id="search_task_type_id"></td>
               </tr>
               <tr>
                  <td>Task Name</td>
                  <td><input type="text" name="search_task_name" id="search_task_name_id"></td>
               </tr>
               <tr>
                  <td>Customer Name</td>
                  <td><input type="text" name="search_customer_name" id="search_customer_name_id"></td>
               </tr>
               <tr>
                  <td>Plant Name</td>
                  <td><input type="text" name="search_plant_name" id="search_plant_name_id"></td>
               </tr>
            </table>
         </form>
      </div>
      <div id="task-table">
         <ul class="nav nav-tabs">
            <li class="active"><a id="tab" data-toggle="tab" href="#tab_one">Details</a></li>
            <li><a data-toggle="tab" href="#tab_two">Additional Details</a></li>
         </ul>
         <div class="tab-content">
            <div id="tab_one" class="tab-pane fade in active">
               <form id="task-table-form" autocomplete="off">
                 <table style="width:40%;">

                     <tr>
                       <td>Task Type</td>
                       <td> <input type="text" name="task_type_name" id="task_type" disabled>  </td>
                     </tr>
                     <tr>
                       <td>Task Name</td>
                       <td><input type="text" name="task_name" id="task_name" disabled></td>
                     </tr>
                       <tr>
                         <td>Customer Name</td>
                         <td> <input type="text" name="customer_id" id="customer_id" disabled>  </td>
                       </tr>
                       <tr>
                         <td>Plant Name</td>
                         <td><input type="text" name="plant_id" id="plant_id" disabled></td>
                       </tr>
                       <tr>
                         <td>Portal Name</td>
                         <td><input type="text" name="portal_id" id="portal_id" disabled></td>
                       </tr>
                       <tr>
                         <td>Program Name</td>
                         <td><input type="text" name="program_id" id="program_id" disabled></td>
                       </tr>
                       <tr>
                         <td>Distribution strategy</td>
                         <td><input type="checkbox" name="distribution_strategy" id="distribution_strategy"><button type="button" id="show_ds" style ="display:none;">Show_ds</button></td>
                       <tr>
                         <td>Active</td>
                         <td><input type="checkbox" name="avtivename" id="activeid" disabled></td>
                       </tr>
                  </table>
               </form>
            </div>
            <div id="tab_two" class="tab-pane fade">
               <form>
                  <table table style="width:40%; margin-bottom: 20px">
                     <tr>
                        <!-- <td class="details_color"><strong>Details</strong></td> -->
                        <td></td>
                     </tr>
                     <tr>
                        <td>Created By</td>
                        <td><input id ="created_by" type="text" name="" disabled></td>
                     </tr>
                     <tr>
                        <td>Created Date</td>
                        <td><input id ="created_date" type="text" disabled></td>
                     </tr>
                     <tr>
                        <td>Modified By</td>
                        <td><input id = "modified_by" type="text" disabled></td>
                     </tr>
                     <tr>
                        <td>Modified Date</td>
                        <td><input id = "modified_date" type="text" disabled></td>
                     </tr>
                  </table>
               </form>
            </div>
         </div>

         <form>
         <table class="ds_tab">
         </table>
         </form>


      </div>
      <script type="text/javascript">

      var task_results, no_results, search_click, show_all_click, show_no_results;
      var ds_results,ds,ds_data;
         search_click = "NO";
         show_all_click = "YES";
         show_no_results = "TRUE";


          jQuery.validator.addMethod('selectcheck', function(value) {
            return (value != '0');
           }, "");

          $(document).ready(function() {

          function show_all_task() {

            $.ajax({
                type: "GET",
                url: "task_results",
                dataType: "json",
                success: function(data) {


                    var results = data;
                    task_results = data;

                    $('.task_tab tr').remove();
                    $('.task_tab').append('<tr style="background-color: #D3D3D3;"><th>Task Type</th><th>Task Name</th><th>Customer Name</th><th>Plant Name</th><th>Active</th><th>Created By</th><th>Modified By</th></tr>');

                    if (results.length > 12) {

                        $('#list-of-task').addClass("scroll");

                    } else {
                        $('#list-of-task').removeClass("scroll");
                    }

                    for (var i = 0; i < results.length; i++) {
                        if (results[i].active == 0) {
                            active_check = '<td><input type="checkbox" disabled readonly></td>';
                        } else {
                            active_check = '<td><input type="checkbox" checked disabled readonly></td>';
                        }
                        $('.task_tab').append('<tr class = "double_click rows" data-id="' + i + '" id="' + results[i].task_id + '" ><td>' + results[i].task_type_name + '</td><td>' + results[i].task_name + '</td><td>' + results[i].customer_name + '</td><td>' + results[i].plant_name + active_check + '</td><td>' + results[i].created_by + '</td><td>' + results[i].updated_by + '</td></tr>');
                    }



                },
                beforeSend: function() {

                    $("#list-of-task,#back_buttons,#back").addClass("show");
                    $("#search-para").addClass("hide");
                    $("#show_all_task,#search_button").addClass("hide");



                },
                error: function() {

                    $.msgBox({
                        title: "Alert",
                        content: "Something went wrong",
                        type: "alert",
                    });



                }
            });
            }

          $(document).on('click', '#show_all_t', function() {
              search_click = "NO";
              show_all_click = "YES";
              show_all_task();
          });

          $(document).on('click', '#back', function() {

            $('.task_tab tr').remove();
            $("#list-of-task,#back_buttons,#back").removeClass("show");
            $("#search-para").removeClass("hide");
            $("#show_all_task,#search_button").removeClass("hide");

          });

          function display_task_data() {

            var search_task_type_id = $('#search_task_type_id').val();
            var search_task_name_id = $('#search_task_name_id').val();
            var search_customer_name_id = $('#search_customer_name_id').val();
            var search_plant_name_id = $('#search_plant_name_id').val();


            if ((search_task_type_id == "") && (search_task_name_id == "") && (search_plant_name_id == "") && (search_customer_name_id == "")) {

                $.msgBox({
                    title: "Alert",
                    content: "Search Criteria Not Available",
                    type: "alert",
                });

            } else {

                $.ajax({
                    type: "POST",
                    url: "task_search",
                    dataType: "json",
                    data: {

                        "_token": "{{ csrf_token() }}",
                        "search_task_type_id": search_task_type_id,
                        "search_task_name_id": search_task_name_id,
                        "search_plant_name_id": search_plant_name_id,
                        "search_customer_name_id": search_customer_name_id,
                    },

                    success: function(data) {
                      if (data != "0") {
                      var results = data;
                      task_results = data;

                      $('.task_tab tr').remove();
                      $('.task_tab').append('<tr style="background-color: #D3D3D3;"><th>Task Type</th><th>Task Name</th><th>Customer Name</th><th>Plant Name</th><th>Active</th><th>Created By</th><th>Modified By</th></tr>');

                      if (results.length > 12) {

                          $('#list-of-task').addClass("scroll");

                      } else {
                          $('#list-of-task').removeClass("scroll");
                      }

                      for (var i = 0; i < results.length; i++) {
                          if (results[i].active == 0) {
                              active_check = '<td><input type="checkbox" disabled readonly></td>';
                          } else {
                              active_check = '<td><input type="checkbox" checked disabled readonly></td>';
                          }
                          $('.task_tab').append('<tr class = "double_click rows" data-id="' + i + '" id="' + results[i].task_id + '" ><td>' + results[i].task_type_name + '</td><td>' + results[i].task_name + '</td><td>' + results[i].customer_name + '</td><td>' + results[i].plant_name + active_check + '</td><td>' + results[i].created_by + '</td><td>' + results[i].updated_by + '</td></tr>');
                      }
                    }
                    else {
                      if (show_no_results == "TRUE") {

                           $.msgBox({
                               title: "Alert",
                               content: "Result Not available",
                               type: "alert",
                           });

                           $("#list-of-task,#back_buttons,#back").removeClass("show");
                           $("#search-para").removeClass("hide");
                           $("#show_all_task,#search_button").removeClass("hide");


                       } else {
                             $('.task_tab tr').remove();
                             $("#list-of-task,#back_buttons,#back").removeClass("show");
                             $("#search-para").removeClass("hide");
                             $("#show_all_task,#search_button").removeClass("hide");

                       }
                    }


                    },
                    beforeSend: function() {

                        $("#list-of-task,#back_buttons,#back").addClass("show");
                        $("#search-para").addClass("hide");
                        $("#show_all_task,#search_button").addClass("hide");

                    },
                    error: function() {

                        $.msgBox({
                            title: "Error",
                            content: "Something went wrong",
                            type: "error",
                        });
                    }
                });
            };

          }

          $(document).on('click', '#filter', function() {

            search_click = "YES";
            show_all_click = "NO";
            show_no_results = "TRUE";
            display_task_data();


          });

          $(document).on('click', '#cancel', function() {

                  $("#task-table,#update_button").removeClass("show");
                  $("#back,#list-of-task").addClass("show");

                  $('#plant_id-error,#customer_id-error,#task_type-error,#portal_id-error,#program_id-error').remove();
                  $('#plant_id,#customer_id,#task_type,#portal_id,#program_id').removeClass("error");


              });

          $(document).on('dblclick', '.double_click', function() {

                                  var id = $(this).data('id');
                                  $("#tab").click();
                                  var ds = 0;
                                  task_id = task_results[id].task_id;
                                $.ajax({
                                    type: "POST",
                                    url: "task_ds_ref",
                                    data: {
                                        "_token": "{{ csrf_token() }}",
                                        "task_id": task_id,
                                    },

                                    success: function(data) {

                                              if (data != 0)
                                              {
                                                   $('#distribution_strategy').prop('checked', true);
                                                    $('#distribution_strategy').prop('disabled', true);
                                                    $("#show_ds").show();
                                               }
                                               else{
                                                 $("#show_ds").hide();
                                               }
                                    },
                                    beforeSend: function() {

                                    },
                                    error: function() {

                                    }
                                });


                                  $("#task-table,#update_button").addClass("show");
                                  $("#back,#list-of-task").removeClass("show");
                                  $('#plant_id').val(task_results[id].plant_name);
                                  $('#portal_id').val(task_results[id].portal_name);
                                  $('#task_type').val(task_results[id].task_type_name);
                                  $('#task_name').val(task_results[id].task_name);
                                  $('#customer_id').val(task_results[id].customer_name);
                                  $('#program_id').val(task_results[id].program_name);
                                  $('#created_by').val(task_results[id].created_by);
                                  $('#created_date').val(task_results[id].created_at);
                                  $('#modified_by').val(task_results[id].updated_by);
                                  $('#modified_date').val(task_results[id].updated_at);

                                  if (task_results[id].active == 0) {
                                      $('#activeid').prop('checked', false);
                                  } else {
                                      $('#activeid').prop('checked', true);
                                  }
                          });


                          $(".ds_tab").dialog({
                           autoOpen: false,
                           modal: true,
                           width:450,
                           height:150
                           });

                           $('#show_ds').on('click',function(){
                          $('.ds_tab').dialog("open");
                          ds_data();
                             //}
                           });

                           $(' #Cancel-ds').click(function(){
                             $('#set-ds').dialog("close");
                           });



                           function ds_data(){
                             var task_id_ref_ds = task_id;

                                 $.ajax({
                                     type: "POST",
                                     url: "ds_results",
                                     dataType: "json",
                                     data: {

                                         "_token": "{{ csrf_token() }}",
                                         "task_id_ref_ds": task_id_ref_ds,
                                     },

                                     success: function(data) {
                                       if (data != "0") {
                                       var results = data;
                                       ds_results = results;

                                       $('.ds_tab tr').remove();
                                       $('.ds_tab').append('<tr style="background-color: #D3D3D3;"><th class="ds_th">Task Status</th><th class="ds_th">communication_options</th><th class="ds_th">Mail Type</th><th class="ds_th">Mail Address</th></tr>');


                                                for(var i=0; i< results.length; i++)
                                              {
                                                   $('.ds_tab').append('<tr class = "double_click rows" data-id="' + i + '" id="' + results[i].distribution_strategy_id  + '" ><td>' + results[i].status_name + '</td><td>' + results[i].communication_option_name + '</td><td>' +results[i].mail_type + '</td><td>' +results[i].mail_id_address +
                                                     '</td></tr>');
                                               }

                                       }
                                       else{
                                         ds_data="NO";
                                         $('.ds_tab').dialog("close");
                                       }
                                        },
                                           beforeSend: function() {

                                               },
                                           error: function() {

                                                }
                                      });

                                 }





        });

        $(window).load(function() {

            $("#setups").trigger('click');
            $("#task").trigger('click');

            var fetch_customer_name;
            $('input[name="search_customer_name"]').autoComplete({
                minChars: 1,
                source: function(term, response) {
                    try {
                        fetch_customer_name.abort();
                    } catch (e) {}
                    fetch_customer_name = $.getJSON('customer_child_controller/autocomplete_customer_name', {
                        search_customer_name_id: term
                    }, function(data) {
                        response(data);
                    });
                }
            });

            var fetch_task_type;
            $('input[name="search_task_type"]').autoComplete({
                minChars: 1,
                source: function(term, response) {
                    try {
                        fetch_task_type.abort();
                    } catch (e) {}
                    fetch_task_type = $.getJSON('task_controller/autocomplete_task_type', {
                        search_task_type : term
                    }, function(data) {
                        response(data);
                    });
                }
            });

            var fetch_task_name;
            $('input[name="search_task_name"]').autoComplete({
                minChars: 1,
                source: function(term, response) {
                    try {
                        fetch_task_name.abort();
                    } catch (e) {}
                    fetch_task_name = $.getJSON('task_controller/autocomplete_task_name', {
                        search_task_name : term
                    }, function(data) {
                        response(data);
                    });
                }
            });


            var fetch_plant_name;
            $('input[name="search_plant_name"]').autoComplete({
                minChars: 1,
                source: function(term, response) {
                    try {
                        fetch_plant_name.abort();
                    } catch (e) {}
                    fetch_plant_name = $.getJSON('fetch_plant_name', {
                        plant_name: term
                    }, function(data) {
                        response(data);
                    });
                }
            });

        });

      </script>
</div>
@stop
@section('lower_band')
<div class="col-xs-12 lower_band">

      <div  id="search_button">
         <button class="headerbuttons" id="filter" type="button">Search</button>
      </div>
      <div  id="back_buttons">
         <button class="headerbuttons" id="back" type="button">Back</button>
      </div>
      <div id="update_button">
         <button class="headerbuttons" id="cancel" type="button">Back</button>
      </div>

</div>
@stop
