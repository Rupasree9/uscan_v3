@extends('layouts.uscan_master_page')
@section('header')
<script src="js/task_validation.js"></script>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/base/jquery-ui.css" rel="stylesheet">
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/task_change.css">
@stop
@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="col-xs-4 display-title">
      <span>Change Task</span>
   </div>
   <div class="col-xs-2" id = "show_all_task" style="float: right;text-align:center;line-height:30px;">
      <span id="show_all_t">Show All Tasks</span>
   </div>
</div>
@stop
@section('content')
<div class="col-xs-12 content">
   <div id="list-of-task">
      <table style="width:100%;" class="task_tab">
      </table>
      <!-- <table style="width:100%;" class="ds_tab">
         </table> -->
   </div>
   <div id="search-para">
      <form  autocomplete="off">
         <table style="width:40%;">
            <tr>
               <td class="details_color">Search Parameters</td>
            </tr>
            <tr>
               <td>Task Type</td>
               <td><input type="text" name="search_task_type" id="search_task_type_id"></td>
            </tr>
            <tr>
               <td>Task Name</td>
               <td><input type="text" name="search_task_name" id="search_task_name_id"></td>
            </tr>
            <tr>
               <td>Customer Name</td>
               <td><input type="text" name="search_customer_name" id="search_customer_name_id"></td>
            </tr>
            <tr>
               <td>Plant Name</td>
               <td><input type="text" name="search_plant_name" id="search_plant_name_id"></td>
            </tr>
         </table>
      </form>
   </div>
   <div id="task-table">
      <ul class="nav nav-tabs">
         <li class="active"><a id = "tab" data-toggle="tab" href="#tab_one">Details</a></li>
         <li><a data-toggle="tab" href="#tab_two">Additional Details</a></li>
      </ul>
      <div class="tab-content">
         <div id="tab_one" class="tab-pane fade in active">
            <form id="task-table-form" autocomplete="off">
               <table style="width:40%;">
                  <tr>
                     <td>Task Type</td>
                     <td><select id = "task_type" name="task_type_name">
                        </select>
                     </td>
                  </tr>
                  <tr>
                     <td>Task Name</td>
                     <td><input type="text" name="task_name" id="task_name"></td>
                  </tr>
                  <tr>
                     <td>Customer Name</td>
                     <td><select id="customer_id" name ="customer_id">
                        </select>
                     </td>
                  </tr>
                  <tr>
                     <td>Plant Name</td>
                     <td>
                        <select id="plant_id" name ="plant_id">
                           <option value="0">Select</option>
                        </select>
                     </td>
                  </tr>
                  <tr>
                     <td>Portal Name</td>
                     <td>
                        <select id="portal_id" name ="portal_id">
                           <option value="0">Select</option>
                        </select>
                     </td>
                  </tr>
                  <tr>
                     <td>Program Name</td>
                     <td><select id="program_id" name ="program_id">
                        </select>
                     </td>
                  </tr>
                  <tr>
                     <td>Distribution strategy</td>
                     <td><input type="checkbox" name="distribution_strategy" id="distribution_strategy"><button type="button" id="change_ds" style ="display:none;">Change_ds</button></td>
                  </tr>
                  <tr>
                     <td>Active</td>
                     <td><input type="checkbox" name="avtivename" id="activeid"></td>
                  </tr>
               </table>
            </form>
         </div>
         <div id="tab_two" class="tab-pane fade">
            <form>
               <table table style="width:40%; margin-bottom: 20px">
                  <tr>
                     <!-- <td class="details_color"><strong>Details</strong></td> -->
                     <td></td>
                  </tr>
                  <tr>
                     <td>Created By</td>
                     <td><input id ="created_by" type="text" name="" disabled></td>
                  </tr>
                  <tr>
                     <td>Created Date</td>
                     <td><input id ="created_date" type="text" disabled></td>
                  </tr>
                  <tr>
                     <td>Modified By</td>
                     <td><input id = "modified_by" type="text" disabled></td>
                  </tr>
                  <tr>
                     <td>Modified Date</td>
                     <td><input id = "modified_date" type="text" disabled></td>
                  </tr>
               </table>
            </form>
         </div>
      </div>

      <form class="ds_form">
        <div id="ds_content">
         <table class="ds_tab"></table>
       </div>
      </form>

      <form id="set-ds" style="display:none;">
      	<table style="width:100%; margin-bottom: 20px">
      	<tr style="background-color: #eee;">
      		<th class="set_ds_th">Task Status</th>
      		<th class="set_ds_th">Communication Options</th>
          <th class="set_ds_th">Address Type</th>
      		<th class="set_ds_th">Mail Address</th>
      	</tr>

      		<tr>
            <td><select id="task_status">
              </select></td>

      			<td><select id="communication_options">
      				</select></td>

              <td><select id="mail_type">
              <option value="">Select</option>
              <option value="personnel" id="personnel">Personnel</option>
              <option value="group" id="group">Group</option>
              </select></td>

      			<td><select id="mail_address">
      				</select></td>
      		</tr>

      		<tr id="success-msg" style="display: none;">
      			<td></td>
      			<td><p>Information has been saved successfuly!</p></td>
      		</tr>
      	</table>
        <div class="resetgap actiondiv1">
        <div style=" margin-bottom: 20px; ">
        <button class="headerbuttons" type="button" id="save-ds">Save</button>
        <button class="headerbuttons" type="button" id="Reset-ds">Reset</button>
        <button class="headerbuttons" type="button" id="Cancel-ds">Cancel</button>
        </div>
          	</div>
      </form>

   </div>
   <script type="text/javascript">
     var task_id_ref, ds_results, ds, ds_data1, data_status, data_communication_options;
     var task_results, no_results, search_click, show_all_click, show_no_results, mail_address_data, ent_user_id, mail_comm_id;
     var group_mail_address_data, ent_user_group_id, user_group_id,current_ds_count,task_ds_ref_data,ref_task_id;
     search_click = "NO";
     show_all_click = "YES";
     show_no_results = "TRUE";


     jQuery.validator.addMethod('selectcheck', function(value) {
         return (value != '0');
     }, "");

     $(document).ready(function() {

         $('#task_type').on('change', function() {

             if (this.value == 0) {

                 $("#customer_id,#plant_id,#portal_id,#program_id,#distribution_strategy,#activeid").addClass("initial_disable_color");
                 $("#customer_id,#plant_id,#portal_id,#program_id").prop('disabled', true);
                 $('#plant_id').html('<option value="0">Select</option>');
                 $('#portal_id').html('<option value="0">Select</option>');
                 $('#program_id').html('<option value="0">Select</option>');
                 $('#plant_id-error,#customer_id-error,#task_type-error,#portal_id-error,#program_id-error,#task_name-error').remove();
                 $('#plant_id,#customer_id,#task_type,#portal_id,#program_id,#task_name').removeClass("error");
                 $("#customer_id,#program_id,#plant_id,#portal_id").val('0');

             } else {

                 $("#program_id,#plant_id,#portal_id,#distribution_strategy,#activeid,#portal_id,#plant_id").addClass("initial_disable_color");
                 $("#program_id,#plant_id,#portal_id,#distribution_strategy,#activeid,#portal_id,#plant_id").prop('disabled', true);
                 $("#customer_id").removeClass("initial_disable_color");
                 $("#customer_id").prop('disabled', false);
                 $("#plant_id,#portal_id,#customer_id").val('0');
                 $('#program_id').html('<option value="0">Select</option>');
                 $('#plant_id-error,#customer_id-error,#task_type-error,#portal_id-error,#program_id-error,#task_name-error').remove();
                 $('#plant_id,#customer_id,#task_type,#portal_id,#program_id,#task_name').removeClass("error");

             }

         });

         function show_all_task() {

             $.ajax({
                 type: "GET",
                 url: "task_results",
                 dataType: "json",
                 success: function(data) {


                     var results = data;
                     task_results = data;

                     $('.task_tab tr').remove();
                     $('.task_tab').append('<tr style="background-color: #D3D3D3;"><th>Task Type</th><th>Task Name</th><th>Customer Name</th><th>Plant Name</th><th>Active</th><th>Created By</th><th>Modified By</th><th>Edit</th><th>Delete</th></tr>');

                     if (results.length > 12) {

                         $('#list-of-task').addClass("scroll");

                     } else {
                         $('#list-of-task').removeClass("scroll");
                     }

                     for (var i = 0; i < results.length; i++) {
                         if (results[i].active == 0) {
                             active_check = '<td><input type="checkbox" disabled readonly></td>';
                         } else {
                             active_check = '<td><input type="checkbox" checked disabled readonly></td>';
                         }
                         $('.task_tab').append('<tr class = "double_click rows" data-id="' + i + '" id="' + results[i].task_id + '" ><td>' + results[i].task_type_name + '</td><td>' + results[i].task_name + '</td><td>' + results[i].customer_name + '</td><td>' + results[i].plant_name + active_check + '</td><td>' + results[i].created_by + '</td><td>' + results[i].updated_by +
                             '</td><td><button id="' + i + '" type="button" class="edit_task btn btn-default glyphicon glyphicon-edit edit_color" aria-label="Left Align"></button></td><td><button id="' + results[i].task_id + '" type="button" class="delete_task btn btn-default" aria-label="Left Align"><span class="glyphicon glyphicon-remove delete_color" aria-hidden="true"></span></button></td></tr>');
                     }

                 },
                 beforeSend: function() {

                     $("#list-of-task,#back_buttons,#back").addClass("show");
                     $("#search-para").addClass("hide");
                     $("#show_all_task,#search_button").addClass("hide");



                 },
                 error: function() {

                     $.msgBox({
                         title: "Alert",
                         content: "Something went wrong",
                         type: "alert",
                     });

                 }
             });
         }

         $(document).on('click', '#show_all_t', function() {
             search_click = "NO";
             show_all_click = "YES";
             show_all_task();
         });

         $(document).on('click', '#back', function() {

             $('.task_tab tr').remove();
             $("#list-of-task,#back_buttons,#back").removeClass("show");
             $("#search-para").removeClass("hide");
             $("#show_all_task,#search_button").removeClass("hide");

         });

         function display_task_data() {

             var search_task_type_id = $('#search_task_type_id').val();
             var search_task_name_id = $('#search_task_name_id').val();
             var search_customer_name_id = $('#search_customer_name_id').val();
             var search_plant_name_id = $('#search_plant_name_id').val();


             if ((search_task_type_id == "") && (search_task_name_id == "") && (search_plant_name_id == "") && (search_customer_name_id == "")) {

                 $.msgBox({
                     title: "Alert",
                     content: "Search Criteria Not Available",
                     type: "alert",
                 });

             } else {

                 $.ajax({
                     type: "POST",
                     url: "task_search",
                     dataType: "json",
                     data: {

                         "_token": "{{ csrf_token() }}",
                         "search_task_type_id": search_task_type_id,
                         "search_task_name_id": search_task_name_id,
                         "search_plant_name_id": search_plant_name_id,
                         "search_customer_name_id": search_customer_name_id,
                     },

                     success: function(data) {
                         if (data != "0") {
                             var results = data;
                             task_results = data;
                             $("#list-of-task,#back_buttons,#back").addClass("show");
                             $("#search-para").addClass("hide");
                             $("#show_all_task,#search_button").addClass("hide");

                             $('.task_tab tr').remove();
                             $('.task_tab').append('<tr style="background-color: #D3D3D3;"><th>Task Type</th><th>Task Name</th><th>Customer Name</th><th>Plant Name</th><th>Active</th><th>Created By</th><th>Modified By</th><th>Edit</th><th>Delete</th></tr>');

                             if (results.length > 12) {

                                 $('#list-of-task').addClass("scroll");

                             } else {
                                 $('#list-of-task').removeClass("scroll");
                             }

                             for (var i = 0; i < results.length; i++) {
                                 if (results[i].active == 0) {
                                     active_check = '<td><input type="checkbox" disabled readonly></td>';
                                 } else {
                                     active_check = '<td><input type="checkbox" checked disabled readonly></td>';
                                 }
                                 $('.task_tab').append('<tr class = "double_click rows" data-id="' + i + '" id="' + results[i].task_id + '" ><td>' + results[i].task_type_name + '</td><td>' + results[i].task_name + '</td><td>' + results[i].customer_name + '</td><td>' + results[i].plant_name + active_check + '</td><td>' + results[i].created_by + '</td><td>' + results[i].updated_by +
                                     '</td><td><button id="' + i + '" type="button" class="edit_task btn btn-default glyphicon glyphicon-edit edit_color" aria-label="Left Align"></button></td><td><button id="' + results[i].task_id + '" type="button" class="delete_task btn btn-default" aria-label="Left Align"><span class="glyphicon glyphicon-remove delete_color" aria-hidden="true"></span></button></td></tr>');
                             }
                         } else {
                             if (show_no_results == "TRUE") {

                                 $.msgBox({
                                     title: "Alert",
                                     content: "Result Not available",
                                     type: "alert",
                                 });

                                 $("#list-of-task,#back_buttons,#back").removeClass("show");
                                 $("#search-para").removeClass("hide");
                                 $("#show_all_task,#search_button").removeClass("hide");


                             } else {
                                 $('.task_tab tr').remove();
                                 $("#list-of-task,#back_buttons,#back").removeClass("show");
                                 $("#search-para").removeClass("hide");
                                 $("#show_all_task,#search_button").removeClass("hide");

                             }
                         }


                     },
                     beforeSend: function() {



                     },
                     error: function() {

                         $.msgBox({
                             title: "Error",
                             content: "Something went wrong",
                             type: "error",
                         });
                     }
                 });
             };

         }

         $(document).on('click', '#filter', function() {

             search_click = "YES";
             show_all_click = "NO";
             show_no_results = "TRUE";
             display_task_data();


         });

         $(document).on('click', '.delete_task', function() {

             var msg = "Do you want to delete the task";
              ref_task_id = $(this).attr('id');
             $.msgBox({
                 title: "Confirm",
                 content: msg,
                 type: "confirm",
                 buttons: [{
                     value: "Yes"
                 }, {
                     value: "No"
                 }],
                 success: function(result) {
                     if (result == "Yes") {

                         $.ajax({
                             type: "POST",
                             url: "task_delete",
                             data: {
                                 "_token": "{{ csrf_token() }}",
                                 "task_id": ref_task_id,
                             },
                             success: function(data) {

                                 $.msgBox({
                                     title: "Message",
                                     content: "Task Deleted Successfully",
                                     type: "info",
                                 });

                                 if (search_click == "YES") {
                                     show_no_results = "FALSE";
                                     display_task_data();
                                 }

                                 if (show_all_click == "YES") {
                                     show_no_results = "FALSE";
                                     show_all_task();
                                 }


                                 $.ajax({
                                     type: "POST",
                                     url: "delete_ds_ref_task",
                                     data: {
                                         "_token": "{{ csrf_token() }}",
                                         "task_id": ref_task_id,
                                     },
                                     success: function(data) {
                                     },
                                     beforeSend: function() {

                                     },
                                     error: function() {

                                     }
                                 });


                             },
                             beforeSend: function() {

                             },
                             error: function() {

                                 $.msgBox({
                                     title: "Alert",
                                     content: "Something went wrong",
                                     type: "alert",
                                 });

                             }
                         });


                     } else {

                     }
                 }
             });

         });

         $(document).on('click', '.edit_task', function() {
             var id = $(this).attr('id');
             $("#tab").click();

             customer_id = task_results[id].customer_id;
             plant_id = task_results[id].plant_id;
             get_task_type = task_results[id].task_type_id;

             $('#plant_id').empty();

             if (customer_id != 0) {
                 $.ajax({
                     type: "POST",
                     url: "task_get_plant_for_customer",
                     // dataType: "json",
                     data: {
                         "_token": "{{ csrf_token() }}",
                         "customer_id": customer_id,
                     },

                     success: function(data) {

                         if (data != 0) {
                             for (var i = 0; i < data.length; i++) {
                                 if (i == '0') {
                                     $('#plant_id').append("<option id =\"null\" value=\"0\">Select</option>");
                                 }
                                 var plant_name = data[i].plant_name
                                 var plant_id = data[i].plant_id
                                 $('#plant_id').append("<option id =" + plant_id + " value=" + plant_id + ">" + plant_name + "</option>");

                             }

                             $('#plant_id').val(task_results[id].plant_id);
                         }

                     },
                     beforeSend: function() {

                     },
                     error: function() {

                     }
                 });

             }

             $('#program_id').empty();
             $.ajax({
                 type: "POST",
                 url: "task_get_program_for_customer_and_type",
                 // dataType: "json",
                 data: {
                     "_token": "{{ csrf_token() }}",
                     "customer_id": customer_id,
                     "task_type": get_task_type,
                 },

                 success: function(data) {

                     if (data != 0) {
                         for (var i = 0; i < data.length; i++) {
                             if (i == '0') {
                                 $('#program_id').append("<option id =\"null\" value=\"0\">Select</option>");
                             }
                             var program_name = data[i].program_name;
                             var program_id = data[i].program_id;
                             $('#program_id').append("<option id =" + program_id + " value=" + program_id + ">" + program_name + "</option>");

                         }
                         $('#program_id').val(task_results[id].program_id);

                     } else {
                         $('#program_id').append("<option id =\"null\" value=\"0\">No program available</option>");
                     }
                 },
                 beforeSend: function() {

                 },
                 error: function() {

                 }
             });


             $('#portal_id').empty();

             if (plant_id != 0) {

                 $.ajax({
                     type: "POST",
                     url: "task_get_portal_for_plant",
                     data: {
                         "_token": "{{ csrf_token() }}",
                         "plant_id": plant_id,
                         "customer_id": customer_id
                     },

                     success: function(data) {

                         if (data != 0) {


                             for (var i = 0; i < data.length; i++) {
                                 if (i == '0') {
                                     $('#portal_id').append("<option id =\"null\" value=\"0\">Select</option>");
                                 }
                                 var portal_name = data[i].portal_name;
                                 var portal_id = data[i].portal_id;
                                 $('#portal_id').append("<option id =" + portal_id + " value=" + portal_id + ">" + portal_name + "</option>");

                             }

                             $('#portal_id').val(task_results[id].portal_id);


                         }

                         $("#task-table,#update_button").addClass("show");
                         $("#back,#list-of-task").removeClass("show");


                     },
                     beforeSend: function() {

                     },
                     error: function() {

                     }
                 });

             }


             var ds = 0;
             task_id = task_results[id].task_id;
             $.ajax({
                 type: "POST",
                 url: "task_ds_ref",
                 data: {
                     "_token": "{{ csrf_token() }}",
                     "task_id": task_id,
                 },

                 success: function(data) {

                   if (data != 0) {

                       task_ds_ref_data = "YES";
                       $('#distribution_strategy').prop('checked', true);
                       $("#change_ds").show();


                   } else {
                     task_ds_ref_data = "NO";
                     $('#distribution_strategy').prop('checked', false);
                       $("#change_ds").hide();
                   }
                 },
                 beforeSend: function() {

                 },
                 error: function() {

                 }
             });


             task_id_ref = task_results[id].task_id;


             $('#task_type').val(task_results[id].task_type_id);
             $('#task_name').val(task_results[id].task_name);
             $('#customer_id').val(task_results[id].customer_id);

             $('#created_by').val(task_results[id].created_by);
             $('#created_date').val(task_results[id].created_at);
             $('#modified_by').val(task_results[id].updated_by);
             $('#modified_date').val(task_results[id].updated_at);

             if (task_results[id].active == 0) {
                 $('#activeid').prop('checked', false);
             } else {
                 $('#activeid').prop('checked', true);
             }


         });

         function get_task_type() {
             $('#task_type').empty();
             $('#task_type').append("<option id =\"null\" value=\"0\">Select</option>");
             $.ajax({
                 type: "GET",
                 url: "task_type",
                 dataType: "json",
                 success: function(data) {

                     //console.log(JSON.stringify(data));
                     for (var i = 0; i < data.length; i++) {

                         var task_type_name = data[i].task_type_name;
                         var task_type_id = data[i].task_type_id;
                         $('#task_type').append("<option id =" + task_type_id + " value=" + task_type_id + ">" + task_type_name + "</option>");
                     }
                 },
                 beforeSend: function() {},
                 error: function() {
                     //  alert('Data not found...!!!');
                 }

             });

         }

         get_task_type();

         function get_customer() {

             $('#customer_id').empty();
             $('#customer_id').append("<option id =\"null\" value=\"0\">Select</option>");
             $.ajax({
                 type: "GET",
                 url: "cust_name",
                 dataType: "json",
                 success: function(data) {

                     // 	console.log(JSON.stringify(data));
                     for (var i = 0; i < data.length; i++) {
                         var customer_name = data[i].customer_name
                         var customer_id = data[i].customer_id
                         $('#customer_id').append("<option id =" + customer_id + " value=" + customer_id + ">" + customer_name + "</option>");
                     }
                 },
                 beforeSend: function() {},
                 error: function() {
                     alert('Data not found...!!!');
                 }

             });

         }

         get_customer();

         $('#customer_id').on('change', function() {

             var customer_id = $(this).val();
             var get_task_type = $("#task_type").val();


             if (customer_id != 0) {
                 $(".initial_disable ").prop('disabled', true);
                 $("#plant_id").prop('disabled', false);
                 $("#plant_id").removeClass("initial_disable_color");


             } else {
                 $("#plant_id,#portal_id,#program_id,#distribution_strategy,#activeid").addClass("initial_disable_color");
                 $("#plant_id,#portal_id,#program_id").prop('disabled', true);
                 //  $("#program_id").val('0');
                 $('#program_id').html('<option value="0">Select</option>');
                 $('#plant_id,#portal_id,#program_id').removeClass("error");
                 $('#plant_id-error,#portal_id-error,#program_id-error').remove();

             }

             $('#plant_id').empty();

             if (customer_id != 0) {


                 $.ajax({
                     type: "POST",
                     url: "task_get_plant_for_customer",
                     // dataType: "json",
                     data: {
                         "_token": "{{ csrf_token() }}",
                         "customer_id": customer_id,
                     },

                     success: function(data) {

                         if (data != 0) {
                             for (var i = 0; i < data.length; i++) {
                                 if (i == '0') {
                                     $('#plant_id').append("<option id =\"null\" value=\"0\">Select</option>");
                                 }
                                 var plant_name = data[i].plant_name
                                 var plant_id = data[i].plant_id
                                 $('#plant_id').append("<option id =" + plant_id + " value=" + plant_id + ">" + plant_name + "</option>");

                             }

                             $('#portal_id').html('<option value="0">Select</option>');
                         } else {
                             $('#plant_id').append("<option id =\"null\" value=\"0\">No plant available</option>");
                             $('#portal_id').html('<option value="0">Select</option>');
                         }
                     },
                     beforeSend: function() {

                     },
                     error: function() {

                     }
                 });

                 $('#program_id').empty();
                 $.ajax({
                     type: "POST",
                     url: "task_get_program_for_customer_and_type",
                     // dataType: "json",
                     data: {
                         "_token": "{{ csrf_token() }}",
                         "customer_id": customer_id,
                         "task_type": get_task_type,
                     },

                     success: function(data) {

                         if (data != 0) {
                             for (var i = 0; i < data.length; i++) {
                                 if (i == '0') {
                                     $('#program_id').append("<option id =\"null\" value=\"0\">Select</option>");
                                 }
                                 var program_name = data[i].program_name;
                                 var program_id = data[i].program_id;
                                 $('#program_id').append("<option id =" + program_id + " value=" + program_id + ">" + program_name + "</option>");

                             }

                         } else {
                             $('#program_id').append("<option id =\"null\" value=\"0\">No program available</option>");
                         }
                     },
                     beforeSend: function() {

                     },
                     error: function() {

                     }
                 });

             } else {

                 $('#plant_id').html('<option value="0">Select </option>');
                 $('#portal_id').html('<option value="0">Select</option>');
             }

         });

         $('#plant_id').on('change', function() {

             var plant_id = $(this).val();
             var customer_id = $('#customer_id').val();
             $('#portal_id').empty();
             if (plant_id != 0) {
                 $("#portal_id").prop('disabled', false);
                 $("#portal_id").removeClass("initial_disable_color");


             } else {
                 $("#portal_id").addClass("initial_disable_color");
                 $("#portal_id").prop('disabled', true);
                 $('#portal_id,#program_id').removeClass("error");
                 $('#portal_id-error,#program_id-error').remove();
             }

             if (plant_id != 0) {

                 $.ajax({
                     type: "POST",
                     url: "task_get_portal_for_plant",
                     data: {
                         "_token": "{{ csrf_token() }}",
                         "plant_id": plant_id,
                         "customer_id": customer_id
                     },

                     success: function(data) {

                         if (data != 0) {


                             for (var i = 0; i < data.length; i++) {
                                 if (i == '0') {
                                     $('#portal_id').append("<option id =\"null\" value=\"0\">Select</option>");
                                 }
                                 var portal_name = data[i].portal_name;
                                 var portal_id = data[i].portal_id;
                                 $('#portal_id').append("<option id =" + portal_id + " value=" + portal_id + ">" + portal_name + "</option>");

                             }


                         } else {
                             $('#portal_id').append("<option id =\"null\" value=\"0\">No portal available</option>");
                         }

                     },
                     beforeSend: function() {

                     },
                     error: function() {

                     }
                 });

             } else {
                 $('#portal_id').html('<option value="0">Select</option>');
             }
         });
         $('#portal_id').on('change', function() {

             var portal_id = $(this).val();
             if (portal_id != 0) {
                 $("#program_id,#distribution_strategy,#activeid").prop('disabled', false);
                 $("#program_id,#distribution_strategy,#activeid").removeClass("initial_disable_color");


             } else {
                 $("#program_id,#distribution_strategy,#activeid").addClass("initial_disable_color");
                 $("#program_id,#distribution_strategy,#activeid").prop('disabled', true);
                 $("#program_id").val('0');

                 $('#program_id').removeClass("error");
                 $('#program_id-error').remove();
             }
         });



         $(document).on('click', '#cancel', function() {

             $("#change_ds").hide();
             $("#task-table,#update_button").removeClass("show");
             $("#back,#list-of-task").addClass("show");
             $("#customer_id,#plant_id,#portal_id,#program_id").removeClass("initial_disable_color");
             $("#customer_id,#plant_id,#portal_id,#program_id").prop('disabled', false);
             $('#plant_id-error,#customer_id-error,#task_type-error,#portal_id-error,#program_id-error').remove();
             $('#plant_id,#customer_id,#task_type,#portal_id,#program_id').removeClass("error");

            myarray.length = 0;

         });


         $(document).on('click', '#update', function() {
             if ($('#task-table-form').valid()) {


                 $.msgBox({
                     title: "Confirmation",
                     content: "Do you want to update the task",
                     type: "confirm",
                     buttons: [{
                         value: "Yes"
                     }, {
                         value: "No"
                     }],
                     success: function(result) {
                         if (result == "Yes") {


                             update_task_data();

                         } else {

                         }
                     }
                 });


             }

         });

         function update_task_data() {

             show_no_results = "FALSE";


             var task_type = $('#task_type').val();
             var task_name = $('#task_name').val();
             var customer_id = $('#customer_id').val();
             var plant_id = $('#plant_id').val();
             var portal_id = $('#portal_id').val();
             var program_id = $('#program_id').val();
             console.log(myarray);
             if ($('#activeid').is(":checked")) {
                 active = 1;

             } else {

                 active = 0;

             }

             if ($('#distribution_strategy').is(":checked")) {
                 distribution_strategy = 1;

             } else {
                 distribution_strategy = 0;

             }

             $.ajax({
                 type: "POST",
                 url: "task_update",
                 data: {
                     "_token": "{{ csrf_token() }}",
                     "task_type_id": task_type,
                     "task_name": task_name,
                     "customer_id": customer_id,
                     "plant_id": plant_id,
                     "portal_id": portal_id,
                     "program_id": program_id,
                     "active": active,
                     "distribution_strategy": myarray,
                     "distribution_strategy_check": distribution_strategy,
                     "task_id": task_id_ref,

                 },
                 success: function(data) {

                     if (search_click == "YES") {
                         display_task_data();

                     }

                     if (show_all_click == "YES") {
                         show_no_results = "TRUE";
                         show_all_task();
                     }

                     $.msgBox({
                         title: "Message",
                         content: "Task updated successfully",
                         type: "info",
                     });

                     $('#task-table-form')[0].reset();
                     $("#update_button,#task-table").removeClass("show");
                     $("#back,#list-of-task").addClass("show");
                     myarray.length = 0;
                 },
                 beforeSend: function() {


                 },
                 error: function() {
                     $.msgBox({
                         title: "Alert",
                         content: "Something went wrong while update",
                         type: "alert",
                     });

                     var ds_val = $('#distribution_strategy').val();
                     if (ds_val == "on") {
                         $('#distribution_strategy').prop('checked', false);
                     }

                 }
             });

         }



         $(document).on('dblclick', '.double_click', function() {

             var id = $(this).data('id');

             $("#tab").click();

             customer_id = task_results[id].customer_id;
             plant_id = task_results[id].plant_id;
             get_task_type = task_results[id].task_type_id;

             $('#plant_id').empty();

             if (customer_id != 0) {
                 $.ajax({
                     type: "POST",
                     url: "task_get_plant_for_customer",
                     // dataType: "json",
                     data: {
                         "_token": "{{ csrf_token() }}",
                         "customer_id": customer_id,
                     },

                     success: function(data) {

                         if (data != 0) {
                             for (var i = 0; i < data.length; i++) {
                                 if (i == '0') {
                                     $('#plant_id').append("<option id =\"null\" value=\"0\">Select</option>");
                                 }
                                 var plant_name = data[i].plant_name
                                 var plant_id = data[i].plant_id
                                 $('#plant_id').append("<option id =" + plant_id + " value=" + plant_id + ">" + plant_name + "</option>");

                             }

                             $('#plant_id').val(task_results[id].plant_id);
                         }

                     },
                     beforeSend: function() {

                     },
                     error: function() {

                     }
                 });

             }

             $('#program_id').empty();
             $.ajax({
                 type: "POST",
                 url: "task_get_program_for_customer_and_type",
                 // dataType: "json",
                 data: {
                     "_token": "{{ csrf_token() }}",
                     "customer_id": customer_id,
                     "task_type": get_task_type,
                 },

                 success: function(data) {

                     if (data != 0) {
                         for (var i = 0; i < data.length; i++) {
                             if (i == '0') {
                                 $('#program_id').append("<option id =\"null\" value=\"0\">Select</option>");
                             }
                             var program_name = data[i].program_name;
                             var program_id = data[i].program_id;
                             $('#program_id').append("<option id =" + program_id + " value=" + program_id + ">" + program_name + "</option>");

                         }
                         $('#program_id').val(task_results[id].program_id);

                     } else {
                         $('#program_id').append("<option id =\"null\" value=\"0\">No program available</option>");
                     }
                 },
                 beforeSend: function() {

                 },
                 error: function() {

                 }
             });


             $('#portal_id').empty();

             if (plant_id != 0) {

                 $.ajax({
                     type: "POST",
                     url: "task_get_portal_for_plant",
                     data: {
                         "_token": "{{ csrf_token() }}",
                         "plant_id": plant_id,
                         "customer_id": customer_id
                     },

                     success: function(data) {

                         if (data != 0) {


                             for (var i = 0; i < data.length; i++) {
                                 if (i == '0') {
                                     $('#portal_id').append("<option id =\"null\" value=\"0\">Select</option>");
                                 }
                                 var portal_name = data[i].portal_name;
                                 var portal_id = data[i].portal_id;
                                 $('#portal_id').append("<option id =" + portal_id + " value=" + portal_id + ">" + portal_name + "</option>");

                             }

                             $('#portal_id').val(task_results[id].portal_id);


                         }

                         $("#task-table,#update_button").addClass("show");
                         $("#back,#list-of-task").removeClass("show");


                     },
                     beforeSend: function() {

                     },
                     error: function() {

                     }
                 });

             }

             var ds = 0;
             task_id = task_results[id].task_id;
             $.ajax({
                 type: "POST",
                 url: "task_ds_ref",
                 data: {
                     "_token": "{{ csrf_token() }}",
                     "task_id": task_id,
                 },

                 success: function(data) {

                     if (data != 0) {

                         task_ds_ref_data = "YES";
                         $('#distribution_strategy').prop('checked', true);
                         $("#change_ds").show();


                     } else {
                       task_ds_ref_data = "NO";
                       $('#distribution_strategy').prop('checked', false);
                         $("#change_ds").hide();
                     }

                 },
                 beforeSend: function() {

                 },
                 error: function() {

                 }
             });


             task_id_ref = task_results[id].task_id;
             $('#task_type').val(task_results[id].task_type_id);
             $('#task_name').val(task_results[id].task_name);
             $('#customer_id').val(task_results[id].customer_id);

             $('#created_by').val(task_results[id].created_by);
             $('#created_date').val(task_results[id].created_at);
             $('#modified_by').val(task_results[id].updated_by);
             $('#modified_date').val(task_results[id].updated_at);

             if (task_results[id].active == 0) {
                 $('#activeid').prop('checked', false);
             } else {
                 $('#activeid').prop('checked', true);
             }


         });

     });

     $(window).load(function() {

         $("#setups").trigger('click');
         $("#task").trigger('click');

         var fetch_customer_name;
         $('input[name="search_customer_name"]').autoComplete({
             minChars: 1,
             source: function(term, response) {
                 try {
                     fetch_customer_name.abort();
                 } catch (e) {}
                 fetch_customer_name = $.getJSON('customer_child_controller/autocomplete_customer_name', {
                     search_customer_name_id: term
                 }, function(data) {
                     response(data);
                 });
             }
         });

         var fetch_task_type;
         $('input[name="search_task_type"]').autoComplete({
             minChars: 1,
             source: function(term, response) {
                 try {
                     fetch_task_type.abort();
                 } catch (e) {}
                 fetch_task_type = $.getJSON('task_controller/autocomplete_task_type', {
                     search_task_type: term
                 }, function(data) {
                     response(data);
                 });
             }
         });

         var fetch_task_name;
         $('input[name="search_task_name"]').autoComplete({
             minChars: 1,
             source: function(term, response) {
                 try {
                     fetch_task_name.abort();
                 } catch (e) {}
                 fetch_task_name = $.getJSON('task_controller/autocomplete_task_name', {
                     search_task_name: term
                 }, function(data) {
                     response(data);
                 });
             }
         });


         var fetch_plant_name;
         $('input[name="search_plant_name"]').autoComplete({
             minChars: 1,
             source: function(term, response) {
                 try {
                     fetch_plant_name.abort();
                 } catch (e) {}
                 fetch_plant_name = $.getJSON('fetch_plant_name', {
                     plant_name: term
                 }, function(data) {
                     response(data);
                 });
             }
         });
     });

     //distribution_strategy starts
     $.ajax({
         type: "GET",
         url: "task_status",
         success: function(data) {

             if (data != 0) {
                 data_status = data;
                 $('#task_status').empty();
                 $('#task_status').append("<option id =\"null\" value=\"0\">Select</option>");

                 for (var i = 0; i < data.length; i++)
                    {
                     var status_id = data[i].status_id;
                     var status_name = data[i].status_name;
                     $('#task_status').append("<option id =" + status_id + " value=" + status_id + ">" + status_name + "</option>");
                    }

             }

         },
         beforeSend: function() {

         },
         error: function() {

         }
     });

     $.ajax({
         type: "GET",
         url: "communication_options",
         success: function(data) {

             if (data != 0) {
                 data_communication_options = data;
                 $('#communication_options').empty();
                 $('#communication_options').append("<option id =\"null\" value=\"0\">Select</option>");

                 for (var i = 0; i < data.length; i++) {

                     var communication_option_id = data[i].communication_option_id;
                     var communication_option_name = data[i].communication_option_name;
                     $('#communication_options').append("<option id =" + communication_option_id + " value=" + communication_option_id + ">" + communication_option_name + "</option>");
                 }
             }

         },
         beforeSend: function() {

         },
         error: function() {

         }
     });

     $.ajax({
         type: "GET",
         url: "mail_address",
         success: function(data) {

             if (data != 0) {
                 mail_address_data = data;

             }
         },
         beforeSend: function() {

         },
         error: function() {

         }
     });

     $.ajax({
         type: "GET",
         url: "group_mail_address",
         success: function(data) {

             if (data != 0) {
                 group_mail_address_data = data;
             }
         },
         beforeSend: function() {

         },
         error: function() {

         }
     });

     $(".ds_form").dialog({
         autoOpen: false,
         modal: true,
         width: 750,
     });

     $("#set-ds").dialog({
      modal: true,
      autoOpen: false,
      width:850,
      height:170
      });


     function ds_data() {
         var task_id_ref_ds = task_id_ref;

         $.ajax({
             type: "POST",
             url: "ds_results",
             dataType: "json",
             data: {

                 "_token": "{{ csrf_token() }}",
                 "task_id_ref_ds": task_id_ref_ds,
             },

             success: function(data) {
                 if (data != "0") {
                                 var results = data;
                                 ds_results = results;
                                 ds_data1 = "YES";

                                 $('.ds_tab tr').remove();
                                $('.resetgap2').remove();
                                 $('.ds_tab').append('<tr style="background-color: #D3D3D3;"><th class="ds_th">Task Status</th><th class="ds_th">communication_options</th><th class="ds_th">Mail Type</th><th class="ds_th">Mail Address</th><th>Delete</th></tr>');
                                 $('.ds_form').append('	<div class="resetgap2 actiondiv2"><div style=" margin-bottom: 20px; "> <button id="update_ds" class="dsbuttons" type="button"><span class="btn_txt">Save</span></button><button class="dsbuttons" id="ds_close" type="button" ><span class="btn_txt">Close</span></button></div></div>');

                                 for (var j = 0; j < results.length; j++) {

                                     $('.ds_tab').append('<tr data-id="' + j + '" id="' + results[j].distribution_strategy_id + '"><td><select id="task_status' + j + '"></select></td><td><select id="communication_options' + j + '"></select></td><td><select class = "mail_type_change" data-id="' + j + '"  id="mail_type' + j + '"><option>personnel</option><option>group</option></select></td><td data-id="' + results[j].distribution_strategy_id + '"><select id="mail_address' + j + '" ></select></td><td><button id="' + j + '" type="button" class="delete_ds btn btn-default" aria-label="Left Align"><span class="glyphicon glyphicon-remove delete_color" aria-hidden="true"></span></button></td></tr>');

                                     ref_ds_id = results[j].distribution_strategy_id;

                                     var task_status = results[j].status_name;
                                     var communication_options = results[j].communication_option_name;
                                     var mail_address = results[j].mail_address;
                                     var mail_type = results[j].mail_type;
                                     var mail_id = results[j].mail_id;
                                     var mail_id_address = results[j].mail_id_address;


                                     for (var i = 0; i < data_status.length; i++) {

                                         var status_name = data_status[i].status_name;
                                         var status_id = data_status[i].status_id;
                                         $('#' + "task_status" + j).append("<option id =" + status_id + " value=" + status_id + ">" + status_name + "</option>");

                                     }
                                     $('#' + "task_status" + j).val(results[j].status_id);

                                     for (var i = 0; i < data_communication_options.length; i++) {
                                         var communication_option_name = data_communication_options[i].communication_option_name;
                                         var communication_option_id = data_communication_options[i].communication_option_id;
                                         $('#' + "communication_options" + j).append("<option id =" + communication_option_id + " value=" + communication_option_id + ">" + communication_option_name + "</option>");
                                     }
                                     $('#' + "communication_options" + j).val(results[j].communication_options);
                                     $('#' + "mail_type" + j).val(results[j].mail_type);


                                     if (mail_type == "personnel") {
                                        // $('#' + "mail_address" + j).empty();
                                         for (var i = 0; i < mail_address_data.length; i++) {
                                             ent_user_id = mail_address_data[i].ent_user_id;
                                             mail_comm_id = mail_address_data[i].mail_comm_id;
                                             $('#' + "mail_address" + j).append("<option id =" + mail_comm_id + " value=" + mail_comm_id + ">" + ent_user_id + "</option>");
                                         }
                                         $('#' + "mail_address" + j).val(results[j].mail_id);
                                     } else if (mail_type == "group") {
                                         //$('#' + "mail_address" + j).empty();

                                         for (var i = 0; i < group_mail_address_data.length; i++) {
                                             ent_user_group_id = group_mail_address_data[i].ent_user_group_id;
                                             user_group_id = group_mail_address_data[i].user_group_id;
                                             $('#' + "mail_address" + j).append("<option id =" + user_group_id + " value=" + user_group_id + ">" + ent_user_group_id + "</option>");
                                         }
                                         $('#' + "mail_address" + j).val(results[j].mail_id);

                                     }
                                 }
                    }
                    else if(data == "0")
                     {
                       task_ds_ref_data = "NO";
                     $('.ds_form').dialog("close");
                     var ds_val = $('#distribution_strategy').val();
                     if (ds_val == "on")
                     {
                        $('#distribution_strategy').prop('checked', false);
                     }
                     $('#change_ds').hide();
                  }
             },
             beforeSend: function() {

             },
             error: function() {

             }
         });

     }


$('#distribution_strategy').click(function() {
   if ($("#distribution_strategy").is(':checked'))
   {

       if(task_ds_ref_data == "NO")
                   {
                           $('#set-ds').dialog("open");
                           $(".resetgap").addClass("show");
                           $.ajax({
                              type: "GET",
                              url: "get_current_ds_val",
                              success: function(data) {
                                for (var i = 0; i < data.length; i++)
                                {
                                    current_ds_count = data[i].distribution_strategy_id;
                                     console.log(current_ds_count);
                               }
                              },
                              beforeSend: function() {

                              },
                              error: function() {

                              }
                          });
                     }
                    else if(task_ds_ref_data == "YES")
                        {
                           $('#change_ds').show();

                        }
                 }
                 else if ($("#distribution_strategy").is(':unchecked'))
                 {
                   $('#change_ds').hide();
                 }
      });

     $('#change_ds').on('click', function() {
          if(task_ds_ref_data == "YES")
           {
                   ds_data();
                  $('.ds_tab tr').remove();
                  $('.ds_form').dialog("open");
                  $('.resetgap2').remove();
          }
     });


       $(' #Cancel-ds').click(function(){
              myarray.length = 0;
              reset_ds();
           $('#set-ds').dialog("close");
            var ds_val = $('#distribution_strategy').val();
            if (ds_val == "on")
            {
               $('#distribution_strategy').prop('checked', false);
            }
         });


     $(document).on('change', '.mail_type_change', function() {
         var id = $(this).data("id");
         if (this.value == 'personnel') {
             $('#mail_address' + id).empty();
             $.ajax({
                 type: "GET",
                 url: "mail_address",
                 success: function(data) {

                     if (data != 0) {
                         for (var i = 0; i < data.length; i++) {
                             var ent_user_id = data[i].ent_user_id;
                             var mail_comm_id = data[i].mail_comm_id;
                             $('#mail_address' + id).append("<option id =" + mail_comm_id + " value=" + mail_comm_id + ">" + ent_user_id + "</option>");
                         }

                     }
                 },
                 beforeSend: function() {

                 },
                 error: function() {

                 }
             });
         } else if (this.value == 'group') {
             $('#mail_address' + id).empty();
             $.ajax({
                 type: "GET",
                 url: "group_mail_address",
                 success: function(data) {

                     if (data != 0) {
                         for (var i = 0; i < data.length; i++) {
                             var ent_user_group_id = data[i].ent_user_group_id;
                             var user_group_id = data[i].user_group_id;
                             $('#mail_address' + id).append("<option id =" + user_group_id + " value=" + user_group_id + ">" + ent_user_group_id + "</option>");
                         }

                     }
                 },
                 beforeSend: function() {

                 },
                 error: function() {

                 }
             });
         }
     });



     var myarray = [];
     $(document).on('click', '#update_ds', function() {

         for (var i = 0; i < ds_results.length; i++) {
             var distribution_strategy_id = ds_results[i].distribution_strategy_id;
             var status_id = $('#task_status' + i).val();
             var communication_options = $('#communication_options' + i).val();
             var mail_type = $('#mail_type' + i).val();
             var mail_id = $('#mail_address' + i).val();
             var ds_update_data = {
                 "distribution_strategy_id": distribution_strategy_id,
                 "task_status": status_id,
                 "communication_options": communication_options,
                 "mail_type": mail_type,
                 "mail_id": mail_id
             }
             myarray.push(ds_update_data);
             reset_ds();
         }
     });



     $(document).on('click', '#ds_close', function() {
         $('.ds_form').dialog("close");
     });




function delete_ds()
{
                       $.ajax({
                           type: "POST",
                           url: "ds_delete",
                           data: {
                               "_token": "{{ csrf_token() }}",
                               "distribution_strategy_id": ref_ds_id,
                           },
                           success: function(data) {

                               $.msgBox({
                                   title: "Message",
                                   content: "DS Deleted Successfully",
                                   type: "info",
                               });

                                $(ref_ds_id).remove();
                                 ds_data();

                           },
                           beforeSend: function() {

                           },
                           error: function() {

                           }
                       });
           }




     $(document).on('click', '.delete_ds', function() {

         var msg = "Do you want to delete the DS";
         $.msgBox({
             title: "Confirm",
             content: msg,
             type: "confirm",
             buttons: [{
                 value: "Yes"
             }, {
                 value: "No"
             }],

             success: function(result) {
                 if (result == "Yes") {
                   delete_ds();
                 } else {

                 }
             }
         });

     });
//creating new ds starts here
function populateSelectOptions(result, selectId, valueAttr, displayAttr) {
  $('#' + selectId).empty();
    $('#'+ selectId).append("<option id =\"null\" value=\"\">Select Mail Address</option>");

  if(result == null || result.length == 0) {
    alert('Data not found...!!!');
  } else {
    for (var i = 0; i < result.length; i++)
    {
      var value = result[i][valueAttr];
      var display = result[i][displayAttr];
      console.log("selectId",selectId);
      $('#'+ selectId).append("<option value='"+ value +"'>"+display+"</option>");

    }
  }
}

function getDataFromDB(methodType, url, dataType, selectId, valueAttr, displayAttr, data) {

  $.ajax({
    type:methodType,
    url:url,
    dataType:dataType,
    data:data?data:{},
    success:function(result){

        populateSelectOptions(result, selectId, valueAttr, displayAttr);
        console.log("result",result);

    },
    error: function() {
      alert('Error while fetching the record');
    }
  });
}

function onChangeEvent(selectedId, methodType, url, altUrl, dataType, selectId, valueAttr, displayAttr){
  $('#'+selectedId).on('change', function(){
    var selectedValue = $('#'+selectedId).val();

    if(Array.isArray(selectedValue)) {
      selectedValue = selectedValue.join('\',\'');

    }
    var data = {"_token": "{{ csrf_token() }}", filterKey:selectedValue};

    if(selectedValue == ""){
      url = altUrl;
    }

    getDataFromDB(methodType, url, dataType, selectId, valueAttr, displayAttr, data);

  });
}

$('#mail_type').on('change', function() {

  if(this.value == 'personnel')
   {
     console.log(this.value);
      getDataFromDB('GET', 'mail_address','json', 'mail_address', 'mail_comm_id', 'ent_user_id');
   }

  else if(this.value == 'group')
   {
       console.log(this.value);
              getDataFromDB('GET', 'group_mail_address', 'json', 'mail_address', 'user_group_id', 'ent_user_group_id');
     }

   });

        var myarray = [];
        $(document).on('click', '#save-ds', function() {

                var task_status = $('#task_status').val();
                var communication_options = $('#communication_options').val();
                var mail_type = $('#mail_type').val();
                var mail_address = $('#mail_address').val();
                current_ds_count = current_ds_count +1;

                var ds =  {
                        "distribution_strategy_id": current_ds_count,
                        "task_status": task_status,
                        "communication_options": communication_options,
                        "mail_type": mail_type,
                        "mail_id": mail_address
                    }

                myarray.push(ds);
                console.log(myarray);
                reset_ds();
        });

            function reset_ds()
              {
                    $('#set-ds')[0].reset();
              }


            $('#Reset-ds').click(function(){
                  reset_ds();
            });



//creating new ds starts here


     //distribution_strategy ends
 </script>
</div>
@stop
@section('lower_band')
<div class="col-xs-12 lower_band">
   <div  id="search_button">
      <button class="headerbuttons" id="filter" type="button">Search</button>
   </div>
   <div  id="back_buttons">
      <button class="headerbuttons" id="back" type="button">Back</button>
   </div>
   <div id="update_button" style="float:right">
      <button class="headerbuttons" id="update" type="button">Update</button>
      <button class="headerbuttons" id="cancel" type="button">Cancel</button>
   </div>
</div>
@stop
