@extends('layouts.uscan_master_page')
@section('header')

   <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/base/jquery-ui.css" rel="stylesheet">
   <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/task_create.css">
<script src="js/task_validation.js"></script>
@stop
@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="col-xs-4 display-title">
      Create Task
   </div>
</div>
@stop
@section('content')
<div class="col-xs-12 content">
   <ul class="nav nav-tabs">
      <li class="active"><a class="tab_one"  data-toggle="tab" href="#tab_one">Details</a></li>
   </ul>
   <div class="tab-content">
      <div id="tab_one" class="tab-pane fade in active custom_tab">
         <form id="task-table-form" autocomplete="off" class="task-table">
            <table style="width:40%;">
               <tr>
                  <td>Task Type</td>
                  <td><select id = "task_type" name="task_type_name">
                     </select>
                  </td>
               </tr>
               <tr>
                  <td>Task Name</td>
                  <td><input type="text" name="task_name" id="task_name"></td>
               </tr>
               <tr>
                  <td>Customer Name</td>
                  <td><select id="customer_id" class = "initial_disable_color" name ="customer_id">
                     </select>
                  </td>
               </tr>
               <tr>
                  <td>Plant Name</td>
                  <td>
                     <select id="plant_id" class = "initial_disable initial_disable_color" name ="plant_id">
                        <option value="0">Select</option>
                     </select>
                  </td>
               </tr>
               <tr>
                  <td>Portal Name</td>
                  <td>
                     <select id="portal_id" class = "initial_disable initial_disable_color" name ="portal_id">
                        <option value="0">Select</option>
                     </select>
                  </td>
               </tr>
               <tr>
                  <td>Program Name</td>
                  <td><select id="program_id" class = "initial_disable initial_disable_color" name ="program_id">
                      <option value="0">Select</option>
                     </select>
                  </td>
               </tr>
               <tr>
                  <td>Distribution strategy</td>
                  <td><input type="checkbox" class = "" name="distribution_strategy" id="distribution_strategy"></td>
               </tr>
               <tr>
                  <td>Active</td>
                  <td><input type="checkbox" class = "initial_disable_color"  name="avtivename" id="activeid"></td>
               </tr>
            </table>
         </form>
      </div>
   </div>


<form id="set-ds" style="display:none;">
	<table style="width:100%; margin-bottom: 20px">
	<tr style="background-color: #eee;">
		<th class="set_ds_th">Task Status</th>
		<th class="set_ds_th">Communication Options</th>
    <th class="set_ds_th">Address Type</th>
		<th class="set_ds_th">Mail Address</th>
	</tr>

		<tr>
      <td><select id="task_status">
        </select></td>

			<td><select id="communication_options">
				</select></td>

        <td><select id="mail_type">
        <option value="">Select</option>
        <option value="personnel" id="personnel">Personnel</option>
        <option value="group" id="group">Group</option>
        </select></td>

			<td><select id="mail_address">

				</select></td>
		</tr>

		<tr id="success-msg" style="display: none;">
			<td></td>
			<td><p>Information has been saved successfuly!</p></td>
		</tr>
	</table>

	<div class="resetgap actiondiv1">
	<div style=" margin-bottom: 20px; ">
		<button class="headerbuttons" type="button" id="save-ds">Save</button>
		<button class="headerbuttons" type="button" id="Reset-ds">Reset</button>
    <button class="headerbuttons" type="button" id="Cancel-ds">Cancel</button>
	</div>
	</div>
</form>



   <script type="text/javascript">
      jQuery.validator.addMethod('selectcheck', function(value) {
          return (value != '0');
      }, "");

      $(".initial_disable").prop('disabled', true);
      $("#customer_id").prop('disabled', true);

      function get_task_type() {
          $('#task_type').empty();
          $('#task_type').append("<option id =\"null\" value=\"0\">Select</option>");
          $.ajax({
              type: "GET",
              url: "task_type",
              dataType: "json",
              success: function(data) {

                  console.log(JSON.stringify(data));
                  for (var i = 0; i < data.length; i++) {

                      var task_type_name = data[i].task_type_name;
                      var task_type_id = data[i].task_type_id;
                      $('#task_type').append("<option id =" + task_type_id + " value=" + task_type_id + ">" + task_type_name + "</option>");
                  }
              },
              beforeSend: function() {},
              error: function() {
                  //  alert('Data not found...!!!');
              }

          });
      }

      get_task_type();

      function get_customer() {

          $('#customer_id').empty();
          $('#customer_id').append("<option id =\"null\" value=\"0\">Select</option>");
          $.ajax({
              type: "GET",
              url: "cust_name",
              dataType: "json",
              success: function(data) {

                  // 	console.log(JSON.stringify(data));
                  for (var i = 0; i < data.length; i++) {
                      var customer_name = data[i].customer_name
                      var customer_id = data[i].customer_id
                      $('#customer_id').append("<option id =" + customer_id + " value=" + customer_id + ">" + customer_name + "</option>");
                  }
              },
              beforeSend: function() {},
              error: function() {
                  alert('Data not found...!!!');
              }

          });

      }

      get_customer();


      $('#customer_id').on('change', function() {

          var customer_id = $(this).val();
          var get_task_type = $("#task_type").val();

          if(customer_id != 0)
          {
            $(".initial_disable ").prop('disabled', true);
            $("#plant_id").prop('disabled', false);
            $("#plant_id").removeClass("initial_disable_color");


          }
          else {
            $("#plant_id,#portal_id,#program_id,#distribution_strategy,#activeid").addClass("initial_disable_color");
            $(".initial_disable").prop('disabled', true);
            $("#program_id").val('0');
            $('#plant_id,#portal_id,#program_id').removeClass("error");
            $('#plant_id-error,#portal_id-error,#program_id-error').remove();

          }


          $('#plant_id').empty();

          if (customer_id != 0) {


              $.ajax({
                  type: "POST",
                  url: "task_get_plant_for_customer",
                  // dataType: "json",
                  data: {
                      "_token": "{{ csrf_token() }}",
                      "customer_id": customer_id,
                  },

                  success: function(data) {

                      if (data != 0) {
                          for (var i = 0; i < data.length; i++) {
                              if (i == '0') {
                                  $('#plant_id').append("<option id =\"null\" value=\"0\">Select</option>");
                              }
                              var plant_name = data[i].plant_name
                              var plant_id = data[i].plant_id
                              $('#plant_id').append("<option id =" + plant_id + " value=" + plant_id + ">" + plant_name + "</option>");

                          }

                          $('#portal_id').html('<option value="0">Select</option>');
                      } else {
                          $('#plant_id').append("<option id =\"null\" value=\"0\">No plant available</option>");
                          $('#portal_id').html('<option value="0">Select</option>');
                      }
                  },
                  beforeSend: function() {

                  },
                  error: function() {

                  }
              });


              $('#program_id').empty();
              $.ajax({
                  type: "POST",
                  url: "task_get_program_for_customer_and_type",
                  // dataType: "json",
                  data: {
                      "_token": "{{ csrf_token() }}",
                      "customer_id": customer_id,
                      "task_type": get_task_type,
                  },

                  success: function(data) {

                    if (data != 0) {
                        for (var i = 0; i < data.length; i++) {
                            if (i == '0') {
                                $('#program_id').append("<option id =\"null\" value=\"0\">Select</option>");
                            }
                            var program_name = data[i].program_name;
                            var program_id = data[i].program_id;
                            $('#program_id').append("<option id =" + program_id + " value=" + program_id + ">" + program_name + "</option>");

                        }

                    } else {
                        $('#program_id').append("<option id =\"null\" value=\"0\">No program available</option>");
                    }
                  },
                  beforeSend: function() {

                  },
                  error: function() {

                  }
              });

          } else {

              $('#plant_id').html('<option value="0">Select</option>');
              $('#portal_id').html('<option value="0">Select</option>');
              $('#program_id').html('<option value="0">Select</option>');
          }

      });

      $('#plant_id').on('change', function() {

          var plant_id = $(this).val();
          var cust_id = $('#customer_id').val();
          $('#portal_id').empty();

          if(plant_id != 0)
          {
            $("#portal_id").prop('disabled', false);
            $("#portal_id").removeClass("initial_disable_color");


          }
          else {
            $("#portal_id").addClass("initial_disable_color");
            $("#portal_id").prop('disabled', true);
            $('#portal_id,#program_id').removeClass("error");
            $('#portal_id-error,#program_id-error').remove();
          }


          if (plant_id != 0) {

              $.ajax({
                  type: "POST",
                  url: "task_get_portal_for_plant",
                  data: {
                      "_token": "{{ csrf_token() }}",
                      "plant_id": plant_id,
                      "customer_id" : cust_id
                  },

                  success: function(data) {

                      if (data != 0) {


                          for (var i = 0; i < data.length; i++) {
                              if (i == '0') {
                                  $('#portal_id').append("<option id =\"null\" value=\"0\">Select</option>");
                              }
                              var portal_name = data[i].portal_name;
                              var portal_id = data[i].portal_id;
                              $('#portal_id').append("<option id =" + portal_id + " value=" + portal_id + ">" + portal_name + "</option>");

                          }


                      } else {
                          $('#portal_id').append("<option id =\"null\" value=\"0\">No portal available</option>");
                      }

                  },
                  beforeSend: function() {

                  },
                  error: function() {

                  }
              });

          } else {
              $('#portal_id').html('<option value="0">Select</option>');
          }
      });

      $('#portal_id').on('change', function() {

          var portal_id = $(this).val();
          if(portal_id != 0)
          {
            $("#program_id,#distribution_strategy,#activeid").prop('disabled', false);
            $("#program_id,#distribution_strategy,#activeid").removeClass("initial_disable_color");


          }
          else {
            $("#program_id,#distribution_strategy,#activeid").addClass("initial_disable_color");
            $("#program_id,#distribution_strategy,#activeid").prop('disabled', true);
            $("#program_id").val('0');

            $('#program_id').removeClass("error");
            $('#program_id-error').remove();
          }
        });



        $('#task_type').on('change', function() {

            if (this.value == 0) {

              $("#customer_id,#plant_id,#portal_id,#program_id,#distribution_strategy,#activeid").addClass("initial_disable_color");
              $(".initial_disable,#customer_id").prop('disabled', true);
              $('#plant_id').html('<option value="0">Select</option>');
              $('#portal_id').html('<option value="0">Select</option>');
              $('#program_id').html('<option value="0">Select</option>');
              $('#plant_id-error,#customer_id-error,#task_type-error,#portal_id-error,#program_id-error,#task_name-error').remove();
              $('#plant_id,#customer_id,#task_type,#portal_id,#program_id,#task_name').removeClass("error");
              $("#customer_id,#program_id,#plant_id,#portal_id").val('0');
            }
            else {

              $("#program_id,#plant_id,#portal_id,#distribution_strategy,#activeid,#portal_id,#plant_id").addClass("initial_disable_color");
              $("#program_id,#plant_id,#portal_id,#distribution_strategy,#activeid,#portal_id,#plant_id").prop('disabled', true);
              $("#customer_id").removeClass("initial_disable_color");
              $("#customer_id").prop('disabled', false);
              $("#plant_id,#portal_id,#customer_id").val('0');
              $('#program_id').html('<option value="0">Select</option>');
              $('#plant_id-error,#customer_id-error,#task_type-error,#portal_id-error,#program_id-error,#task_name-error').remove();
              $('#plant_id,#customer_id,#task_type,#portal_id,#program_id,#task_name').removeClass("error");

            }

        });

      $(document).on('click', '#save_button', function() {

          if ($('#task-table-form').valid()) {

              var task_type = $('#task_type').val();
              var task_name = $('#task_name').val();
              var customer_id = $('#customer_id').val();
            //  var distribution_strategy = $('#distribution_strategy').val();
              var plant_id = $('#plant_id').val();
              var portal_id = $('#portal_id').val();
              var program_id = $('#program_id').val();

              if ($('#activeid').is(":checked")) {
                  active = 1;

              } else {

                  active = 0;

              }

              // if ($('#distribution_strategy').is(":checked")) {
              //
              //     distribution_strategy = 1;
              //
              // } else {
              //
              //     distribution_strategy = 0;
              //
              // }
            //alert(distribution_strategy);
          //  alert( + task_name + customer_id  + plant_id  + portal_id +  program_id  + active +  distribution_strategy)


              $.ajax({
                  type: "POST",
                  url: "task_insert",
                  data: {
                      "_token": "{{ csrf_token() }}",
                      "task_type": task_type,
                      "task_name": task_name,
                      "customer_id": customer_id,
                      "plant_id": plant_id,
                      "portal_id": portal_id,
                      "program_id": program_id,
                      "active": active,
                      "distribution_strategy": myarray,
                  },
                  success: function(data) {

                      $.msgBox({
                          title: "Message",
                          content: data,
                          type: "info",
                      });

                      $('#task-table-form')[0].reset();
                      $("#plant_id,#portal_id,#program_id,#distribution_strategy,#activeid").addClass("initial_disable_color");
                      $(".initial_disable").prop('disabled', true);
                      $("#customer_id").prop('disabled', true);
                      $("#customer_id").addClass("initial_disable_color");
                      $('#plant_id').html('<option value="0">Select</option>');
                      $('#portal_id').html('<option value="0">Select</option>');
                      $('#program_id').html('<option value="0">Select</option>');
                      myarray.length = 0;

                  },


                  beforeSend: function() {

                  },
                  error: function() {}

              });


          } else {}
      });

      $(document).on('click', '#cancel', function(){

                $('#task-table-form')[0].reset();
                $("#customer_id,#plant_id,#portal_id,#program_id,#distribution_strategy,#activeid").addClass("initial_disable_color");
                $(".initial_disable").prop('disabled', true);
                $("#customer_id").prop('disabled', true);
                $("#customer_id").addClass("initial_disable_color");
                $('#plant_id').html('<option value="0">Select</option>');
                $('#portal_id').html('<option value="0">Select</option>');
                $('#program_id').html('<option value="0">Select</option>');
                $('#plant_id-error,#customer_id-error,#task_type-error,#portal_id-error,#program_id-error,#task_name-error').remove();
                $('#plant_id,#customer_id,#task_type,#portal_id,#program_id,#task_name').removeClass("error");
                $('#distribution_strategy').find(':checked').attr('checked', '');
                  myarray.length = 0;
                  reset();
             });


             //distribution_strategy starts
             		$("#set-ds").dialog({
             		autoOpen: false,
             		width:850,
             		height:170

             		});

             		$('#distribution_strategy').on('change',function(){
             			if (jQuery(this).is(":checked")) {
             			$('#set-ds').dialog("open");
             			}
             		});

                $('#Cancel-ds').click(function(){
                       myarray.length = 0;
                       reset();
               			$('#set-ds').dialog("close");
                     var ds_val = $('#distribution_strategy').val();
                     if (ds_val == "on")
                     {
                        $('#distribution_strategy').prop('checked', false);
                     }
               		});



                function get_task_status() {
                    $('#task_status').empty();
                    $('#task_status').append("<option id =\"null\" value=\"0\">Select</option>");
                    $.ajax({
                        type: "GET",
                        url: "task_status",
                        dataType: "json",
                        success: function(data) {

                            console.log(JSON.stringify(data));
                            for (var i = 0; i < data.length; i++) {

                                var status_id = data[i].status_id;
                                var status_name = data[i].status_name;
                                $('#task_status').append("<option id =" + status_id + " value=" + status_id + ">" + status_name + "</option>");
                                }
                           },
                        beforeSend: function() {},
                        error: function() {

                        }
                      });
                   }

                function get_communication_options() {
                    $('#communication_options').empty();
                    $('#communication_options').append("<option id =\"null\" value=\"0\">Select</option>");
                    $.ajax({
                        type: "GET",
                        url: "communication_options",
                        dataType: "json",
                        success: function(data) {

                            console.log(JSON.stringify(data));
                            for (var i = 0; i < data.length; i++) {

                                var communication_option_id = data[i].communication_option_id;
                                var communication_option_name = data[i].communication_option_name;
                                $('#communication_options').append("<option id =" + communication_option_id + " value=" + communication_option_id + ">" + communication_option_name + "</option>");
                            }
                        },
                        beforeSend: function() {},
                        error: function() {

                        }

                    });
                }



              get_task_status();
              get_communication_options();

              function populateSelectOptions(result, selectId, valueAttr, displayAttr) {
                $('#' + selectId).empty();
                  $('#'+ selectId).append("<option id =\"null\" value=\"\">Select Mail Address</option>");

                if(result == null || result.length == 0) {
                  alert('Data not found...!!!');
                } else {
                  for (var i = 0; i < result.length; i++)
                  {
                    var value = result[i][valueAttr];
                    var display = result[i][displayAttr];
                    console.log("selectId",selectId);
                    $('#'+ selectId).append("<option value='"+ value +"'>"+display+"</option>");

                  }
                }
              }


              function getDataFromDB(methodType, url, dataType, selectId, valueAttr, displayAttr, data) {

                $.ajax({
                  type:methodType,
                  url:url,
                  dataType:dataType,
                  data:data?data:{},
                  success:function(result){

                      populateSelectOptions(result, selectId, valueAttr, displayAttr);
                      console.log("result",result);

                  },
                  error: function() {
                    alert('Error while fetching the record');
                  }
                });
              }



              function onChangeEvent(selectedId, methodType, url, altUrl, dataType, selectId, valueAttr, displayAttr){
                $('#'+selectedId).on('change', function(){
                  var selectedValue = $('#'+selectedId).val();

                  if(Array.isArray(selectedValue)) {
                    selectedValue = selectedValue.join('\',\'');

                  }
                  var data = {"_token": "{{ csrf_token() }}", filterKey:selectedValue};

                  if(selectedValue == ""){
                    url = altUrl;
                  }

                  getDataFromDB(methodType, url, dataType, selectId, valueAttr, displayAttr, data);

                });
              }

        //var rr = $('#mail_type').val();
      $('#mail_type').on('change', function() {

        if(this.value == 'personnel')
         {
           console.log(this.value);
            getDataFromDB('GET', 'mail_address','json', 'mail_address', 'mail_comm_id', 'ent_user_id');
         }

        else if(this.value == 'group')
         {
             console.log(this.value);
                    getDataFromDB('GET', 'group_mail_address', 'json', 'mail_address', 'user_group_id', 'ent_user_group_id');
           }

         });

              var myarray = [];

              $(document).on('click', '#save-ds', function() {

                      var task_status = $('#task_status').val();
                      var communication_options = $('#communication_options').val();
                      var mail_type = $('#mail_type').val();
                      var mail_address = $('#mail_address').val();

                      var ds =  {
                              "task_status": task_status,
                              "communication_options": communication_options,
                              "mail_type": mail_type,
                              "mail_address": mail_address
                          }

                      myarray.push(ds);
                      console.log(myarray);
                      reset();
              });

                  function reset()
                    {
                          $('#set-ds')[0].reset();
                    }

                  $('#Reset-ds').click(function(){
                        reset();
                  });
             //distribution_strategy ends






      $(window).load(function() {
          $("#setups").trigger('click');
          $("#task").trigger('click');
      });
   </script>
</div>
@stop
@section('lower_band')
<div class="col-xs-12 lower_band">
   <button class="headerbuttons" type="button" style="float:right;" id="cancel">Cancel</button>
   <button id="save_button" type="button" class="headerbuttons" style="float:right;" type="submit">Save</button>
</div>
@stop
