@extends('layouts.uscan_master_page')
@section('header')
    <!-- <link rel="stylesheet" href="css/screen.css"> -->
    <link rel="stylesheet" type="text/css" href="css/portal.css">
    <script src="js/portal_validation.js"></script>
@stop

@section('upper_band')
<div class="col-xs-12 upper_band">
      <div class="col-xs-4 display-title">
          Create Portal Master
      </div>
</div>
@stop

@section('content')
<div class="col-xs-12 content">

  <ul class="nav nav-tabs">
        <li class="active"><a class="tab_one"  data-toggle="tab" href="#tab_one">Details</a></li>
  </ul>

  <div class="tab-content">
       <div id="tab_one" class="tab-pane fade in active custom_tab">
          <form id="portal-table" autocomplete="off" class="portal-table">
            <table style="width:40%;">
								<tr>
									<td>Customer Name</td>
									<td><select id = "customer_id" name="customer_id">
                  </select> </td>
								</tr>
						   		<tr>
										<td>Plant Name</td>
                    <td><select id="plant_id" class = "initial_disable_color" name ="plant_id">
                      <option value="0">Select Customer</option>
                    </select> </td>
										</tr>
										<tr>
											<td>Portal Name</td>
											<td><input type="text" name="portal_name" id="portal_name"></td>
										</tr>
										<tr>
											<td>Portal URL</td>
											<td><input type="text" name="portal_url" id="portal_url"></td>
										</tr>
										<tr>
											<td>User ID</td>
											<td><input type="text" name="portal_user_id" id="portal_user_id"></td>
										</tr>
										<tr>
											<td>Password</td>
											<td><input type="password" name="portal_password" id="portal_password"></td>
										</tr>
										<tr>
											<td>Repeat Password</td>
											<td><input type="password" name="portal_repeat_password" id="portal_repeat_password"></td>
										</tr>
										<tr>
											<td>Max Attempts</td>
											<td><input type="number" name="max_attempt" id="max_attempt"></td>
										</tr>
										<tr>
											<td>Password Expires</td>
											<td><input type="date" name="password_expiry_date" id="password_expiry_date"></td>
										</tr>
                    <tr>
                      <td>Active</td>
                      <td><input type="checkbox" name="avtivename" id="activeid"></td>
                    </tr>
			       </table>
		     </form>
       </div>
    </div>


<script type="text/javascript">

jQuery.validator.addMethod('selectcheck', function (value) {
return (value != '0');
     }, "");
$(document).ready(function(){

	$.ajax({
							type: "GET",
							url: "cust_name",
							dataType: "json",
							success: function(data) {
								var results = data;
								console.log(JSON.stringify(data));
								for (var i = 0; i < results.length; i++)
								{
									 if (i == '0')
									 {
										 $('#customer_id').append("<option id =\"null\" value=\"0\">Select</option>");
									 }
								var customer_name = results[i].customer_name
								var customer_id   = results[i].customer_id
								 $('#customer_id').append("<option id ="+ customer_id +" value="+customer_id+">"+customer_name+"</option>");
								}
							},
							beforeSend: function() {
							},
							error: function() {
								alert('Data not found...!!!');
							}

						});



            $('#customer_id').on('change', function() {

                var customer_id = $(this).val();
                // alert(customer_id != 0);
                $('#plant_id').empty();

                if (customer_id != 0) {
                $("#plant_id").prop('disabled', false);
                $("#plant_id").removeClass("initial_disable_color");

                    $.ajax({
                        type: "POST",
                        url: "task_get_plant_for_customer",
                        // dataType: "json",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "customer_id": customer_id,
                        },

                        success: function(data) {

                            if (data != 0) {
                                for (var i = 0; i < data.length; i++) {
                                    if (i == '0') {
                                        $('#plant_id').append("<option id =\"null\" value=\"0\">Select Customer</option>");
                                    }
                                    var plant_name = data[i].plant_name
                                    var plant_id = data[i].plant_id
                                    $('#plant_id').append("<option id =" + plant_id + " value=" + plant_id + ">" + plant_name + "</option>");

                                }


                            } else {
                                $('#plant_id').append("<option id =\"null\" value=\"0\">No plant available</option>");
                            }
                        },
                        beforeSend: function() {

                        },
                        error: function() {

                        }
                    });

                } else {

                    $('#plant_id').html('<option value="0">Select Customer </option>');

                }

            });

            var active = 0;

					$(document).on('click', '#save_button', function() {
            if ($('#portal-table').valid()) {

      				 var customer_id = $('#customer_id').val();
    					 var plant_id = $('#plant_id').val();
    					 var portal_name = $('#portal_name').val();
    					 var portal_url = $('#portal_url').val();
    					 var portal_user_id = $('#portal_user_id').val();
    					 var portal_password = $('#portal_password').val();
    					 var portal_repeat_password = $('#portal_repeat_password').val();

               if ($('#max_attempt').val() == "")
               {
                 var max_attempt = 1;
               }
               else
               {
                 var max_attempt = $('#max_attempt').val();
               }

               var max_attempt = $('#max_attempt').val();
               var password_expiry_date = $('#password_expiry_date').val();
    					 if ($('#activeid').is(":checked"))
    					 {
    							  active = 1;
    					 }
    					 else
    					 {
    							  active = 0;
    					 }

    					 $.ajax({
    						 type: "POST",
    						 url: "portal_create_store",
    						 data: {
    						 "_token":"{{ csrf_token() }}",
    						"customer_id":customer_id,
    						 "plant_id":plant_id,
    						 "portal_name":portal_name,
    						 "portal_url":portal_url,
    						 "portal_user_id":portal_user_id,

    						 "portal_password":portal_password,
    						 "portal_repeat_password":portal_repeat_password,
    						 "max_attempt":max_attempt,
    						 "password_expiry_date":password_expiry_date,
                 "active":active,

    						 },
    						 success: function(data) {
                   $.msgBox({
                                title:"Message",
                                type:"info",
                                content:"Portal created successfully",
                           });
                $('#plant_id').html('<option value="0">Select Customer </option>');
                           reset();
    						 },
    						 beforeSend: function() {
    							 $('#msg').show();
    						 },
    						 error: function() {
                   $.msgBox({
                                title:"Error",
                                type:"error",
                                content:"Something went wrong",
                           });
    						 }
    					 });

            }
					});


          function reset()
             {
                 $('#portal-table')[0].reset();
                 $('#plant_id-error,#customer_id-error,#portal_name-error,#portal_url-error,#portal_user_id-error,#portal_password-error,#portal_repeat_password-error').remove();
                 $('#plant_id,#customer_id,#portal_name,#portal_url,#portal_user_id,#portal_password,#portal_repeat_password').removeClass("error");



             }

             $('#cancel').click(function(){
                       reset();
                   });

						$(document).on('click', '#portal_master_display', function() {
							 $("#master_data").trigger('click');
							 $("#portal_master").trigger('click');
							 $("#two").show();
						});
				});

$(window).on('load', function() {
		$("#master_data").trigger('click');
		$("#portal_master").trigger('click');
});


</script>
</div>
@stop

@section('lower_band')
<div class="col-xs-12 lower_band">
  <button class="headerbuttons" type="button" style="float:right;" id="cancel">Cancel</button>
  <button id="save_button" class="headerbuttons" style="float:right;" type="submit">Save</button>

</div>
@stop
