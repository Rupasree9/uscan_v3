@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" type="text/css" href="css/portal.css">
@stop

@section('upper_band')
<div class="col-xs-12 upper_band">
      <div class="col-xs-4">
           <p class="display-title" >Display Portal Master</p>
      </div>
      <div class="col-xs-2" style="float: right;text-align:center;line-height:30px;">
         <span  id="show_all" style="">Show All Portals</span>
      </div>
</div>
@stop
@section('content')
<div class="col-xs-12 content">
 <div id="search-para">
   <form autocomplete="off">
      <table style="width:40%;">
         <tr>
            <td class="details_color">Search Parameters</td>
         </tr>
         <tr>
            <td>Customer Name</td>
            <td><input type="text" name="search_customer_name" id="search_customer_name_id"></td>
         </tr>
         <tr>
            <td>Plant Name</td>
            <td><input type="text" name="search_plant_name" id="search_plant_name_id"></td>
         </tr>
         <tr>
            <td>Portal Name</td>
            <td><input type="text" name="search_portal_name" id="search_portal_name_id"></td>
         </tr>
         <tr>
            <td>Portal URL</td>
            <td><input type="text" name="search_portal_url_name" id="search_portal_purl"></td>
         </tr>
      </table>
   </form>
</div>

<div  id ="create_page">
     <ul class="nav nav-tabs">
           <li class="active"><a class="tab_one"  data-toggle="tab" href="#tab_one">Details</a></li>
           <li><a class="tab_two"  data-toggle="tab" href="#tab_two">Additional Details</a></li>
     </ul>
     <div class="tab-content">

        <div id="tab_one" class="tab-pane fade in active custom_tab">
          <form id="portal-table" autocomplete="off">
            <table style="width:50%;">
               <tr>
                  <td>Customer Name</td>
                  <td><input type="text" id="customer_id" value="" disabled>
                  </td>
               </tr>
               <tr>
                  <td>Plant Name</td>
                  <td><input type="text" id="plant_id" value="" disabled>
                  </td>
               </tr>
               <tr>
                  <td>Portal Name</td>
                  <td><input type="text" name="portal_name" id="portal_name" disabled></td>
               </tr>
               <tr>
                  <td>Portal URL</td>
                  <td><input type="text" name="portal_url" id="portal_url" disabled></td>
               </tr>
               <tr>
                  <td>User ID</td>
                  <td><input type="text" name="portal_user_id" id="portal_user_id" disabled></td>
              </tr>
               <tr>
                  <td>Password</td>
                  <td><input type="text" name="portal_password" id="portal_password" disabled></td>
               </tr>
               <tr>
                  <td>Repeat Password</td>
                  <td><input type="text" name="portal_repeat_password" id="portal_repeat_password" disabled></td>
               </tr>
               <tr>
                  <td>Max Attempts</td>
                  <td><input type="text" name="max_attempt" id="max_attempt" disabled></td>
               </tr>
               <tr>
                  <td>Password Expires</td>
                  <td><input type="date" name="password_expiry_date" id="password_expiry_date" disabled></td>
               </tr>
               <tr>
                 <td>Locked</td>
                 <td><input type="checkbox" name="lockname" id="lockid" disabled ></td>
               </tr>
               <tr>
                 <td>Active</td>
                 <td><input type="checkbox" name="avtivename" id="activeid" disabled></td>
               </tr>
            </table>
         </form>
      </div>

        <div id="tab_two" class="tab-pane fade custom_tab">
          <form autocomplete="off">
            <table style="width:50%;">
             <tr>
               <td>Created By</td>
               <td><input type="text" name="created_by" id="created_by" disabled></td>
             </tr>
             <tr>
               <td>Created Date</td>
               <td><input type="text" name="created_at" id="created_at" disabled></td>
             </tr>
             <tr>
               <td>Modified By</td>
               <td><input type="text" name="updated_by" id="updated_by" disabled></td>
             </tr>
             <tr>
               <td>Modified Date</td>
               <td><input type="text" name="updated_at" id="updated_at" disabled></td>
             </tr>
            </table>
           </form>
      </div>
</div>
</div>

<div id="list-of-portal"  class="div1">
   <table style="width:100%;" class="tab">
   </table>
</div>



<script type="text/javascript">
    $(document).ready(function() {

        var search_click, show_all_click, no_results;
        search_click = "0";
        show_all_click = "0";

        $.ajax({
            type: "GET",
            url: "cust_name",
            dataType: "json",
            success: function(data) {
                var results = data;
                console.log(JSON.stringify(data));
                for (var i = 0; i < results.length; i++) {
                    if (i == '0') {
                        $('#customer_id').append("<option id =\"null\" value=\"0\">Select</option>");
                    }
                    var customer_name = results[i].customer_name
                    var customer_id = results[i].customer_id
                    $('#customer_id').append("<option id =" + customer_id + " value=" + customer_id + ">" + customer_name + "</option>");
                }
            },
            beforeSend: function() {},
            error: function() {
                $.msgBox({
                    title: "Error",
                    content: "Customer data not available in database",
                    type: "error",
                   });
            }
        });

        $.ajax({
            type: "GET",
            url: "plant_name",
            dataType: "json",
            success: function(data) {
                var results = data;
                console.log(JSON.stringify(data));
                for (var i = 0; i < results.length; i++) {
                    if (i == '0') {
                        $('#plant_id').append("<option id =\"null\" value=\"0\">Select</option>");
                    }
                    var plant_name = results[i].plant_name
                    var plant_id = results[i].plant_id
                    $('#plant_id').append("<option id =" + plant_id + " value=" + plant_id + ">" + plant_name + "</option>");
                }
            },
            beforeSend: function() {},
            error: function() {
                $.msgBox({
                    title: "Error",
                    content: "Plant data not available in database",
                    type: "error",
                });
            }
        });
        // getting customer and plant ends here



        function receive() {
            var search_results, showall_results;

            $.ajax({
                type: "GET",
                url: "portal_display1",
                dataType: "json",
                success: function(data) {
                    var results = data;
                    results_showall = data;
                    if (results != 0) {
                        console.log(JSON.stringify(results));
                        $('.tab tr').remove();
                        $('.tab').append('<tr style="background-color: #D3D3D3;"><th style="display:none;">Portal id</th><th>Customer name</th><th>Plant name</th><th>Portal name</th><th>Portal url</th><th>User id</th><th>Locked</th><th>Active</td></tr>');

                        if (results.length > 12) {

                            $('#list-of-portal').addClass("scroll");

                        } else {
                            $('#list-of-portal').removeClass("scroll");
                        }

                        for (var i = 0; i < results.length; i++) {

                            if (results[i].locked == 0) {
                                locked_check = '<input type="checkbox" disabled readonly>';
                            } else {
                                locked_check = '<input type="checkbox" checked disabled readonly>';
                            }

                            if (results[i].active == 0) {
                                active_check = '<input type="checkbox" disabled readonly>';
                            } else {
                                active_check = '<input type="checkbox" checked disabled readonly>';
                            }
                            $('.tab').append('<tr data-id="' + i + '" class="showall_tr rows"><td style="display:none;">' + results[i].portal_id + '</td><td>' + results[i].customer_name + '</td><td>' + results[i].plant_name + '</td><td>' + results[i].portal_name + '</td><td>' + results[i].portal_url + '</td><td>' + results[i].portal_user_id +
                                '</td><td>' + locked_check + '</td><td>' + active_check + '</td></tr>')
                        }

                    } else {

                        $('#search-para').show();
                        $('#filter').hide();
                        $('#back2').hide();
                    }
                },
                beforeSend: function() {

                },
                error: function() {

                }
            });
        }


        function search_show() {
            var search_portal_name_id = $('#search_portal_name_id').val();
            var search_portal_purl = $('#search_portal_purl').val();
            var search_customer_name_id = $('#search_customer_name_id').val();
            var search_plant_name_id = $('#search_plant_name_id').val();

            if ((search_portal_name_id == "") && (search_portal_purl == "") && (search_customer_name_id == "")&& (search_plant_name_id == ""))
             {
                  // $('#search-para').show();
                  // $('#back2').hide();
                  // $('#filter').show();
                  $('#search-para,#filter').show();
                  $('#back2').hide();

                  $.msgBox({
                         title:"Alert",
                         content:"Search Criteria Not Available",
                         type:"alert",
                        });
              }
            else
             {

                $.ajax({
                    type: "POST",
                    url: "portal_display_show",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "search_portal_name_id": search_portal_name_id,
                        "search_portal_purl": search_portal_purl,
                        "search_customer_name_id": search_customer_name_id,
                        "search_plant_name_id": search_plant_name_id,
                    },
                    success: function(data) {
                        var results = data;
                        search_results = data;
                        console.log(JSON.stringify(results));
                        $('.tab tr').remove();
                        $('.tab').append('<tr style="background-color: #D3D3D3;"><th style="display:none;">Portal id</th><th>Customer name</th><th>Plant name</th><th>Portal name</th><th>Portal url</th><th>User id</th><th>Locked</th><th>Active</th></tr>');

                        for (var i = 0; i < results.length; i++) {
                            if (results[i].locked == 0) {
                                locked_check = '<input type="checkbox" disabled readonly>';
                            } else {
                                locked_check = '<input type="checkbox" checked disabled readonly>';
                            }

                            if (results[i].active == 0) {
                                active_check = '<input type="checkbox" disabled readonly>';
                            } else {
                                active_check = '<input type="checkbox" checked disabled readonly>';
                            }

                            $('.tab').append('<tr class = "search_tr rows" data-id="' + i + '"><td style="display:none;">' + results[i].portal_id + '</td><td>' + results[i].customer_name + '</td><td>' + results[i].plant_name + '</td><td>' + results[i].portal_name + '</td><td>' + results[i].portal_url + '</td><td>' + results[i].portal_user_id +
                                '</td><td>' + locked_check + '</td><td>' + active_check + '</td></tr>')
                        }
                        $('#search-para,#filter').hide();
                        $('#back2,.tab').show();
                        // $('#search-para').hide();
                        // $('#back2').show();
                        // $('.tab').show();
                        // $('#filter').hide();
                    },
                    beforeSend: function() {

                    },
                    error: function() {

                    }

                });
            }
        }


        $(document).on('click', '#show_all', function() {
            search_click = "0";
            show_all_click = "1";
              $('.tab tr').remove();
            receive();
            $('#search-para,#show_all,#filter').hide();
            $('#back2,.tab').show();

            //$('#search-para,#show_all').hide();
            // $('#back2').show();
            // $('.tab').show();
            // $('#filter').hide();

        });


        $(document).on('click', '#filter', function() {
          search_click = "1";
          show_all_click = "0";
            $('.tab tr').remove();
            $('#search-para,#show_all,#filter').hide();
            $('#back2,.tab').show();
            search_show();

            // $('#search-para').hide();
            // $('#back2').show();
            // $('.tab').show();
            // $('#filter').hide();


        });


        $(document).on('dblclick', '.showall_tr', function() {
            var id = $(this).data("id");
            $('.tab_one').click();
            $('#create_page,#back1').show();
            $('#back2,.tab,#show_all,#search-para').hide();

            // $('#create_page').show();
            // $('#back1').show();
            // $('#back2').hide();
            // $('.tab').hide();
            // $('#show_all').hide();
            // $('#search-para').hide();

            portal_id_ref = results_showall[id].portal_id;
            $('#customer_id').val(results_showall[id].customer_name);
            $('#plant_id').val(results_showall[id].plant_name);
            $('#portal_name').val(results_showall[id].portal_name);
            $('#portal_url').val(results_showall[id].portal_url);
            $('#portal_user_id').val(results_showall[id].portal_user_id);
            $('#portal_password').val(results_showall[id].portal_password);
            $('#portal_repeat_password').val(results_showall[id].portal_repeat_password);
            $('#max_attempt').val(results_showall[id].max_attempt);
            $('#password_expiry_date').val(results_showall[id].password_expiry_date);
            $('#created_by').val(results_showall[id].created_by);
            $('#created_at').val(results_showall[id].created_at);
            $('#updated_by').val(results_showall[id].updated_by);
            $('#updated_at').val(results_showall[id].updated_at);


            if (results_showall[id].locked == 0) {
                $('#lockid').prop('checked', false);
            } else {
                $('#lockid').prop('checked', true);
            }

            if (results_showall[id].active == 0) {
                $('#activeid').prop('checked', false);
            } else {
                $('#activeid').prop('checked', true);
            }
        });

        $(document).on('dblclick', '.search_tr', function() {
            var id = $(this).data("id");
            $('.tab_one').click();
            $('#create_page,#back1').show();
            $('#back2,.tab,#show_all,#search-para').hide();

            // $('#create_page').show();
            // $('#back1').show();
            // $('#back2').hide();
            // $('.tab').hide();
            // $('#show_all').hide();
            // $('#search-para').hide();

            portal_id_ref = search_results[id].portal_id;
            $('#customer_id').val(search_results[id].customer_name);
            $('#plant_id').val(search_results[id].plant_name);
            $('#portal_name').val(search_results[id].portal_name);
            $('#portal_url').val(search_results[id].portal_url);
            $('#portal_user_id').val(search_results[id].portal_user_id);
            $('#portal_password').val(search_results[id].portal_password);
            $('#portal_repeat_password').val(search_results[id].portal_repeat_password);
            $('#max_attempt').val(search_results[id].max_attempt);
            $('#password_expiry_date').val(search_results[id].password_expiry_date);
            $('#created_by').val(search_results[id].created_by);
            $('#created_at').val(search_results[id].created_at);
            $('#updated_by').val(search_results[id].updated_by);
            $('#updated_at').val(search_results[id].updated_at);

            if (search_results[id].locked == 0) {
                $('#lockid').prop('checked', false);
            } else {
                $('#lockid').prop('checked', true);
            }

            if (search_results[id].active == 0) {
                $('#activeid').prop('checked', false);
            } else {
                $('#activeid').prop('checked', true);
            }
        });

        $('#back1').click(function() {

            if (show_all_click == "1") {
                receive();
                $('#search-para,#create_page,#back1').hide();
                $('#show_all,.tab,#back2').show();

                // $('#search-para').hide();
                // $('#show_all').show();
                // //$('#back2').show();
                // $('#back2').hide();
                // $('.tab').show();
                // $('#create_page').hide();
                // $('#back1').hide();
            }
               if(search_click == "1")
               {
                 search_show();
                 $('#search-para,#back2,#create_page,#back1').hide();
                 //$('#search-para,#back2,#create_page,#back1').hide();
                 $('#show_all,.tab').show();

                // $('#search-para').hide();
                // $('#show_all').show();
                // $('#back2').show();
                // $('.tab').show();
                // $('#create_page').hide();
                // $('#back1').hide();
            }

        });

        $('#back2').click(function() {

            $('#search-para,#show_all,#filter').show();
            $('.tab,#back2').hide();

            // $('#search-para').show();
            // $('#show_all').show();
            // $('.tab').hide();
            // $('#filter,#show_all').show();
            // $('#back2').hide();

        });
});

        $(window).on('load', function() {
            $("#master_data").trigger('click');
            $("#portal_master").trigger('click');

            var xhr;
            $('input[name="search_portal_name"]').autoComplete({
                minChars: 1,
                source: function(term, response){
                    try { xhr.abort(); } catch(e){}
                    xhr = $.getJSON('portal_controller/autocomplete_portal_name', { search_portal_name_id: term }, function(data){ response(data); });
                }
            });

            var xhr2;
            $('input[name="search_portal_url_name"]').autoComplete({
                minChars: 1,
                source: function(term, response){
                    try { xhr2.abort(); } catch(e){}
                    xhr2 = $.getJSON('portal_controller/autocomplete_portal_url', { search_portal_url: term }, function(data){ response(data); });
                }
            });

            var xhr3;
            $('input[name="search_customer_name"]').autoComplete({
                minChars: 1,
                source: function(term, response){
                    try { xhr3.abort(); } catch(e){}
                    xhr3 = $.getJSON('portal_controller/autocomplete_customer_name', { search_customer_name_id: term }, function(data){ response(data); });
                }
            });

            var xhr4;
            $('input[name="search_plant_name"]').autoComplete({
                minChars: 1,
                source: function(term, response){
                    try { xhr4.abort(); } catch(e){}
                    xhr4 = $.getJSON('portal_controller/autocomplete_plant_name', { search_plant_name_id: term }, function(data){ response(data); });
                }
            });

    });
</script>
</div>

@stop
@section('lower_band')
<div class="col-xs-12 lower_band">
   <button class="headerbuttons" id="filter" type="button">Search</button>
   <button class="headerbuttons" id="back1" type="button">Back</button>
   <button class="headerbuttons" id="back2" type="button">Back</button>
</div>
@stop
