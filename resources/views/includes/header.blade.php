<div class="col-md-3" onclick="window.location='{{ url("/")}}'" style="cursor:pointer">
  <div id="mainheader" style="text-align:center;">
   <p style="color:#295070; background: -webkit-linear-gradient(white,#99b9c9);
  -webkit-background-clip: text;-webkit-text-fill-color: transparent; font-weight:bold;font-size:30px; margin-bottom:0px;margin-top:0px;">U.&nbspS&nbspC&nbspA&nbspN </p>
   <span style="color:#07253e;font-weight:bold;font-size:12px;"> Supply Chain Analytics </span>
 </div>
 </div>

 <div class="col-md-9" style="float:right;margin-top: 1%;padding-right: 30px;">

      <div id = "logout" onMouseOver="this.style.color='white'" onMouseOut="this.style.color='rgb(186, 208, 218)'" style="color:rgb(186, 208, 218);float:right;">
        <b> <span style="cursor:pointer"> Log out&nbsp <span> <span class="glyphicon glyphicon-log-out"> </span></b>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
      </div>

      <div style="text-align: right;float:right;padding-right: 10px;">
        <b> <span style="color:#41253e;padding-right:5px; text-align:right">
          Welcome, {{ Auth::user()->first_name }}
        </span> </b>
      </div>

 </div>

<script type="text/javascript">
          $(document).ready(function() {

            $('#logout').on('click', function() {

                 $.msgBox({
                     title: "Confirmation",
                     content: "Do you want to logout..?",
                     type: "confirm",
                     buttons: [{
                         value: "Yes"
                     }, {
                         value: "No"
                     }],
                     success: function(result) {
                         if (result == "Yes") {

                              $( "#logout-form" ).submit();

                         } else {

                         }
                     }
                 });
            });

          });
</script>
